
import 'dart:ui';

import 'package:intl/intl.dart';

class DateUtil{

  static String formatDateTime(String dateString, Locale defaultLocale){
    var dateTime = DateTime.parse(dateString);
    DateFormat dateFormat;
    if(defaultLocale.languageCode == 'en'){
       dateFormat = DateFormat("dd MMM yyyy h:mm a");
    }else{
      dateFormat = DateFormat("dd MMM yyyy HH:mm","fr");
    }

    return dateFormat.format(dateTime);
  }

  static String formatDate(String dateString, Locale defaultLocale){
    var dateTime = DateTime.parse(dateString);
    DateFormat dateFormat;
    if(defaultLocale.languageCode == 'en'){
      dateFormat = DateFormat("dd MMM yyyy");
    }else{
      dateFormat = DateFormat("dd MMM yyyy", "fr");
    }

    return dateFormat.format(dateTime);
  }

  static String formatDateLetters(String dateString, Locale defaultLocale){
    var dateTime = DateTime.parse(dateString);
    DateFormat dateFormat;
    if(defaultLocale.languageCode == 'en'){
      dateFormat = DateFormat("EEEEE dd, MMMM");
    }else{
      dateFormat = DateFormat("EEEEE dd, MMMM","fr");
    }

    return dateFormat.format(dateTime);
  }

  static String formatDay(String dateString, Locale defaultLocale){
    var dateTime = DateTime.parse(dateString);
    DateFormat dateFormat;
    if(defaultLocale.languageCode == 'en'){
      dateFormat = DateFormat("dd MMM");
    }else{
      dateFormat = DateFormat("dd MMM","fr");
    }

    return dateFormat.format(dateTime);
  }

  static String formatTime(String dateString, Locale defaultLocale){
    var dateTime = DateTime.parse(dateString);
    DateFormat dateFormat;
    if(defaultLocale.languageCode == 'en'){
      dateFormat = DateFormat("HH:mm");
    }else{
      dateFormat = DateFormat("HH:mm","fr");
    }

    return dateFormat.format(dateTime);
  }


  static String get24HourTo12HourString(DateTime time){
    if(time.hour==0){
//      String hour = time.hour.toString().length<2?"0"+time.hour.toString():time.hour.toString();
      String minute = time.minute.toString().length<2?"0"+time.minute.toString():time.minute.toString();
      return "12:$minute AM";
    }else if(time.hour<12){
      String hour = time.hour.toString().length<2?"0"+time.hour.toString():time.hour.toString();
      String minute = time.minute.toString().length<2?"0"+time.minute.toString():time.minute.toString();
      return "$hour:$minute AM";
    }else if(time.hour==12){
//      String hour = time.hour.toString().length<2?"0"+time.hour.toString():time.hour.toString();
      String minute = time.minute.toString().length<2?"0"+time.minute.toString():time.minute.toString();
      return "12:$minute PM";
    }else{
      String hour = (time.hour-12).toString().length<2?"0"+(time.hour-12).toString():(time.hour-12).toString();
      String minute = time.minute.toString().length<2?"0"+time.minute.toString():time.minute.toString();
      return "$hour:$minute PM";
    }
  }

  static String formatDayMonth(String dateString, Locale defaultLocale){
    var dateTime = DateTime.parse(dateString);
    DateFormat dateFormat;
    if(defaultLocale !=null && defaultLocale.languageCode == 'en'){
      dateFormat = DateFormat("dd MMM");
    }else{
      dateFormat = DateFormat("dd MMM");
    }

    return dateFormat.format(dateTime);
  }

}