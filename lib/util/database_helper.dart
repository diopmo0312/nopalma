import 'dart:convert';

import 'package:easy_services/models/request.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;

class DatabaseHelper{

  String registrationDataTable = "Login";
  String cartDataTable = "Cart";
  String requestDataTable = "Request";

  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    //Get the directory path for both Android and iOS to store database.
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    //Open/create the database at a given path
    String path = p.join(documentDirectory.path, "epressing_demo.db");
    var ourDb = await openDatabase(path, version:1, onCreate: _createDb);
    return ourDb;
  }

  void _createDb(Database db, int newVersion) async{
    await db.execute("CREATE TABLE Login(id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT)");
    await db.execute("CREATE TABLE Cart(id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT)");
    await db.execute("CREATE TABLE Request(id INTEGER PRIMARY KEY AUTOINCREMENT, url VARCHAR(225) NULL, method VARCHAR(225) NULL, data TEXT)");
    print("Table is created");
  }
   Future<List<Map<String, dynamic>>> getDataMapList() async{
      Database dbClient = await this.db;
      var result = dbClient.query(registrationDataTable, limit: 1);
      return result;
   }

  Future<List<Map<String, dynamic>>> getCartDataMapList() async{
    Database dbClient = await this.db;
    var result = dbClient.query(cartDataTable, limit: 1);
    return result;
  }


  Future<List<Map<String, dynamic>>> getRequestMapList() async{
    Database dbClient = await this.db;
    var result = dbClient.query(requestDataTable, limit: 1);
    return result;
  }

  Future<Request> getRequestWhere(String url, String method) async{
    Database dbClient = await this.db;
    var result = await dbClient.query(requestDataTable, limit: 1, where:"url = ? and method=?", whereArgs: [url, method] );
    return result.isEmpty?null:Request.fromJson(jsonDecode(result.first["data"]));
  }

  Future<int> insertRequest(Map<String, dynamic> profile) async {
    print(" =>>>> INSERT 1 $profile");
    Database dbClient = await this.db;
    return dbClient.insert(requestDataTable, profile);
  }

  Future<int> insertCartData(Map<String, dynamic> profile) async {
    print(" =>>>> INSERT 2 $profile");
    Database dbClient = await this.db;
    return dbClient.insert(cartDataTable, profile);
  }

   Future<int> insertRegistrationData(Map<String, dynamic> profile) async {
     print(" =>>>> INSERT 3 $profile");
     Database dbClient = await this.db;
     return dbClient.insert(registrationDataTable, profile);
   }

  Future<int> deleteCartData(int id) async{
    Database dbClient = await this.db;
    return dbClient.rawDelete("DELETE FROM $cartDataTable WHERE id = $id");
  }

  Future<int> deleteRegistrationData(int id) async{
    Database dbClient = await this.db;
    return dbClient.rawDelete("DELETE FROM $registrationDataTable WHERE id = $id");
  }

  Future<int> deleteAllRegistrationData() async{
    Database dbClient = await this.db;
    return dbClient.rawDelete("DELETE FROM $registrationDataTable");
  }
  Future<int> deleteAllCartData() async{
    Database dbClient = await this.db;
    return dbClient.rawDelete("DELETE FROM $cartDataTable");
  }

  Future<int> deleteAllRequest() async{
    Database dbClient = await this.db;
    return dbClient.rawDelete("DELETE FROM $requestDataTable");
  }



}