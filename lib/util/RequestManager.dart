import 'dart:convert';

import 'package:easy_services/models/request.dart';
import 'package:easy_services/util/database_helper.dart';


class RequestManager{

  static Future<Request> findRequest(String url, method) async {
    DatabaseHelper databaseHelper = DatabaseHelper();

    return await databaseHelper.getRequestWhere(url, method);

  }



  static void saveRequest(Request request) async {
    DatabaseHelper databaseHelper = DatabaseHelper();

    var result =  await databaseHelper.getRequestWhere(request.url, request.method);
    if(result == null){
      Map<String, dynamic> data = new Map();
      data["url"] = request.url;
      data["method"] = request.method;
      data["data"] = jsonEncode(request);

      await databaseHelper.insertRequest(data);
    }else{
      if(request.made_at.difference(result.made_at).inMinutes > 10){
        await databaseHelper.deleteAllRequest();

        Map<String, dynamic> data = new Map();
        data["url"] = request.url;
        data["method"] = request.method;
        data["data"] = jsonEncode(request);

        await databaseHelper.insertRequest(data);
      }
    }
  }
}