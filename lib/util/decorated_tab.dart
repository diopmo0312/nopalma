import 'package:flutter/material.dart';

class DecoratedTab extends StatelessWidget{

  DecoratedTab({@required this.tab, @required this.decoration});

  final Tab tab;
  final BoxDecoration decoration;


  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(child: Container(decoration: decoration,padding: EdgeInsets.all(15.0),)),
        tab,
      ],
    );
  }
}