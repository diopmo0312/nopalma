import 'dart:convert';

import 'package:easy_services/models/cart.dart';
import 'package:easy_services/models/cart_item.dart';
import 'package:easy_services/util/database_helper.dart';

class CartManager{

  static void obtainCart(Function onSuccess) {
    DatabaseHelper databaseHelper = DatabaseHelper();
    databaseHelper.getCartDataMapList()
    .then((result){
        Cart cart;
        if(result != null && result.isNotEmpty){
          var resultData = result.first;
          String cartJson=resultData["data"];
          if(cartJson != null){
            Map<String, dynamic> cartMap = jsonDecode(cartJson);
              cart = Cart.fromJson(cartMap);
               onSuccess(cart);
          }
        
          
        }else{
          cart = new Cart([]);
          databaseHelper.insertCartData({"data":cart.toJsonString()})
          .then((resQuery){
               onSuccess(cart);
          }).catchError((error){
            print("Error found $error");
          
          });
        }

       

    }).catchError((error){
      print("Error found $error");
    
    });

  }

  static Future<Cart> addToCart(CartItem cartItem) async {
    DatabaseHelper databaseHelper = DatabaseHelper();
    var result = await databaseHelper.getCartDataMapList();
    Cart cart;
    if(result != null && result.isNotEmpty){
      var resultData = result.first;
          String cartJson=resultData["data"];
          if(cartJson != null){
            Map<String, dynamic> cartMap = jsonDecode(cartJson);
              cart = Cart.fromJson(cartMap);
            
          }
    }else{
      cart = new Cart([]);

    }

    List<CartItem> cartItems = cart.cartItems;
    if(cartItems == null){
      cartItems = [];
    }

    if(cartItems != null){
      if(cartItems.isNotEmpty && cartItems.contains(cartItem)){
        cartItems.remove(cartItem);
      }

      cartItems.add(cartItem);
      cart = new Cart(cartItems);

    }else{
       cart = new Cart([cartItem]);

    }

    var resQueryDelete = await databaseHelper.deleteAllCartData();
    var resQuery = await databaseHelper.insertCartData({"data":cart.toJsonString()});

      return cart;

  }

  static Future<Cart> removeToCart(CartItem cartItem) async {
    DatabaseHelper databaseHelper = DatabaseHelper();
    var result = await databaseHelper.getCartDataMapList();
    Cart cart;
    if(result != null && result.isNotEmpty){
      var resultData = result.first;
          String cartJson=resultData["data"];
          if(cartJson != null){
            Map<String, dynamic> cartMap = jsonDecode(cartJson);
              cart = Cart.fromJson(cartMap);
          }
    }else{
      cart = new Cart([]);
      var resQuery = await databaseHelper.insertCartData({"data":cart.toJsonString()});
    }

    List<CartItem> cartItems = cart.cartItems;
    if(cartItems == null){
      cartItems = [];
    }

    if(cartItems != null){
      if(cartItems.isNotEmpty && cartItems.contains(cartItem)){
        cartItems.remove(cartItem);
      }
      cart = new Cart(cartItems);
      var resQueryDelete = await databaseHelper.deleteAllCartData();
      var resQuery = await databaseHelper.insertCartData({"data":cart.toJsonString()});

    }

    result = await databaseHelper.getCartDataMapList();
    if(result != null && result.isNotEmpty){
      var resultData = result.first;
          String cartJson=resultData["data"];
          if(cartJson != null){
            Map<String, dynamic> cartMap = jsonDecode(cartJson);
              cart = Cart.fromJson(cartMap);
          }

      return  cart;
    }else{
      return new Cart([]);
    }


  }

  static Future<Cart> emptyCart() async {
    DatabaseHelper databaseHelper = DatabaseHelper();
    var result = await databaseHelper.getCartDataMapList();
    Cart cart = new Cart([]);
    var resQueryDelete = await databaseHelper.deleteAllCartData();
    var resQuery = await databaseHelper.insertCartData({"data":cart.toJsonString()});
    return cart;

  }
}