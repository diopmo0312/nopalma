import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/util/CartManager.dart';
import 'package:easy_services/util/database_helper.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginManager{

  static void signIn(Registrationdata profile, Function onSuccess, Function onError){

    DatabaseHelper databaseHelper = DatabaseHelper();
    databaseHelper.deleteAllRegistrationData().then((int res){
      print("All Login deleted");
      databaseHelper.insertRegistrationData({
        "data":profile.toJsonString()
      })
          .then((int result){
        print("Login saved");
        onSuccess(profile);
      }).catchError((error){
        print("Error found $error");
        onError(error);
      });

    }).catchError((e){

      print("Deletion Error found $e");
      onError(e);

    });

  }

  static void signOut(Function onSuccess, Function onError){
    DatabaseHelper databaseHelper = DatabaseHelper();
    databaseHelper.deleteAllRegistrationData().then((int res){
      print("All Login deleted");
      //FirebaseAuth.instance.signOut();
      CartManager.emptyCart();
      LangStrings.setLang('');
       onSuccess();
    }).catchError((e){

      print("Deletion Error found $e");
      onError(e);

    });
  }

  static void connectedUser(Function onSuccess, Function onError){
    DatabaseHelper databaseHelper = DatabaseHelper();
    databaseHelper.getDataMapList()
        .then((List<Map<String, dynamic>> result){

            if(result != null && result.isNotEmpty){
              
              var loginData = result.first;
              if(loginData != null){

                String profileJson= loginData["data"];
                //print("List Result $profileJson");
                if(profileJson !=null && profileJson.isNotEmpty){
                    Map<String, dynamic> profileMap = jsonDecode(profileJson);

                    var profile = Registrationdata.fromJson(profileMap);
                    print("Profile found $profile");
                    if(profile != null){
                      onSuccess(profile);
                    }else{
                      onError(Exception("profile not found"));
                    }
                }else{
                  onError(Exception("profile not found"));
                }

              }
            }
    }).catchError((error){
      print("Error found $error");
      onError(error);
    });
  }
}