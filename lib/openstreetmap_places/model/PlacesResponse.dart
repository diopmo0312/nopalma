import 'dart:convert';

import 'package:easy_services/openstreetmap_places/model/Place.dart';

class PlacesResponse{

  List<Place> data;

  PlacesResponse(this.data);

  static PlacesResponse fromJson(String json){

    List list =  jsonDecode(json);

    if(list == null){

     list = [];

    }

    return new PlacesResponse(list.map((e) => Place.fromJson(e as Map)).toList());

  }


}