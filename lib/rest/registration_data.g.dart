// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'registration_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Registrationdata _$RegistrationdataFromJson(Map<String, dynamic> json) {
  return Registrationdata(
    json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
    (json['notifications'] as List)
        ?.map((e) => e == null
            ? null
            : CustomerNotification.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['orders'] as List)
        ?.map(
            (e) => e == null ? null : Order.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['payment_methods'] as List)
        ?.map((e) => e == null
            ? null
            : PaymentMethod.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['pressing_clothes'] as List)
        ?.map((e) => e == null
            ? null
            : PressingClothes.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['shoe_repairings'] as List)
        ?.map((e) => e == null
            ? null
            : ShoeRepairing.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['retouchings'] as List)
        ?.map((e) =>
            e == null ? null : Retouching.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['artisan_categories'] as List)
        ?.map((e) => e == null
            ? null
            : ArtisanCategory.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['linge_kilo_options'] as List)
        ?.map((e) => e == null
            ? null
            : LingeKiloOption.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['subscriptions'] as List)
        ?.map((e) =>
            e == null ? null : Subscription.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['customer_subscriptions'] as List)
        ?.map((e) => e == null
            ? null
            : CustomerSubscription.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['pressing_categories'] as List)
        ?.map((e) => e == null
            ? null
            : PressingCategory.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['services'] as List)
        ?.map((e) =>
            e == null ? null : Service.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['session'] == null
        ? null
        : Session.fromJson(json['session'] as Map<String, dynamic>),
    (json['delivery_addresses'] as List)
        ?.map((e) => e == null
            ? null
            : DeliveryAddress.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['devices'] as List)
        ?.map((e) => e == null
            ? null
            : CustomerDevice.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['formulas'] as List)
        ?.map((e) =>
            e == null ? null : Formula.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$RegistrationdataToJson(Registrationdata instance) =>
    <String, dynamic>{
      'user': instance.user,
      'notifications': instance.notifications,
      'orders': instance.orders,
      'payment_methods': instance.payment_methods,
      'pressing_clothes': instance.pressing_clothes,
      'shoe_repairings': instance.shoe_repairings,
      'retouchings': instance.retouchings,
      'artisan_categories': instance.artisan_categories,
      'linge_kilo_options': instance.linge_kilo_options,
      'subscriptions': instance.subscriptions,
      'customer_subscriptions': instance.customer_subscriptions,
      'pressing_categories': instance.pressing_categories,
      'services': instance.services,
      'session': instance.session,
      'delivery_addresses': instance.delivery_addresses,
      'devices': instance.devices,
      'formulas': instance.formulas,
    };
