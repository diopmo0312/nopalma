// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'artisan_category_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ArtisanCategoryResult _$ArtisanCategoryResultFromJson(
    Map<String, dynamic> json) {
  return ArtisanCategoryResult(
    json['success'] as bool,
    json['message'] as String,
    (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : ArtisanCategory.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ArtisanCategoryResultToJson(
        ArtisanCategoryResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
