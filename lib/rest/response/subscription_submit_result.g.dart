// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'subscription_submit_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubscriptionSubmitResult _$SubscriptionSubmitResultFromJson(
    Map<String, dynamic> json) {
  return SubscriptionSubmitResult(
    (json['subscription_payments'] as List)
        ?.map((e) => e == null
            ? null
            : SubscriptionPayment.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['customer_subscriptions'] as List)
        ?.map((e) => e == null
            ? null
            : CustomerSubscription.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['subscription'] == null
        ? null
        : Subscription.fromJson(json['subscription'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$SubscriptionSubmitResultToJson(
        SubscriptionSubmitResult instance) =>
    <String, dynamic>{
      'subscription_payments': instance.subscription_payments,
      'customer_subscriptions': instance.customer_subscriptions,
      'subscription': instance.subscription,
    };
