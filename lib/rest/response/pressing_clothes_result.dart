import 'package:easy_services/models/pressing_clothes.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'pressing_clothes_result.g.dart';

@JsonSerializable()
class PressingClothesResult{

  bool success;
  String message;
  @JsonKey(name: 'data')
  List<PressingClothes> data;


  PressingClothesResult(this.success, this.message, this.data);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory PressingClothesResult.fromJson(Map<String, dynamic> json) => _$PressingClothesResultFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$PressingClothesResultToJson(this);

}