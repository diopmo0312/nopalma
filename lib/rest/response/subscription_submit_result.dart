import 'package:easy_services/models/customer_subscription.dart';
import 'package:easy_services/models/subscription.dart';
import 'package:easy_services/models/subscription_payment.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `SubscriptionSubmitResult` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'subscription_submit_result.g.dart';

@JsonSerializable()
class SubscriptionSubmitResult{

  List<SubscriptionPayment> subscription_payments; 
  List<CustomerSubscription> customer_subscriptions;
  Subscription subscription;

  SubscriptionSubmitResult(this.subscription_payments, this.customer_subscriptions, this.subscription);

      /// A necessary factory constructor for creating a new SubscriptionSubmitResult instance
  /// from a map. Pass the map to the generated `_$SubscriptionSubmitResultFromJson()` constructor.
  /// The constructor is named after the source class, in this case SubscriptionSubmitResult.
  factory SubscriptionSubmitResult.fromJson(Map<String, dynamic> json) => _$SubscriptionSubmitResultFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$SubscriptionSubmitResultToJson`.
  Map<String, dynamic> toJson() => _$SubscriptionSubmitResultToJson(this);


}