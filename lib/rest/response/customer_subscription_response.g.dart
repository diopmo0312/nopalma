// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_subscription_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerSubscriptionResponse _$CustomerSubscriptionResponseFromJson(
    Map<String, dynamic> json) {
  return CustomerSubscriptionResponse(
    json['success'] as bool,
    json['message'] as String,
    (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : CustomerSubscription.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CustomerSubscriptionResponseToJson(
        CustomerSubscriptionResponse instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
