// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_history_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderHistoryResult _$OrderHistoryResultFromJson(Map<String, dynamic> json) {
  return OrderHistoryResult(
    json['success'] as bool,
    json['message'] as String,
    (json['data'] as List)
        ?.map((e) =>
            e == null ? null : OrderHistory.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$OrderHistoryResultToJson(OrderHistoryResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
