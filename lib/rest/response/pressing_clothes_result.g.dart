// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pressing_clothes_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PressingClothesResult _$PressingClothesResultFromJson(
    Map<String, dynamic> json) {
  return PressingClothesResult(
    json['success'] as bool,
    json['message'] as String,
    (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : PressingClothes.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$PressingClothesResultToJson(
        PressingClothesResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
