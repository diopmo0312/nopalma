import 'package:easy_services/models/order_history.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'order_history_result.g.dart';

@JsonSerializable()
class OrderHistoryResult{

  bool success;
  String message;
  @JsonKey(name: 'data')
  List<OrderHistory> data;


  OrderHistoryResult(this.success, this.message, this.data);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory OrderHistoryResult.fromJson(Map<String, dynamic> json) => _$OrderHistoryResultFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$OrderHistoryResultToJson(this);

}