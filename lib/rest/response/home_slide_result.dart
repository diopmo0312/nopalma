import 'package:easy_services/models/home_slide.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `RetouchingResult` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'home_slide_result.g.dart';

@JsonSerializable()
class HomeSlideResult{
  bool success;
  String message;
  @JsonKey(name: 'data')
  List<HomeSlide> data;

  HomeSlideResult(this.success, this.message, this.data);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory HomeSlideResult.fromJson(Map<String, dynamic> json) => _$HomeSlideResultFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$HomeSlideResultToJson(this);
}