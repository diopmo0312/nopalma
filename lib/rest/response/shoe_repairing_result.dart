import 'package:easy_services/models/shoe_repairing.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `RetouchingResult` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'shoe_repairing_result.g.dart';

@JsonSerializable()
class ShoeRepairingResult {

  bool success;
  String message;
  @JsonKey(name: 'data')
  List<ShoeRepairing> data;  

  ShoeRepairingResult(this.success, this.message, this.data);

      /// A necessary factory constructor for creating a new ShoeRepairingResult instance
  /// from a map. Pass the map to the generated `_$ShoeRepairingResultFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory ShoeRepairingResult.fromJson(Map<String, dynamic> json) => _$ShoeRepairingResultFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$ShoeRepairingResultToJson`.
  Map<String, dynamic> toJson() => _$ShoeRepairingResultToJson(this);

  
}