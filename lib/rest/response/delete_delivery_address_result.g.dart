// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'delete_delivery_address_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeleteDeliveryAddressResult _$DeleteDeliveryAddressResultFromJson(
    Map<String, dynamic> json) {
  return DeleteDeliveryAddressResult(
    json['success'] as bool,
    json['message'] as String,
  );
}

Map<String, dynamic> _$DeleteDeliveryAddressResultToJson(
        DeleteDeliveryAddressResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
    };
