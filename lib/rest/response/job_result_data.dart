import 'package:easy_services/models/job.dart';
import 'package:json_annotation/json_annotation.dart';

part 'job_result_data.g.dart';

@JsonSerializable()
class JobResultData{

  Job pickup;
  Job delivery;

  JobResultData(this.pickup, this.delivery);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory JobResultData.fromJson(Map<String, dynamic> json) => _$JobResultDataFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$JobResultDataToJson(this);



}