// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'registration_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegistrationResult _$RegistrationResultFromJson(Map<String, dynamic> json) {
  return RegistrationResult(
    json['success'] as bool,
    json['message'] as String,
    json['data'] == null
        ? null
        : Registrationdata.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$RegistrationResultToJson(RegistrationResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
