// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_slide_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeSlideResult _$HomeSlideResultFromJson(Map<String, dynamic> json) {
  return HomeSlideResult(
    json['success'] as bool,
    json['message'] as String,
    (json['data'] as List)
        ?.map((e) =>
            e == null ? null : HomeSlide.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$HomeSlideResultToJson(HomeSlideResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
