import 'package:easy_services/models/linge_kilo_option.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `LingeKiloOptionResult` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'linge_kilo_option_result.g.dart';

@JsonSerializable()
class LingeKiloOptionResult{

  bool success;
  String message;
  @JsonKey(name: 'data')
  List<LingeKiloOption> data;

  LingeKiloOptionResult(this.success, this.message, this.data);

    /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory LingeKiloOptionResult.fromJson(Map<String, dynamic> json) => _$LingeKiloOptionResultFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$LingeKiloOptionResultToJson(this);

  
}