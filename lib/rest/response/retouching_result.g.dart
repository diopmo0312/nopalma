// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'retouching_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RetouchingResult _$RetouchingResultFromJson(Map<String, dynamic> json) {
  return RetouchingResult(
    json['success'] as bool,
    json['message'] as String,
    (json['data'] as List)
        ?.map((e) =>
            e == null ? null : Retouching.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$RetouchingResultToJson(RetouchingResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
