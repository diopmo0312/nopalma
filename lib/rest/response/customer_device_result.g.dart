// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_device_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerDeviceResult _$CustomerDeviceResultFromJson(Map<String, dynamic> json) {
  return CustomerDeviceResult(
    json['success'] as bool,
    json['message'] as String,
    (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : CustomerDevice.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CustomerDeviceResultToJson(
        CustomerDeviceResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
