// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_delivery_address_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerDeliveryAddressResult _$CustomerDeliveryAddressResultFromJson(
    Map<String, dynamic> json) {
  return CustomerDeliveryAddressResult(
    json['success'] as bool,
    json['message'] as String,
    (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : DeliveryAddress.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CustomerDeliveryAddressResultToJson(
        CustomerDeliveryAddressResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
