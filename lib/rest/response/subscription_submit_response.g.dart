// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'subscription_submit_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubscriptionSubmitResponse _$SubscriptionSubmitResponseFromJson(
    Map<String, dynamic> json) {
  return SubscriptionSubmitResponse(
    json['success'] as bool,
    json['message'] as String,
    json['data'] == null
        ? null
        : SubscriptionSubmitResult.fromJson(
            json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$SubscriptionSubmitResponseToJson(
        SubscriptionSubmitResponse instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
