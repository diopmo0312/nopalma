// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'OrderData.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderData _$OrderDataFromJson(Map<String, dynamic> json) {
  return OrderData(
    json['order'] == null
        ? null
        : Order.fromJson(json['order'] as Map<String, dynamic>),
    json['transaction'] == null
        ? null
        : InvoicePayment.fromJson(json['transaction'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$OrderDataToJson(OrderData instance) => <String, dynamic>{
      'order': instance.order,
      'transaction': instance.transaction,
    };
