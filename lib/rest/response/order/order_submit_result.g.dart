// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_submit_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderSubmitResult _$OrderSubmitResultFromJson(Map<String, dynamic> json) {
  return OrderSubmitResult(
    json['success'] as bool,
    json['message'] as String,
    json['data'] == null
        ? null
        : OrderData.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$OrderSubmitResultToJson(OrderSubmitResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
