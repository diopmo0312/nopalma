import 'package:easy_services/models/invoice_payment.dart';
import 'package:easy_services/models/order.dart';

import 'package:json_annotation/json_annotation.dart';

part 'OrderData.g.dart';

@JsonSerializable()
class OrderData{

  Order order;

  InvoicePayment transaction;

  OrderData(this.order, this.transaction);


  factory OrderData.fromJson(Map<String, dynamic> json) => _$OrderDataFromJson(json);

  Map<String, dynamic> toJson() => _$OrderDataToJson(this);


}