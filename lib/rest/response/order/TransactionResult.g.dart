// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TransactionResult.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionResult _$TransactionResultFromJson(Map<String, dynamic> json) {
  return TransactionResult(
    json['success'] as bool,
    json['message'] as String,
    json['data'] == null
        ? null
        : InvoicePayment.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$TransactionResultToJson(TransactionResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
