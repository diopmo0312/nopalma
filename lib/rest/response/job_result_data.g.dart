// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'job_result_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JobResultData _$JobResultDataFromJson(Map<String, dynamic> json) {
  return JobResultData(
    json['pickup'] == null
        ? null
        : Job.fromJson(json['pickup'] as Map<String, dynamic>),
    json['delivery'] == null
        ? null
        : Job.fromJson(json['delivery'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JobResultDataToJson(JobResultData instance) =>
    <String, dynamic>{
      'pickup': instance.pickup,
      'delivery': instance.delivery,
    };
