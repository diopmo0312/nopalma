import 'package:easy_services/rest/registration_data.dart';

import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'registration_result.g.dart';

@JsonSerializable()

class RegistrationResult{


  RegistrationResult(this.success, this.message, this.data);

  bool success;
  String message;
  @JsonKey(name: 'data')
  Registrationdata data;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory RegistrationResult.fromJson(Map<String, dynamic> json) => _$RegistrationResultFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$RegistrationResultToJson(this);






}