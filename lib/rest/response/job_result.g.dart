// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'job_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JobResult _$JobResultFromJson(Map<String, dynamic> json) {
  return JobResult(
    json['success'] as bool,
    json['message'] as String,
    json['data'] == null
        ? null
        : JobResultData.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JobResultToJson(JobResult instance) => <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
