// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shoe_repairing_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShoeRepairingResult _$ShoeRepairingResultFromJson(Map<String, dynamic> json) {
  return ShoeRepairingResult(
    json['success'] as bool,
    json['message'] as String,
    (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : ShoeRepairing.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ShoeRepairingResultToJson(
        ShoeRepairingResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
