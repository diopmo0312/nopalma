// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_notification_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerNotificationResult _$CustomerNotificationResultFromJson(
    Map<String, dynamic> json) {
  return CustomerNotificationResult(
    json['success'] as bool,
    json['message'] as String,
    (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : CustomerNotification.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CustomerNotificationResultToJson(
        CustomerNotificationResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
