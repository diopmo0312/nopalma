import 'package:easy_services/rest/response/subscription_submit_result.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `SubscriptionSubmitResponse` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'subscription_submit_response.g.dart';

@JsonSerializable()
class SubscriptionSubmitResponse{

  bool success;
  String message;
  @JsonKey(name: 'data')
  SubscriptionSubmitResult data;

  SubscriptionSubmitResponse(this.success, this.message, this.data);


        /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$SubscriptionSubmitResponseFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory SubscriptionSubmitResponse.fromJson(Map<String, dynamic> json) => _$SubscriptionSubmitResponseFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$SubscriptionSubmitResponseToJson`.
  Map<String, dynamic> toJson() => _$SubscriptionSubmitResponseToJson(this);



}