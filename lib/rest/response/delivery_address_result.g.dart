// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'delivery_address_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeliveryAddressResult _$DeliveryAddressResultFromJson(
    Map<String, dynamic> json) {
  return DeliveryAddressResult(
    json['success'] as bool,
    json['message'] as String,
    json['data'] == null
        ? null
        : DeliveryAddress.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DeliveryAddressResultToJson(
        DeliveryAddressResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
