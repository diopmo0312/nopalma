// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'linge_kilo_option_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LingeKiloOptionResult _$LingeKiloOptionResultFromJson(
    Map<String, dynamic> json) {
  return LingeKiloOptionResult(
    json['success'] as bool,
    json['message'] as String,
    (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : LingeKiloOption.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$LingeKiloOptionResultToJson(
        LingeKiloOptionResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
