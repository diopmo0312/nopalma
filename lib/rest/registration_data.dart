import 'package:easy_services/models/artisan_category.dart';
import 'package:easy_services/models/customer_device.dart';
import 'package:easy_services/models/customer_subscription.dart';
import 'package:easy_services/models/delivery_address.dart';
import 'package:easy_services/models/customer_notification.dart';
import 'package:easy_services/models/formula.dart';
import 'package:easy_services/models/order.dart';
import 'package:easy_services/models/payment_method.dart';
import 'package:easy_services/models/pressing_category.dart';
import 'package:easy_services/models/pressing_clothes.dart';
import 'package:easy_services/models/retouching.dart';
import 'package:easy_services/models/linge_kilo_option.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/models/session.dart';
import 'package:easy_services/models/shoe_repairing.dart';
import 'package:easy_services/models/subscription.dart';
import 'package:easy_services/models/user.dart';

import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'registration_data.g.dart';

@JsonSerializable()

class Registrationdata{

  @JsonKey(name: 'user')
  User user;

  @JsonKey(name: 'notifications')
  List<CustomerNotification> notifications;

  @JsonKey(name: 'orders')
  List<Order> orders;

  @JsonKey(name: 'payment_methods')
  List<PaymentMethod> payment_methods;

  @JsonKey(name: 'pressing_clothes')
  List<PressingClothes> pressing_clothes;

  @JsonKey(name: 'shoe_repairings')
  List<ShoeRepairing> shoe_repairings;

  @JsonKey(name: 'retouchings')
  List<Retouching> retouchings;
  
  @JsonKey(name: 'artisan_categories')
  List<ArtisanCategory> artisan_categories;
  
  @JsonKey(name: 'linge_kilo_options')
  List<LingeKiloOption> linge_kilo_options;

  @JsonKey(name: 'subscriptions')
  List<Subscription> subscriptions;

  @JsonKey(name: 'customer_subscriptions')
  List<CustomerSubscription> customer_subscriptions;

  @JsonKey(name: 'pressing_categories')
  List<PressingCategory> pressing_categories;

  @JsonKey(name: 'services')
  List<Service> services;

  @JsonKey(name: 'session')
  Session session;

  @JsonKey(name: 'delivery_addresses')
  List<DeliveryAddress> delivery_addresses;

  @JsonKey(name: 'devices')
  List<CustomerDevice> devices;

  @JsonKey(name: 'formulas')
  List<Formula> formulas;



  Registrationdata(this.user, this.notifications, this.orders,
      this.payment_methods,
      this.pressing_clothes,
      this.shoe_repairings,
      this.retouchings,
      this.artisan_categories,
      this.linge_kilo_options,
      this.subscriptions,
      this.customer_subscriptions,
      this.pressing_categories,
      this.services, this.session,
      this.delivery_addresses,
      this.devices,
      this.formulas
      );

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory Registrationdata.fromJson(Map<String, dynamic> json) => _$RegistrationdataFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$RegistrationdataToJson(this);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  String toJsonString(){
    return jsonEncode(toJson());/*{
      'user': this.user.toJson().toString(),
      'business': this.business.toJson().toString(),
      'agent': this.agent.toJson().toString(),
    };*/
  }



}