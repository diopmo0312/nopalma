import 'dart:async';
import 'dart:convert';
import 'package:easy_services/rest/response/customer_delivery_address_result.dart';
import 'package:easy_services/rest/response/delete_delivery_address_result.dart';
import 'package:easy_services/rest/response/delivery_address_result.dart';
import 'package:easy_services/util/constants.dart';
import 'package:http/http.dart' as http;

class DeliveryAddressRepository{

  static Future<DeliveryAddressResult> store (
      String address_name,
      double latitude,
      double longitude,
      String place,
      String town,
      String  address_typed,
      String address_type,
      int customer_id,
      Function showProgress,
      Function dismissProgress
      ) async {

    showProgress();

    var url = Constants.API_URL+"deliveryAddresses";

    var response = await http.post(
        url,
        body: {
          "address_name":address_name,
          "latitude":"$latitude",
          "longitude":"$longitude",
          "place":place,
          "town":town,
          "address_typed":address_typed,
          "address_type":address_type,
          "customer_id":"$customer_id"
        },
        headers: {
          "Accept":"application/json"
        }
    );

    String jsonString  = response.body;
    Map resultMap = jsonDecode(jsonString);
    DeliveryAddressResult result = DeliveryAddressResult.fromJson(resultMap);

    dismissProgress();

    return result;

  }

  static Future<DeleteDeliveryAddressResult> delete (
      int id,
      Function showProgress,
      Function dismissProgress) async{

    showProgress();

    var url = Constants.API_URL+"deliveryAddresses/$id";

    var response = await http.delete(url);

    String jsonString  = response.body;
    Map resultMap = jsonDecode(jsonString);
    DeleteDeliveryAddressResult result = DeleteDeliveryAddressResult.fromJson(resultMap);

    dismissProgress();

    return result;
  }

  static Future<CustomerDeliveryAddressResult> find (
      int customer_id,
      Function showProgress,
      Function dismissProgress,
      [String query="", String orderQuery = "&orderBy=created_at&sortedBy=desc"]
      ) async{
    showProgress();

    var url = Constants.API_URL+"deliveryAddresses?customer_id=$customer_id";

    var response = await http.get(
        url,
        headers: {
          "Accept":"application/json"
        }
    );

    print("CustomerDeliveryAddressResult URL ${response.request}");

    String jsonString  = response.body;
    Map registrationResultMap = jsonDecode(jsonString);
    CustomerDeliveryAddressResult result = CustomerDeliveryAddressResult.fromJson(registrationResultMap);

    dismissProgress();

    return result;

  }

}