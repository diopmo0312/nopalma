import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/response/pressing_clothes_result.dart';
import 'package:easy_services/util/LoginManager.dart';
import 'package:easy_services/util/constants.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class PressingClothesRepository{


  static Future<PressingClothesResult> _find () async{

    var url = Constants.API_URL+"pressingClothers";

    var response = await http.get(
        url,
        headers: {
          "Accept":"application/json"
        }
    );

    print("PressingClothesResult URL ${response.request}");

    String jsonString  = response.body;
    Map registrationResultMap = jsonDecode(jsonString);
    PressingClothesResult result = PressingClothesResult.fromJson(registrationResultMap);

    return result;

  }

  static findAndStore( Function showProgress,
      Function dismissProgress, Function onSuccess, Function onError){
      showProgress();
    _find()
        .then((value){

        if(value.success && value.data != null && value.data.isNotEmpty){

          LoginManager.connectedUser((Registrationdata profile){

            Registrationdata newProfile = profile;

              newProfile.pressing_clothes = value.data;
              dismissProgress();

              LoginManager.signIn(newProfile, onSuccess, onError);


          }, (errorException){
            dismissProgress();
            onError(errorException);

          });

        }else{

         dismissProgress();
         onError(Exception(value.message));

        }


    })
        .catchError((error){
      dismissProgress();
      onError(error);
    });
  }

}