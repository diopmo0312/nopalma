import 'package:easy_services/rest/response/customer_subscription_response.dart';
import 'package:easy_services/rest/response/subscription_submit_response.dart';
import 'package:easy_services/util/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class SubscriptionRepository{

  static Future<CustomerSubscriptionResponse> find(int customerId,
      Function showProgress,
      Function dismissProgress,
      [String query="", String orderQuery = "&orderBy=created_at&sortedBy=desc"]
      )async{

         showProgress();

    if(query.isEmpty){
      query = "customer_id=$customerId";
    }
    var url = Constants.API_URL+"customerSubscriptions?$query;";

    var response = await http.get(
        url,
        headers: {
          "Accept":"application/json"
        }
    );

    print("Orders URL ${response.request}");

    String jsonString  = response.body;
    Map registrationResultMap = jsonDecode(jsonString);
    CustomerSubscriptionResponse result = CustomerSubscriptionResponse.fromJson(registrationResultMap);

    dismissProgress();

    return result;

  }

  static Future<SubscriptionSubmitResponse> create (
      String jsonBody,
      Function showProgress,
      Function dismissProgress
      ) async{
    showProgress();

    var url = Constants.API_URL+"subscriptionPayments";

    var response = await http.post(
        url,
        body: jsonBody,
        headers: {
          "Accept":"application/json",
          "Content-Type": "application/json"
        }
    );

    print("SubscriptionSubmitResult URL ${response.request}");

    String jsonString  = response.body;
    Map registrationResultMap = jsonDecode(jsonString);
    SubscriptionSubmitResponse result = SubscriptionSubmitResponse.fromJson(registrationResultMap);
    
    dismissProgress();

    return result;

  }

}