import 'dart:async';
import 'dart:convert';
import 'package:easy_services/models/customer_device.dart';
import 'package:easy_services/models/request.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/response/customer_device_result.dart';
import 'package:easy_services/rest/response/customer_notification_result.dart';
import 'package:easy_services/rest/response/home_slide_result.dart';
import 'package:easy_services/rest/response/login/login_result.dart';
import 'package:easy_services/rest/response/registration_result.dart';
import 'package:easy_services/util/RequestManager.dart';
import 'package:easy_services/util/constants.dart';
import 'package:http/http.dart' as http;

class UserRepository {
  static Future<RegistrationResult> submitRegistration(
      Map<String, String> jsonBody,
      Function showProgress,
      Function dismissProgress) async {
    showProgress();

    var url = Constants.API_URL + "customer_sign_up";

    var response = await http
        .post(url, body: jsonBody, headers: {"Accept": "application/json"});

    String jsonString = response.body;
    print("Body $jsonString");

    Map registrationResultMap = jsonDecode(jsonString);
    RegistrationResult result =
        RegistrationResult.fromJson(registrationResultMap);

    dismissProgress();

    return result;
  }

  static Future<RegistrationResult> submitEdition(
      int id,
      Map<String, String> jsonBody,
      Function showProgress,
      Function dismissProgress) async {
    showProgress();

    var url = Constants.API_URL + "customers/$id";

    var response = await http
        .put(url, body: jsonBody, headers: {"Accept": "application/json"});

    String jsonString = response.body;
    print("Body $jsonString");

    Map registrationResultMap = jsonDecode(jsonString);
    RegistrationResult result =
        RegistrationResult.fromJson(registrationResultMap);

    dismissProgress();

    return result;
  }

  static Future<RegistrationResult> edit(int id, Map<String, String> jsonBody,
      Function showProgress, Function dismissProgress) async {
    showProgress();

    var url = Constants.API_URL + "customers/$id";

    var response = await http
        .put(url, body: jsonBody, headers: {"Accept": "application/json"});

    String jsonString = response.body;
    Map registrationResultMap = jsonDecode(jsonString);
    RegistrationResult result =
        RegistrationResult.fromJson(registrationResultMap);

    dismissProgress();

    return result;
  }

  static Future<LoginResult> login(String dialCode, String phone,
      Function showProgress, Function dismissProgress) async {
    showProgress();

    var url = Constants.API_URL + "customer_login";
    print('Url de login ===> $url');
    print('Dial code ===> $dialCode');
    print('Phone number ===> $phone');

    var response = await http.post(url, body: {
      "dial_code": dialCode,
      "phone_number": phone,
    }, headers: {
      "Accept": "application/json",
    });
    dismissProgress();

    print('Response ===> ${response.body}');

    if (response.statusCode == 200) {
      String jsonString = response.body;
      print("JSON BODY >>> $jsonString");
      Map registrationResultMap = jsonDecode(jsonString);
      print("MAP >>>> $registrationResultMap");
      LoginResult result = LoginResult.fromJson(registrationResultMap);
      print("Login Result >>>> $result");

      return result;
    } else {
      throw response;
    }
  }

  static Future<LoginResult> refreshData(String dialCode, String phone) async {
    // showProgress();

    var url = Constants.API_URL + "customer_login";
    print('Url de login ===> $url');
    print('Dial code ===> $dialCode');
    print('Phone number ===> $phone');

    var response = await http.post(url, body: {
      "dial_code": dialCode,
      "phone_number": phone,
    }, headers: {
      "Accept": "application/json",
    });
    // dismissProgress();

    print('Response ===> ${response.body}');

    if (response.statusCode == 200) {
      String jsonString = response.body;
      print("JSON BODY >>> $jsonString");
      Map registrationResultMap = jsonDecode(jsonString);
      print("MAP >>>> $registrationResultMap");
      LoginResult result = LoginResult.fromJson(registrationResultMap);
      print("Login Result >>>> $result");

      return result;
    } else {
      throw response;
    }
  }

  //CustomerDevices
  static Future<CustomerDeviceResult> postCustomerDevices(
      Registrationdata profile,
      String firebaseId,
      Function showProgress,
      Function dismissProgress) async {
    showProgress();

    final String url = Constants.API_URL + "customerDevices";

    final http.Response response = await http.post(url,
        body: {"customer_id": "${profile.user.id}", "firebase_id": firebaseId},
        headers: {"Accept": "application/json"});
    dismissProgress();

    if (response.statusCode == 200) {
      String jsonString = response.body;
      print("Customer Devices $jsonString");
      Map resultMap = jsonDecode(jsonString);
      CustomerDeviceResult customerDeviceResult =
          CustomerDeviceResult.fromJson(resultMap);

      return customerDeviceResult;
    } else {
      throw response;
    }
  }

  static Future<CustomerNotificationResult> findNotifications(
      int customerId, Function showProgress, Function dismissProgress) async {
    showProgress();

    var url = Constants.API_URL + "customerNotifications?user_id=$customerId";

    var response = await http.get(url, headers: {"Accept": "application/json"});

    String jsonString = response.body;
    print("CustomerNotifications $jsonString");
    Map resultMap = jsonDecode(jsonString);
    CustomerNotificationResult result =
        CustomerNotificationResult.fromJson(resultMap);
    dismissProgress();

    return result;
  }

  static Future<HomeSlideResult> findSlides(
      Function showProgress, Function dismissProgress) async {
    showProgress();

    var url = Constants.API_URL + "homeSlides";

    String jsonString = "";

    var request = await RequestManager.findRequest(url, "GET");

    bool canMakeRequest = true;
    if (request != null) {
      canMakeRequest =
          DateTime.now().difference(request.made_at).inMinutes > 10;
      jsonString = request.responseBody;
    }

    if (canMakeRequest) {
      var response =
          await http.get(url, headers: {"Accept": "application/json"});

      jsonString = response.body;

      await RequestManager.saveRequest(
          new Request(url, "GET", jsonString, DateTime.now()));
    }

    //print("HomeSlideResult $jsonString");
    Map resultMap = jsonDecode(jsonString);
    HomeSlideResult result = HomeSlideResult.fromJson(resultMap);
    dismissProgress();

    return result;
  }

  static Future<LoginResult> editPic(int jobId, String signatureBase64,
      String fileName, Function showProgress, Function dismissProgress) async {
    showProgress();

    var url = Constants.API_URL + "customers-edit-pic";

    var response = await http.post(url, body: {
      'customer_id': "$jobId",
      'photo': signatureBase64,
      'photo_filename': fileName
    }, headers: {
      "Accept": "application/json"
    });
    String jsonString = response.body;
    print("Send Picture URL ${response.request}");
    print("Body  $jsonString");
    Map registrationResultMap = jsonDecode(jsonString);
    LoginResult result = LoginResult.fromJson(registrationResultMap);

    dismissProgress();

    return result;
  }
}
