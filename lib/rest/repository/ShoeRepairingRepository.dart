import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/response/shoe_repairing_result.dart';
import 'package:easy_services/util/LoginManager.dart';
import 'package:easy_services/util/constants.dart';

class ShoeRepairingRepository {
  static Future<ShoeRepairingResult> _find () async{

    var url = Constants.API_URL+"shoe_repairings";

    var response = await http.get(
        url,
        headers: {
          "Accept":"application/json"
        }
    );

    print("ShoeRepairingResult URL ${response.request}");

    String jsonString  = response.body;
    Map registrationResultMap = jsonDecode(jsonString);
    ShoeRepairingResult result = ShoeRepairingResult.fromJson(registrationResultMap);

    return result;

  }

  static findAndStore( Function showProgress,
      Function dismissProgress, Function onSuccess, Function onError){
      showProgress();
    _find()
        .then((value){

        if(value.success && value.data != null && value.data.isNotEmpty){

          LoginManager.connectedUser((Registrationdata profile){

            Registrationdata newProfile = profile;

              newProfile.shoe_repairings = value.data;
              dismissProgress();

              LoginManager.signIn(newProfile, onSuccess, onError);


          }, (errorException){
            dismissProgress();
            onError(errorException);

          });

        }else{

         dismissProgress();
         onError(Exception(value.message));

        }


    })
        .catchError((error){
      dismissProgress();
      onError(error);
    });
  }
}