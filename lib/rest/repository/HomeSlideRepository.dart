import 'package:easy_services/rest/response/home_slide_result.dart';
import 'package:easy_services/util/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class HomeSlideRepository{


  static Future<HomeSlideResult> all (
      Function showProgress,
      Function dismissProgress
      ) async{
    showProgress();


    var url = Constants.API_URL+"homeSlides";

    var response = await http.get(
        url,
        headers: {
          "Accept":"application/json"
        }
    );

    print("HomeSlides URL ${response.request}");

    String jsonString  = response.body;
    Map resultMap = jsonDecode(jsonString);
    HomeSlideResult result = HomeSlideResult.fromJson(resultMap);

    dismissProgress();

    return result;

  }
}