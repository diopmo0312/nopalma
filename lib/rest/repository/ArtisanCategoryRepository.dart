import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/response/artisan_category_result.dart';
import 'package:easy_services/rest/response/retouching_result.dart';
import 'package:easy_services/util/LoginManager.dart';
import 'package:easy_services/util/constants.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class ArtisanCategoryRepository {
  static Future<ArtisanCategoryResult> _find() async {
    var url = Constants.API_URL + "artisan_categories";

    var response = await http.get(url, headers: {"Accept": "application/json"});

    print("ArtisanCategoryResult URL ${response.request}");

    String jsonString = response.body;
    Map registrationResultMap = jsonDecode(jsonString);
    ArtisanCategoryResult result =
        ArtisanCategoryResult.fromJson(registrationResultMap);

    print("ArtisanCategoryResult ${result}");
    return result;
  }

  static findAndStore(Function showProgress, Function dismissProgress,
      Function onSuccess, Function onError) {
    showProgress();
    _find().then((value) {
      if (value.success && value.data != null && value.data.isNotEmpty) {
        LoginManager.connectedUser((Registrationdata profile) {
          Registrationdata newProfile = profile;
          print("DATA TO SAVE ====> ${value.data}");
          print("NEW PROFILE ====> ${newProfile.artisan_categories = value.data}");
          print("DATA SAVED ====> ${newProfile.artisan_categories}");

          newProfile.artisan_categories = value.data;
          dismissProgress();

          LoginManager.signIn(newProfile, onSuccess, onError);
        }, (errorException) {
          dismissProgress();
          onError(errorException);
        });
      } else {
        dismissProgress();
        onError(Exception(value.message));
      }
    }).catchError((error) {
      dismissProgress();
      onError(error);
    });
  }
}
