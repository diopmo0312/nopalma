import 'package:easy_services/rest/response/job_result.dart';
import 'package:easy_services/rest/response/order/TransactionResult.dart';
import 'package:easy_services/rest/response/order/order_result.dart';
import 'package:easy_services/rest/response/order/order_submit_result.dart';
import 'package:easy_services/rest/response/order_history_result.dart';
import 'package:easy_services/util/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class OrderRepository{

  static Future<OrderSubmitResult> create (
      String jsonBody,
      Function showProgress,
      Function dismissProgress
      ) async{
    showProgress();

    var url = Constants.API_URL+"orders";

    var response = await http.post(
        url,
        body: jsonBody,
        headers: {
          "Accept":"application/json",
          "Content-Type": "application/json"
        }
    );


    print("OrderSubmitResult URL ${response.request}");

    String jsonString  = response.body;
    Map registrationResultMap = jsonDecode(jsonString);
    OrderSubmitResult result = OrderSubmitResult.fromJson(registrationResultMap);

    dismissProgress();

    return result;

  }

  static Future<OrderResult> find (
      int customer_id,
      Function showProgress,
      Function dismissProgress,
   [String query="", String orderQuery = "&orderBy=created_at&sortedBy=desc"]
      ) async{
    showProgress();

    if(query.isEmpty){
      query = "customer_id=$customer_id";
    }
    var url = Constants.API_URL+"orders?$query";

    var response = await http.get(
        url,
        headers: {
          "Accept":"application/json"
        }
    );

    print("Orders URL ${response.request}");

    String jsonString  = response.body;
    Map registrationResultMap = jsonDecode(jsonString);
    OrderResult result = OrderResult.fromJson(registrationResultMap);

    dismissProgress();

    return result;

  }


  static Future<OrderHistoryResult> getOrderHistories (
      int order_id,
      Function showProgress,
      Function dismissProgress
      ) async{
    showProgress();


    var url = Constants.API_URL+"orderHistories?order_id=$order_id";

    var response = await http.get(
        url,
        headers: {
          "Accept":"application/json"
        }
    );

    print("OrderHistory URL ${response.request}");

    String jsonString  = response.body;
    Map registrationResultMap = jsonDecode(jsonString);
    OrderHistoryResult result = OrderHistoryResult.fromJson(registrationResultMap);

    dismissProgress();

    return result;

  }

  static Future<JobResult> getJobs (
      int orderId,
      Function showProgress,
      Function dismissProgress
      ) async{
    showProgress();


    var url = Constants.API_URL+"order-jobs/$orderId";

    var response = await http.get(
        url,
        headers: {
          "Accept":"application/json"
        }
    );

    print("OrderHistory URL ${response.request}");

    String jsonString  = response.body;
    Map registrationResultMap = jsonDecode(jsonString);
    JobResult result = JobResult.fromJson(registrationResultMap);

    dismissProgress();

    return result;

  }

  static Future<TransactionResult> checkPayment (
      String reference,
      Function showProgress,
      Function dismissProgress
      ) async{
    showProgress();

    var url = Constants.API_URL+"invoice_payments/gateway/notify/touch_pay/checkpayment/$reference";

    var response = await http.get(
        url,
        headers: {
          "Accept":"application/json"
        }
    );

    print("TransactionResults URL ${response.request}");

    String jsonString  = response.body;
    Map registrationResultMap = jsonDecode(jsonString);
    TransactionResult result = TransactionResult.fromJson(registrationResultMap);

    dismissProgress();

    return result;

  }



}