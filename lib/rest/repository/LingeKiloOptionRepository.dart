import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/response/linge_kilo_option_result.dart';
import 'package:easy_services/util/LoginManager.dart';
import 'package:easy_services/util/constants.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class LingeKiloOptionRepository {
  static Future<LingeKiloOptionResult> _find() async {

    var url = Constants.API_URL + "linge_kilo_options";

    var response = await http.get(url, headers: {"Accept": "application/json"});

    print("LingeKiloOptionResult URL ${response.request}");

    String jsonString = response.body;
    Map registrationResultMap = jsonDecode(jsonString);
    LingeKiloOptionResult result =
        LingeKiloOptionResult.fromJson(registrationResultMap);

    print("LingeKiloOptionResult ${result}");
    return result;
  }

  static findAndStore(Function showProgress, Function dismissProgress,
      Function onSuccess, Function onError) {
    showProgress();
    _find().then((value) {
      if (value.success && value.data != null && value.data.isNotEmpty) {
        /* Data to save on local storage */
        LoginManager.connectedUser((Registrationdata profile) {
          Registrationdata newProfile = profile;
          print("DATA TO SAVE LingeKiloOptionResult====> ${value.data}");
          print("NEW PROFILE LingeKiloOptionResult ====> ${newProfile.linge_kilo_options = value.data}");
          print("DATA SAVED LingeKiloOptionResult ====> ${newProfile.linge_kilo_options}");

          newProfile.linge_kilo_options = value.data;
          dismissProgress();

          LoginManager.signIn(newProfile, onSuccess, onError);
        }, (errorException) {
          dismissProgress();
          onError(errorException);
        });
      } else {
        dismissProgress();
        onError(Exception(value.message));
      }
    }).catchError((error) {
      dismissProgress();
      onError(error);
    });
  }
}
