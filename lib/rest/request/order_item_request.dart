import 'dart:convert';

import 'package:easy_services/models/ServicePricing.dart';
import 'package:easy_services/models/linge_kilo_option.dart';
import 'package:easy_services/models/pressing_clothes.dart';
import 'package:easy_services/models/retouching.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/models/shoe_repairing.dart';
import 'package:easy_services/models/artisan_category.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'order_item_request.g.dart';

@JsonSerializable()
class OrderItemRequest{

  String service_slug;

  String product_slug;

  String meta_data;

  int quantity;

  String quantity_unit;

  String unit_price;

  String currency;

  OrderItemRequest(this.service_slug,this.product_slug, this.meta_data, this.quantity,
      this.quantity_unit, this.unit_price, this.currency);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory OrderItemRequest.fromJson(Map<String, dynamic> json) => _$OrderItemRequestFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$OrderItemRequestToJson(this);

  PressingClothes pressingClothes(){
    PressingClothes pressingClothes = PressingClothes.fromJson(jsonDecode(meta_data));
    return pressingClothes;
  }

  Retouching retouching(){
    Retouching retouching = Retouching.fromJson(jsonDecode(meta_data));
    return retouching;
  }

  ShoeRepairing shoeRepairing(){
    ShoeRepairing shoeRepairing = ShoeRepairing.fromJson(jsonDecode(meta_data));
    return shoeRepairing;
  }

  Service service(){
     Service service = Service.fromJson(jsonDecode(meta_data));
    return service;
  }

  LingeKiloOption option(){
    LingeKiloOption service = LingeKiloOption.fromJson(jsonDecode(meta_data));
    return service;
  }

  ArtisanCategory artisanCategory(){
    ArtisanCategory artisanCategory = ArtisanCategory.fromJson(jsonDecode(meta_data));
    return artisanCategory;
  }


  ServicePricing servicePricing(){
    if(service_slug == "pressing"){
      return pressingClothes();
    }else if(service_slug  == "retouche"){

      return retouching();
    }else if(service_slug  == "linge_au_kilo"){
      return option();
    }else if(service_slug  == "repassage"){
      return service();
    }else if(service_slug  == "coordonnerie"){
      return shoeRepairing();
    }else if(service_slug  == "ouvrier_et_artisan"){
      return artisanCategory();
    }else{
      return shoeRepairing();
    }

  }

  @override
  bool operator ==(other) {
    return (other as OrderItemRequest).product_slug== this.product_slug;
  }



}