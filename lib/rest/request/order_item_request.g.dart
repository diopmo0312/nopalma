// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_item_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderItemRequest _$OrderItemRequestFromJson(Map<String, dynamic> json) {
  return OrderItemRequest(
    json['service_slug'] as String,
    json['product_slug'] as String,
    json['meta_data'] as String,
    json['quantity'] as int,
    json['quantity_unit'] as String,
    json['unit_price'] as String,
    json['currency'] as String,
  );
}

Map<String, dynamic> _$OrderItemRequestToJson(OrderItemRequest instance) =>
    <String, dynamic>{
      'service_slug': instance.service_slug,
      'product_slug': instance.product_slug,
      'meta_data': instance.meta_data,
      'quantity': instance.quantity,
      'quantity_unit': instance.quantity_unit,
      'unit_price': instance.unit_price,
      'currency': instance.currency,
    };
