// This file contains ALL Strings in ALL Languages
// import 'LangStrings.dart' in order to use it
// To add other languages, you have to
// 1. Include another element in listLang
// 2. Create another static const listStrings_XX, in which XX is the language code defined by YOU
// 3. Modify the function gs()


class LangStrings {
  // strLang is the Current Language selected by the User
  // *** NOT by the system locale of the mobile phone ***
  // e.g. You can use LangStrings.setLang('EN') to set Current Language to English
  static var strLang = '';

  // List of Language
  static const listLang = [
    // List of Languages available
    { 'lang':'EN', 'lang_desc':'English'},
    { 'lang':'FR', 'lang_desc':'French'},

  ];

  // vars for English
  static const listStrings_EN = [
    // General
    { 'title':'screen_login_title', 'content':'Login in'},

    //Service
    { 'title':'Pressing', 'content':'Pressing'},
    { 'title':'PressingDesc', 'content':'Dry cleaning service of clothing. Tariffied by the piece, your clothes are collected, treated and delivered in 48 Hours'},
    { 'title':'LingeauKilo', 'content':'Kilo linen'},
    { 'title':'LingeauKiloDesc', 'content':'Your everyday clothes (jeans, polo shirt, Tee-shirt etc.) are priced per kilogram.'},
    { 'title':'RepassageauKilo', 'content':'Kilo ironing'},
    { 'title':'RepassageauKiloDesc', 'content':'Rate per kilogram your clothes are collected, ironed and delivered.'},
    { 'title':'Retouche', 'content':'Retouch'},
    { 'title':'RetoucheDesc', 'content':'Touch-up service for all your clothes.'},
    { 'title':'Cordonnerie', 'content':'Shoe Repairing'},
    { 'title':'CordonnerieDesc', 'content':'Your shoes are repaired and pampered by our craftsmen / boot makers.'},

    { 'title':'edit_phone_number', 'content':'Phone number'},

    { 'title':'dialog_error_title', 'content':'Error encountered'},
    { 'title':'dialog_error_message', 'content':'Sorry error encountered.'},

    { 'title':'dialog_error_message_bad_phone', 'content':'Check that you have entered the phone number.'},

    { 'title':'button_next_title', 'content':'Next'},

    { 'title':'screen_phone_verification_title', 'content':'Phone Verification'},
    { 'title':'label_edit_code_verification', 'content':'Enter Code Verification'},

    { 'title':'button_retry_title', 'content':'Retry'},

    { 'title':'particulier', 'content':'Particular'},
    { 'title':'business', 'content':'Business'},

    { 'title':'screen_registration_title', 'content':'Create Account'},
    { 'title':'label_type_of_account', 'content':'Type of account'},

    { 'title':'edit_first_name', 'content':'Surname'},
    { 'title':'edit_last_name', 'content':'first names'},
    { 'title':'edit_email', 'content':'Email'},
    { 'title':'label_edit_company_name', 'content':'Company Name'},
    { 'title':'edit_company_name', 'content':'Company Name'},

    { 'title':'label_terms', 'content':"I accept the terms of use and privacy policy."},
    { 'title':'button_save_title', 'content':"Save"},

    { 'title':'screen_deliveries_addresses', 'content':"Deliveries Addresses"},
    { 'title':'location_type_house_title', 'content':"House"},
    { 'title':'location_type_office_title', 'content':"Office"},
    { 'title':'location_type_others_title', 'content':"Other"},

    { 'title':'screen_geolocation_title', 'content':"Geolocation"},
    { 'title':'edit_search_title', 'content':"Search a place"},
    { 'title':'button_terminate_title', 'content':"Terminate"},


    { 'title':'screen_location_detail_title', 'content':"Location Detail"},
    { 'title':'label_address_title', 'content':"Address"},
    { 'title':'edit_address_title', 'content':"Address"},
    { 'title':'edit_appart_num_title', 'content':"Number Appart / Villa"},
    { 'title':'edit_town_title', 'content':"Town"},
    { 'title':'edit_additional_address_title', 'content':"Additional address"},
    { 'title':'edit_additional_address_hint', 'content':"Near Pharmacie du Bonheur"},

    { 'title':'menu_home_title', 'content':"Home"},
    { 'title':'menu_orders_title', 'content':"Orders"},
    { 'title':'menu_notifications_title', 'content':"Notifications"},
    { 'title':'menu_subscriptions_title', 'content':"Subscriptions"},
    { 'title':'menu_profile_title', 'content':"Profile"},


    { 'title':'screen_mycart_title', 'content':"My Cart"},
    { 'title':'screen_empty_cart_title', 'content':"Empty Cart."},
    { 'title':'subtotal_title', 'content':"Subtotal"},

    { 'title':'no_data_message', 'content':"No Data Avalaible"},

    { 'title':'business_account', 'content':"Business Account"},
    { 'title':'basic_account', 'content':"Basic Account"},
    { 'title':'unknown_name', 'content':"Unknown Name"},
    { 'title':'profile_detail', 'content':"Profile Detail"},
    { 'title':'subscriptions', 'content':"Subscriptions"},
    { 'title':'inviter_un_ami', 'content':"Invite a friend"},
    { 'title':'logout', 'content':"Logout"},


    { 'title':'screen_order_summary_title', 'content':"Order Summary"},
    { 'title':'field_order_numero_title', 'content':"Order N°"},
    { 'title':'field_pickup_date_title', 'content':"Pickup Date"},
    { 'title':'field_artisan_pickup_date_title', 'content':"Passage Date"},
    { 'title':'field_pickup_address_title', 'content':"Pickup Address"},
    { 'title':'field_delivery_date_title', 'content':"Delivery Date"},
    { 'title':'field_delivery_address_title', 'content':"Delivery Address"},
    { 'title':'field_amount_title', 'content':"Amount"},
    { 'title':'field_invoice_title', 'content':"See Invoice"},

    { 'title':'address_not_available_message', 'content':"Address not available"},
    { 'title':'field_services_title', 'content':"Services"},

    { 'title':'screen_choose_address_title', 'content':"Choose an address"},
    { 'title':'screen_define_slot_title', 'content':"Define Slot"},
    { 'title':'field_lave-seche-plie_title', 'content':"Washed - Dried - Folded"},
    { 'title':'field_lave-seche-repasse_title', 'content':"Washed - Dried - Ironed"},
    { 'title':'screen_invoice_preview_title', 'content':"Invoice Preview"},
    { 'title':'field_subtitle_title', 'content':"SubTotal"},
    { 'title':'dialog_edit_quantity_label', 'content':"Quantity"},

    { 'title':'field_select_clothes_label', 'content':"Select clothes"},
    { 'title':'screen_order_detail_title', 'content':"Order Detail"},
    { 'title':'field_evaluate_order_title', 'content':"Evaluate your order experience"},
    { 'title':'field_share_your_experience_title', 'content':"Share your experience in a few words"},
    { 'title':'edit_type_something_here_hint', 'content':"Type something here"},
    { 'title':'button_give_feedback_title', 'content':"Give a Feedback"},

    { 'title':'screen_invoice_title', 'content':"Invoice"},
    { 'title':'field_delivery_fees_title', 'content':"Delivery Fees"},
    { 'title':'field_tax_title', 'content':"TVA"},
    { 'title':'field_total_title', 'content':"Total"},


    { 'title':'screen_my_subscriptions_title', 'content':"My Subscriptions"},
    { 'title':'dont_have_subscription_message', 'content':"You don't have Subscription"},

    { 'title':'screen_subscribe_title', 'content':"Subscribe"},
    { 'title':'field_quantity_label', 'content':"Quantity"},

    { 'title':'field_choose_payment_method_title', 'content':"Choose Payment Method"},
    { 'title':'subscription_successful_msg', 'content':"Your Subscription is successful"},
    { 'title':'month', 'content':"Month"},


    { 'title':'screen_checkout_title', 'content':"Checkout"},

    { 'title':'screen_option_of_delivery_title', 'content':"Option of Delivery"},
    { 'title':'field_choose_formula_title', 'content':"Please choose the appropriate formula"},
    { 'title':'screen_option_of_artisan_title', 'content':"Option of Visit"},
    { 'title':'field_artisan_choose_formula_title', 'content':"Please enter your availability and your diagnosis"},

    { 'title':'field_slot_option_title', 'content':"60 min time slot"},

    { 'title':'field_delay_option_one_title', 'content':"48 hours processing time"},
    { 'title':'field_delivery_option_one_title', 'content':"Delivery charges: 1500 XOF"},

    { 'title':'field_delay_option_two_title', 'content':"24 hour processing time"},
    { 'title':'field_delivery_option_two_title', 'content':"Delivery charges: 3000 XOF"},

    { 'title':'field_choose_schedule_title', 'content':"Choose a pickup and delivery schedule"},
    { 'title':'field_pickup_title', 'content':"Pickup"},
    { 'title':'field_artisan_pickup_title', 'content':"Visit"},
    { 'title':'field_delivery_title', 'content':"Delivery"},
    { 'title':'field_choose_artisan_schedule_title', 'content':"Choose a visit schedule"},

    { 'title':'field_pickup_delivery_title', 'content':"Define your pickup and delivery location:"},
    { 'title':'field_artisan_pickup_delivery_title', 'content':"Define your visit location:"},

    { 'title':'field_add_pickup_title', 'content':"Add Pickup"},
    { 'title':'field_add_delivery_title', 'content':"Add Delivery"},
    { 'title':'field_add_slot_title', 'content':"Add a slot"},
    { 'title':'field_add_artisan_pickup_title', 'content':"Add Visit"},
    { 'title':'field_add_artisan_slot_title', 'content':"Add a slot"},


    { 'title':'screen_types_retouching_title', 'content':"Select types of retouching"},
    { 'title':'screen_types_repairing_title', 'content':"Select types of repairing"},
    { 'title':'screen_types_artisan_title', 'content':"Select desired craftsman"},

    { 'title':'screen_settings_title', 'content':"Settings"},
    { 'title':'field_language_title', 'content':"Language"},

    { 'title':'field_HISTORY_title', 'content':"HISTORY"},
    { 'title':'field_PICKUP_title', 'content':"PICKUP"},
    { 'title':'field_DELIVERY_title', 'content':"DELIVERY"},

    { 'title':'job_arrived_message', 'content':" is arrived."},
    { 'title':'job_started_message', 'content':" is coming to you."},
    { 'title':'job_completed_message', 'content':"Completed by"},
    { 'title':'job_failed_message', 'content':"Failed by"},
    { 'title':'job_canceled_message', 'content':"Canceled by"},

    { 'title':'internet_error_message', 'content':"No Internet Connection"},

    { 'title':'order_is_waiting_message', 'content':"Your order is waiting."},
    { 'title':'order_is_managed_message', 'content':"Your order is managed."},
    { 'title':'order_is_started_message', 'content':"Your order is started"},
    { 'title':'order_is_successful_message', 'content':"Your order is successful"},
    { 'title':'order_is_failed_message', 'content':"Your order is failed"},
    { 'title':'order_is_cancel_message', 'content':"Your order is cancel"},
    { 'title':'order_edited_message', 'content':"Your order is edited"},

    { 'title':'order_customer_pickuped_message', 'content':"Pickup done successfully."},
    { 'title':'order_waiting_customer_pickup_message', 'content':"Pickup waiting."},

    { 'title':'order_waiting_customer_delivery_message', 'content':"Delivery pending."},
    { 'title':'order_customer_delivered_message', 'content':"Delivery made successfully."},

    { 'title':'dialog_delete_item_title', 'content':"Confirm Deletion"},
    { 'title':'dialog_delete_item_message', 'content':"Are you sure you want to delete?"},


    { 'title':'dialog_button_confirm', 'content':"Confirm"},
    { 'title':'dialog_button_cancel', 'content':"Cancel"},

    { 'title':'hello_message', 'content':"Hello "},

    { 'title':'button_subscribe_title', 'content':"Subscribe"},

    { 'title':'Clothes_title', 'content':"Clothes"},
    { 'title':'Suits_Tailors_title', 'content':"Suits or Tailors"},
    { 'title':'no_delivery_fees_title', 'content':"No Delivery Fees"},
    { 'title':'of_clothes_title', 'content':" of Clothes"},

    { 'title':'Passage_every2weeks_title', 'content':"Passage every 2 weeks"},
    { 'title':'Passage_every_week_title', 'content':"Passage every week"},
    { 'title':'Nothing_title', 'content':"Nothing"},

    { 'title':'Active_title', 'content':"Active"},
    { 'title':'Inactive_title', 'content':"Inactive"},

    { 'title':'Expiration_Date_title', 'content':"Expiration Date"},

    { 'title':'screen_profile_title', 'content':'Profile'},
    { 'title':'invite_friend_message', 'content':"I invite you to download the Nopalma Services app to make life easy."},
    { 'title':'screen_edit_profile_title', 'content':"Edit Profile"},

    { 'title':'View_Receipt_title', 'content':"View Receipt"},

    { 'title':'label_account_created', 'content':"Registration date"},

    { 'title':'dialog_error_bad_code_message', 'content':"Bad Code Entered"},
    { 'title':'diagnostis', 'content':"Observation" },
    { 'title':'diagnostis_hint', 'content':"Your observation" },
    { 'title':'travel_rate', 'content':"Travel costs + diagnosis" },




  ];

  // vars for French
  static const listStrings_FR = [
    // General
    { 'title':'screen_login_title', 'content':'Se Connecter'},

    //Service
    { 'title':'Pressing', 'content':'Pressing'},
    { 'title':'PressingDesc', 'content':'Service de nettoyage à sec de vêtement. Vos vêtements sont collectés, traités puis livrés en 48 Heures.'},

    { 'title':'LingeauKilo', 'content':'Blanchisserie'},
    { 'title':'LingeauKiloDesc', 'content':'Votre linge du quotidien (Jeans, polo,Tee-shirt etc.) est tarifié au kilogramme.'},


    { 'title':'RepassageauKilo', 'content':'Repassage'},
    { 'title':'RepassageauKiloDesc', 'content':'Vos vêtements au kilogramme sont collectés, repassés puis livrés.'},

    { 'title':'Retouche', 'content':'Retouche'},
    { 'title':'RetoucheDesc', 'content':'Service de retouche pour tous vos vêtements.'},

    { 'title':'Cordonnerie', 'content':'Cordonnerie'},
    { 'title':'CordonnerieDesc', 'content':'Vos chaussures sont réparées et chouchoutées par nos artisans/bottiers.'},


    { 'title':'edit_phone_number', 'content':'Téléphone'},

    { 'title':'dialog_error_title', 'content':'Erreur Rencontrée'},
    { 'title':'dialog_error_message', 'content':'Désolé !  Nous avons rencontré des erreurs.'},

    { 'title':'dialog_error_message_bad_phone', 'content':'Vérifiez que vous avez saisi le numéro de téléphone.'},

    { 'title':'button_next_title', 'content':'Suivant'},

    { 'title':'screen_phone_verification_title', 'content':'Vérification du téléphone'},
    { 'title':'label_edit_code_verification', 'content':'Entrer la vérification du code'},

    { 'title':'button_retry_title', 'content':'Réessayez'},

    { 'title':'particulier', 'content':'Particulier'},
    { 'title':'business', 'content':'Business'},

    { 'title':'screen_registration_title', 'content':'Créer un compte'},
    { 'title':'label_type_of_account', 'content':'Type de Compte'},

    { 'title':'edit_first_name', 'content':'Nom'},
    { 'title':'edit_last_name', 'content':'Prénoms'},
    { 'title':'edit_email', 'content':'Email'},
    { 'title':'label_edit_company_name', 'content':'Nom de la compagnie'},
    { 'title':'edit_company_name', 'content':'Raison Sociale'},

    { 'title':'label_terms', 'content':"J'accepte les conditions d'utilisation et la politique de confidentialité."},
    { 'title':'button_save_title', 'content':"Enregistrer"},

    { 'title':'screen_deliveries_addresses', 'content':"Adresses Livraisons"},
    { 'title':'location_type_house_title', 'content':"Maison"},
    { 'title':'location_type_office_title', 'content':"Bureau"},
    { 'title':'location_type_others_title', 'content':"Autre"},

    { 'title':'screen_geolocation_title', 'content':"Géolocalisation"},
    { 'title':'edit_search_title', 'content':"Rechercher un lieu"},
    { 'title':'button_terminate_title', 'content':"Terminer"},


    { 'title':'screen_location_detail_title', 'content':"Détail de la localisation"},
    { 'title':'label_address_title', 'content':"Adresse"},
    { 'title':'edit_address_title', 'content':"Adresse"},
    { 'title':'edit_appart_num_title', 'content':"Numéro Appart / Villa"},
    { 'title':'edit_town_title', 'content':"Quartier"},
    { 'title':'edit_additional_address_title', 'content':"Adresse supplémentaire"},
    { 'title':'edit_additional_address_hint', 'content':"Près de Pharmacie du Bonheur"},

    { 'title':'menu_home_title', 'content':"Accueil"},
    { 'title':'menu_orders_title', 'content':"Commandes"},
    { 'title':'menu_notifications_title', 'content':"Notifications"},
    { 'title':'menu_subscriptions_title', 'content':"Abonnements"},
    { 'title':'menu_profile_title', 'content':"Profil"},

    { 'title':'screen_mycart_title', 'content':"Mon Panier"},
    { 'title':'screen_empty_cart_title', 'content':"Panier vide."},
    { 'title':'subtotal_title', 'content':"Sous-total"},

    { 'title':'no_data_message', 'content':"Pas de données disponibles"},


    { 'title':'business_account', 'content':"Compte d'entreprise"},
    { 'title':'basic_account', 'content':"Compte de base"},
    { 'title':'unknown_name', 'content':"Nom inconnu"},
    { 'title':'profile_detail', 'content':"Détail du profil"},
    { 'title':'subscriptions', 'content':"Abonnements"},
    { 'title':'inviter_un_ami', 'content':"Inviter un ami"},
    { 'title':'logout', 'content':"Déconnexion"},



    { 'title':'screen_order_summary_title', 'content':"Récapitulatif de la commande"},
    { 'title':'field_order_numero_title', 'content':"Commande N°"},
    { 'title':'field_pickup_date_title', 'content':"Date de ramassage"},
    { 'title':'field_artisan_pickup_date_title', 'content':"Date de passage"},
    { 'title':'field_pickup_address_title', 'content':"Adresse de ramassage"},
    { 'title':'field_delivery_date_title', 'content':"Date de livraison"},
    { 'title':'field_delivery_address_title', 'content':"Adresse de livraison"},
    { 'title':'field_amount_title', 'content':"Montant"},
    { 'title':'field_invoice_title', 'content':"Voir Facture"},

    { 'title':'address_not_available_message', 'content':"Adresse non disponible"},
    { 'title':'field_services_title', 'content':"Services"},

    { 'title':'screen_choose_address_title', 'content':"Choisissez une adresse"},
    { 'title':'screen_define_slot_title', 'content':"Définir le créneaux horaire"},
    { 'title':'field_lave-seche-plie_title', 'content':"Lavé - Séché - Plié"},
    { 'title':'field_lave-seche-repasse_title', 'content':"Lavé - Séché - Repassé"},
    { 'title':'screen_invoice_preview_title', 'content':"Prévusialisation Facture"},
    { 'title':'field_subtitle_title', 'content':"Sous-total"},
    { 'title':'dialog_edit_quantity_label', 'content':"Quantité"},

    { 'title':'field_select_clothes_label', 'content':"Sélectionnez des vêtements"},
    { 'title':'screen_order_detail_title', 'content':"Détails de la commande"},
    { 'title':'field_evaluate_order_title', 'content':"Évaluez votre expérience de commande"},
    { 'title':'field_share_your_experience_title', 'content':"Partagez votre expérience en quelques mots"},
    { 'title':'edit_type_something_here_hint', 'content':"Tapez quelque chose ici"},
    { 'title':'button_give_feedback_title', 'content':"Aviser"},

    { 'title':'screen_invoice_title', 'content':"Facture"},
    { 'title':'field_delivery_fees_title', 'content':"Frais de livraison"},
    { 'title':'field_tax_title', 'content':"TVA"},
    { 'title':'field_total_title', 'content':"Total"},


    { 'title':'screen_my_subscriptions_title', 'content':"Mes Abonnements"},
    { 'title':'dont_have_subscription_message', 'content':"Vous n'avez pas d'abonnement"},

    { 'title':'screen_subscribe_title', 'content':"Souscrire"},
    { 'title':'field_quantity_label', 'content':"Quantité"},

    { 'title':'field_choose_payment_method_title', 'content':"Choisissez le mode de paiement"},
    { 'title':'subscription_successful_msg', 'content':"Votre abonnement est réussi"},
    { 'title':'month', 'content':"Mois"},


    { 'title':'screen_checkout_title', 'content':"Passer la commande"},

    { 'title':'screen_option_of_delivery_title', 'content':"Option de livraison"},
    { 'title':'field_choose_formula_title', 'content':"Veuillez choisir la formule appropriée"},
    { 'title':'screen_option_of_artisan_title', 'content':"Option de visite"},
    { 'title':'field_artisan_choose_formula_title', 'content':"Veuillez renseigner votre disponibilité et votre diagnostic"},

    { 'title':'field_slot_option_title', 'content':"Créneau horaire de 60 min"},

    { 'title':'field_delay_option_one_title', 'content':"Délai de traitement de 48 heures"},
    { 'title':'field_delivery_option_one_title', 'content':"Frais de livraison: 1500 FCFA"},

    { 'title':'field_delay_option_two_title', 'content':"Délai de traitement de 24 heures"},
    { 'title':'field_delivery_option_two_title', 'content':"Frais de livraison: 3000 FCFA"},

    { 'title':'field_choose_schedule_title', 'content':"Choisissez un horaire de ramassage et de livraison"},
    { 'title':'field_pickup_title', 'content':"Ramasser"},
    { 'title':'field_artisan_pickup_title', 'content':"Disponibilité"},
    { 'title':'field_delivery_title', 'content':"Livraison"},
    { 'title':'field_choose_artisan_schedule_title', 'content':"Choisissez un horaire de passage"},

    { 'title':'field_pickup_delivery_title', 'content':"Définir votre lieu de ramassage et de livraison:"},
    { 'title':'field_artisan_pickup_delivery_title', 'content':"Définir le lieu de visite:"},

    { 'title':'field_add_pickup_title', 'content':"Ajouter un ramassage"},
    { 'title':'field_add_delivery_title', 'content':"Ajouter une livraison"},
    { 'title':'field_add_slot_title', 'content':"Ajouter un créneau"},
    { 'title':'field_add_artisan_pickup_title', 'content':"Ajouter le lieu"},
    { 'title':'field_add_artisan_slot_title', 'content':"Ajouter un créneau"},


    { 'title':'screen_types_retouching_title', 'content':"Sélectionnez les types de retouches"},
    { 'title':'screen_types_repairing_title', 'content':"Sélectionnez les types de réparation"},
    { 'title':'screen_types_artisan_title', 'content':"Sélectionnez les artisans souhaités"},

    { 'title':'screen_settings_title', 'content':"Paramètres"},
    { 'title':'field_language_title', 'content':"Langue"},

    { 'title':'field_HISTORY_title', 'content':"HISTORIQUE"},
    { 'title':'field_PICKUP_title', 'content':"RAMASSAGE"},
    { 'title':'field_DELIVERY_title', 'content':"LIVRAISON"},

    { 'title':'job_arrived_message', 'content':" est arrivé."},
    { 'title':'job_started_message', 'content':" vient à vous."},
    { 'title':'job_completed_message', 'content':"Terminé par"},
    { 'title':'job_failed_message', 'content':"A échoué par"},
    { 'title':'job_canceled_message', 'content':"Annulé par"},

    { 'title':'internet_error_message', 'content':"Pas de connexion Internet"},

    { 'title':'order_is_waiting_message', 'content':"Votre commande est en attente."},
    { 'title':'order_is_managed_message', 'content':"Votre commande est traitée."},
    { 'title':'order_is_started_message', 'content':"Votre commande est lancée."},
    { 'title':'order_is_successful_message', 'content':"Votre commande est réussie."},
    { 'title':'order_is_failed_message', 'content':"Votre commande a échoué."},
    { 'title':'order_is_cancel_message', 'content':"Votre commande est annulée."},
    { 'title':'order_edited_message', 'content':"Votre commande a été modifée."},

    { 'title':'order_customer_pickuped_message', 'content':"Ramassage effecuté avec succès."},
    { 'title':'order_waiting_customer_pickup_message', 'content':"Ramassage en attente."},

    { 'title':'order_waiting_customer_delivery_message', 'content':"Livraison en attente."},
    { 'title':'order_customer_delivered_message', 'content':"Livraison effectuée avec succès."},

    { 'title':'dialog_delete_item_title', 'content':"Confirmer la suppression"},
    { 'title':'dialog_delete_item_message', 'content':"Êtes-vous sûr de vouloir supprimer?"},

    { 'title':'dialog_button_confirm', 'content':"Confirmer"},
    { 'title':'dialog_button_cancel', 'content':"Annuler"},

    { 'title':'hello_message', 'content':"Salut "},

    { 'title':'button_subscribe_title', 'content':"Souscrire"},

    { 'title':'Clothes_title', 'content':"Vêtements"},
    { 'title':'Suits_Tailors_title', 'content':"Costumes ou tailleurs"},
    { 'title':'no_delivery_fees_title', 'content':"Pas de frais de livraison"},
    { 'title':'of_clothes_title', 'content':" de Vêtements"},

    { 'title':'Passage_every2weeks_title', 'content':"Passage toutes les 2 semaines"},
    { 'title':'Passage_every_week_title', 'content':"Passage chaque semaine"},
    { 'title':'Nothing_title', 'content':"Rien"},

    { 'title':'Active_title', 'content':"Actif"},
    { 'title':'Inactive_title', 'content':"Inactif"},

    { 'title':'Expiration_Date_title', 'content':"Date d'expiration"},

    { 'title':'screen_profile_title', 'content':'Profil'},
    { 'title':'invite_friend_message', 'content':"Je t'invite à télécharger l'appli Nopalma Client pour rendre la vie facile."},
    { 'title':'screen_edit_profile_title', 'content':"Modifier Profil"},
    { 'title':'View_Receipt_title', 'content':"Voir le Reçu"},

    { 'title':'label_account_created', 'content':"Date d'Inscription"},
    { 'title':'dialog_error_bad_code_message', 'content':"Mauvais Code Entré"},

    /*{ 'title':'sign_up', 'content':'Sign Up'},
    { 'title':'login', 'content':'Login'},*/

    { 'title':'sign_up', 'content':"S'inscrire"},
    { 'title':'login', 'content':"S'identifier"},

//    { 'title':"Already_have_an_account", 'content':"Already have an account?"},
    { 'title':"Already_have_an_account", 'content':"Avez vous déjà un compte?"},
    { 'title':'diagnostis', 'content':"Observation" },
    { 'title':'diagnostis_hint', 'content':"Vos remarques" },
    { 'title':'travel_rate', 'content':"Frais de déplacement + diagnostic" },


  ];


  // To set Current Language
  static void setLang(strLangParm) {
    if (strLangParm == '') {
      strLang = 'EN';
    } else {
      strLang = strLangParm;
    }
  }

  // Get a string by 'title', and return 'content',
  // According to the Current Language strLang
  static String gs(strKey) {
    switch (strLang) {
      case 'EN':
        for (var i=0; i< listStrings_EN.length; i++) {
          if (listStrings_EN[i]['title'] == strKey) {
            return listStrings_EN[i]['content'];
          }
        }
        return '';
      case 'FR':
        for (var i=0; i< listStrings_FR.length; i++) {
          if (listStrings_FR[i]['title'] == strKey) {
            return listStrings_FR[i]['content'];
          }
        }
        return '';
    }
  }
}