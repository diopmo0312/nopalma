import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/ui/mapbox_autocomplete_screen.dart';
import 'package:easy_services/ui/slide/slide_screen.dart';
import 'package:easy_services/ui/splashscreen_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:intl/intl.dart';

void main()async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  Intl.defaultLocale = 'fr_FR';
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {


    if(LangStrings.strLang == '' ){
      LangStrings.setLang("FR");
    }


    return MaterialApp(
      title: 'Nopalma Client',
      debugShowCheckedModeBanner: false,

      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'), // English
        const Locale('fr', 'FR'), // French
        // ... other locales the app supports
      ],

      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primaryColor: AppColors.nopalmaBlue,
          primaryColorDark: AppColors.colorPrimaryDark,
          accentColor: AppColors.nopalmaYellow,
        fontFamily: "ArialRoundedMT",
      ),
      home:  new SplashScreen(
          seconds: 5,
          navigateAfterSeconds: new SlideScreen()
      ),
    );
  }
}


