import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/models/account_type.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/UserRepository.dart';
import 'package:easy_services/rest/response/registration_result.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/delivery_address_screen.dart';
import 'package:easy_services/ui/login/connexion_screen.dart';
import 'package:easy_services/ui/main/main_screen.dart';
import 'package:easy_services/util/LoginManager.dart';
import 'package:easy_services/util/country_picker/country.dart';
import 'package:easy_services/util/country_picker/flutter_country_picker.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:auto_size_text/auto_size_text.dart';

class RegistrationScreen extends StatefulWidget {

  final String dialingCode;
  final String phoneNumber;
  final String countryCode;

  RegistrationScreen(this.dialingCode, this.phoneNumber, this.countryCode);

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();

}

class _RegistrationScreenState extends State<RegistrationScreen> {



  TextEditingController firstNameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController companyNameController = new TextEditingController();

  List<AccountType> accountTypes = [];

  AccountType _currentAccountType;

  bool _termsAccepted = false;
  bool hasCompany = false;

  void _termsAcceptedChanged(bool value) => setState(() => _termsAccepted = value);

  String phone;

  bool _loading = true;

  @override
  void initState() {
    super.initState();

   phone = "+"+widget.dialingCode+""+widget.phoneNumber;
   phoneController.text = phone;

    accountTypes.add(new AccountType("particulier", LangStrings.gs("particulier")));
    accountTypes.add(new AccountType("business", LangStrings.gs("business")));

    _currentAccountType = accountTypes[0];

    refreshView();

  }

  void refreshView(){

    setState(() {

      hasCompany = _currentAccountType.slug == "business";

    });
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: new AppBar(
        primary: true,
        centerTitle: false,
        title: new Text(LangStrings.gs("screen_registration_title"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
      ),
      body: new Stack(
        children: <Widget>[
          new SingleChildScrollView(
            child: new Container(
              padding: EdgeInsets.all(16.0),
              child: new Column(
                children: <Widget>[
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Text(LangStrings.gs("label_type_of_account"), textAlign: TextAlign.start, style: new TextStyle(fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                  new Container(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0, bottom: 5.0),
                    margin: EdgeInsets.only(top: 5.0),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(color: Colors.black54)
                    ),
                    child: new DropdownButton(
                        isExpanded:true,
                        value: _currentAccountType.slug,
                        items: accountTypes.map((AccountType accountType){
                          return new DropdownMenuItem<String>(
                              value:accountType.slug,
                              child: new Text(accountType.name)
                          );
                        }).toList(),
                        onChanged: ((String itemSelected){
                          setState((){
                            _currentAccountType = accountTypes.where((AccountType aT)=>aT.slug == itemSelected).first;
                            refreshView();
                          });
                        })
                    ),
                  ),
                  const SizedBox(height: 15.0,),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Text(LangStrings.gs("edit_phone_number"), textAlign: TextAlign.start, style: new TextStyle(fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                  const SizedBox(height: 10.0,),
                  new TextFormField(
                    controller: phoneController,
                    decoration: new InputDecoration(
                        contentPadding: EdgeInsets.all(20.0),
                        border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                        hintText: '01000000',
                        hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                    style: new TextStyle(color: Colors.black, fontSize: 18.0),
                    keyboardType: TextInputType.number,

                  ),
                  const SizedBox(height: 15.0,),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Text(LangStrings.gs("edit_first_name"), textAlign: TextAlign.start, style: new TextStyle(fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                  const SizedBox(height: 10.0,),
                  new TextFormField(
                    controller: firstNameController,
                    decoration: new InputDecoration(
                        contentPadding: EdgeInsets.all(20.0),
                        border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                        hintText: LangStrings.gs("edit_first_name"),
                        hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                    style: new TextStyle(color: Colors.black, fontSize: 18.0),
                    keyboardType: TextInputType.text,

                  ),
                  const SizedBox(height: 15.0,),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Text(LangStrings.gs("edit_last_name"), textAlign: TextAlign.start, style: new TextStyle(fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                  const SizedBox(height: 10.0,),
                  new TextFormField(
                    controller: lastNameController,
                    decoration: new InputDecoration(
                        contentPadding: EdgeInsets.all(20.0),
                        border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                        hintText: LangStrings.gs("edit_last_name"),
                        hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                    style: new TextStyle(color: Colors.black, fontSize: 18.0),
                    keyboardType: TextInputType.text,

                  ),

                  const SizedBox(height: 15.0,),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Text(LangStrings.gs("edit_email"), textAlign: TextAlign.start, style: new TextStyle(fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                  const SizedBox(height: 10.0,),
                  new TextFormField(
                    controller: emailController,
                    decoration: new InputDecoration(
                        contentPadding: EdgeInsets.all(20.0),
                        border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                        hintText: LangStrings.gs("edit_email"),
                        hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                    style: new TextStyle(color: Colors.black, fontSize: 18.0),
                    keyboardType: TextInputType.text,

                  ),
                  const SizedBox(height: 15.0,),
                  /*Offstage(
                    offstage: !hasCompany,
                    child: new Column(
                      children: <Widget>[
                        new Container(
                            alignment: Alignment.topLeft,
                            child: new Text(LangStrings.gs("label_edit_company_name"), textAlign: TextAlign.start, style: new TextStyle(fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                        const SizedBox(height: 10.0,),
                        new TextFormField(
                          controller: companyNameController,
                          decoration: new InputDecoration(
                              contentPadding: EdgeInsets.all(20.0),
                              border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                              hintText: LangStrings.gs("edit_company_name"),
                              hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                          style: new TextStyle(color: Colors.black, fontSize: 18.0),
                          keyboardType: TextInputType.text,

                        )
                      ],
                    ),
                  ),*/


                  new Container(
                    margin: EdgeInsets.only(top: 10),

                    height: 100.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Checkbox(
                            value: _termsAccepted,
                            onChanged: _termsAcceptedChanged
                        ),
                        new InkWell(
                          onTap: ()async{
                            //Go to Terms Web Site
                            var target_url= "https://google.ci/terms";
                            if (await canLaunch(target_url)) {
                              await launch(target_url);
                            } else {

                            }
                          },
                          child: new AutoSizeText(
                              "J'accepte les conditions d'utilisation et \nla politique de confidentialité.",
                              maxLines: 3,
                              maxFontSize: 20.0,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 12.0,
                              style: new TextStyle(fontSize: 14.0, color: AppColors.nopalmaBlue, decoration: TextDecoration.underline), 
                            )
                        )
                      ],
                    ),
                  ),

                  new Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 20.0),
                    height: 70.0,
                    child: RaisedButton(
                      color: AppColors.nopalmaYellow,
                      padding: const EdgeInsets.all(10.0),
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(5.0)
                      ),
                      onPressed:(){

                        String timeZone = DateTime.now().timeZoneName;

                        Map<String, String>  jsonBody = {
                          "dial_code": widget.dialingCode,
                          "country_code": widget.countryCode,
                          "timezone": timeZone,
                          "phone_number":widget.phoneNumber,
                          "first_name":firstNameController.text.trim(),
                          "last_name":lastNameController.text.trim(),
                          "account_type":_currentAccountType.slug
                        };

                        if(emailController.text.contains('@')){
                          jsonBody["email"] = emailController.text;
                        }
                        createCustomer(jsonBody);

                      },
                      child: new Padding(padding: EdgeInsets.all(10.0),
                        child: new Text(LangStrings.gs("button_save_title"), style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold, fontFamily: 'ArialRoundedMT'),),
                      ),

                    ),
                  ),
                  new InkWell(
                    onTap: (){
                      Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => new ConnexionScreen()),
                      );
                    },
                    child: new Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(top: 20.0,right: 10.0, left: 10.0),
                      child:  new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text(LangStrings.gs("Already_have_an_account"),style: new TextStyle(decoration: TextDecoration.underline, fontSize: 16.0),),
                          new SizedBox(width: 5.0,),
                          new Text(LangStrings.gs("login"),style: new TextStyle(
                              decoration: TextDecoration.underline,
                              color: Theme.of(context).primaryColor, fontSize: 16.0),)
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 50.0,),


                ],
              ),
            ),
          ),

          new Offstage(
            offstage: _loading,
            child: progressView(),
          )
        ],
      )
    );
  }

  void createCustomer(Map<String, String> jsonBody){


    UserRepository.submitRegistration(
        jsonBody,
        showProgress,
        dismissProgress
    ).then((RegistrationResult value){

      if(value.success == true){
        //Successful Operation
        //Save Data in Local Database
        LoginManager.signIn( value.data, (Registrationdata profile){
          
             Navigator.pushReplacement(context,
               MaterialPageRoute(builder: (context) => new DeliveryAddressScreen(profile:profile)),
             );
      

        }, (Exception e){
          print("Error found $e");
          new Dialogs().error(
              context,
              LangStrings.gs("dialog_error_title"),
              LangStrings.gs("dialog_error_message"),(){

            createCustomer(jsonBody);

          },(){

          });
        });
      }else{
        //Display Error
        new Dialogs().error(
            context,
            LangStrings.gs("dialog_error_title"),
            value.message,(){

          createCustomer(jsonBody);

        },(){});
      }


    }).catchError((error){
      //dismissProgress();
      print("Error found $error");
      new Dialogs().error(
          context,
          LangStrings.gs("dialog_error_title"),
          LangStrings.gs("dialog_error_message"),(){

        createCustomer(jsonBody);

      },(){});

    });

  }


  void showProgress() {
    setState(() {
      _loading = false;

    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }

  Widget progressView(){
    return new Stack(
      children: [
        new Container(
            color: Colors.white
        ),
        new Opacity(
          opacity: 0.3,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }


}


