import 'dart:io';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/UserRepository.dart';
import 'package:easy_services/rest/response/login/login_result.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/delivery_address_screen.dart';
import 'package:easy_services/ui/main/main_screen.dart';
import 'package:easy_services/ui/register/registration_screen.dart';
import 'package:easy_services/util/Logger.dart';
import 'package:easy_services/util/LoginManager.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class CodeVerificationScreen extends StatefulWidget {

  String dialingCode;
  String phoneNumber;
  String countryCode;

  CodeVerificationScreen(this.dialingCode, this.phoneNumber, this.countryCode);

  @override
  _CodeVerificationScreenState createState() => _CodeVerificationScreenState();
}

class _CodeVerificationScreenState extends State<CodeVerificationScreen> {
  static const String TAG = "AUTH";

  // Firebase
  final FirebaseAuth _auth = FirebaseAuth.instance;

  String phone;

  bool _loading = true;

  String verificationId;

  TextEditingController smsCodeController = TextEditingController();

  Future<Null> _verifyPhoneNumber() async {
    Logger.log(TAG, message: "Got phone number as: $phone");

    final PhoneCodeAutoRetrievalTimeout autoRetrieve = (String verId) {
      this.verificationId = verId;
    };

    final PhoneCodeSent smsCodeSent = (String verId, [int forceCodeResend]) {
      this.verificationId = verId;
    };

    final PhoneVerificationCompleted verifiedSuccess = (AuthCredential auth) {
      print('verified');
      loadProfile();
    };

    final PhoneVerificationFailed verifiedFailed = (error) {
      print('Error ' + error.message);
      new Dialogs().simpleError(
          context, LangStrings.gs("dialog_error_title"), error.message, () {});
    };

    showProgress();

    await _auth.verifyPhoneNumber(
        phoneNumber: phone,
        timeout: const Duration(seconds: 30),
        codeSent: smsCodeSent,
        codeAutoRetrievalTimeout: autoRetrieve,
        verificationCompleted: verifiedSuccess,
        verificationFailed: verifiedFailed);

    Logger.log(TAG, message: "Returning null from _verifyPhoneNumber");

    return null;
  }

  @override
  void initState() {
    super.initState();

    phone = "+" + widget.dialingCode + "" + widget.phoneNumber;

    _verifyPhoneNumber().then((Null) {
      dismissProgress();

      Logger.log(TAG, message: "then Code Sent");
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        primary: true,
        centerTitle: false,
        title: new Text(
          LangStrings.gs("screen_phone_verification_title"),
          style:
              new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),
        ),
      ),
      body: new Stack(
        children: <Widget>[
          new SingleChildScrollView(
            child: new Container(
              padding: EdgeInsets.all(20.0),
              child: new Column(
                children: <Widget>[
                  const SizedBox(
                    height: 50.0,
                  ),
                  new Image.asset(
                    'assets/nopalma/LOGO-NOPALMA-1.png',
                    height: 120,
                  ),
                  const SizedBox(
                    height: 70.0,
                  ),
                  Container(
                      alignment: Alignment.topLeft,
                      child: new Text(
                        LangStrings.gs("label_edit_code_verification"),
                        textAlign: TextAlign.start,
                        style: new TextStyle(
                            fontSize: 16.0, color: Colors.black54),
                      )),
                  const SizedBox(
                    height: 10.0,
                  ),
                  new TextFormField(
                    controller: smsCodeController,
                    decoration: new InputDecoration(
                        contentPadding: EdgeInsets.all(20.0),
                        border: new OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 2.0, color: Colors.black54),
                            borderRadius: BorderRadius.circular(5.0)),
                        hintText: '10000',
                        hintStyle: new TextStyle(
                            color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                    style: new TextStyle(color: Colors.black, fontSize: 18.0),
                    keyboardType: TextInputType.numberWithOptions(),
                  ),
                  Container(
                      height: 70.0,
                      width: double.infinity,
                      margin: EdgeInsets.only(top: 50.0),
                      child: RaisedButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                        color: AppColors.nopalmaYellow,
                        padding: const EdgeInsets.all(10.0),
                        onPressed: () {
                          login();
                        },
                        child: Text(
                          LangStrings.gs("button_next_title"),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'ArialRoundedMT'),
                        ),
                      )),
                  Container(
                    height: 70.0,
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 20.0),
                    child: RaisedButton(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                      color: AppColors.nopalmaBlue,
                      padding: const EdgeInsets.all(10.0),
                      onPressed: () {
                        _verifyPhoneNumber().then((Null) {
                          Logger.log(TAG, message: "Code Sent");
                        });
                      },
                      child: Text(
                        LangStrings.gs("button_retry_title"),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'ArialRoundedMT'),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          new Offstage(
            offstage: _loading,
            child: progressView(),
          )
        ],
      ),
    );
  }

  void login() {
    final AuthCredential credential = PhoneAuthProvider.credential(
        verificationId: this.verificationId,
        smsCode: smsCodeController.text.trim());

    _auth.signInWithCredential(credential).then((user) {
      Logger.log(TAG, message: "Code vérifié avec succès!");
      loadProfile();
    }).catchError((e) {
      print(e);
      new Dialogs().simpleError(context, LangStrings.gs("dialog_error_title"),
          LangStrings.gs("dialog_error_bad_code_message"), () {});
    });
  }

  void loadProfile() {
    String dialCode = widget.dialingCode.trim();
    String phoneNumber = widget.phoneNumber.trim();
    print("dialCode =====> $dialCode");
    print("phoneNumber =====> $phoneNumber");
    if (dialCode.isEmpty || phoneNumber.isEmpty) {
      new Dialogs().simpleError(context, LangStrings.gs("dialog_error_title"),
          LangStrings.gs("dialog_error_message"), () {
        // Navigator.pop(context);
      });
    } else {
      UserRepository.login(widget.dialingCode, widget.phoneNumber, showProgress,
              dismissProgress)
          .then((LoginResult value) {
        if (value is LoginResult) {
          if (value.success == true) {
            print('SAVING >>>> ${value.data}');

            LoginManager.signIn(value.data, (Registrationdata profile) {
              if (profile.delivery_addresses != null &&
                  profile.delivery_addresses.isNotEmpty) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => new MainScreen(profile: profile)),
                );
              } else {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          new DeliveryAddressScreen(profile: profile)),
                );
              }
            }, (error) {
              new Dialogs().simpleError(
                  context,
                  LangStrings.gs("dialog_error_title"),
                  LangStrings.gs("dialog_error_message"),
                  () {});
            });
          } else {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => new RegistrationScreen(
                      widget.dialingCode,
                      widget.phoneNumber,
                      widget.countryCode)),
            );
          }
        }

        if (value is Response) {
          print('Error found $value');
          new Dialogs().simpleError(
              context,
              LangStrings.gs("dialog_error_title"),
              LangStrings.gs("dialog_error_message"), () {
            // Navigator.pop(context);
          });
        }
      }).catchError((error) {
        dismissProgress();

        print("loadProfile() >>> catchError >>> $error");
        var message;
        if (error is HttpException || error is SocketException) {
          message = LangStrings.gs("internet_error_message");
        } else {
          message = LangStrings.gs("dialog_error_message");
        }

        new Dialogs().simpleError(
            context, LangStrings.gs("dialog_error_title"), message, () {
          // Navigator.pop(context);
        });
      });
    }
  }

  void showProgress() {
    if (mounted) {
      setState(() {
        _loading = false;
      });
    }
  }

  void dismissProgress() {
    if (mounted) {
      setState(() {
        _loading = true;
      });
    }
  }

  Widget progressView() {
    return new Stack(
      children: [
        new Container(color: Colors.white),
        new Opacity(
          opacity: 0.3,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }
}
