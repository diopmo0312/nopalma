import 'dart:async';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/login/code_verification_screen.dart';
import 'package:easy_services/ui/register/registration_screen.dart';
import 'package:easy_services/util/country_picker/flutter_country_picker.dart';
import 'package:flutter/material.dart';

import '../../rest/registration_data.dart';
import '../../util/LoginManager.dart';
import '../delivery_address_screen.dart';
import '../main/main_screen.dart';

class ConnexionScreen extends StatefulWidget {
  @override
  ConnexionScreenState createState() {
    return new ConnexionScreenState();
  }
}

class ConnexionScreenState extends State<ConnexionScreen> {
  Country _currentCountry;

  TextEditingController phoneNumberController = new TextEditingController();

  @override
  void initState() {
    _currentCountry = Country.SN;
    //_currentCountry = Country.CI;
    phoneNumberController.text = "";
    // phoneNumberController.text = "56888385";
    super.initState();

    Timer.run(() {
      LoginManager.connectedUser((Registrationdata profile) {
        if (profile.delivery_addresses != null &&
            profile.delivery_addresses.isNotEmpty) {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => new MainScreen(profile: profile)),
          );
        } else {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new DeliveryAddressScreen(profile: profile)),
          );
        }
      }, (error) {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        primary: true,
        centerTitle: false,
        title: new Text(
          "Nopalma",
          style:
              new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),
        ),
      ),
      body: new SingleChildScrollView(
        child: new Container(
          padding: EdgeInsets.all(18.0),
          child: Center(
            child: new Column(
              children: <Widget>[
                const SizedBox(
                  height: 50.0,
                ),
                new Image.asset(
                  'assets/nopalma/LOGO-NOPALMA-1.png',
                  height: 120,
                ),
                const SizedBox(
                  height: 70.0,
                ),
                CountryPicker(
                  onChanged: (Country country) {
                    setState(() {
                      _currentCountry = country;
                    });
                  },
                  selectedCountry: _currentCountry,
                ),
                const SizedBox(
                  height: 20.0,
                ),
                Container(
                    alignment: Alignment.topLeft,
                    child: new Text(
                      LangStrings.gs("edit_phone_number"),
                      textAlign: TextAlign.start,
                      style: new TextStyle(
                          fontSize: 16.0,
                          color: Colors.grey,
                          fontFamily: 'ArialRoundedMT'),
                    )),
                const SizedBox(
                  height: 10.0,
                ),
                new TextFormField(
                  controller: phoneNumberController,
                  decoration: new InputDecoration(
                      contentPadding: EdgeInsets.all(20.0),
                      border: new OutlineInputBorder(
                          borderSide:
                              BorderSide(width: 2.0, color: Colors.black54),
                          borderRadius: BorderRadius.circular(5.0)),
                      hintText: '0100000000',
                      hintStyle: new TextStyle(
                          color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                  style: new TextStyle(color: Colors.black, fontSize: 18.0),
                  keyboardType: TextInputType.number,
                ),
                new Container(
                  height: 70.0,
                  width: double.infinity,
                  margin: EdgeInsets.only(top: 20.0),
                  child: RaisedButton(
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                    color: AppColors.nopalmaYellow,
                    padding: const EdgeInsets.all(10.0),
                    onPressed: () {
                      if (phoneNumberController.text.trim().isNotEmpty) {
                        String phoneNumber  = phoneNumberController.text.trim();
                        phoneNumber = phoneNumber.split(' ').join('');
                        phoneNumber = phoneNumber.replaceAll(' ', '');
                        phoneNumber = phoneNumber.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
                        phoneNumber = phoneNumber.replaceAll(new RegExp(r"\s+"), "");

                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => new CodeVerificationScreen(
                                  _currentCountry.dialingCode,
                                  phoneNumber,
                                  _currentCountry.isoCode)),
                        );
                      } else {
                        new Dialogs().simpleError(
                            context,
                            LangStrings.gs("dialog_error_title"),
                            LangStrings.gs("dialog_error_message_bad_phone"),
                            () {});
                      }
                    },
                    child: Text(
                      LangStrings.gs("button_next_title"),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'ArialRoundedMT'),
                    ),
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
