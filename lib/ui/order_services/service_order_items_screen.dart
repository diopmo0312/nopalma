import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/ShowOrderItemWidget.dart';
import 'package:easy_services/models/ServicePricing.dart';
import 'package:easy_services/models/cart.dart';
import 'package:easy_services/models/cart_item.dart';
import 'package:easy_services/models/linge_kilo_option.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/request/order_item_request.dart';
import 'package:easy_services/ui/cart/cart_screen.dart';
import 'package:easy_services/util/CartManager.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';

class ServiceOrderItemsScreen extends StatefulWidget {
  final Registrationdata profile;

  final Service service;

  final List<OrderItemRequest> items;

  final CartItem cartItem;

  ServiceOrderItemsScreen(
      this.profile, this.service, this.items, this.cartItem);

  @override
  _ServiceOrderItemsScreenState createState() =>
      _ServiceOrderItemsScreenState();
}

class _ServiceOrderItemsScreenState extends State<ServiceOrderItemsScreen> {
  double tax = 0.0;

  double total = 1500.0;
  double feesDelivery = 1500.0;
  double subTotal = 1500.0;

  String currency = "XOF";

  List<OrderItemRequest> _items;

  Map<String, int> selection;

  Cart _cart;
  CartItem _cartItem;

  List<ServicePricing> servicePricingList;

  ProgressDialog pr;

  @override
  void initState() {
    if (widget.service.slug == "pressing") {
      servicePricingList =
          widget.profile == null ? [] : widget.profile.pressing_clothes;
    } else if (widget.service.slug == "retouche") {
      servicePricingList =
          widget.profile == null ? [] : widget.profile.retouchings;
    } else if (widget.service.slug == "linge_au_kilo") {
      List<LingeKiloOption> options = [];
      /* options.add(new LingeKiloOption("lave-seche-plie", "Lavé - Séché - Plié",
          "Washed - Dried - Folded", "1000", "XOF"));
      options.add(new LingeKiloOption("lave-seche-repasse",
          "Lavé - Séché - Repassé", "Washed - Dried - Ironed", "1500", "XOF")); */

      options = widget.profile.linge_kilo_options;
      servicePricingList = options;
    } else if (widget.service.slug == "repassage") {
      List<Service> services = [];
      services.add(widget.service);
      servicePricingList = services;
    } else if (widget.service.slug == "coordonnerie") {
      servicePricingList =
          widget.profile == null ? [] : widget.profile.shoe_repairings;
    } else {}

    _items = widget.items;

    if (widget.cartItem != null) {
      _cartItem = widget.cartItem;
    }

    pr = new ProgressDialog(context);

    pr.style(message: 'Veuillez patientez...');

    loadCart();

    super.initState();
  }

  void showProgressDialog() {
    pr.show();
  }

  void dismissProgressDialog() {
    pr.hide();
  }

  void loadCart() {
    CartManager.obtainCart((cart) {
      setState(() {
        _cart = cart;
        initSelection();

        calculateInvoice();
      });
    });
  }

  void initSelection() {
    selection = new Map<String, int>();

    if (_cartItem != null) {
      if (_cartItem.items != null && _cartItem.items.isNotEmpty) {
        _cartItem.items.forEach((o) {
          selection[o.servicePricing().slugValue()] = o.quantity;
        });
      } else {
        selection = new Map<String, int>();
      }
    } else {
      selection = new Map<String, int>();
    }
  }

  void calculateInvoice() {
    subTotal = 0.0;
    _items.forEach((OrderItemRequest o) {
      ServicePricing servicePricing = o.servicePricing();
      int unitPrice = int.parse(servicePricing.priceValue());
      int itemSubTotal = unitPrice * o.quantity;
      subTotal += itemSubTotal;
    });

    tax = subTotal * 0.18;
    total = feesDelivery + tax + subTotal;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        primary: true,
        title: new Text(
          LangStrings.gs("screen_invoice_preview_title"),
          style:
              new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),
        ),
      ),
      body: new Container(
        height: MediaQuery.of(context).size.height - 80.0,
        child: new Column(
          children: <Widget>[
            new Container(
              height: MediaQuery.of(context).size.height - 400.0,
              color: AppColors.getColorHexFromStr("#f2f9fd"),
              child: new ListView(
                children: widget.items.map((item) {
                  return new ShowOrderItemWidget(
                      widget.profile, widget.service, item, () {
                    _showQuantityDialog(item.pressingClothes().slug,
                        item.pressingClothes().name, item.quantity);
                  }, (OrderItemRequest orderItemRequest) {
                    //On Delete
                  });
                }).toList(),
              ),
            ),
            new Container(
              padding: EdgeInsets.only(
                  bottom: 15.0, right: 15.0, left: 15.0, top: 15.0),
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.black12))),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text(
                    LangStrings.gs("field_subtitle_title"),
                    style: new TextStyle(
                        color: Colors.black,
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold),
                  ),
                  new Text(
                    '$subTotal $currency',
                    style: new TextStyle(
                        color: AppColors.nopalmaYellow,
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20.0,
            )
          ],
        ),
      ),
      bottomSheet: new Container(
        padding: EdgeInsets.all(16.0),
        child: new Container(
          width: double.infinity,
          height: 80.0,
          padding: EdgeInsets.all(10.0),
          child: new RaisedButton(
              textColor: Colors.white,
              color: AppColors.nopalmaYellow,
              padding: EdgeInsets.all(10.0),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(5.0)),
              child: new Text(
                LangStrings.gs("button_save_title"),
                style: new TextStyle(
                    fontSize: 18.0,
                    color: Colors.white,
                    fontFamily: 'ArialRoundedMT',
                    fontWeight: FontWeight.bold),
              ),
              onPressed: widget.service.slug == "linge_au_kilo" ? () {
                new Dialogs().confirm(
                          context,
                          'Confirmation de quantité',
                          "Vous etes sur le point de passer une commande d'au moins 1 KG que vous pourrez modifier après.",
                          () {},
                          save);
                // save();
              } : save
          ),
        ),
      ),
    );
  }

  void updateCartItem() async {
    _items.clear();

    subTotal = 0.0;

    selection.forEach((String indexKey, int indexValue) {
      ServicePricing servicePricing =
          servicePricingList.firstWhere((pc) => pc.slugValue() == indexKey);
      int unitPrice = int.parse(servicePricing.priceValue());
      int itemSubTotal = unitPrice * indexValue;
      subTotal += itemSubTotal;
      _items.add(new OrderItemRequest(
          widget.service.slugValue(),
          servicePricing.serviceSlugValue(),
          servicePricing.toJsonEncoded(),
          indexValue,
          servicePricing.quantityUnitValue(),
          servicePricing.priceValue(),
          servicePricing.currencyValue()));
    });

    _cartItem = new CartItem(
        widget.service.slug, widget.service.name, _items, "$subTotal", "XOF");

    //Save in the Cart
    _cart = await CartManager.addToCart(_cartItem);
  }

  _showQuantityDialog(String serviceSlug, String title, int q) async {
    TextEditingController _controller = new TextEditingController(text: '$q');
    await showDialog<String>(
      context: context,
      builder: (BuildContext ctx) => new Center(
        child: new SingleChildScrollView(
          child: new _SystemPadding(
            child: new AlertDialog(
              contentPadding: const EdgeInsets.all(16.0),
              title: new Text(title),
              content: new Row(
                children: <Widget>[
                  new Expanded(
                    child: new TextField(
                      controller: _controller,
                      autofocus: false,
                      decoration: new InputDecoration(
                          labelText: 'Quantité', hintText: '0'),
                    ),
                  )
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                    child: const Text('ANNULER'),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                new FlatButton(
                    child: const Text('MODIFIER'),
                    onPressed: () {
                      setState(() {
                        selection[serviceSlug] =
                            int.parse(_controller.text.trim());
                        updateCartItem();
                        calculateInvoice();
                      });
                      Navigator.pop(context);
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }

  void save() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => new CartScreen(widget.profile)));
  }
}

class _SystemPadding extends StatelessWidget {
  final Widget child;

  _SystemPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return new AnimatedContainer(
        padding: mediaQuery.viewInsets,
        duration: const Duration(milliseconds: 300),
        child: child);
  }
}
