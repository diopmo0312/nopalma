import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/ShoeRepairingWidget.dart';
import 'package:easy_services/generic_ui/type_widget.dart';
import 'package:easy_services/models/cart.dart';
import 'package:easy_services/models/cart_item.dart';
import 'package:easy_services/models/pressing_category.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/models/shoe_repairing.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/ShoeRepairingRepository.dart';
import 'package:easy_services/rest/request/order_item_request.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/order_services/service_order_items_screen.dart';
import 'package:easy_services/util/CartManager.dart';
import 'package:flutter/material.dart';

class CreateShoeRepairingScreen extends StatefulWidget {
  final Registrationdata profile;

  final Service service;

  CreateShoeRepairingScreen(this.profile, this.service);

  @override
  _CreateShoeRepairingScreenState createState() =>
      _CreateShoeRepairingScreenState();
}

class _CreateShoeRepairingScreenState extends State<CreateShoeRepairingScreen> {
  bool _loading = true;
  List<PressingCategory> _pressingCategories;
  Tween _currentPressingCategory;

  bool hasSelection;
  Registrationdata _profile;

  Map<String, int> selectedShoeRepairing;
  List<ShoeRepairing> _shoeRepairingList;
  List<ShoeRepairing> allShoeRepairingList;

  Cart _cart;
  CartItem _cartItem;
  List<OrderItemRequest> items = [];
  double subTotal = 0.0;

  List<Widget> _pricingViews = [];

  @override
  void initState() {
    super.initState();

    _profile = widget.profile;
    _pressingCategories = [
      new PressingCategory(1, "Homme", "Man", "homme"),
      new PressingCategory(2, "Femme", "Woman", "femme")
    ];
    _currentPressingCategory = new Tween(
        begin: _pressingCategories[0].slug, end: _pressingCategories[0].name);
    _shoeRepairingList = widget.profile.shoe_repairings;
    allShoeRepairingList = widget.profile.shoe_repairings;
            print("widget.profile.shoe_repairings ===> ${widget.profile.shoe_repairings}");

    selectedShoeRepairing = new Map<String, int>();

    loadCart();

    if (allShoeRepairingList != null) {
      if (allShoeRepairingList.isEmpty) {
        loadShoeRepairingList();
      }
      // filterShoeRepairingBy(_currentPressingCategory.begin);
    } else {
      loadShoeRepairingList();
    }
  }

  void loadCart() {
    CartManager.obtainCart((cart) {
      setState(() {
        _cart = cart;

        var cartItemList = _cart.cartItems;
        if (cartItemList != null && cartItemList.isNotEmpty) {
          var cartItemServicesList =
              cartItemList.map((c) => c.service_slug).toList();
          bool yesContains =
              cartItemServicesList.contains(widget.service.slugValue());

          if (yesContains) {
            _cartItem = cartItemList.firstWhere(
                (c) => c.service_slug == widget.service.slugValue(),
                orElse: () => null);
            if (_cartItem == null) {
              _cartItem = new CartItem(widget.service.slugValue(),
                  widget.service.nameValue(), [], "0.0", "XOF");
            }
          } else {}
        } else {
          _cartItem = new CartItem(widget.service.slugValue(),
              widget.service.nameValue(), [], "0.0", "XOF");
        }
        initSelection();
      });
    });
  }

  void initSelection() {
    selectedShoeRepairing = new Map<String, int>();

    if (_cartItem != null) {
      if (_cartItem.items != null && _cartItem.items.isNotEmpty) {
        _cartItem.items.forEach((o) {
          selectedShoeRepairing[o.shoeRepairing().slugValue()] = o.quantity;
        });
      } else {
        selectedShoeRepairing = new Map<String, int>();
      }
    } else {
      selectedShoeRepairing = new Map<String, int>();
    }

    refreshUI();
  }

  Future<Cart> updateCartItem() async {
    items.clear();
    subTotal = 0.0;

    selectedShoeRepairing.forEach((String indexKey, int indexValue) {
      ShoeRepairing shoeRepairing =
          allShoeRepairingList.firstWhere((pc) => pc.slugValue() == indexKey);
      int pressingClothesPrice = int.parse(shoeRepairing.price);
      int itemSubTotal = pressingClothesPrice * indexValue;
      subTotal += itemSubTotal;
      items.add(new OrderItemRequest(
          widget.service.slugValue(),
          shoeRepairing.slugValue(),
          jsonEncode(shoeRepairing),
          indexValue,
          shoeRepairing.quantityUnitValue(),
          shoeRepairing.priceValue(),
          shoeRepairing.currencyValue()));
    });

    _cartItem = new CartItem(widget.service.slugValue(),
        widget.service.nameValue(), items, "$subTotal", "XOF");

    //Save in the Cart
    return await CartManager.addToCart(_cartItem);
  }

  void refreshUI() {
    var mPricingViews = _shoeRepairingList.map((sr) {
      return new ShoeRepairingWidget(
          shoeRepairing: sr,
          isSelected: selectedShoeRepairing.containsKey(sr.slugValue()),
          initialQuantity: getQuantity(selectedShoeRepairing, sr),
          onQuantityChanged: (ShoeRepairing sr, int q) {
            if (q == 0) {
              setState(() {
                selectedShoeRepairing.remove(sr.slugValue());
                refreshUI();
              });
            } else {
              setState(() {
                selectedShoeRepairing[sr.slugValue()] = q;
                refreshUI();
              });
            }
          });
    }).toList();

    if (mounted) {
      setState(() {
        _pricingViews = mPricingViews;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    hasSelection = selectedShoeRepairing.isNotEmpty;
    if (_shoeRepairingList == null) {
      _shoeRepairingList = [];
    }

    return new Scaffold(
        appBar: new AppBar(
          primary: true,
          title: new Text(
            widget.service.nameValue(),
            style: new TextStyle(
                color: Colors.white, fontFamily: 'ArialRoundedMT'),
          ),
        ),
        body: new Stack(
          children: <Widget>[
            new SingleChildScrollView(
              child: new Container(
                padding: EdgeInsets.all(10.0),
                child: new Column(
                  children: <Widget>[
                    new Container(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            LangStrings.gs("screen_types_repairing_title"),
                            style: new TextStyle(
                                fontSize: 18.0,
                                color: Colors.black38,
                                fontFamily: 'ArialRoundedMT'),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          // new TypeWidget(
                          //   value: _currentPressingCategory,
                          //   items: _pressingCategories
                          //       .map((pressingCategory) => new Tween(
                          //           begin: pressingCategory.slug,
                          //           end: pressingCategory.name))
                          //       .toList(),
                          //   onChanged: (Tween selected) {
                          //     setState(() {
                          //       // print("selected ===> ${[selected.begin]}");
                          //       // print(
                          //       //     "Cordonnerie ===> ${widget.profile.shoe_repairings}");
                          //       _currentPressingCategory = selected;
                          //       print("selected ===> ${_currentPressingCategory.begin}");
                          //       filterShoeRepairingBy(
                          //           _currentPressingCategory.begin);
                          //     });
                          //   },
                          // ),
                          new Container(
                              padding: EdgeInsets.only(top: 20.0),
                              height:
                                  MediaQuery.of(context).size.height - 260.0,
                              child: new ListView(
                                children: _pricingViews,
                              )),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            new Offstage(
              offstage: _loading,
              child: progressView(),
            )
          ],
        ),
        bottomSheet: new Container(
          padding: EdgeInsets.all(18.0),
          child: new Container(
            width: double.infinity,
            height: 70.0,
            padding: EdgeInsets.all(10.0),
            child: new RaisedButton(
                textColor: Colors.white,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(5.0)),
                color: hasSelection ? AppColors.nopalmaYellow : Colors.grey,
                padding: EdgeInsets.all(10.0),
                child: new Text(
                  LangStrings.gs("button_next_title"),
                  style: new TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                      fontFamily: 'ArialRoundedMT',
                      fontWeight: FontWeight.bold),
                ),
                onPressed: hasSelection
                    ? () {
                        goNext();
                      }
                    : null),
          ),
        ));
  }

  int getQuantity(Map map, ShoeRepairing sr) {
    if (selectedShoeRepairing.containsKey(sr.slugValue())) {
      return selectedShoeRepairing["${sr.slugValue()}"];
    } else {
      return 0;
    }
  }

  Widget progressView() {
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  void showProgress() {
    setState(() {
      _loading = false;
    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }

  void filterShoeRepairingBy(String slug) {
            // print("Slug =>>>> $slug");
            // print("allShoeRepairingList =>>>> $allShoeRepairingList");
    if (allShoeRepairingList != null) {
      setState(() {
        _shoeRepairingList = allShoeRepairingList.where((p) {
          if (p.category_slug != null) {
            List<dynamic> categories = jsonDecode(p.category_slug);
            List<String> categoriesStrings = categories.map((c) {
            print("c =>>>> ${categories}");
              String categoryString = c.toString();
              if (categoryString != null) {
                // print("categoryString ===> $categoryString");
                return categoryString;
              } else {
                return "";
              }
            }).toList();
            // print("Categories Slug =>>>> $categories");
            // print("Categories SlugSSSSSS =>>>> $categoriesStrings");
            if (categoriesStrings != null && categoriesStrings.isNotEmpty) {
              return categoriesStrings.contains(slug);
            } else {
              return false;
            }
          } else {
            return false;
          }
        }).toList();
      });
    }
  }

  void loadShoeRepairingList() {
    ShoeRepairingRepository.findAndStore(showProgress, dismissProgress,
        (Registrationdata profile) {
      if (profile != null) {
        setState(() {
          _profile = profile;

          allShoeRepairingList = profile == null ? [] : profile.shoe_repairings;

          if (allShoeRepairingList != null) {
            filterShoeRepairingBy(_currentPressingCategory.begin);
          }
        });
      } else {
        print("Error Profile is Null");
      }
    }, (error) {
      print("Error found $error");
    });
  }

  void goNext() {
    updateCartItem().then((value) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => new ServiceOrderItemsScreen(
                  _profile, widget.service, items, _cartItem)));
    });
  }
}
