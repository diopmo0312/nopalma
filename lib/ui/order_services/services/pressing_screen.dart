import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/ClothesWidget.dart';
import 'package:easy_services/generic_ui/type_widget.dart';
import 'package:easy_services/models/cart.dart';
import 'package:easy_services/models/cart_item.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/PressingClothesRepository.dart';
import 'package:easy_services/rest/request/order_item_request.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/models/pressing_category.dart';
import 'package:easy_services/models/pressing_clothes.dart';
import 'package:easy_services/ui/order_services/service_order_items_screen.dart';
import 'package:easy_services/util/CartManager.dart';
import 'package:flutter/material.dart';

class PressingScreen extends StatefulWidget {
  final Registrationdata profile;

  final Service service;

  PressingScreen(this.profile, this.service);

  @override
  _PressingScreenState createState() => _PressingScreenState();
}

class _PressingScreenState extends State<PressingScreen> {
  List<PressingCategory> _pressingCategories;

  List<PressingClothes> _pressingClothes;
  List<PressingClothes> allPressingClothes;
  Map<String, int> selectedClothes;
  Tween _currentPressingCategory;

  bool hasSelection;

  bool _loading = true;
  Registrationdata _profile;

  Cart _cart;
  CartItem _cartItem;
  List<OrderItemRequest> items = [];
  double subTotal = 0.0;

  List<Widget> _pricingViews = [];

  @override
  void initState() {
    super.initState();

    loadCart();

    _profile = widget.profile;
    _pressingCategories = widget.profile == null
        ? [
            new PressingCategory(1, "Homme", "Man", "homme"),
            new PressingCategory(2, "Femme", "Woman", "femme"),
            new PressingCategory(3, "Enfant", "Kid", "enfant"),
            new PressingCategory(4, "Autre", "Other", "autres"),
          ]
        : widget.profile.pressing_categories;
    _currentPressingCategory = new Tween(
        begin: _pressingCategories[0].slug,
        end: LangStrings.strLang == 'FR'
            ? _pressingCategories[0].name
            : _pressingCategories[0].name_en);
    allPressingClothes = _profile == null ? [] : _profile.pressing_clothes;

    if (allPressingClothes != null) {
      if (allPressingClothes.isEmpty) {
        loadPressingClothes();
      }

      // filterPressingClothesBy(_currentPressingCategory.begin);c
    } else {
      loadPressingClothes();
    }
  }

  void loadCart() {
    CartManager.obtainCart((cart) {
      setState(() {
        _cart = cart;

        var cartItemList = _cart.cartItems;
        if (cartItemList != null && cartItemList.isNotEmpty) {
          var cartItemServicesList =
              cartItemList.map((c) => c.service_slug).toList();

          bool yesContains =
              cartItemServicesList.contains(widget.service.slugValue());

          if (yesContains) {
            _cartItem = cartItemList.firstWhere(
                (c) => c.service_slug == widget.service.slugValue(),
                orElse: () => null);
            if (_cartItem == null) {
              _cartItem = new CartItem(widget.service.slugValue(),
                  widget.service.nameValue(), [], "0.0", "XOF");
            }
          } else {}
        } else {
          _cartItem = new CartItem(widget.service.slugValue(),
              widget.service.nameValue(), [], "0.0", "XOF");
        }
        initSelection();
      });
    });
  }

  void initSelection() {
    selectedClothes = new Map<String, int>();

    if (_cartItem != null) {
      if (_cartItem.items != null && _cartItem.items.isNotEmpty) {
        _cartItem.items.forEach((o) {
          selectedClothes[o.pressingClothes().slugValue()] = o.quantity;
        });
      }
    }
    refreshUI();
  }

  Future<Cart> updateCartItem() async {
    items.clear();
    subTotal = 0.0;

    selectedClothes.forEach((String indexKey, int indexValue) {
      PressingClothes pressingClothes =
          allPressingClothes.firstWhere((pc) => pc.slugValue() == indexKey);
      int pressingClothesPrice = int.parse(pressingClothes.priceValue());
      int itemSubTotal = pressingClothesPrice * indexValue;
      subTotal += itemSubTotal;
      items.add(new OrderItemRequest(
          widget.service.slugValue(),
          pressingClothes.slugValue(),
          jsonEncode(pressingClothes),
          indexValue,
          pressingClothes.quantityUnitValue(),
          pressingClothes.priceValue(),
          pressingClothes.currencyValue()));
    });

    _cartItem = new CartItem(widget.service.slug, widget.service.nameValue(),
        items, "$subTotal", "XOF");

    //Save in the Cart
    return await CartManager.addToCart(_cartItem);
  }

  void refreshUI() {
    print('PRESSING VETEMENT LENGTH ===> ${allPressingClothes.length}');
    var mPricingViews = allPressingClothes.map((p) {
      return new ClothesWidget(
          initialQuantity: selectedClothes.keys.contains(p.slugValue())
              ? selectedClothes["${p.slugValue()}"]
              : 0,
          pressingClothes: p,
          isSelected: selectedClothes.keys.contains(p.slugValue()),
          onQuantityChanged: (PressingClothes pc, int q) {
            if (q == 0) {
              setState(() {
                if (selectedClothes != null) {
                  selectedClothes.remove(pc.slugValue());
                  refreshUI();
                }
              });
            } else {
              setState(() {
                if (selectedClothes != null) {
                  selectedClothes[pc.slugValue()] = q;
                  refreshUI();
                }
              });
            }
          });
    }).toList();

    if (mounted) {
      setState(() {
        _pricingViews = mPricingViews;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (selectedClothes == null) {
      selectedClothes = new Map();
    }

    hasSelection = selectedClothes.isNotEmpty;
    if (_pressingClothes == null) {
      _pressingClothes = [];
    }

    print("Lang ${LangStrings.strLang}");

    return new Scaffold(
        appBar: new AppBar(
          primary: true,
          title: new Text(
            widget.service.nameValue(),
            style: new TextStyle(
                color: Colors.white, fontFamily: 'ArialRoundedMT'),
          ),
        ),
        body: new Stack(
          children: <Widget>[
            new SingleChildScrollView(
              child: new Container(
                padding: EdgeInsets.all(10.0),
                child: new Column(
                  children: <Widget>[
                    new Container(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            LangStrings.gs("field_select_clothes_label"),
                            style: new TextStyle(
                                fontSize: 18.0,
                                color: Colors.black38,
                                fontFamily: 'ArialRoundedMT'),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          /* new TypeWidget(
                          value: _currentPressingCategory,
                          items: _pressingCategories.map((pressingCategory)=>new Tween(begin:pressingCategory.slug, end:LangStrings.strLang=='FR'?pressingCategory.name:pressingCategory.name_en)).toList(),
                          onChanged: (Tween selected){
                            setState(() {
                              print("Categorie ${selected.begin}");
                              _currentPressingCategory = selected;
                                filterPressingClothesBy(_currentPressingCategory.begin);
                            });
                          },
                        ), */

                          new Container(
                              padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                              height:
                                  MediaQuery.of(context).size.height - 260.0,
                              child: new GridView.count(
                                crossAxisCount: 2,
                                children: _pricingViews,
                              )),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            new Offstage(
              offstage: _loading,
              child: progressView(),
            )
          ],
        ),
        bottomSheet: new Container(
          padding: EdgeInsets.all(10.0),
          child: new Container(
            width: double.infinity,
            height: 80.0,
            padding: EdgeInsets.all(10.0),
            child: new RaisedButton(
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(5.0)),
                textColor: Colors.white,
                color: hasSelection ? AppColors.nopalmaYellow : Colors.grey,
                padding: EdgeInsets.all(10.0),
                child: new Text(
                  LangStrings.gs("button_next_title"),
                  style: new TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                      fontFamily: 'ArialRoundedMT',
                      fontWeight: FontWeight.bold),
                ),
                onPressed: hasSelection
                    ? () {
                        goNext();
                      }
                    : null),
          ),
        ));
  }

  void goNext() {
    updateCartItem().then((value) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => new ServiceOrderItemsScreen(
                  _profile, widget.service, items, _cartItem)));
    });
  }

  void loadPressingClothes() {
    PressingClothesRepository.findAndStore(showProgress, dismissProgress,
        (Registrationdata profile) {
      if (profile != null) {
        setState(() {
          _profile = profile;

          allPressingClothes = profile == null ? [] : profile.pressing_clothes;

          if (allPressingClothes != null) {
            // filterPressingClothesBy(_currentPressingCategory.begin);
          }
        });
      } else {
        print("Error Profile is Null");
      }
    }, (error) {
      print("Error found $error");
    });
  }

  Widget progressView() {
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  void showProgress() {
    setState(() {
      _loading = false;
    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }

  /* void filterPressingClothesBy(String slug) {
    if (allPressingClothes != null) {
      setState(() {
        _pressingClothes = allPressingClothes.where((p) {
          if (p.category_slug != null) {
            List<dynamic> categories = jsonDecode(p.category_slug);
            List<String> categoriesStrings = categories.map((c) {
              String categoryString = c.toString();
              if (categoryString != null) {
                return categoryString;
              } else {
                return "";
              }
            }).toList();
            //print("Categories Slug =>>>> $categories");
            //print("Categories SlugSSSSSS =>>>> $categoriesStrings");
            if (categoriesStrings != null && categoriesStrings.isNotEmpty) {
              return categoriesStrings.contains(slug);
            } else {
              return false;
            }
          } else {
            return false;
          }
        }).toList();
      });
    }
  } */
}
