import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/RetouchingWidget.dart';
import 'package:easy_services/models/cart.dart';
import 'package:easy_services/models/cart_item.dart';
import 'package:easy_services/models/retouching.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/RetouchingRepository.dart';
import 'package:easy_services/rest/request/order_item_request.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/order_services/service_order_items_screen.dart';
import 'package:easy_services/util/CartManager.dart';
import 'package:flutter/material.dart';

class CreateRetouchingScreen extends StatefulWidget {

  final Registrationdata profile;

  final Service service;

  CreateRetouchingScreen(this.profile, this.service);

  @override
  _CreateRetouchingScreenState createState() => _CreateRetouchingScreenState();
}

class _CreateRetouchingScreenState extends State<CreateRetouchingScreen> {

  bool _loading = true;


  bool hasSelection= false;
  Registrationdata _profile;

  Map<String, int> selectedRetouchings ;
  List<Retouching> _retouchingList;
  List<Retouching> allRetouchingList;

  Cart _cart;
  CartItem _cartItem;
  List<OrderItemRequest> items = [];
  double subTotal = 0.0;

  List<Widget> _pricingViews = [];

  @override
  void initState() {
    super.initState();

    _profile = widget.profile;

    _retouchingList = widget.profile.retouchings;

    selectedRetouchings = new Map<String, int>();

    loadCart();

    if(allRetouchingList != null){

      if(allRetouchingList.isEmpty){
        loadRetouchingList();
      }


    }else{
      loadRetouchingList();
    }

  }

  void loadCart(){
    CartManager.obtainCart((cart){
      setState(() {

        _cart = cart;

        var cartItemList = _cart.cartItems;
        if(cartItemList != null && cartItemList.isNotEmpty){
          var cartItemServicesList = cartItemList.map((c)=>c.service_slug).toList();


          bool yesContains = cartItemServicesList.contains(widget.service.slugValue());

          if(yesContains){
            _cartItem = cartItemList.firstWhere((c)=>c.service_slug == widget.service.slugValue(), orElse: ()=>null);
            if(_cartItem == null){
              _cartItem = new CartItem(widget.service.slugValue(), widget.service.nameValue(), [], "0.0", "XOF");
            }
          }else{

          }
        }else{
          _cartItem = new CartItem(widget.service.slugValue(), widget.service.nameValue(), [], "0.0", "XOF");
        }
        initSelection();

      });
    
    });
    


  }

  void initSelection(){

    selectedRetouchings = new Map<String, int>();

    if(_cartItem != null){

      if(_cartItem.items != null && _cartItem.items.isNotEmpty){

        _cartItem.items.forEach((o){
          selectedRetouchings[o.pressingClothes().slugValue()] = o.quantity;
        });

      }else{
        selectedRetouchings = new Map<String, int>();
      }
    }else{
      selectedRetouchings = new Map<String, int>();
    }

    refreshUI();
  }

  Future<Cart> updateCartItem()async{

    items.clear();
    subTotal = 0.0;

    selectedRetouchings.forEach((String indexKey, int indexValue){
      Retouching  retouching = _retouchingList.firstWhere((pc)=>pc.slug == indexKey);
      int pressingClothesPrice = int.parse(retouching.price);
      int itemSubTotal= pressingClothesPrice *indexValue;
      subTotal += itemSubTotal;
      items.add(new OrderItemRequest(
          widget.service.slugValue(),
          retouching.slugValue(),
          jsonEncode(retouching),
          indexValue,
          retouching.quantityUnitValue(),
          retouching.price,
          retouching.currency));
    });

    _cartItem = new CartItem(widget.service.slug, widget.service.nameValue(), items, "$subTotal", "XOF");

    //Save in the Cart
    return await CartManager.addToCart(_cartItem);

  }

  void  refreshUI(){

    var mPricingViews =_retouchingList.map((r){
      return new RetouchingWidget(
          retouching:r,
          isSelected:selectedRetouchings.containsKey(r.slug),
          initialQuantity: selectedRetouchings.containsKey(r.slug)?selectedRetouchings[r.slug]:0,
          onQuantityChanged:(Retouching r,int q){
            if(q == 0){
              setState(() {
                selectedRetouchings.remove(r.slug);
                refreshUI();
              });
            }else{
              setState(() {
                selectedRetouchings[r.slug] = q;
                refreshUI();
              });
            }

          }
      );}
    ).toList();

    if(mounted){
      setState(() {
        _pricingViews = mPricingViews;
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    hasSelection = selectedRetouchings.isNotEmpty;
    if(_retouchingList == null){
      _retouchingList = [];
    }

    return new Scaffold(
        appBar:  new AppBar(
        primary: true,
        title: new Text(widget.service.nameValue(), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
        ),
        body: new Stack(
          children: <Widget>[
            new SingleChildScrollView(
              child: new Container(
                padding: EdgeInsets.all(10.0),
                child: new Column(
                  children: <Widget>[
                    new Container(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(LangStrings.gs("screen_types_retouching_title"), style: new TextStyle(fontSize:18.0,color: Colors.black38, fontFamily: 'ArialRoundedMT'),),
                          SizedBox(height: 10.0,),
                          
                          new Container(
                              padding: EdgeInsets.only(top: 20.0),
                              height: MediaQuery.of(context).size.height - 260.0,
                              child:new ListView(
                                children: _pricingViews,
                              )
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            new Offstage(
              offstage: _loading,
              child: progressView(),
            )
          ],
        ),
        bottomSheet: new Container(
          padding:EdgeInsets.all( 18.0),
          child: new Container(
            width: double.infinity,
            height: 80.0,
            padding: EdgeInsets.all(10.0),
            child: new RaisedButton(
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(5.0)
                ),
                textColor: Colors.white,
                color: hasSelection?AppColors.nopalmaYellow:Colors.grey,
                padding: EdgeInsets.all(10.0),
                child: new Text(LangStrings.gs("button_next_title"), style: new TextStyle(fontSize:18.0,color: Colors.white, fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold), ),
                onPressed: hasSelection?(){
                  goNext();
                }:null),
          ),
        )
    );
  }

  Widget progressView(){
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  void showProgress() {
    setState(() {
      _loading = false;

    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }


  void loadRetouchingList() {
       RetouchingRepository
    .findAndStore(showProgress, dismissProgress, (Registrationdata profile){
      if(profile != null){

        setState(() {
          _profile = profile;
          allRetouchingList  = profile == null?[]: profile.retouchings;

        });

      }else{
        print("Error Profile is Null");
      }
    }, (error){
          print("Error found $error");
    });

  }

  void goNext() {

    updateCartItem()
        .then((value){
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => new ServiceOrderItemsScreen( _profile, widget.service, items,_cartItem))
      );
    });

  }
}
