import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/ArtisanCartegoryWidget.dart';
import 'package:easy_services/models/artisan_category.dart';
import 'package:easy_services/models/cart.dart';
import 'package:easy_services/models/cart_item.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/ArtisanCategoryRepository.dart';
import 'package:easy_services/rest/request/order_item_request.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/order_services/service_order_items_screen.dart';
import 'package:easy_services/util/CartManager.dart';
import 'package:flutter/material.dart';
import 'package:easy_services/util/database_helper.dart';
import 'package:easy_services/models/ServicePricing.dart';

class CreateArtisanScreen extends StatefulWidget {
  final Registrationdata profile;

  final Service service;

  CreateArtisanScreen(this.profile, this.service);

  @override
  _CreateArtisanScreenState createState() => _CreateArtisanScreenState();
}

class _CreateArtisanScreenState extends State<CreateArtisanScreen> {
  bool _loading = true;

  bool hasSelection = false;
  Registrationdata _profile;

  Map<String, int> selectedArtisans;
  List<ArtisanCategory> _artisanCategoryList;
  List<ArtisanCategory> allArtisanCategoryList;

  Cart _cart;
  CartItem _cartItem;
  List<OrderItemRequest> items = [];
  double subTotal = 0.0;

  List<Widget> _pricingViews = [];

  @override
  void initState() {
    super.initState();

    _profile = widget.profile;

    _artisanCategoryList = widget.profile.artisan_categories;
    selectedArtisans = new Map<String, int>();

    // loadCart();

    if (allArtisanCategoryList != null) {
      if (allArtisanCategoryList.isEmpty) {
        loadArtisanCategoryList();
      }
    } else {
      loadArtisanCategoryList();
    }
  }

  /* void loadCart() {
    CartManager.obtainCart((cart) {
      setState(() {
        _cart = cart;

        var cartItemList = _cart.cartItems;
        if (cartItemList != null && cartItemList.isNotEmpty) {
          var cartItemServicesList =
              cartItemList.map((c) => c.service_slug).toList();

          bool yesContains =
              cartItemServicesList.contains(widget.service.slugValue());

          if (yesContains) {
            _cartItem = cartItemList.firstWhere(
                (c) => c.service_slug == widget.service.slugValue(),
                orElse: () => null);
            if (_cartItem == null) {
              _cartItem = new CartItem(widget.service.slugValue(),
                  widget.service.nameValue(), [], "0.0", "XOF");
            }
          } else {}
        } else {
          _cartItem = new CartItem(widget.service.slugValue(),
              widget.service.nameValue(), [], "0.0", "XOF");
        }
        // initSelection();
      });
    });
  } */

  /* void initSelection() {
    selectedArtisans = new Map<String, int>();
    if (_cartItem != null) {
      if (_cartItem.items != null && _cartItem.items.isNotEmpty) {
        _cartItem.items.forEach((o) {
          selectedArtisans[o.pressingClothes().slugValue()] = o.quantity;
        });
      } else {
        selectedArtisans = new Map<String, int>();
      }
    } else {
      selectedArtisans = new Map<String, int>();
    }

    refreshUI();
  } */

  /* void refreshUI() {
    var mPricingViews = _artisanCategoryList.map((artisan) {
      print("artisan ===>>> $artisan");
      return new ArtisanCategoryWidget(
        profile: widget.profile,
        artisan_category: artisan,
        item: items,
        cartItem: _cartItem,
        isSelected: selectedArtisans.containsKey(artisan.slug),
      );
    }).toList();

    if (mounted) {
      setState(() {
        _pricingViews = mPricingViews;
      });
    }
  } */

  @override
  Widget build(BuildContext context) {
    // hasSelection = selectedArtisans.isNotEmpty;
    // if (_artisanCategoryList == null) {
    //   _artisanCategoryList = _profile.artisan_categories;
    //   print("_artisanCategoryList ###-===> ${_artisanCategoryList}");
    // }

    return new Scaffold(
      appBar: new AppBar(
        primary: true,
        title: new Text(
          widget.service.nameValue(),
          style:
              new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),
        ),
      ),
      body: new Stack(
        children: <Widget>[
          new SingleChildScrollView(
            child: new Container(
              padding: EdgeInsets.all(10.0),
              child: new Column(
                children: <Widget>[
                  new Container(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          LangStrings.gs("screen_types_artisan_title"),
                          style: new TextStyle(
                              fontSize: 18.0,
                              color: Colors.black38,
                              fontFamily: 'ArialRoundedMT'),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        new Container(
                            padding: EdgeInsets.only(top: 20.0),
                            height: MediaQuery.of(context).size.height,
                            child: new ListView(
                              children: _pricingViews,
                            ))
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget progressView() {
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  void showProgress() {
    setState(() {
      _loading = false;
    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }

  void loadArtisanCategoryList() {
    ArtisanCategoryRepository.findAndStore(showProgress, dismissProgress,
        (Registrationdata profile) {
      if (profile != null) {
        setState(() {
          _profile = profile;
          allArtisanCategoryList =
              profile == null ? [] : profile.artisan_categories;
          var mPricingViews = allArtisanCategoryList.map((artisan) {
            print("artisan ===>>> $artisan");
            return new ArtisanCategoryWidget(
              profile: widget.profile,
              artisan_category: artisan,
              item: items,
              cartItem: _cartItem,
              isSelected: selectedArtisans.containsKey(artisan.slug),
            );
          }).toList();

          if (mounted) {
            setState(() {
              _pricingViews = mPricingViews;
            });
          }
          print(
              "RETRIEVE DATA SAVED: Artisan categories ====> ${allArtisanCategoryList}");
        });
      } else {
        print("Error Profile is Null");
      }
    }, (error) {
      print("Error found $error");
    });
  }

  
}
