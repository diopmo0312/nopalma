import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/generic_ui/choose_linge_kilo_option_item.dart';
import 'package:easy_services/models/cart.dart';
import 'package:easy_services/models/cart_item.dart';
import 'package:easy_services/models/linge_kilo_option.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/request/order_item_request.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/cart/cart_screen.dart';
import 'package:easy_services/ui/order_services/service_order_items_screen.dart';
import 'package:easy_services/util/CartManager.dart';
import 'package:flutter/material.dart';
import 'package:easy_services/rest/repository/LingeKiloOptionRepository.dart';

class CreateLingeKiloScreen extends StatefulWidget {
  final Registrationdata profile;

  final Service service;

  CreateLingeKiloScreen(this.profile, this.service);

  @override
  _CreateLingeKiloScreenState createState() => _CreateLingeKiloScreenState();
}

class _CreateLingeKiloScreenState extends State<CreateLingeKiloScreen> {
  List<LingeKiloOption> options = [];
  //List<LingeKiloOption> _optionList;

  bool _loading = true;

  Cart _cart;
  CartItem _cartItem;
  List<OrderItemRequest> items = [];
  double subTotal = 0.0;

  Map<String, int> selectedLingeKilo;

  LingeKiloOption _currentLingeKiloOption;

  //List<Widget> _pricingViews = [];

  @override
  void initState() {
    super.initState();
    print('Profile ===> ${widget.profile}');
    print('Linge au kilo 000ZZZ ===> ${widget.profile.linge_kilo_options}');

    if (widget.profile.linge_kilo_options != null &&
        widget.profile.linge_kilo_options.isNotEmpty) {
      options = widget.profile.linge_kilo_options;
      _currentLingeKiloOption = widget.profile.linge_kilo_options.first;
      print('Linge au kilo 000ZZZ ===> ${widget.profile.linge_kilo_options}');
    }
    // print("00000000000000000000000");
    // print('Linge au kilo 000ZZZ ===> ${widget.profile.linge_kilo_options}');

    //if (options != null) {
    // if (options.isEmpty) {
    //  loadLingeKiloOptionList();
    //}
    //} else {
    // loadLingeKiloOptionList();
    //}
    // print(widget.service);
    // _currentLingeKiloOption = options[0];

    loadCart();
  }

  void loadCart() {
    CartManager.obtainCart((cart) {
      setState(() {
        _cart = cart;

        var cartItemList = cart.cartItems;

        if (cartItemList != null && cartItemList.isNotEmpty) {
          var cartItemServicesList =
              cartItemList.map((c) => c.service_slug).toList();

          bool yesContains =
              cartItemServicesList.contains(widget.service.slugValue());

          if (yesContains) {
            _cartItem = cartItemList.firstWhere(
                (c) => c.service_slug == widget.service.slugValue(),
                orElse: () => null);
            if (_cartItem == null) {
              _cartItem = new CartItem(
                  widget.service.slugValue(),
                  widget.service.nameValue(),
                  [
                    OrderItemRequest(
                        widget.service.slugValue(),
                        _currentLingeKiloOption.slugValue(),
                        _currentLingeKiloOption.toJsonEncoded(),
                        1,
                        "KG",
                        _currentLingeKiloOption.priceValue(),
                        _currentLingeKiloOption.currencyValue())
                  ],
                  _currentLingeKiloOption.priceValue(),
                  _currentLingeKiloOption.currencyValue());
            }
          } else {
            _cartItem = new CartItem(
                widget.service.slugValue(),
                widget.service.nameValue(),
                [
                  OrderItemRequest(
                      widget.service.slugValue(),
                      _currentLingeKiloOption.slugValue(),
                      _currentLingeKiloOption.toJsonEncoded(),
                      1,
                      "KG",
                      _currentLingeKiloOption.priceValue(),
                      _currentLingeKiloOption.currencyValue())
                ],
                _currentLingeKiloOption.priceValue(),
                _currentLingeKiloOption.currencyValue());
          }
        } else {
          _cartItem = new CartItem(
              widget.service.slug,
              widget.service.nameValue(),
              [
                OrderItemRequest(
                    widget.service.slugValue(),
                    _currentLingeKiloOption.slugValue(),
                    _currentLingeKiloOption.toJsonEncoded(),
                    1,
                    "KG",
                    _currentLingeKiloOption.priceValue(),
                    _currentLingeKiloOption.currencyValue())
              ],
              _currentLingeKiloOption.priceValue(),
              _currentLingeKiloOption.currencyValue());
        }
        initSelection();
      });
    });
  }

  void initSelection() {
    selectedLingeKilo = new Map<String, int>();

    if (_cartItem != null) {
      if (_cartItem.items != null && _cartItem.items.isNotEmpty) {
        var orderItemRequest = _cartItem.items[0];
        selectedLingeKilo[orderItemRequest.option().slugValue()] =
            orderItemRequest.quantity;
      } else {
        selectedLingeKilo = new Map<String, int>();
        selectedLingeKilo[_currentLingeKiloOption.slugValue()] = 1;
      }
    } else {
      selectedLingeKilo = new Map<String, int>();
      selectedLingeKilo[_currentLingeKiloOption.slugValue()] = 1;
    }
  }

  Future<Cart> updateCartItem() async {
    items.clear();
    subTotal = 0.0;

    selectedLingeKilo.forEach((String indexKey, int indexValue) {
      LingeKiloOption retouching = options
          .firstWhere((pc) => pc.slugValue() == indexKey, orElse: () => null);
      if (retouching != null) {
        int pressingClothesPrice = int.parse(retouching.price);
        int itemSubTotal = pressingClothesPrice * indexValue;
        subTotal += itemSubTotal;
        items.add(new OrderItemRequest(
            widget.service.slugValue(),
            retouching.slugValue(),
            retouching.toString(),
            indexValue,
            retouching.quantityUnitValue(),
            retouching.priceValue(),
            retouching.currencyValue()));
      }
    });

    _cartItem = new CartItem(widget.service.slugValue(),
        widget.service.nameValue(), items, "$subTotal", "XOF");

    //Save in the Cart
    return await CartManager.addToCart(_cartItem);
  }

  @override
  Widget build(BuildContext context) {
    if (options == null) {
      options = [];
    }
    return new Scaffold(
        appBar: new AppBar(
          primary: true,
          title: new Text(
            widget.service.nameValue(),
            style: new TextStyle(
                color: Colors.white, fontFamily: 'ArialRoundedMT'),
          ),
        ),
        body: new Stack(
          children: <Widget>[
            new Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container(
                    margin: EdgeInsets.all(10.0),
                    child: new Column(
                      /* children: <Widget>[
                      new ChooseLingeKiloOptionItem(
                          widget.service, options[0], _currentLingeKiloOption,
                          (LingeKiloOption selection) {
                        setState(() {
                          _currentLingeKiloOption = selection;
                          selectedLingeKilo.clear();
                          selectedLingeKilo[
                              _currentLingeKiloOption.slugValue()] = 1;
                          updateCartItem();
                        });
                      }),
                      new ChooseLingeKiloOptionItem(
                          widget.service, options[1], _currentLingeKiloOption,
                          (LingeKiloOption selection) {
                        setState(() {
                          _currentLingeKiloOption = selection;
                          selectedLingeKilo.clear();
                          selectedLingeKilo[
                              _currentLingeKiloOption.slugValue()] = 1;
                          updateCartItem();
                        });
                      })
                    ] */
                      children: options
                          .map((e) => new ChooseLingeKiloOptionItem(
                                  widget.service, e, _currentLingeKiloOption,
                                  (LingeKiloOption selection) {
                                setState(() {
                                  _currentLingeKiloOption = selection;
                                  selectedLingeKilo.clear();
                                  selectedLingeKilo[
                                      _currentLingeKiloOption.slugValue()] = 1;
                                  updateCartItem();
                                });
                              }))
                          .toList(),
                      //children: _pricingViews,
                    ),
                  ),
                ],
              ),
            ),
            new Offstage(
              offstage: _loading,
              child: progressView(),
            )
          ],
        ),
        bottomSheet: new Container(
          padding: EdgeInsets.all(18.0),
          child: new Container(
            width: double.infinity,
            height: 80.0,
            padding: EdgeInsets.all(10.0),
            child: new RaisedButton(
                textColor: Colors.white,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(5.0)),
                color: _currentLingeKiloOption != null
                    ? AppColors.nopalmaYellow
                    : Colors.grey,
                padding: EdgeInsets.all(10.0),
                child: new Text(
                  LangStrings.gs("button_next_title"),
                  style: new TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                      fontFamily: 'ArialRoundedMT',
                      fontWeight: FontWeight.bold),
                ),
                onPressed: _currentLingeKiloOption != null
                    ? () {
                        goNext();
                      }
                    : null),
          ),
        ));
  }

  void goNext() {
    updateCartItem().then((value) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => new ServiceOrderItemsScreen(
                  widget.profile, widget.service, items, _cartItem)));
    });
  }

  void showProgress() {
    setState(() {
      _loading = false;
    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }

  Widget progressView() {
    return new Stack(
      children: [
        new Container(color: Colors.white),
        new Opacity(
          opacity: 0.3,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

/*
  void loadLingeKiloOptionList() {
    LingeKiloOptionRepository.findAndStore(showProgress, dismissProgress,
        (Registrationdata profile) {
      if (profile != null) {
        setState(() {});
          options = profile == null ? [] : profile.linge_kilo_options;
          _currentLingeKiloOption = options[0];

          // loadCart();
          // initSelection();

          var mPricingViews = options.map((option) {
            // print("option ===>>> $option");
            return new ChooseLingeKiloOptionItem(
                widget.service, options[option.id - 1], _currentLingeKiloOption,
                (LingeKiloOption selection) {
              setState(() {
                _currentLingeKiloOption = selection;

                selectedLingeKilo.clear();
                selectedLingeKilo[_currentLingeKiloOption.slugValue()] = 1;
                print(selectedLingeKilo[_currentLingeKiloOption.slugValue()]);
                updateCartItem();
              });
            });
          }).toList();

          if (mounted) {
            setState(() {
              _pricingViews = mPricingViews;
            });
          }

          print("RETRIEVE DATA SAVED: LingeKiloOption ====> ${options}");
        });
      } else {
        print("Error Profile is Null");
      }
    }, (error) {
      print("Error found $error");
    });
  }*/

}
