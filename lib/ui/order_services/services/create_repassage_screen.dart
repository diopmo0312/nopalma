import 'dart:async';
import 'dart:convert';

import 'package:easy_services/models/cart.dart';
import 'package:easy_services/models/cart_item.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/request/order_item_request.dart';
import 'package:easy_services/ui/cart/cart_screen.dart';
import 'package:easy_services/ui/order_services/service_order_items_screen.dart';
import 'package:easy_services/util/CartManager.dart';
import 'package:flutter/material.dart';

class CreateRepassageScreen extends StatefulWidget {

  final Registrationdata profile;

  final Service service;

  CreateRepassageScreen(this.profile, this.service);

  @override
  _CreateRepassageScreenState createState() => _CreateRepassageScreenState();


}

class _CreateRepassageScreenState extends State<CreateRepassageScreen> {

  Cart _cart;
  CartItem _cartItem;
  List<OrderItemRequest> items = [];
  double subTotal = 0.0;

  Map<String, int> selectedRepassage = new Map();

  @override
  void initState() {
    super.initState();
    loadCart();
  }

  void loadCart(){
    CartManager.obtainCart((cart){
      setState(() {

        _cart = cart;

        var cartItemList = _cart.cartItems;
        if(cartItemList != null && cartItemList.isNotEmpty){
          var cartItemServicesList = cartItemList.map((c)=>c.service_slug).toList();


          bool yesContains = cartItemServicesList.contains(widget.service.slugValue());

          if(yesContains){
            _cartItem = cartItemList.firstWhere((c)=>c.service_slug == widget.service.slugValue(), orElse: ()=>null);
            if(_cartItem == null){
              _cartItem = new CartItem(widget.service.slugValue(), widget.service.nameValue(), [], "0.0", "XOF");
            }
          }
        }else{
          _cartItem = new CartItem(widget.service.slugValue(), widget.service.nameValue(), [], "0.0", "XOF");
        }
        initSelection();

      });

    });



  }

  void initSelection(){

    selectedRepassage = new Map<String, int>();

    if(_cartItem != null){

      if(_cartItem.items != null && _cartItem.items.isNotEmpty){
        var orderItemRequest = _cartItem.items[0];
        selectedRepassage[orderItemRequest.service().slugValue()] = orderItemRequest.quantity;
      }else{
        selectedRepassage = new Map<String, int>();
        selectedRepassage[widget.service.slugValue()] = 1;
      }
    }else{
      selectedRepassage = new Map<String, int>();
      selectedRepassage[widget.service.slugValue()] = 1;
    }
  }

  Future<Cart> updateCartItem()async{

    if(selectedRepassage == null){
      selectedRepassage = new Map<String, int>();
      selectedRepassage[widget.service.slugValue()] = 1;
    }

      items.clear();
      subTotal = 0.0;

      selectedRepassage.forEach((String indexKey, int indexValue){
        Service service = widget.service;
        int pressingClothesPrice = int.parse(service.priceValue());
        int itemSubTotal= pressingClothesPrice *indexValue;
        subTotal += itemSubTotal;
        items.add(new OrderItemRequest(
            widget.service.slugValue(),
            widget.service.slugValue(),
            jsonEncode(service),
            indexValue,
            widget.service.quantityUnitValue(),
            service.priceValue(),
            service.currencyValue()));
      });

      _cartItem = new CartItem(widget.service.slug,widget.service.nameValue(), items, "$subTotal", "XOF");

      //Save in the Cart
      return await CartManager.addToCart(_cartItem);




  }
  @override
  Widget build(BuildContext context) {

    goNext();
    return new Scaffold(
        appBar: new AppBar(
        primary: true,
        title: new Text(widget.service.nameValue(), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
    ),
    body: new Stack(
    children: <Widget>[
      new Container(
        color: Colors.white,
      )
    ]
    ),

    );
  }

  void goNext(){
    updateCartItem()
    .then((value){

      Timer(Duration(seconds: 1),(){
        return  Navigator
            .pushReplacement(context,
            MaterialPageRoute(builder: (context) => new ServiceOrderItemsScreen(widget.profile, widget.service, items,_cartItem))
        );
      });



    });





  }
}
