import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/models/customer_subscription.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/SubscriptionRepository.dart';
import 'package:easy_services/generic_ui/MySubscriptionWidget.dart';
import 'package:flutter/material.dart';

class MySubscriptionsScreen extends StatefulWidget {

  final Registrationdata profile;
  MySubscriptionsScreen(this.profile);

  
  @override
  _MySubscriptionsScreenState createState() => _MySubscriptionsScreenState();
}

class _MySubscriptionsScreenState extends State<MySubscriptionsScreen> {

  List<CustomerSubscription> _customerSubscriptions;
   bool _loading = true;

  @override
  void initState() {
    super.initState();
    _customerSubscriptions = [];
    loadCustomerSubscriptions();
    
      }
      @override
      Widget build(BuildContext context) {

        
        return new Scaffold(
            appBar:  new AppBar(
            primary: true,
            title: new Text(LangStrings.gs("screen_my_subscriptions_title"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
            ),
          body: new Stack(
            children: <Widget>[
                _customerSubscriptions.isEmpty? new Container(
                    color: Colors.white,
                    child: new Center(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Icon(Icons.do_not_disturb,size: 80.0, color: Colors.black12,),
                          new Text( LangStrings.gs("dont_have_subscription_message"), style: new TextStyle(fontSize:20.0, fontFamily: "ArialRoundedMT", color:Colors.grey),),

                        ],
                      ),
                    ),
                  ):ListView(
                    children: _customerSubscriptions.map((sub)=> new MySubscriptionWidget(widget.profile, sub)).toList(),
                  ),
                 new Offstage(
               offstage: _loading,
               child: progressView(),
             )
            ])
        );
      }
    
      Widget progressView(){
        return new Stack(
          children: [
            new Container(
              color: Colors.white,
            ),
            new Center(
              child: new CircularProgressIndicator(),
            ),
          ],
        );
      }
    
      void showProgress() {
        setState(() {
          _loading = false;
    
        });
      }
    
      void dismissProgress() {
        setState(() {
          _loading = true;
        });
      }
    
      
       void loadCustomerSubscriptions() {
          SubscriptionRepository.find(
            widget.profile.user.id, showProgress, dismissProgress)
             .then((value){
                if(value.success){

                    setState(() {
                      if(value.data != null){
                          _customerSubscriptions = value.data;
                      }else{
                          _customerSubscriptions = [];
                      }
                      
                    });
                  
                }else{

                  new Dialogs().simpleError(
                      context,
                      LangStrings.gs("dialog_error_title"),
                      value.message,(){});

                }

              })
                  .catchError((error){
                print("Error found $error");

                new Dialogs().simpleError(
                    context,
                    LangStrings.gs("dialog_error_title"),
                    LangStrings.gs("dialog_error_message"),(){});
              });



       }
}