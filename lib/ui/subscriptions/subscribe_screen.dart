import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/models/payment_method.dart';
import 'package:easy_services/models/subscription.dart';
import 'package:easy_services/rest/repository/SubscriptionRepository.dart';
import 'package:easy_services/ui/subscriptions/my_subscriptions_screen.dart';
import 'package:flutter/material.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:toast/toast.dart';

class SubscribeScreen extends StatefulWidget {

  Subscription subscription;

  Registrationdata profile;
  
  SubscribeScreen(this.profile,this.subscription);

  @override
  _SubscribeScreenState createState() => _SubscribeScreenState();
}

class _SubscribeScreenState extends State<SubscribeScreen> {

  Registrationdata _profile;
  bool _loading = true;
  int _quantity = 1;
  double total = 0;

  List<PaymentMethod> paymentMethods;

  String paymentMethodSelected;

  String _currentSlotWeekDay = "Samedi";
  String _currentSlotTime = "08:00 - 08:59";

  List<String> slots = ['Lundi', "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];
  List<String> slotValues = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];

  List<String> slotTimes = ["08:00 - 08:59", "09:00 - 09:59", "10:00 - 10:59", "11:00 - 11:59", "12:00 - 12:59", "13:00 - 13:59", "14:00 - 14:59", "15:00 - 15:59", "16:00 - 16:59", "17:00 - 17:59"];

  @override
  void initState() {
    super.initState();
    _profile = widget.profile;
   
    calculateInvoice();
     if(widget.profile.payment_methods != null && widget.profile.payment_methods.isNotEmpty){
      paymentMethods = widget.profile.payment_methods;
      paymentMethodSelected = paymentMethods[0].slug;
    }else{
      paymentMethods = [];
    }

  }

  void  calculateInvoice(){
      total = double.parse(widget.subscription.cost) * _quantity;
  }


  @override
  Widget build(BuildContext context) {

    double quantityControlHeight = 20.0 * 1.2 ;
    double quantityControlTextSize = 15.0 * 1.2 ;

    return  new Scaffold(
        appBar:  new AppBar(
        primary: true,
        title: new Text(LangStrings.gs("screen_subscribe_title"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
        ),
      body: new Stack(
        children: <Widget>[
            new SingleChildScrollView(
              child: new Column(
                children: <Widget>[
                    new Container(
                       alignment: Alignment.topLeft,
                      margin: EdgeInsets.fromLTRB(8.0,8.0,8.0,0.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.0),
                          border: Border.all( color: getServiceBgColor(widget.subscription.service_slug), width: 1.0)
                      ),
                      child: new ListTile(
                        leading: getServiceImage(widget.subscription.service_slug),
                        title: new Text(widget.subscription.title, 
                          style:new TextStyle(
                            color: Colors.black,
                            fontFamily: "ArialRoundedMT", 
                            fontSize:20.0, 
                            fontWeight: FontWeight.bold)),
                        subtitle: new Text(widget.subscription.service_name,
                          style: new TextStyle(
                            color: Colors.black,
                            fontFamily: "ArialRoundedMT",
                            fontSize: 16.0,
                          )
                        ),    
                        trailing: new Container(
                           width: 140.0,
                            height: 50.0,
                                alignment: Alignment.center,
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Text(widget.subscription.cost_currency,
                                    style: new TextStyle(
                                      color: Colors.black,
                                      fontFamily: "ArialRoundedMT",
                                      fontSize: 14.0,
                                    )
                                    ),
                                    new Text(widget.subscription.cost,
                                    style: new TextStyle(
                                      color: Colors.black,
                                      fontFamily: "ArialRoundedMT",
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold
                                    )
                                    ),
                                    new Text("/"+getPer(widget.subscription), style: new TextStyle(
                                      color: Colors.grey,
                                      fontFamily: "ArialRoundedMT",
                                      fontSize: 12.0
                                    ),)
                                                  ],
                                                ),
                            ),
                  
                      ),
                    ),
            SizedBox(height: 20.0,),
            new Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.fromLTRB(8.0,8.0,8.0,0.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.0),
                          border: Border.all(color: Colors.black12, width: 1.0)
                      ),
                      child: ListTile(
                        title: new Container(
                          width: MediaQuery.of(context).size.width - 150.0,
                          child: new Text(LangStrings.gs("field_quantity_label"), style: new TextStyle(fontSize:15.0,color: Colors.black38, fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold),),
                        ),
                        subtitle: new Text(widget.subscription.cost+" "+widget.subscription.cost_currency),
                        trailing: new Container(
                          width: 100.0,
                          height: quantityControlHeight,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              new InkWell(
                                onTap: (){
                                  setState(() {
                                    _quantity --;
                                    if(_quantity <0){
                                      _quantity = 0;
                                    }
                                      calculateInvoice();
                                    
                                  });
                                },
                                child:  new Container(
                                  height: quantityControlHeight,
                                  width:  quantityControlHeight,
                                  padding: EdgeInsets.only(bottom: 5.0),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: AppColors.getColorHexFromStr("#d8d8d8")),
                                      borderRadius: BorderRadius.circular(quantityControlHeight)
                                  ),
                                  child:  Center(
                                    child: new Text("-", style: new TextStyle(fontSize: quantityControlTextSize, color: Colors.black, ),),
                                  ),
                                ),
                              ),
                              new SizedBox(width: 10.0,),
                              new Container(
                                child:  Center(
                                  child: new Text("$_quantity", style: new TextStyle(fontSize: quantityControlTextSize, color: Colors.black, ),),
                                ),
                              ),
                              new SizedBox(width: 10.0,),
                              new InkWell(
                                onTap: (){
                                  setState(() {
                                    _quantity ++;

                                      calculateInvoice();
                                
                                  });

                                },
                                child:  new Container(
                                  height: quantityControlHeight,
                                  width:  quantityControlHeight,
                                  padding: EdgeInsets.only(bottom: 5.0),
                                  decoration: BoxDecoration(
                                      color: AppColors.nopalmaBlue,
                                      border: Border.all(color: AppColors.nopalmaBlue),
                                      borderRadius: BorderRadius.circular(quantityControlHeight)
                                  ),
                                  child:  Center(
                                    child: new Text("+", style: new TextStyle(fontSize: quantityControlTextSize, color: Colors.white, ),),
                                  ),
                                ),
                              ) ,
                            ],
                          ),
                        ),
                      ),
            ), 
            SizedBox(height: 20.0,),
            new Container(
              padding: EdgeInsets.all(10.0),
              margin: EdgeInsets.all(10.0),
               decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all( color: Colors.black12, width: 1.0)
                ),
              child:new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Text(LangStrings.gs("field_total_title"), style: new TextStyle(color: Colors.black, fontSize:18.0,fontWeight: FontWeight.bold),),
                      new Text('$total XOF', style: new TextStyle(color: Colors.black ,fontSize:18.0,fontWeight: FontWeight.bold),)
                    ],
                  )
            ),
              new Container(
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        margin: EdgeInsets.only(top: 10.0),
                        padding: EdgeInsets.only(left: 15.0),
                        alignment: Alignment.topLeft,
                        child: new Text(LangStrings.gs("field_choose_payment_method_title"),style: new TextStyle(color: Colors.grey, fontSize:16.0,fontWeight: FontWeight.bold) ),
                      ),
                       new Container(
                        height: 30.0 * 3,
                        margin: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: ListView(
                          children: paymentMethods.map((p) => 
                          new Container(
                            height: 30.0,
                        child: new Row(
                          children: <Widget>[
                            new Radio(
                              value: p.slug,
                              groupValue: paymentMethodSelected ,
                              onChanged: (String paymentMethodValue){
                                  setState(() {
                                    paymentMethodSelected = paymentMethodValue;
                                  });
                              },
                            ),
                            new SizedBox(width: 10.0,),
                            new Text(LangStrings.strLang=='FR'?p.title:p.title_en,style: new TextStyle(color: Colors.black, fontSize:18.0,fontWeight: FontWeight.bold) )
                          ],
                        ),
                      )
                          ).toList(),
                        ),
                      ),

                    ],
                  ),
                ),

                  new Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.all(10.0),
                      child: new Text("Jour de Passage", textAlign: TextAlign.start, style: new TextStyle(fontSize: 14.0, color: Colors.black,fontWeight: FontWeight.bold), )),
                  new Container(
                    width: double.infinity,
                    margin: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                        border: Border.all(width: 0.8, color: Colors.grey[500]),
                        borderRadius: BorderRadius.circular(5.0)
                    ),
                    child: new DropdownButtonHideUnderline(
                        child: new DropdownButton(
                          value: _currentSlotWeekDay,
                          items: slots.map((String value){return new DropdownMenuItem<String>(
                              value: value,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new Text(value, style: new TextStyle(color: Colors.black, fontWeight: FontWeight.bold,fontSize: 14.0),),
                              )
                          );}).toList(),
                          onChanged: (String selected) {
                            setState(() {
                              _currentSlotWeekDay = selected;
                            });
                          },


                        )
                    ),
                  ),

                  new Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.all(10.0),
                      child: new Text("Créneaux Horaires", textAlign: TextAlign.start, style: new TextStyle(fontSize: 14.0, color: Colors.black,fontWeight: FontWeight.bold), )),

                  new Container(
                    width: double.infinity,
                    margin: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                        border: Border.all(width: 0.8, color: Colors.grey[500]),
                        borderRadius: BorderRadius.circular(5.0)
                    ),
                    child: new DropdownButtonHideUnderline(
                        child: new DropdownButton(
                          value: _currentSlotTime,
                          items: slotTimes.map((String value){return new DropdownMenuItem<String>(
                              value: value,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new Text(value, style: new TextStyle(color: Colors.black, fontWeight: FontWeight.bold,fontSize: 14.0),),
                              )
                          );}).toList(),
                          onChanged: (String selected) {
                            setState(() {
                              _currentSlotTime = selected;
                            });
                          },


                        )
                    ),
                  ),

                new SizedBox(height: 30.0,),
                new Container(
                  width: double.infinity,
                  height: 80.0,
                  padding: EdgeInsets.all(10.0),
                  child: new RaisedButton(
                      shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0)
                    ),
                      textColor: Colors.white,
                      color:AppColors.nopalmaYellow,
                      padding: EdgeInsets.all(10.0),
                      child: new Text(LangStrings.gs("button_terminate_title"), style: new TextStyle(fontSize:18.0,color: Colors.white, fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold), ),
                      onPressed: (){
                        goNext();

                      }),
                )


                ],
              ),
            ),
            new Offstage(
           offstage: _loading,
           child: progressView(),
         )
        ],
      )
    );
  }

  void goNext() async{

     Map<String, dynamic> jsonBody = {
        "subscription_id":widget.subscription.id,
        "customer_id":widget.profile.user.id,
        "amount":widget.subscription.cost,
        "currency":widget.subscription.cost_currency,
        "payment_method":paymentMethodSelected,
        "subscription_quantity":_quantity,
        "slot_week_day": slotValues[slots.indexOf(_currentSlotWeekDay)],
        "slot_time": _currentSlotTime
        //"payment_reference":"ADD PAYMENT REFERENCE IF PAYMENT"
        //"coupon":"coupon"
     };

     final String jsonBodyString = jsonEncode(jsonBody);

    SubscriptionRepository.create(jsonBodyString, showProgress, dismissProgress)
    .then((value){

      if(value.success){

         Toast.show(LangStrings.gs("subscription_successful_msg"), context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);


        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => new MySubscriptionsScreen(widget.profile)),
        );
      }else{

        new Dialogs().simpleError(
            context,
            LangStrings.gs("dialog_error_title"),
            value.message,(){});

      }

    })
        .catchError((error){
      print("Error found $error");

      new Dialogs().simpleError(
          context,
          LangStrings.gs("dialog_error_title"),
          LangStrings.gs("dialog_error_message"),(){});
    });

  }

    Image getServiceImage(String service_slug) {
    if(service_slug == "pressing"){
      return Image.asset("assets/sssss@2x.png", color: getServiceBgColor(service_slug),);
    }else if(service_slug == "linge_au_kilo"){
      return Image.asset("assets/cleancult_05_2x@2x.png", color: getServiceBgColor(service_slug),);
    }else if(service_slug == "repassage"){
      return Image.asset("assets/555@2x.png", color: getServiceBgColor(service_slug),);
    }else if(service_slug == "retouche"){
      return Image.asset("assets/RETROUC@2x.png", color: getServiceBgColor(service_slug),);
    }else if(service_slug == "coordonnerie"){
      return Image.asset("assets/CORD@2x.png", color: getServiceBgColor(service_slug),);
    }else{
      return Image.asset("assets/555@2x.png", color: getServiceBgColor(service_slug),);
    }

  }

    Color getServiceBgColor(String service_slug) {
    if(service_slug == "pressing"){
      return AppColors.getColorHexFromStr("#00d4db");
    }else if(service_slug == "linge_au_kilo"){
      return AppColors.getColorHexFromStr("#fe8b16");
    }else if(service_slug == "repassage"){
      return AppColors.getColorHexFromStr("#00adf0");
    }else if(service_slug == "retouche"){
      return AppColors.getColorHexFromStr("#8fc650");
    }else if(service_slug == "coordonnerie"){
      return AppColors.getColorHexFromStr("#ffb501");
    }else{
      return AppColors.getColorHexFromStr("#00d4db");
    }

  }

  String getPer(Subscription subscription) {
    if(subscription.delay_unit == "month"){
      return LangStrings.gs("month");
    }else{
      return LangStrings.gs("month");
    }
  }

  

    Widget progressView(){
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  void showProgress() {
    setState(() {
      _loading = false;

    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }
}