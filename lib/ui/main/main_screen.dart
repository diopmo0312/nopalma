import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/models/FirebaseMessage.dart';
import 'package:easy_services/models/cart.dart';
import 'package:easy_services/models/customer_device.dart';
import 'package:easy_services/models/linge_kilo_option.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/UserRepository.dart';
import 'package:easy_services/rest/repository/LingeKiloOptionRepository.dart';
import 'package:easy_services/rest/response/customer_device_result.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/main/pages/home_screen.dart';
import 'package:easy_services/ui/main/pages/notification_screen.dart';
import 'package:easy_services/ui/main/pages/profile_screen.dart';
import 'package:easy_services/ui/main/pages/subscriptions_screen.dart';
import 'package:easy_services/ui/cart/cart_screen.dart';
import 'package:easy_services/ui/orders/list_order_screen.dart';
import 'package:easy_services/util/CartManager.dart';
import 'package:easy_services/util/LoginManager.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:device_info/device_info.dart';

class MainScreen extends StatefulWidget {
  final Registrationdata profile;

  final int position;

  MainScreen({this.profile, this.position = 0});

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  int _index;

  Cart _cart;

  int cartItemCount = 0;

  bool _loading = true;

  Registrationdata _profile;

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  List<LingeKiloOption> options = [];

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(
        initialIndex: widget.position, length: 5, vsync: this);
    _index = widget.position;

    _profile = widget.profile;

    firebaseCloudMessagingListeners();

    loadCart();

    loadFirebaseId(_profile);
    //loadProfile();
    refreshData();
    // loadLingeKiloOptionList();
  }

  void loadLingeKiloOptionList() {
    LingeKiloOptionRepository.findAndStore(showProgress, dismissProgress,
        (Registrationdata profile) {
      if (profile != null) {
        setState(() {
          _profile = profile;

          options = profile == null ? [] : profile.linge_kilo_options;
          print('Linge au kilo 000 ===> ${widget.profile.linge_kilo_options}');
        });
      } else {
        print("Error Profile is Null");
      }
    }, (error) {
      print("Error found $error");
    });
  }

  void firebaseCloudMessagingListeners() {
    if (Platform.isIOS) iOSPermission();

    _firebaseMessaging.getToken().then((token) {
      //print(token);
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
        var notification = message["notification"];
        /* var firebaseMessage = new FirebaseMessage(
            title: notification['title'], body: notification['body']); */
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  void iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  void loadCart() {
    CartManager.obtainCart((cart) {
      setState(() {
        _cart = cart;
        cartItemCount = _cart.cartItems.length;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    //_tabController.index = _index;

    return new Scaffold(
      appBar: new AppBar(
        primary: true,
        centerTitle: false,
        title: new Text(
          "Nopalma",
          style:
              new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),
        ),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(
                Icons.refresh,
                color: Colors.white,
              ),
              onPressed: () {
                refreshData();
                print("Refresh !");
              }),
          new Padding(
              padding: const EdgeInsets.all(10.0),
              child: new Container(
                  height: 150.0,
                  width: 30.0,
                  child: new GestureDetector(
                      onTap: () {
                        /*Navigator.of(context).push(
                                new MaterialPageRoute(
                                    builder:(BuildContext context) =>
                                    new CartItemsScreen()
                                )
                            );*/
                      },
                      child: Stack(children: <Widget>[
                        new IconButton(
                            icon: new Icon(
                              Icons.shopping_cart,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              openCart();
                            }),
                        new Positioned(
                            child: new Stack(
                          children: <Widget>[
                            new Icon(Icons.brightness_1,
                                size: 20.0, color: AppColors.nopalmaYellow),
                            new Positioned(
                                top: 4.0,
                                right: 5.5,
                                child: new Center(
                                  child: new Text(
                                    "$cartItemCount",
                                    style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 11.0,
                                        fontWeight: FontWeight.w500),
                                  ),
                                )),
                          ],
                        )),
                      ]))))
        ],
      ),
      body: new Stack(
        children: <Widget>[
          new TabBarView(
            children: <Widget>[
              new HomeScreen(_profile),
              new ListOrderScreen(_profile),
              new NotificationScreen(_profile),
              new SubscriptionsScreen(_profile),
              new ProfileScreen(_profile)
            ],
            controller: _tabController,
          ),
          new Offstage(
            offstage: _loading,
            child: progressView(),
          )
        ],
      ),
      bottomNavigationBar: new BottomNavigationBar(
        currentIndex: _index,
        fixedColor: Colors.black,
        type: BottomNavigationBarType.fixed,
        unselectedItemColor: Colors.black54,
        unselectedLabelStyle: TextStyle(color: Colors.black54),
        backgroundColor: Colors.white,
        elevation: 2.0,
        items: <BottomNavigationBarItem>[
          new BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: Colors.black54,
            ),
            title: new Text(LangStrings.gs("menu_home_title"),
                style: new TextStyle(
                  color: Colors.black54,
                )),
            activeIcon: Icon(
              Icons.home,
              color: AppColors.nopalmaBlue,
            ),
          ),
          new BottomNavigationBarItem(
            icon: Icon(
              Icons.local_offer,
              color: Colors.black54,
            ),
            title: new Text(LangStrings.gs("menu_orders_title"),
                style: new TextStyle(
                  color: Colors.black54,
                )),
            activeIcon: Icon(
              Icons.local_offer,
              color: AppColors.nopalmaBlue,
            ),
          ),
          new BottomNavigationBarItem(
              icon: Icon(Icons.notifications, color: Colors.black54),
              title: new Text(LangStrings.gs("menu_notifications_title"),
                  style: new TextStyle(
                    color: Colors.black54,
                  )),
              activeIcon: Icon(
                Icons.notifications,
                color: AppColors.nopalmaBlue,
              )),
          new BottomNavigationBarItem(
            icon: Image.asset(
              'assets/choices@2x.png',
              color: Colors.black54,
            ),
            title: new Text(
              LangStrings.gs("menu_subscriptions_title"),
              style: new TextStyle(
                color: Colors.black54,
              ),
            ),
            activeIcon: Icon(
              Icons.description,
              color: AppColors.nopalmaBlue,
            ),
          ),
          new BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
              color: Colors.black54,
            ),
            title: new Text(LangStrings.gs("menu_profile_title"),
                style: new TextStyle(
                  color: Colors.black54,
                )),
            activeIcon: Icon(
              Icons.person,
              color: AppColors.nopalmaBlue,
            ),
          ),
        ],
        onTap: (int position) {
          setState(() {
            this._index = position;
            _tabController.index = position;
          });
        },
      ),
    );
  }

  void loadProfile() {
    UserRepository.login(widget.profile.user.dial_code,
            widget.profile.user.phone_number, showProgress, dismissProgress)
        .then((value) {
      if (value.success) {
        LoginManager.signIn(value.data, (Registrationdata profile) {
          if (mounted) {
            setState(() {
              _profile = profile;
            });
          }

          loadFirebaseId(profile);
        }, (error) {
          new Dialogs().simpleError(
              context,
              LangStrings.gs("dialog_error_title"),
              LangStrings.gs("dialog_error_message"), () {
            Navigator.pop(context);
          });
        });
      } else {}
    }).catchError((error) {
      print("LoginResult $error");
      var message;
      if (error is HttpException || error is SocketException) {
        message = LangStrings.gs("internet_error_message");
      } else {
        message = LangStrings.gs("dialog_error_message");
      }

      new Dialogs().simpleError(
          context, LangStrings.gs("dialog_error_title"), message, () {});
    });
  }

  void refreshData() {
    UserRepository.login(widget.profile.user.dial_code,
            widget.profile.user.phone_number, () {}, () {})
        .then((value) {
      if (value.success) {
        print('SAVING >>>> ${value.data}');
        LoginManager.signIn(value.data, (Registrationdata profile) {
          if (mounted) {
            setState(() {
              _profile = profile;
              print("PROFIL ===> ${_profile.linge_kilo_options}");
            });
          }

          loadFirebaseId(profile);
        }, (error) {
          new Dialogs().simpleError(
              context,
              LangStrings.gs("dialog_error_title"),
              LangStrings.gs("dialog_error_message"), () {
            Navigator.pop(context);
          });
        });
      } else {}
    }).catchError((error) {
      print("LoginResult $error");
      var message;
      if (error is HttpException || error is SocketException) {
        message = LangStrings.gs("internet_error_message");
      } else {
        message = LangStrings.gs("dialog_error_message");
      }

      new Dialogs().simpleError(
          context, LangStrings.gs("dialog_error_title"), message, () {});
    });
  }

  loadFirebaseId(Registrationdata profile) async {
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

    String firebaseId = await _firebaseMessaging.getToken();
    print('Firebase ID ====> $firebaseId');
    List<CustomerDevice> devices = profile.devices;
    if (devices == null) {
      devices = [];
    }
    //List<String> deviceTokens = devices.map((c)=>c.firebase_id).toList();
    //print("Devices $deviceTokens");
    //if(!deviceTokens.contains(firebaseId)){
    //print('Firebase ID $firebaseId');

    /* DeviceInfoPlugin deviceInfo = new DeviceInfoPlugin();

    Map<String, String> devicesData = new Map();

    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      devicesData["device_id"] = androidInfo.id;
      devicesData["device_model"] = androidInfo.model;
      devicesData["device_os"] = "Android";
      devicesData["device_os_version"] = androidInfo.version.baseOS;
      devicesData["device_model_type"] = "android";
      devicesData["device_meta_data"] =
          jsonEncode(_readAndroidBuildData(androidInfo));
    } else {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      devicesData["device_id"] = iosInfo.identifierForVendor;
      devicesData["device_model"] = iosInfo.model;
      devicesData["device_os"] = iosInfo.systemName;
      devicesData["device_os_version"] = iosInfo.systemVersion;
      devicesData["device_model_type"] = "iphone";
      devicesData["device_meta_data"] = jsonEncode(_readIosDeviceInfo(iosInfo));
    } */

    UserRepository.postCustomerDevices(
            profile, firebaseId, showProgress, dismissProgress)
        .then((CustomerDeviceResult p) {})
        .catchError((error) {
      dismissProgress();
      print("CustomerDevicesResult $error");
      /*      var message;
        if(error is HttpException || error is SocketException){
          message = LangStrings.gs("internet_error_message");
        }else{
          message  = LangStrings.gs("dialog_error_message");
        }

        new Dialogs().simpleError(
            context,
            LangStrings.gs("dialog_error_title"),
            message,(){

        });*/
    });

    // }
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }

  void showProgress() {
    setState(() {
      _loading = false;
    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }

  Widget progressView() {
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Opacity(
          opacity: 0.3,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  void openCart() async {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => new CartScreen(_profile)));
  }
}
