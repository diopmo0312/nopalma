import 'dart:io';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/login/connexion_screen.dart';
import 'package:easy_services/ui/profile/show_profile_screen.dart';
import 'package:easy_services/ui/settings_screen.dart';
import 'package:easy_services/ui/subscriptions/my_subscriptions_screen.dart';
import 'package:easy_services/util/LoginManager.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share_me/flutter_share_me.dart';


class ProfileScreen extends StatefulWidget {

  final Registrationdata profile;

  ProfileScreen(this.profile);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();

}

class _ProfileScreenState extends State<ProfileScreen> {

  String title = "";

  String username = "";

  String companyName;

  // Firebase
  final FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  void initState() {
    super.initState();

    if(widget.profile != null){
      if(widget.profile.user != null){

        if(widget.profile.user.account_type == "business"){
          title = LangStrings.gs("business_account");
          companyName = widget.profile.user.company_name;

        }else{
          title = LangStrings.gs("basic_account");
        }

        if(widget.profile.user.name != null){
          username = widget.profile.user.name;
        }
      }
    }else{
      title = LangStrings.gs("basic_account");
      username = LangStrings.gs("unknown_name");
    }

  }


  @override
  Widget build(BuildContext context) {

    return new SingleChildScrollView(
        child: new Container(
          child: new Column(
            children: <Widget>[
              new Container(
                child: new Column(
                  children: <Widget>[
                    new SizedBox(height: 20.0,),
                    new Container(
                      height: 120.0,
                      width: 120.0,
                      child:  Icon(Icons.person, color: Colors.white, size: 80.0,),
                      decoration: BoxDecoration(
                          color: AppColors.getColorHexFromStr("#dedede"),
                          shape: BoxShape.circle,
                          border: Border.all(color: AppColors.getColorHexFromStr("#dedede"))
                      ),
                    ),
                    new SizedBox(height: 10.0,),
                    new Container(
                      child: new Text(username,
                        style: new TextStyle(color: Colors.black, fontSize: 20.0,
                            fontWeight: FontWeight.bold, fontFamily: "ArialRoundedMT"),),
                    ),
                    new SizedBox(height: 5.0,),
                    new Offstage(
                      offstage: companyName == null,
                      child: new Container(
                        child: new Text(companyName==null?"":companyName,
                          style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, fontFamily: "ArialRoundedMT"),),
                      ),
                    )
                  ],
                ),
              ),
              Divider(color: Colors.black12,),
              new Container(
                height: MediaQuery.of(context).size.height - 200,
                child: new ListView(
                  children: <Widget>[
                    ListTile(
                      leading: new Container(
                        height: 50.0,
                        width: 50.0,
                        decoration: BoxDecoration(
                            color: AppColors.nopalmaBlue,
                            shape: BoxShape.circle,
                            border: Border.all(color: AppColors.nopalmaBlue)),
                        child: Icon(Icons.person, color: Colors.white,)
                      ),
                      title: new Text(LangStrings.gs("profile_detail"), style: TextStyle(color: Colors.black45, fontSize:18.0, fontFamily: "ArialRoundedMT"),),
                      onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => new ShowProfileScreen(widget.profile)),
                        );
                      },
                    ),
                    ListTile(
                      leading: new Container(
                        height: 50.0,
                        width: 50.0,
                        decoration: BoxDecoration(
                            color: AppColors.nopalmaBlue,
                            shape: BoxShape.circle,
                            border: Border.all(color: AppColors.nopalmaBlue)),
                        child: Icon(Icons.list, color: Colors.white,)
                      ),
                      title: new Text(LangStrings.gs("subscriptions"), style: TextStyle(color: Colors.black45, fontSize:18.0, fontFamily: "ArialRoundedMT"),),
                      onTap: (){
                               Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => new MySubscriptionsScreen(widget.profile)),
                              );
                      },
                    ),
                    ListTile(
                      leading: new Container(
                        height: 50.0,
                        width: 50.0,
                        decoration: BoxDecoration(
                            color: AppColors.nopalmaBlue,
                            shape: BoxShape.circle,
                            border: Border.all(color: AppColors.nopalmaBlue)),
                        child: Icon(Icons.share, color: Colors.white,)
                      ),
                      title: new Text(LangStrings.gs("inviter_un_ami"), style: TextStyle(color: Colors.black45, fontSize:18.0, fontFamily: "ArialRoundedMT"),),
                      onTap: ()async{
                          if(Platform.isAndroid){
                            var response =
                                await FlutterShareMe().shareToSystem(msg: LangStrings.gs("invite_friend_message"));
                            if (response == 'success') {
                              print('navigate success');
                            }
                          }
                      },
                    ),
                    ListTile(
                      leading: new Container(
                          height: 50.0,
                          width: 50.0,
                          decoration: BoxDecoration(
                              color: AppColors.nopalmaBlue,
                              shape: BoxShape.circle,
                              border: Border.all(color: AppColors.nopalmaBlue)),
                          child: Icon(Icons.settings, color: Colors.white,)
                      ),
                      title: new Text(LangStrings.gs("screen_settings_title"), style: TextStyle(color: Colors.black45, fontSize:18.0, fontFamily: "ArialRoundedMT"),),
                      onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => new SettingsScreen(widget.profile)),
                        );
                      },
                    ),
                    ListTile(
                      leading: new Container(
                        height: 50.0,
                        width: 50.0,
                        decoration: BoxDecoration(
                            color: AppColors.nopalmaBlue,
                            shape: BoxShape.circle,
                            border: Border.all(color: AppColors.nopalmaBlue)),
                        child: Icon(Icons.exit_to_app, color: Colors.white,)
                      ),
                      title: new Text(LangStrings.gs("logout"), style: TextStyle(color: Colors.black45, fontSize:18.0, fontFamily: "ArialRoundedMT"),),
                      onTap: (){

                        _auth.signOut();
                        LoginManager.signOut((){

                          Navigator
                              .of(context)
                              .pushReplacement(
                              new MaterialPageRoute(
                                  builder: (BuildContext context)=> new ConnexionScreen()
                              )
                          );

                        }, (error){

                        });

                      },
                    )
                  ],
                ),
              )
            ],
          ),
        )
    );
  }
}
