import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/SubscriptionWidget.dart';
import 'package:easy_services/models/subscription.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:flutter/material.dart';

class SubscriptionsScreen extends StatefulWidget {

  final Registrationdata profile;

  SubscriptionsScreen(this.profile);

  @override
  _SubscriptionsScreenState createState() => _SubscriptionsScreenState();
}

class _SubscriptionsScreenState extends State<SubscriptionsScreen> {

  List<Subscription> _subscriptions;
    @override
  void initState() {
    super.initState();
    if(widget.profile.subscriptions != null){
        _subscriptions = widget.profile.subscriptions;
    }else{
      _subscriptions = [];
    }
    

  }
  
  @override
  Widget build(BuildContext context) {

    return _subscriptions.isEmpty? new Container(
      color: Colors.white,
      child: new Center(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Icon(Icons.do_not_disturb,size: 80.0, color: Colors.black12,),
            new Text(LangStrings.gs("no_data_message"), style: new TextStyle(fontSize:20.0, fontFamily: "ArialRoundedMT", color:Colors.grey),),

          ],
        ),
      ),
    ):ListView(
      children:_subscriptions.map((sub)=> new SubscriptionWidget(widget.profile, sub)).toList()
    );
  }

  List<Widget> buildUi() {
      final List<Widget> list = [];


      //list.addAll();


      return list;

  }
}
