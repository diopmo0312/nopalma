import 'dart:io';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/generic_ui/NotificationWidget.dart';
import 'package:easy_services/models/customer_notification.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/UserRepository.dart';
import 'package:flutter/material.dart';

class NotificationScreen extends StatefulWidget {

  final Registrationdata profile;

  NotificationScreen(this.profile);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {

  List<CustomerNotification> notifications;

  bool _loading = true;

  @override
  void initState() {
    super.initState();
    loadCustomerNotifications();
  }

  @override
  Widget build(BuildContext context) {

    if(notifications == null){
      notifications = [];
    }

    notifications.sort((a,b)=>b.id.compareTo(a.id));

    return new Stack(
      children: <Widget>[
        notifications.isNotEmpty?
        ListView(
          children: notifications.map((customerNotification)=>new NotificationWidget(widget.profile,customerNotification)).toList(),
        ):new Container(
          color: Colors.white,
          child: new Center(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Icon(Icons.do_not_disturb,size: 80.0, color: Colors.black12,),
                new Text(LangStrings.gs("no_data_message"), style: new TextStyle(fontSize:20.0, fontFamily: "ArialRoundedMT", color:Colors.grey),),

              ],
            ),
          ),
        ),

        new Offstage(
          offstage: _loading,
          child: progressView(),
        )

      ],
    );
  }



  void showProgress() {
    if(mounted){
      setState(() {
        _loading = false;

      });
    }
  }

  void dismissProgress() {
    if(mounted){
      setState(() {
        _loading = true;
      });
    }

  }

  Widget progressView(){
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Opacity(
          opacity: 0.3,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  void loadCustomerNotifications() {
    UserRepository.findNotifications(widget.profile.user.id, showProgress, dismissProgress)
        .then((value){
          if(value.success){
            if(mounted){
              setState(() {
                notifications = value.data;
              });
            }
          }
         })
        .catchError((error){
          print("CustomerDevicesResult $error");
            var message;
            if(error is HttpException || error is SocketException){
              message = LangStrings.gs("internet_error_message");
            }else{
              message  = LangStrings.gs("dialog_error_message");
            }

            new Dialogs().simpleError(
                context,
                LangStrings.gs("dialog_error_title"),
                message,(){

            });

        });

  }
}
