import 'package:easy_services/generic_ui/service_view.dart';
import 'package:easy_services/generic_ui/slider_ui.dart';
import 'package:geolocator/geolocator.dart';
import 'package:easy_services/models/home_slide.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/UserRepository.dart';
import 'package:easy_services/ui/main/main_screen.dart';
import 'package:easy_services/util/carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeScreen extends StatefulWidget {
  final Registrationdata profile;

  HomeScreen(this.profile);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<HomeSlide> slides;

  bool _loading = true;

  @override
  void initState() {
    super.initState();
    slides = [];
    loadSlides();

    // print(Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high));
  }

  @override
  Widget build(BuildContext context) {
    List<Service> services = [];

    if (widget.profile != null) {
      services = widget.profile.services == null ? [] : widget.profile.services;
      services.sort((service1, service2) {
        if (service1.rank == null) {
          return -1;
        }

        if (service2.rank == null) {
          return -1;
        }

        return service1.rank.compareTo(service2.rank);
      });
    }

    return new Stack(
      children: <Widget>[
        new SingleChildScrollView(
          child: new Column(
            children: <Widget>[
              new Container(
                width: double.infinity,
                height: 200.0,
                child: slides.isNotEmpty
                    ? new Carousel(
                        dotBgColor: Colors.transparent,
                        images: slides
                            .map((s) => new CarouselImage(
                                    new NetworkImage(s.image_url),
                                    s.title,
                                    s.subtitle,
                                    s.button_title, () async {
                                  print("Ok ${s.action}");
                                  if (s.action == "open_browser") {
                                    print("open_browser");
                                    if (await canLaunch(s.target_url)) {
                                      await launch(s.target_url);
                                    } else {}
                                  } else if (s.action == "open_subscription") {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                new MainScreen(
                                                  profile: widget.profile,
                                                  position: 3,
                                                )));
                                  } else if (s.action == "") {
                                  } else {}
                                }))
                            .toList(),
                      )
                    : new Container(),
              ),
              SizedBox(
                height: 20.0,
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                child: new Column(
                  children: services.map((service) {
                    return new ServiceView(widget.profile, service);
                  }).toList(),
                ),
              )
            ],
          ),
        ),
        new Offstage(
          offstage: _loading,
          child: progressView(),
        )
      ],
    );
  }

  void showProgress() {
    setState(() {
      _loading = false;
    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }

  Widget progressView() {
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Opacity(
          opacity: 0.3,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  loadSlides() {
    UserRepository.findSlides(showProgress, dismissProgress).then((value) {
      if (value.success) {
        if (mounted) {
          setState(() {
            this.slides = value.data;
          });
        }
      }
    }).catchError((error) {
      print('Error found $error');

      if (mounted) {
        setState(() {
          this.slides = [];
        });
      }
    });
  }
}
