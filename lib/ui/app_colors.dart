
import 'package:flutter/material.dart';

class AppColors{

  static Color getColorHexFromStr(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  static final Color colorAccent = getColorHexFromStr('#00d3da');
  static final Color colorPrimary = Colors.red;
  static final Color colorPrimaryDark = Colors.red[800];
  static final Color button_positive_color= getColorHexFromStr('#00b3b9');
  static final Color button_negative_color= getColorHexFromStr('#09a6e3');
  static final Color form_bg_color= getColorHexFromStr('#FF6200');
  static final Color rounded_button_bg_color= getColorHexFromStr('#013a51');
  static final Color nopalmaBlue = getColorHexFromStr('#7F65F3');
  static final Color nopalmaYellow = getColorHexFromStr('#EFC500');

}