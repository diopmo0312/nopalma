import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/delivery_address_item_widget.dart';
import 'package:easy_services/models/delivery_address.dart';
import 'package:easy_services/openstreetmap_places/OpenStreetMapPlacesWidget.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/main/main_screen.dart';
import 'package:flutter/material.dart';

class DeliveryAddressScreen extends StatefulWidget {

  final Registrationdata profile;

  DeliveryAddressScreen({this.profile});
  
  @override
  _DeliveryAddressScreenState createState() => _DeliveryAddressScreenState();
}

class _DeliveryAddressScreenState extends State<DeliveryAddressScreen> {

  DeliveryAddress placeHouse, placeOffice, placeOther;


  @override
  Widget build(BuildContext context) {

    bool haveOnePlace = false;
    if(placeHouse != null){
      haveOnePlace = true;
    }

    if(placeOffice != null){
      haveOnePlace = true;
    }

    if(placeOther != null){
      haveOnePlace = true;
    }

    return new Scaffold(
      appBar: new AppBar(
        primary: true,
        centerTitle: false,
        title: new Text(LangStrings.gs("screen_deliveries_addresses"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
      ),
    body: new Container(

      child: new Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Container(
            child: new Column(
              children: <Widget>[
                SizedBox(height: 10.0,),
                new DeliveryAddressItemWidget(locationTypeImage: "house-outline.png", locationTypeTitle: LangStrings.gs("location_type_house_title"), addressName:address(placeHouse), addButtonCallback:(){

                  retrieveLocation("house");

                }, closeButtonCallback:(){
                  setState(() {
                    placeHouse  = null;
                  });
                }),
                new DeliveryAddressItemWidget(locationTypeImage:"working-with-laptop.png" , locationTypeTitle: LangStrings.gs("location_type_office_title"), addressName:address(placeOffice), addButtonCallback:(){

                  retrieveLocation("office");

                }, closeButtonCallback:(){
                  setState(() {
                    placeOffice  = null;
                  });
                }),
                new DeliveryAddressItemWidget(locationTypeImage:"placeholder@2x.png", locationTypeTitle: LangStrings.gs("location_type_others_title"), addressName:address(placeOther), addButtonCallback:(){

                  retrieveLocation("other");
                }, closeButtonCallback:(){
                  setState(() {
                    placeOther  = null;
                  });
                }),
              ],
            ),
          ),

          new Expanded(child: new Container()),

          new Container(
            height: 70.0,
            width: double.infinity,

            padding: EdgeInsets.only(left: 15.0, right: 15.0),
            child: RaisedButton(
              color: haveOnePlace?AppColors.nopalmaYellow:AppColors.button_negative_color,
              padding: const EdgeInsets.all(10.0),
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(5.0)
              ),
              onPressed:haveOnePlace?(){
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => new MainScreen(profile:widget.profile)),
                );
              }:null,
              child: new Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(LangStrings.gs("button_save_title"), style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold, fontFamily: 'ArialRoundedMT'),),
              ),

            ),
          ),
          Container(
            height: 70.0,
            width: double.infinity,
            margin: EdgeInsets.only(top: 20.0, bottom: 80.0),
            padding: EdgeInsets.only(left: 15.0, right: 15.0),
            child: RaisedButton(
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(5.0)
              ),
              color: AppColors.nopalmaYellow,
              padding: const EdgeInsets.all(10.0),

              onPressed:(){
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => new MainScreen(profile:widget.profile)),
                );
              },
              child: Text("Ignorer", style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold, fontFamily: 'ArialRoundedMT'),),

            ),
          )

        ],
      ),
    )
    );
  }

  String address( DeliveryAddress deliveryAddress){
    if(deliveryAddress == null){
      return "";
    }else{
      return deliveryAddress.address_name;//p.name+', '+p.formattedAddress;
    }
  }
  void retrieveLocation(String type) async{
    DeliveryAddress deliveryAddress  = await Navigator.push(
      context,
      //MaterialPageRoute(builder: (context) => new MapboxAutocompleteScreen(profile:widget.profile,type:type)),
     MaterialPageRoute(builder: (context) => new OpenStreetMapPlacesWidget(profile:widget.profile,type:type)),
    );

    setState(() {
      if(deliveryAddress.address_type == "house"){
        placeHouse = deliveryAddress;
      }else if(deliveryAddress.address_type  == "office"){
        placeOffice = deliveryAddress;
      }else if(deliveryAddress.address_type  == "other"){
        placeOther = deliveryAddress;
      }
    });


  }

  String getDeliveryTypeImage(DeliveryAddress deliveryAddress) {
    if(deliveryAddress.address_type == "house"){
      return "house-outline.png";
    }else if(deliveryAddress.address_type == "office"){
      return "working-with-laptop.png";
    }else{
      return  "placeholder@2x.png";
    }
  }

  String getDeliveryTypeTitle(DeliveryAddress deliveryAddress) {
    if(deliveryAddress.address_type == "house"){
      return LangStrings.gs("location_type_house_title");
    }else if(deliveryAddress.address_type == "office"){
      return  LangStrings.gs("location_type_office_title");
    }else{
      return  LangStrings.gs("location_type_others_title");
    }
  }

}


