import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/cinetpay/PaymentResponse.dart';
import 'package:easy_services/util/CartManager.dart';
import 'package:easy_services/ui/orders/detail/order_detail_screen.dart';
import 'package:easy_services/models/order.dart';

class TouchPayScreen extends StatefulWidget {
  final Registrationdata profile;
  final Order order;

  final String agencyCodeData;
  final String secretData;
  final String notificationUrlData;
  final String domainNameData;
  final String amountData;
  final String orderNumberData;
  final String emailData;
  final String clientFirstname;
  final String clientLastname;
  final String clientPhone;

  TouchPayScreen(
      {this.profile,
      this.order,
      this.agencyCodeData,
      this.secretData,
      this.notificationUrlData,
      this.domainNameData,
      this.amountData,
      this.orderNumberData,
      this.emailData,
      this.clientFirstname,
      this.clientLastname,
      this.clientPhone});

  @override
  _TouchPayScreenState createState() => _TouchPayScreenState();
}

class _TouchPayScreenState extends State<TouchPayScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Payer avec TouchPay",
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          new RaisedButton(
              color: Theme.of(context).accentColor,
              onPressed: () {
                // Navigator.pop(context, "close");
                CartManager.emptyCart();
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          new OrderDetailScreen(widget.profile, widget.order)),
                );
              },
              child: new Text(
                "FERMER",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0),
              ))
        ],
      ),
      body: new InAppWebView(
        initialFile: "assets/touchpay.html",
        initialHeaders: {},
        initialOptions: new InAppWebViewWidgetOptions(),
        onWebViewCreated: (InAppWebViewController controller) {
          controller.evaluateJavascript(
              source:
                  'window.addEventListener("flutterInAppWebViewPlatformReady", function(event) {'
                  ''
                  'initData("${widget.agencyCodeData}", "${widget.secretData}", "${widget.notificationUrlData}", "${widget.domainNameData}", "${widget.amountData}", "${widget.orderNumberData}", "${widget.emailData}",  "${widget.clientFirstname}", "${widget.clientLastname}", "${widget.clientPhone}" ); clickFunction();'
                  ''
                  ' });');
          controller.addJavaScriptHandler(
              handlerName: 'success',
              callback: (args) {
                onMessage("success");
                return "success";
              });

          controller.addJavaScriptHandler(
              handlerName: 'error',
              callback: (args) {
                onMessage("error");
                return "error";
              });
        },
        onLoadStart: (InAppWebViewController controller, String url) {
          print("started $url");
        },
        onProgressChanged: (InAppWebViewController controller, int progress) {},
        onConsoleMessage:
            (InAppWebViewController controller, ConsoleMessage message) {
          print("ConsoleMessage ${message.message}");
        },
      ),
    );
  }

  onMessage(String message) {
    if (message != null) {
      PaymentResponse paymentResponse;

      if (message == "success") {
        paymentResponse = PaymentResponse(true, "Votre paiement est validé");
      } else {
        paymentResponse = PaymentResponse(false, "Erreur rencontrée");
      }

      Navigator.of(context).pop(paymentResponse);
    }
  }
}
