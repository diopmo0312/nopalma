import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:flutter/material.dart';

class SettingsScreen extends StatefulWidget {

  final Registrationdata profile;

  SettingsScreen(this.profile);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {

  Language _currentLanguage;
  List<Language> languages = new List();

  @override
  void initState() {
    super.initState();
    languages.add(Language("EN", "Anglais", "English"));
    languages.add(Language("FR", "Français", "French"));

    _currentLanguage = languages.firstWhere((l)=>l.code == LangStrings.strLang);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar:  new AppBar(
        primary: true,
        title: new Text(LangStrings.gs("screen_settings_title"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT')),
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            child: new Column(
              children: <Widget>[
                new Container(
                  margin: EdgeInsets.only(top: 10.0),
                  padding: EdgeInsets.only(left: 15.0),
                  alignment: Alignment.topLeft,
                  child: new Text(LangStrings.gs("field_language_title"),
                      style: new TextStyle(color: Colors.grey, fontSize:16.0,fontWeight: FontWeight.bold, fontFamily: "ArialRoundedMT") ),
                ),
                new Container(
                  height: 60.0,
                  width: double.infinity,
                  padding: EdgeInsets.all(10.0),
                  margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5.0),
                      border: Border.all(color: Colors.black12)
                  ),
                  child: DropdownButtonHideUnderline(
                    child: new DropdownButton<Language>(
                      value: _currentLanguage,
                      underline: null,
                      items: languages.map((language)=>
                        new DropdownMenuItem<Language>(
                          value: language,
                            child: new Text(language.nameValue(),
                                style:new TextStyle(color: Colors.black,fontSize:16.0,
                                    fontFamily: "ArialRoundedMT"))
                        )
                      ).toList(),
                      onChanged: (Language choice){
                        setState(() {
                          _currentLanguage = choice;
                          LangStrings.setLang(choice.code);
                        });
                      },
                    ),
                  )
                ),

              ],
            ),
          ),
        ],
      ),
    );
  }

}

class Language{

  String code;
  String name;
  String name_en;

  Language(this.code, this.name, this.name_en);

  String nameValue(){
    return LangStrings.strLang == "FR"?name:name_en;
  }

  @override
  bool operator ==(other) {
    return (other as Language).code == this.code;
  }


}
