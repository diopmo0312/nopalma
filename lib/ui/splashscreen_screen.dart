import 'dart:async';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {

  final int seconds;
  final Widget navigateAfterSeconds;

  SplashScreen({
    @required this.seconds,
    @required this.navigateAfterSeconds
   });
  @override
  SplashScreenState createState() {
    return new SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();

    Timer(Duration(seconds: this.widget.seconds),(){
      return Navigator
          .of(context)
          .pushReplacement(
          new MaterialPageRoute(
              builder: (BuildContext context)=> widget.navigateAfterSeconds
             // builder: (BuildContext context)=> WelcomeScreen(),
          )
      );
    });

  }
  @override
  Widget build(BuildContext context) {
    return
        new Scaffold(
          body: Container(
            decoration: BoxDecoration(
                color: Colors.white
            ),
            child: new Center(
              child: new Container(
                padding: EdgeInsets.all(80.0),
                child: Image.asset('assets/nopalma/NOPALMA.png',),
              ),
            ),
          ),
        );

  }
}
