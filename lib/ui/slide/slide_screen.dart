import 'dart:async';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/delivery_address_screen.dart';
import 'package:easy_services/ui/login/connexion_screen.dart';
import 'package:easy_services/ui/main/main_screen.dart';
import 'package:easy_services/util/LoginManager.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:easy_services/generic_ui/slider_ui.dart';

class SlideScreen extends StatefulWidget {
  @override
  SlideScreenState createState() {
    return new SlideScreenState();
  }
}

class SlideScreenState extends State<SlideScreen> {

  // Firebase
  final FirebaseAuth _auth = FirebaseAuth.instance;
  List<Slide> slides = new List();

  @override
  void initState() {
    super.initState();

    Timer.run(() {

      LoginManager.connectedUser((Registrationdata profile){

        if(profile.delivery_addresses != null && profile.delivery_addresses.isNotEmpty){
          Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => new MainScreen(profile: profile)),
          );
        }else{

          Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => new DeliveryAddressScreen(profile:profile)),
          );


        }

      }, (error){

      });
    });

    slides.add(
      new Slide(
          title: LangStrings.gs("Pressing"),
          description: LangStrings.gs("PressingDesc"),
          pathImage: "assets/nopalma/slides/pressing_jacket.png",
          colorBegin: Colors.white,
          directionColorBegin: Alignment.topRight,
          colorEnd: Colors.white,
          directionColorEnd: Alignment.bottomCenter,
          styleTitle: new TextStyle(color: AppColors.nopalmaBlue,fontWeight: FontWeight.bold,
            fontSize: 30.0, fontFamily: "ArialRoundedMT"),
          styleDescription: new TextStyle(color: Colors.grey,fontSize: 18.0, fontFamily: "ArialRoundedMT")

      ),
    );
    slides.add(
      new Slide(
          title: LangStrings.gs("LingeauKilo"),
          description: LangStrings.gs("LingeauKiloDesc"),
          pathImage: "assets/nopalma/slides/household.png",
          colorBegin: Colors.white,
          directionColorBegin: Alignment.topRight,
          colorEnd: Colors.white,
          directionColorEnd: Alignment.bottomCenter,
          styleTitle: new TextStyle(color: AppColors.nopalmaBlue,fontWeight: FontWeight.bold,
            fontSize: 30.0, fontFamily: "ArialRoundedMT"),
          styleDescription: new TextStyle(color: Colors.grey,fontSize: 18.0, fontFamily: "ArialRoundedMT")
      ),
    );

    slides.add(
      new Slide(
          title: LangStrings.gs("RepassageauKilo"),
          description: LangStrings.gs("RepassageauKiloDesc"),
          pathImage: "assets/nopalma/slides/repassage_au_kilo.png",
          colorBegin: Colors.white,
          directionColorBegin: Alignment.topRight,
          colorEnd: Colors.white,
          directionColorEnd: Alignment.bottomCenter,
          styleTitle: new TextStyle(color: AppColors.nopalmaBlue,fontWeight: FontWeight.bold,
            fontSize: 30.0, fontFamily: "ArialRoundedMT"),
          styleDescription: new TextStyle(color: Colors.grey,fontSize: 18.0, fontFamily: "ArialRoundedMT")
      ),
    );

    slides.add(
      new Slide(
          title: LangStrings.gs("Retouche"),
          description: LangStrings.gs("RetoucheDesc"),
          pathImage: "assets/nopalma/slides/retouche.png",
          colorBegin: Colors.white,
          directionColorBegin: Alignment.topRight,
          colorEnd: Colors.white,
          directionColorEnd: Alignment.bottomCenter,
          styleTitle: new TextStyle(color: AppColors.nopalmaBlue,fontWeight: FontWeight.bold,
            fontSize: 30.0, fontFamily: "ArialRoundedMT"),
          styleDescription: new TextStyle(color: Colors.grey,fontSize: 18.0, fontFamily: "ArialRoundedMT")
      ),
    );

    slides.add(
      new Slide(
          title: LangStrings.gs("Cordonnerie"),
          description: LangStrings.gs("CordonnerieDesc"),
          pathImage: "assets/nopalma/slides/cordonnerie1.png",
          colorBegin: Colors.white,
          directionColorBegin: Alignment.topRight,
          colorEnd: Colors.white,
          directionColorEnd: Alignment.bottomCenter,
          styleTitle: new TextStyle(color: AppColors.nopalmaBlue,fontWeight: FontWeight.bold,
            fontSize: 30.0, fontFamily: "ArialRoundedMT"),
          styleDescription: new TextStyle(color: Colors.grey,fontSize: 18.0, fontFamily: "ArialRoundedMT")
      ),
    );

;
  }

  void onDonePress() {

    Navigator
        .of(context)
        .pushReplacement(
        new MaterialPageRoute(
            builder: (BuildContext context)=> new ConnexionScreen()
        )
    );
  }

  void onSkipPress() {
    Navigator
        .of(context)
        .pushReplacement(
        new MaterialPageRoute(
            builder: (BuildContext context)=> new ConnexionScreen()
        )
    );
  }

  @override
  Widget build(BuildContext context) {

          final user = _auth.currentUser;

          if(user != null){

            LoginManager.connectedUser((Registrationdata profile){

              Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => new MainScreen(profile: profile)),
              );

            }, (error){

            });

          }


    return new SliderUI(
      slides: this.slides,
      colorActiveDot: AppColors.nopalmaYellow,
      colorDot: Colors.black45,
      colorSkipBtn: AppColors.nopalmaBlue,
      colorDoneBtn: AppColors.nopalmaYellow,

      onDonePress: this.onDonePress,
      onSkipPress: this.onSkipPress,
    );
  }
}
