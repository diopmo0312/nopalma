import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/models/delivery_address.dart';
import 'package:easy_services/models/formula.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/OrderRepository.dart';
import 'package:easy_services/time_slot_picker/slot.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/cart/checkout_screen.dart';
import 'package:easy_services/ui/cart/choose_delivery_address.dart';
import 'package:easy_services/ui/cart/order_complete_screen.dart';
import 'package:easy_services/ui/cart/slot_screen.dart';
import 'package:easy_services/util/CartManager.dart';
import 'package:easy_services/util/DateUtil.dart';
import 'package:flutter/material.dart';

class ChooseTypeOfDeliveryScreen extends StatefulWidget {
  final Registrationdata profile;

  final String services;

  ChooseTypeOfDeliveryScreen(this.profile, this.services);

  @override
  _ChooseTypeOfDeliveryScreenState createState() =>
      _ChooseTypeOfDeliveryScreenState();
}

class _ChooseTypeOfDeliveryScreenState extends State<ChooseTypeOfDeliveryScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  String formulaSelected;
  int feesDelivery;

  bool hasPickup = false;
  bool hasDelivery = false;

  bool hasAddresses = false;

  bool hasSlot = false;

  DeliveryAddress pickupAddress, deliveryAddress;

  Slot pickupSlot, deliverySlot;

  Locale defaultLocale;

  bool _loading = true;

  List<Formula> formulas;
  // Formula formulaChecked;

  @override
  void initState() {
    super.initState();

    if (widget.profile.formulas != null && widget.profile.formulas.isNotEmpty) {
      formulas = widget.profile.formulas;
      formulaSelected = widget.profile.formulas.first.name;
      print('Formula ===> ${formulaSelected}');
    }

    // formulaSelected = "lite";
    feesDelivery = int.parse(formulas.first.delivery_fees);
    print('feesDelivery ===> ${feesDelivery}');
    _tabController = TabController(vsync: this, length: 2);
    loadCart();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void loadCart() {
    CartManager.obtainCart((cart) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    defaultLocale = Locale(LangStrings.strLang.toLowerCase());
    hasPickup = pickupAddress != null;
    hasDelivery = deliveryAddress != null;
    hasAddresses = hasPickup && hasDelivery;

    return new Scaffold(
      appBar: new AppBar(
        primary: true,
        title: new Text(
          LangStrings.gs("screen_option_of_delivery_title"),
          style:
              new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),
        ),
      ),
      body: new Stack(
        children: <Widget>[
          new SingleChildScrollView(
            child: new Container(
              padding: EdgeInsets.only(bottom: 20.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 15.0),
                          child: new Text(
                            LangStrings.gs("field_choose_formula_title"),
                            style: new TextStyle(
                                fontSize: 18.0,
                                color: Colors.blueGrey,
                                fontFamily: "ArialRoundedMT"),
                          ),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        new Padding(
                          padding: EdgeInsets.only(left: 15.0, right: 15.0),
                          child: new Container(
                            height: 60.0,
                            margin: EdgeInsets.only(left: 10.0, right: 10.0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(5.0),
                                  bottomRight: Radius.circular(5.0)),
                            ),
                            child: TabBar(
                              controller: _tabController,
                              onTap: (position) {
                                if (position == 0) {
                                  setState(() {
                                    formulaSelected = formulas.first.name;
                                    feesDelivery =
                                        int.parse(formulas.first.delivery_fees);
                                  });
                                } else {
                                  setState(() {
                                    formulaSelected = formulas[1].name;
                                    feesDelivery =
                                        int.parse(formulas[1].delivery_fees);
                                  });
                                }
                              },
                              tabs: [
                                Tab(
                                    child: new Row(
                                  children: <Widget>[
                                    new Radio(
                                      value: formulas.first.name,
                                      groupValue: formulaSelected,
                                      onChanged: (String formulaValue) {
                                        _tabController.index = 0;
                                        setState(() {
                                          formulaSelected = formulaValue;
                                        });
                                      },
                                    ),
                                    new Text("LITE",
                                        style: new TextStyle(
                                            color: Colors.blueGrey,
                                            fontFamily: "ArialRoundedMT",
                                            fontSize: 14.0))
                                  ],
                                )),
                                Tab(
                                    child: new Row(
                                  children: <Widget>[
                                    new Radio(
                                      value: formulas[1].name,
                                      groupValue: formulaSelected,
                                      onChanged: (String formulaValue) {
                                        _tabController.index = 1;
                                        setState(() {
                                          formulaSelected = formulaValue;
                                        });
                                      },
                                    ),
                                    new Text("EXPRESS",
                                        style: new TextStyle(
                                            color: Colors.blueGrey,
                                            fontFamily: "ArialRoundedMT",
                                            fontSize: 14.0))
                                  ],
                                )),
                              ],
                            ),
                          ),
                        ),
                        new Padding(
                            padding: EdgeInsets.only(left: 10.0, right: 10.0),
                            child: new Container(
                              height: 120.0,
                              margin: EdgeInsets.only(left: 10.0, right: 10.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(5.0),
                                    bottomRight: Radius.circular(5.0)),
                              ),
                              child: TabBarView(
                                controller: _tabController,
                                children: [
                                  new Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border(
                                          bottom: BorderSide(
                                              color: Colors.black12,
                                              width: 2.0),
                                          left: BorderSide(
                                              color: Colors.black12,
                                              width: 2.0),
                                          right: BorderSide(
                                              color: Colors.black12,
                                              width: 2.0),
                                        )),
                                    child: new Column(
                                      children: <Widget>[
                                        new SizedBox(
                                          height: 20.0,
                                        ),
                                        new Row(
                                          children: <Widget>[
                                            new SizedBox(
                                              width: 10.0,
                                            ),
                                            new Container(
                                              width: 10.0,
                                              height: 10.0,
                                              decoration: BoxDecoration(
                                                color: AppColors.nopalmaBlue,
                                                shape: BoxShape.circle,
                                              ),
                                            ),
                                            new SizedBox(
                                              width: 10.0,
                                            ),
                                            new Text(
                                              LangStrings.strLang == 'FR'
                                                  ? "Créneau horaire de ${formulas.first.time_slot} min"
                                                  : "${formulas.first.time_slot} min time slot",
                                              style: new TextStyle(
                                                  color: Colors.grey,
                                                  fontFamily: "ArialRoundedMT",
                                                  fontSize: 16.0),
                                            )
                                          ],
                                        ),
                                        new SizedBox(
                                          height: 10.0,
                                        ),
                                        new Row(
                                          children: <Widget>[
                                            new SizedBox(
                                              width: 10.0,
                                            ),
                                            new Container(
                                              width: 10.0,
                                              height: 10.0,
                                              decoration: BoxDecoration(
                                                color: AppColors.nopalmaBlue,
                                                shape: BoxShape.circle,
                                              ),
                                            ),
                                            new SizedBox(
                                              width: 10.0,
                                            ),
                                            new Text(
                                              LangStrings.strLang == 'FR'
                                                  ? "Délai de traitement de ${formulas.first.delivery_delay} heures"
                                                  : "${formulas.first.delivery_delay} hours processing time",
                                              style: new TextStyle(
                                                  color: Colors.grey,
                                                  fontFamily: "ArialRoundedMT",
                                                  fontSize: 16.0),
                                            )
                                          ],
                                        ),
                                        new SizedBox(
                                          height: 10.0,
                                        ),
                                        new Row(
                                          children: <Widget>[
                                            new SizedBox(
                                              width: 10.0,
                                            ),
                                            new Container(
                                              width: 10.0,
                                              height: 10.0,
                                              decoration: BoxDecoration(
                                                color: AppColors.nopalmaBlue,
                                                shape: BoxShape.circle,
                                              ),
                                            ),
                                            new SizedBox(
                                              width: 10.0,
                                            ),
                                            new Text(
                                              LangStrings.strLang == 'FR'
                                                  ? "Frais de livraison: ${formulas.first.delivery_fees} FCFA"
                                                  : "Delivery charges: ${formulas.first.delivery_fees} XOF",
                                              style: new TextStyle(
                                                  color: Colors.grey,
                                                  fontFamily: "ArialRoundedMT",
                                                  fontSize: 16.0),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  new Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border(
                                          bottom: BorderSide(
                                              color: Colors.black12,
                                              width: 2.0),
                                          left: BorderSide(
                                              color: Colors.black12,
                                              width: 2.0),
                                          right: BorderSide(
                                              color: Colors.black12,
                                              width: 2.0),
                                        )),
                                    child: new Column(
                                      children: <Widget>[
                                        new SizedBox(
                                          height: 10.0,
                                        ),
                                        new Row(
                                          children: <Widget>[
                                            new SizedBox(
                                              width: 10.0,
                                            ),
                                            new Container(
                                              width: 10.0,
                                              height: 10.0,
                                              decoration: BoxDecoration(
                                                color: AppColors.nopalmaBlue,
                                                shape: BoxShape.circle,
                                              ),
                                            ),
                                            new SizedBox(
                                              width: 10.0,
                                            ),
                                            new Text(
                                              LangStrings.strLang == 'FR'
                                                  ? "Créneau horaire de ${formulas[1].time_slot} min"
                                                  : "${formulas[1].time_slot} min time slot",
                                              style: new TextStyle(
                                                  color: Colors.grey,
                                                  fontFamily: "ArialRoundedMT",
                                                  fontSize: 16.0),
                                            )
                                          ],
                                        ),
                                        new SizedBox(
                                          height: 10.0,
                                        ),
                                        new Row(
                                          children: <Widget>[
                                            new SizedBox(
                                              width: 10.0,
                                            ),
                                            new Container(
                                              width: 10.0,
                                              height: 10.0,
                                              decoration: BoxDecoration(
                                                color: AppColors.nopalmaBlue,
                                                shape: BoxShape.circle,
                                              ),
                                            ),
                                            new SizedBox(
                                              width: 10.0,
                                            ),
                                            new Text(
                                              LangStrings.strLang == 'FR'
                                                  ? "Délai de traitement de ${formulas[1].delivery_delay} heures"
                                                  : "${formulas[1].delivery_delay} hour processing time",
                                              style: new TextStyle(
                                                  color: Colors.grey,
                                                  fontFamily: "ArialRoundedMT",
                                                  fontSize: 16.0),
                                            )
                                          ],
                                        ),
                                        new SizedBox(
                                          height: 10.0,
                                        ),
                                        new Row(
                                          children: <Widget>[
                                            new SizedBox(
                                              width: 10.0,
                                            ),
                                            new Container(
                                              width: 10.0,
                                              height: 10.0,
                                              decoration: BoxDecoration(
                                                color: AppColors.nopalmaBlue,
                                                shape: BoxShape.circle,
                                              ),
                                            ),
                                            new SizedBox(
                                              width: 10.0,
                                            ),
                                            new Text(
                                              LangStrings.strLang == 'FR'
                                                  ? "Delivery charges: ${formulas[1].delivery_fees} XOF"
                                                  : "Delivery charges: ${formulas[1].delivery_fees} XOF",
                                              style: new TextStyle(
                                                  color: Colors.grey,
                                                  fontFamily: "ArialRoundedMT",
                                                  fontSize: 16.0),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )),
                        SizedBox(
                          height: 20.0,
                        ),
                        new Padding(
                          padding: EdgeInsets.only(left: 15.0, right: 15.0),
                          child: new Text(
                            LangStrings.gs("field_choose_schedule_title"),
                            style: new TextStyle(
                              fontSize: 18.0,
                              color: Colors.blueGrey,
                              fontFamily: "ArialRoundedMT",
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        new Padding(
                          padding: EdgeInsets.only(left: 15.0, right: 15.0),
                          child: new Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5.0)),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                new Container(
                                  width:
                                      (MediaQuery.of(context).size.width / 2) -
                                          15,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                          right: BorderSide(
                                              color: Colors.black12))),
                                  padding: EdgeInsets.all(10.0),
                                  alignment: Alignment.topLeft,
                                  child: new Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      new Container(
                                        alignment: Alignment.center,
                                        child: new Text(
                                            LangStrings.gs(
                                                "field_pickup_title"),
                                            style: new TextStyle(
                                              fontSize: 16.0,
                                              color: Colors.grey,
                                              fontFamily: "ArialRoundedMT",
                                            )),
                                      ),
                                      showSlot(pickupSlot, true, defaultLocale),
                                    ],
                                  ),
                                ),
                                new Container(
                                  width:
                                      (MediaQuery.of(context).size.width / 2) -
                                          15,
                                  padding: EdgeInsets.all(10.0),
                                  alignment: Alignment.topLeft,
                                  child: new Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      new Container(
                                          alignment: Alignment.center,
                                          child: new Text(
                                              LangStrings.gs(
                                                  "field_delivery_title"),
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Colors.grey,
                                                  fontFamily:
                                                      "ArialRoundedMT"))),
                                      showSlot(
                                          deliverySlot, false, defaultLocale),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        new Padding(
                          padding: EdgeInsets.only(left: 15.0, right: 15.0),
                          child: new Text(
                            LangStrings.gs("field_pickup_delivery_title"),
                            style: new TextStyle(
                              fontSize: 18.0,
                              color: Colors.blueGrey,
                              fontFamily: "ArialRoundedMT",
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        new Container(
                          padding: EdgeInsets.only(left: 15.0, right: 15.0),
                          margin: EdgeInsets.only(left: 10.0, right: 10.0),
                          child: new Column(
                            children: <Widget>[
                              _buildAddressesView(),
                              new Offstage(
                                offstage: hasPickup,
                                child: new InkWell(
                                  onTap: () {
                                    chooseAddress("pickup");
                                  },
                                  child: new Container(
                                    padding: EdgeInsets.all(5.0),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border(
                                            bottom: BorderSide(
                                                width: 2.0,
                                                color: Colors.black12))),
                                    child: new Column(
                                      children: <Widget>[
                                        new Row(
                                          children: <Widget>[
                                            IconButton(
                                                icon: Icon(
                                                  Icons.add,
                                                  color: AppColors.nopalmaBlue,
                                                ),
                                                onPressed: () {
                                                  chooseAddress("pickup");
                                                }),
                                            SizedBox(
                                              width: 5.0,
                                            ),
                                            new Text(
                                              LangStrings.gs(
                                                  "field_add_pickup_title"),
                                              style: new TextStyle(
                                                  color: Colors.black,
                                                  fontFamily: "ArialRoundedMT",
                                                  fontSize: 16.0),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              new Offstage(
                                offstage: hasDelivery,
                                child: new InkWell(
                                  onTap: () {
                                    chooseAddress("delivery");
                                  },
                                  child: new Container(
                                    padding: EdgeInsets.all(5.0),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border(
                                            bottom: BorderSide(
                                                width: 2.0,
                                                color: Colors.black12))),
                                    child: new Column(
                                      children: <Widget>[
                                        new Row(
                                          children: <Widget>[
                                            IconButton(
                                                icon: Icon(
                                                  Icons.add,
                                                  color: AppColors.nopalmaBlue,
                                                ),
                                                onPressed: () {
                                                  chooseAddress("delivery");
                                                }),
                                            SizedBox(
                                              width: 5.0,
                                            ),
                                            new Text(
                                              LangStrings.gs(
                                                  "field_add_delivery_title"),
                                              style: new TextStyle(
                                                  color: Colors.black,
                                                  fontFamily: "ArialRoundedMT",
                                                  fontSize: 16.0),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    width: double.infinity,
                    height: 80.0,
                    padding: EdgeInsets.all(10.0),
                    margin: EdgeInsets.all(10.0),
                    child: new RaisedButton(
                        textColor: Colors.white,
                        color: hasAddresses && hasSlot
                            ? AppColors.nopalmaYellow
                            : Colors.grey,
                        padding: EdgeInsets.all(10.0),
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                        child: new Text(
                          LangStrings.gs("button_next_title"),
                          style: new TextStyle(
                              fontSize: 18.0,
                              color: Colors.white,
                              fontFamily: 'ArialRoundedMT',
                              fontWeight: FontWeight.bold),
                        ),
                        onPressed: hasAddresses && hasSlot
                            ? () {
                                goNext();
                              }
                            : null),
                  )
                ],
              ),
            ),
          ),
          new Offstage(
            offstage: _loading,
            child: progressView(),
          )
        ],
      ),
    );
  }

  void goNext() {
    var pickupAddressId = pickupAddress == null ? -1 : pickupAddress.id;
    var deliveryAddressId = deliveryAddress == null ? -1 : deliveryAddress.id;

    String pickup_slot = "";
    String delivery_slot = "";

    if (pickupSlot != null) {
      pickup_slot =
          "${DateUtil.formatDate(pickupSlot.startTime.toIso8601String(), Locale('en'))}, ${pickupSlot.slotString}";
    }

    if (deliverySlot != null) {
      delivery_slot =
          "${DateUtil.formatDate(deliverySlot.startTime.toIso8601String(), Locale('en'))}, ${deliverySlot.slotString}";
    }

    Map<String, dynamic> jsonBody = {
      "pickup_id": pickupAddressId,
      "delivery_id": deliveryAddressId,
      "pickup_slot": pickup_slot,
      "delivery_slot": delivery_slot,
      "currency": "XOF",
      "service_slug": widget.services,
      "customer_id": widget.profile.user.id,
      "delivery_formula_slug": formulaSelected,
      "delivery_fees": "$feesDelivery",
    };

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                new CheckOutScreen(widget.profile, jsonBody)));
  }

  Widget _buildAddressesView() {
    return new Container(
      child: Column(
        children: <Widget>[
          new Offstage(
            offstage: !hasPickup,
            child: Container(
              height: 80.0,
              width: double.infinity,
              child: ListView(
                scrollDirection: Axis.vertical,
                children: hasPickup
                    ? <Widget>[
                        new Container(
                          padding: EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                  bottom: BorderSide(
                                      width: 2.0, color: Colors.black12))),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Container(
                                child: new Row(
                                  children: <Widget>[
                                    new Text(
                                      "P",
                                      style: new TextStyle(
                                          color: AppColors.nopalmaBlue,
                                          fontFamily: "ArialRoundedMT",
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0),
                                    ),
                                    SizedBox(
                                      width: 20.0,
                                    ),
                                    new Container(
                                      width: MediaQuery.of(context).size.width -
                                          160.0,
                                      child: new Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          new Text(
                                              pickupAddress == null
                                                  ? LangStrings.gs(
                                                      "address_not_available_message")
                                                  : pickupAddress.address_name,
                                              style: new TextStyle(
                                                  color: Colors.black,
                                                  fontFamily: "ArialRoundedMT",
                                                  fontSize: 15.0))
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              new Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: IconButton(
                                      icon: Icon(
                                        Icons.do_not_disturb_on,
                                        size: 24.0,
                                      ),
                                      onPressed: () {
                                        // deleteAddress(pickupAddress.id);
                                        setState(() {
                                          pickupAddress = null;
                                        });
                                      }))
                            ],
                          ),
                        )
                      ]
                    : <Widget>[],
              ),
            ),
          ),
          new Offstage(
            offstage: !hasDelivery,
            child: Container(
              height: 80.0,
              width: double.infinity,
              child: ListView(
                scrollDirection: Axis.vertical,
                children: hasDelivery
                    ? <Widget>[
                        new Container(
                          width: double.infinity,
                          padding: EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                  bottom: BorderSide(
                                      width: 2.0, color: Colors.black12))),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Container(
                                child: new Row(
                                  children: <Widget>[
                                    new Text(
                                      "D",
                                      style: new TextStyle(
                                          color: AppColors.nopalmaBlue,
                                          fontFamily: "ArialRoundedMT",
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0),
                                    ),
                                    SizedBox(
                                      width: 15.0,
                                    ),
                                    new Container(
                                        width:
                                            MediaQuery.of(context).size.width -
                                                160.0,
                                        child: new Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            new Text(
                                                deliveryAddress == null
                                                    ? LangStrings.gs(
                                                        "address_not_available_message")
                                                    : deliveryAddress
                                                        .address_name,
                                                style: new TextStyle(
                                                    color: Colors.black,
                                                    fontFamily:
                                                        "ArialRoundedMT",
                                                    fontSize: 15.0))
                                          ],
                                        ))
                                  ],
                                ),
                              ),
                              new Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: IconButton(
                                      icon: Icon(
                                        Icons.do_not_disturb_on,
                                        size: 24.0,
                                      ),
                                      onPressed: () {
                                        //deleteAddress(deliveryAddress.id);
                                        setState(() {
                                          deliveryAddress = null;
                                        });
                                      }))
                            ],
                          ),
                        )
                      ]
                    : <Widget>[],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _dateSlot(Slot slot, Locale defaultLocale) {
    return new Text(
      /*"sunday, 5 may"*/ slot == null
          ? ""
          : DateUtil.formatDateLetters(
              slot.startTime.toIso8601String(), defaultLocale),
      style: new TextStyle(
          color: Colors.grey,
          fontFamily: "ArialRoundedMT",
          fontSize: 13.0,
          fontWeight: FontWeight.bold),
    );
  }

  Widget _timesSlot(Slot slot) {
    String mSlot = "";
    if (slot != null) {
      String startTimeString =
          DateUtil.formatTime(slot.startTime.toIso8601String(), defaultLocale);
      String endTimeString =
          DateUtil.formatTime(slot.endTime.toIso8601String(), defaultLocale);
      mSlot = startTimeString + " - " + endTimeString;
    }

    return new Text(
      /*"04:46 AM - 08:30 AM"*/ mSlot,
      style: new TextStyle(
          color: Colors.black,
          fontFamily: "ArialRoundedMT",
          fontSize: 14.0,
          fontWeight: FontWeight.bold),
    );
  }

  void chooseAddress(String s) async {
    DeliveryAddress mdeliveryAddress = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => new ChooseDeliveryAddress(widget.profile)),
    );

    setState(() {
      if (s == "pickup") {
        this.pickupAddress = mdeliveryAddress;
        hasPickup = pickupAddress != null;
      } else {
        this.deliveryAddress = mdeliveryAddress;
        hasDelivery = deliveryAddress != null;
      }
    });
  }

  Widget showSlot(Slot slot, bool isPickup, Locale defaultLocale) {
    if (slot == null) {
      return InkWell(
        onTap: () {
          showSlotPicker(slot, isPickup);
        },
        child: new Text(
          LangStrings.gs("field_add_slot_title"),
          style: TextStyle(
              decoration: TextDecoration.underline,
              color: Colors.blue,
              fontWeight: FontWeight.bold,
              fontFamily: "ArialRoundedMT",
              fontSize: 16.0),
        ),
      );
    } else {
      return new InkWell(
        //(MediaQuery.of(context).size.width - 20) /2,
        onTap: () {
          showSlotPicker(slot, isPickup);
        },
        child: new Row(
          children: <Widget>[
            new Container(
              alignment: Alignment.topLeft,
              width: (MediaQuery.of(context).size.width - 170) / 2,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  _timesSlot(slot),
                  _dateSlot(slot, defaultLocale),
                ],
              ),
            ),
            new Container(
              child: IconButton(
                icon: new Icon(
                  Icons.edit,
                  size: 15.0,
                  color: AppColors.nopalmaBlue,
                ),
                onPressed: () {
                  showSlotPicker(slot, isPickup);
                },
              ),
            )
          ],
        ),
      );
    }
  }

  void showSlotPicker(Slot slot, bool isPickup) async {
    DateTime slotDate = DateTime.now();
    // DateTime slotDate = DateTime.now().add(new Duration(days:1));

    if (!isPickup) {

      slotDate = pickupSlot.endTime;
      int days = formulaSelected == "lite" ? 2 : 1;
      slotDate = slotDate.add(Duration(days: days));
    }

    // Slot slotDayResult = await();

    Slot slotResult = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => new SlotScreen(slot, slotDate)),
    );

    setState(() {
      if (slotResult != null) {
        /* DateTime deliveryDateSlot = new DateTime(
            slotResult.startTime.year,
            slotResult.startTime.month,
            slotResult.startTime.day,
            slotResult.startTime.hour,
            slotResult.startTime.minute); */

        if (isPickup) {
          // slotDate = deliveryDateSlot;
          pickupSlot = slotResult;
          // slotDate = slotResult;
          // print("Slot ===> $slot}");
          // print("pickupSlot ===> $deliveryDateSlot");
        } else {
          deliverySlot = slotResult;
        }

        if (pickupSlot != null && deliverySlot != null) {
          hasSlot = true;
        }
      }
    }
    );
    
    
  }

  void showProgress() {
    setState(() {
      _loading = false;
    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }

  Widget progressView() {
    return new Stack(
      children: [
        new Container(color: Colors.white),
        new Opacity(
          opacity: 0.3,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  /* void createOrder(Map<String, dynamic> jsonBody){

    final String jsonBodyString = jsonEncode(jsonBody);

    print("Body $jsonBodyString");

    OrderRepository.create(jsonBodyString, showProgress, dismissProgress)
        .then((value){


      if(value.success){

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => new OrderCompleteScreen(widget.profile, value.data.order)),
        );
      }else{

        new Dialogs().simpleError(
            context,
            LangStrings.gs("dialog_error_title"),
            value.message,(){});

      }

    })
        .catchError((error){
      print("Error found $error");

      new Dialogs().simpleError(
          context,
          LangStrings.gs("dialog_error_title"),
          LangStrings.gs("dialog_error_message"),(){});
    });
  }*/
}
