import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/generic_ui/choose_delivery_address_item_widget.dart';
import 'package:easy_services/models/delivery_address.dart';
import 'package:easy_services/openstreetmap_places/OpenStreetMapPlacesWidget.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/DeliveryAddressRepository.dart';
import 'package:easy_services/rest/response/customer_delivery_address_result.dart';
import 'package:easy_services/rest/response/delete_delivery_address_result.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';

class ChooseDeliveryAddress extends StatefulWidget {

  Registrationdata profile;

  ChooseDeliveryAddress(this.profile);


  @override
  _ChooseDeliveryAddressState createState() => _ChooseDeliveryAddressState();

}

class _ChooseDeliveryAddressState extends State<ChooseDeliveryAddress> {

  DeliveryAddress placeHouse, placeOffice, placeOther;

  List<DeliveryAddress> deliveryAddresses;

  DeliveryAddress selectedAddress;

  bool _loading = true;


  @override
  void initState() {
    super.initState();

    loadCustomerAddress();

  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
        appBar: new AppBar(
          primary: true,
          centerTitle: false,
          title: new Text(LangStrings.gs("screen_choose_address_title"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
        ),
        body: new Stack(
          children: <Widget>[
            new SingleChildScrollView(

              child: new Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container(
                    child: new Column(
                      children: <Widget>[
                        SizedBox(height: 10.0,),
                        new ChooseDeliveryAddressItemWidget(
                            locationTypeImage: "house-outline.png",
                            locationTypeTitle: LangStrings.gs("location_type_house_title"),
                            position: 1,
                            radioGroupValue: selectedValue("house"),
                            handleRadioValueChange:(int value){
                              setState(() {
                                if(value >0){
                                  selectedAddress = placeHouse;
                                }
                              });
                            },
                            deliveryAddress:placeHouse,
                            addButtonCallback:(){

                              retrieveLocation("house");

                            }, closeButtonCallback:(){
                              if(placeHouse != null){
                                deleteAddress(placeHouse.id, placeHouse.address_type );
                              }
                        }),
                        new ChooseDeliveryAddressItemWidget(
                            locationTypeImage: "working-with-laptop.png",
                            locationTypeTitle: LangStrings.gs("location_type_office_title"),
                            position: 2,
                            radioGroupValue: selectedValue("office"),
                            handleRadioValueChange:(int value){
                              setState(() {
                                if(value >0){
                                  selectedAddress = placeOffice;
                                }

                              });
                            },
                            deliveryAddress:placeOffice,
                            addButtonCallback:(){

                              retrieveLocation("office");

                            }, closeButtonCallback:(){

                                if(placeOffice != null){
                                  deleteAddress(placeOffice.id, placeOffice.address_type );
                                }
                        }),
                        new ChooseDeliveryAddressItemWidget(
                            locationTypeImage: "placeholder@2x.png",
                            locationTypeTitle: LangStrings.gs("location_type_others_title"),
                            position: 3,
                            radioGroupValue: selectedValue("other"),
                            handleRadioValueChange:(int value){
                              setState(() {
                                if(value >0){
                                  selectedAddress = placeOther;
                                }
                              });
                            },
                            deliveryAddress:placeOther,
                            addButtonCallback:(){

                              retrieveLocation("other");

                            }, closeButtonCallback:(){

                              if(placeOther != null){
                                deleteAddress(placeOther.id, placeOffice.address_type);
                              }
                        })
                      ],
                    ),
                  ),

                  new Container(
                    height: 70.0,
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 50.0, bottom: 80.0),
                    padding: EdgeInsets.only(left: 15.0, right: 15.0),
                    child: RaisedButton(
                      color: selectedAddress !=null?AppColors.nopalmaYellow:Colors.grey,
                      padding: const EdgeInsets.all(10.0),
                      shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0)
                    ),
                      onPressed:selectedAddress !=null ?(){

                        Navigator.pop(context, selectedAddress);

                      }:null,
                      child: new Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(LangStrings.gs("button_next_title"),
                          style: TextStyle(color: Colors.white, fontSize: 20.0,
                              fontWeight: FontWeight.bold, fontFamily: 'ArialRoundedMT'),),
                      ),

                    ),
                  )

                ],
              ),
            ),
            new Offstage(
              offstage: _loading,
              child: progressView(),
            )
          ],
        )
    );

  }

  void retrieveLocation(String type) async{
    DeliveryAddress deliveryAddress  = await Navigator.push(
      context,
     // MaterialPageRoute(builder: (context) => new MapboxAutocompleteScreen(profile:widget.profile,type:type)),
      MaterialPageRoute(builder: (context) => new OpenStreetMapPlacesWidget(profile:widget.profile,type:type)),
    );

    setState(() {
      if(deliveryAddress.address_type == "house"){
        placeHouse = deliveryAddress;
      }else if(deliveryAddress.address_type  == "office"){
        placeOffice = deliveryAddress;
      }else if(deliveryAddress.address_type  == "other"){
        placeOther = deliveryAddress;
      }
    });


  }

  void loadCustomerAddress()async {
    DeliveryAddressRepository
        .find(widget.profile.user.id,
        showProgress,
        dismissProgress)
        .then((CustomerDeliveryAddressResult result){
      if(result.success){
        if(result.data != null){
          setState(() {
            deliveryAddresses = result.data;

            if(deliveryAddresses != null && deliveryAddresses.isNotEmpty){

              if(deliveryAddresses.where((da)=>da.address_type == "house") != null
               && deliveryAddresses.where((da)=>da.address_type == "house").length >0 ) {
                placeHouse = deliveryAddresses.where((da)=>da.address_type == "house").first;
              }else{
                placeHouse = null;
              }

              if(deliveryAddresses.where((da)=>da.address_type == "office") != null
                  && deliveryAddresses.where((da)=>da.address_type == "office").length >0 ) {
                placeOffice = deliveryAddresses.where((da)=>da.address_type == "office").first;
              }else{
                placeOffice = null;
              }

              if(deliveryAddresses.where((da)=>da.address_type == "other") != null
                  && deliveryAddresses.where((da)=>da.address_type == "other").length >0 ) {
                placeOther = deliveryAddresses.where((da)=>da.address_type == "other").first;
              }else{
                placeOther = null;
              }

            }else{
              placeHouse = null;
              placeOffice = null;
              placeOther = null;
              selectedAddress = null;
            }

            refreshSelection();


          });
        }else{
          setState(() {
            deliveryAddresses = [];
            placeHouse = null;
            placeOffice = null;
            placeOther = null;
            selectedAddress = null;
          });
        }

      }else{
        setState(() {
          deliveryAddresses = [];
          placeHouse = null;
          placeOffice = null;
          placeOther = null;
          selectedAddress = null;
        });
      }
    }).catchError((error){
      print("Error found $error");
      placeHouse = null;
      placeOffice = null;
      placeOther = null;
      selectedAddress = null;
    });

  }

  int selectedValue(String type){
    if(selectedAddress!= null){
      print("SelectedAddress $type ${selectedAddress.id}");
      return selectedAddress.id;
    }else{
      return -1;
    }
  }

  Widget progressView(){
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  void showProgress() {
    setState(() {
      _loading = false;

    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }

  void refreshSelection() {
   setState(() {
     if(placeHouse != null){
       selectedAddress = placeHouse;
     }else{
       // ignore: unnecessary_statements
       selectedAddress = null;
     }

     if(selectedAddress == null && placeOffice != null){
       selectedAddress = placeOffice;
     }

     if(selectedAddress == null && placeOther != null){
       selectedAddress = placeOther;
     }
   });

  }

  void deleteAddress(int id, String type) {

    DeliveryAddressRepository.delete(id, showProgress, dismissProgress)
        .then((DeleteDeliveryAddressResult value){
      if(value.success){

        if(type == "house"){
          placeHouse = null;
          selectedAddress = null;

        }else if(type == "office"){
          placeOffice = null;
          selectedAddress = null;

        }else if(type == "other"){
          placeOther = null;
          selectedAddress = null;

        }
        refreshSelection();

      }
    }).catchError((error){
      new Dialogs().error(
          context,
          'Operation Error',
          error.toString(),(){

        deleteAddress(id, type);

      },(){});
    });

  }

}
