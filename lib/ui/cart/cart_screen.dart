import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/CartItemWidget.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/models/cart.dart';
import 'package:easy_services/models/cart_item.dart';
import 'package:easy_services/models/artisan_category.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/cart/choose_artisan_time_slot_screen.dart';
import 'package:easy_services/ui/main/main_screen.dart';
import 'package:easy_services/ui/cart/choose_type_of_delivery_screen.dart';
import 'package:easy_services/util/CartManager.dart';
import 'package:flutter/material.dart';

class CartScreen extends StatefulWidget {
  final Registrationdata profile;

  CartScreen(this.profile);

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  Cart _cart;

  List<CartItem> _cartItems;

  String currency = "XOF";

  double subTotal = 0.0;

  @override
  void initState() {
    loadCart();
    super.initState();
  }

  void loadCart() {
    CartManager.obtainCart((cart) {
      if (mounted) {
        setState(() {
          _cart = cart;
          calculateSubTotal();
        });
      }
    });
  }

  void calculateSubTotal() {
    subTotal = 0.0;
    if (_cart != null &&
        _cart.cartItems != null &&
        _cart.cartItems.isNotEmpty) {
      _cart.cartItems.forEach((CartItem cartItem) {
        subTotal += double.parse(cartItem.amount);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_cart != null &&
        _cart.cartItems != null &&
        _cart.cartItems.isNotEmpty) {
      currency = _cart.cartItems.first.currency;
      _cartItems = _cart.cartItems;
    } else {
      currency = "XOF";
      _cartItems = [];
    }

    calculateSubTotal();

    return new WillPopScope(
      onWillPop: () {
        Navigator.of(context).pop(true);

        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new MainScreen(profile: widget.profile, position: 0)));
      },
      child: new Scaffold(
        appBar: new AppBar(
          primary: true,
          title: new Text(LangStrings.gs("screen_mycart_title"),
              style: new TextStyle(
                  color: Colors.white, fontFamily: 'ArialRoundedMT')),
          actions: <Widget>[
            IconButton(
                icon: Icon(
                  Icons.clear_all,
                  color: Colors.white,
                ),
                onPressed: () {
                  new Dialogs().confirm(
                      context,
                      LangStrings.gs("dialog_delete_item_title"),
                      LangStrings.gs("dialog_delete_item_message"), () {
                    Navigator.pop(context);
                  }, () async {
                    await CartManager.emptyCart().then((value) {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  new CartScreen(widget.profile)));
                    });
                  });
                })
          ],
        ),
        body: _cartItems.isEmpty
            ? new Container(
                color: Colors.white,
                child: new Center(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        Icons.do_not_disturb,
                        size: 80.0,
                        color: Colors.black12,
                      ),
                      new Text(
                        LangStrings.gs("screen_empty_cart_title"),
                        style: new TextStyle(
                            fontSize: 20.0,
                            fontFamily: "ArialRoundedMT",
                            color: Colors.grey),
                      ),
                    ],
                  ),
                ),
              )
            : ListView(
                children: _cartItems
                    .map((sub) =>
                        new CartItemWidget(widget.profile, sub, (cartItem) {
                          deleteCartItem(cartItem);
                        }))
                    .toList(),
              ),
        bottomSheet: new Container(
          padding: EdgeInsets.all(10.0),
          margin: EdgeInsets.only(top: 18.0),
          child: new ListTile(
            title: new Text(
              LangStrings.gs("subtotal_title"),
              style: new TextStyle(fontFamily: "ArialRoundedMT"),
            ),
            subtitle: new Text(
              "$subTotal $currency",
              style: new TextStyle(
                  fontFamily: "ArialRoundedMT", color: Colors.black),
            ),
            trailing: new Container(
              width: 100.0,
              height: 90.0,
              padding: EdgeInsets.all(8.0),
              child: new RaisedButton(
                  textColor: Colors.white,
                  color: AppColors.nopalmaYellow,
                  padding: EdgeInsets.all(8.0),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                  child: new Text(
                    LangStrings.gs("button_next_title"),
                    style: new TextStyle(
                        fontSize: 18.0,
                        color: Colors.white,
                        fontFamily: 'ArialRoundedMT',
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    goNext();
                  }),
            ),
          ),
        ),
        floatingActionButton: new FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        new MainScreen(profile: widget.profile, position: 0)));
          },
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          backgroundColor: AppColors.nopalmaBlue,
        ),
      ),
    );
  }

  void goNext() {
    if (_cart.cartItems.isNotEmpty) {
      List<String> serviceLists =
          _cart.cartItems.map((c) => c.service_slug).toList();

      String services = jsonEncode(serviceLists);
      // ArtisanCategory artisanCategory_slug = ;
      if (services.contains("ouvrier_et_artisan")) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new ChooseArtisanTimeSlot(widget.profile, services)));
      } else {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new ChooseTypeOfDeliveryScreen(widget.profile, services)));
      }
    }
  }

  void deleteCartItem(CartItem cartItem) async {
    new Dialogs().confirm(context, LangStrings.gs("dialog_delete_item_title"),
        LangStrings.gs("dialog_delete_item_message"), () {
      Navigator.pop(context);
    }, () async {
      await CartManager.removeToCart(cartItem).then((value) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => new CartScreen(widget.profile)));
      });
    });
  }
}
