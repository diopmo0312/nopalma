import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/delivery_address.dart';
import 'package:easy_services/models/order.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/main/main_screen.dart';
import 'package:easy_services/ui/orders/detail/show_order_item_screen.dart';
import 'package:flutter/material.dart';

import 'dart:convert';

class OrderCompleteScreen extends StatefulWidget {

  final Registrationdata profile;

  final Order order;

  OrderCompleteScreen(this.profile, this.order);

  @override
  _OrderCompleteScreenState createState() => _OrderCompleteScreenState();

}

class _OrderCompleteScreenState extends State<OrderCompleteScreen> {

  double tax;

  double total;
  int feesDelivery = 1500;
  double subTotal = 0;

  List<int> itemTotals = [];

  List<String> servicesSlugs;

  @override
  void initState() {
    servicesSlugs = getServices(widget.order.service_slug).map((s)=>s.slug).toList();
    super.initState();
      if(widget.order.delivery_fees != null){
      feesDelivery = int.parse( widget.order.delivery_fees);
    }
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        primary: true,
        title: new Text(LangStrings.gs("screen_order_summary_title"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
      ),
      body: new SingleChildScrollView(
        child: new Container(
          color: AppColors.getColorHexFromStr("#f4fafd"),
          child: new Column(
            children: <Widget>[
              new Container(
                margin: EdgeInsets.all(10.0),
                padding: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(color: AppColors.getColorHexFromStr("#dedede")),
                  color: Colors.white,
                  
                ),
                child: new ListTile(
                  leading: Image.asset("assets/product11@2x.png",width: 70.0, height: 70.0,fit: BoxFit.contain,),
                  title: new Text('${LangStrings.gs("field_order_numero_title")} : ${widget.order.id}', style: new TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontFamily: "ArialRoundedMT",fontSize: 18.0),),
                  subtitle: new Text(getStatus(widget.order), style: new TextStyle(color: AppColors.getColorHexFromStr("#3dc0c6"), fontFamily: "ArialRoundedMT",fontSize: 16.0),),
                  trailing: IconButton(icon: Icon(Icons.keyboard_arrow_down), onPressed: (){

                  }),
                ),
              ),

              new Container(
                height: MediaQuery.of(context).size.height - 400.0,
                width: double.infinity,
                color: AppColors.getColorHexFromStr("#f4fafd"),
                child: ListView(
                  children: <Widget>[

                    new Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                      ),
                      child: new Column(
                        children: <Widget>[
                          ListTile(
                            title: new Text(LangStrings.gs("field_services_title"), style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: 18.0),),
                            subtitle: createServices(widget.order.service_slug)
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                      ),
                      child: new Column(
                        children: <Widget>[
                          ListTile(
                            title: new Text(LangStrings.gs("field_pickup_date_title"), style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: 18.0),),
                            subtitle: new Text(widget.order.pickup_slot/*'28 March 2019, 10:00 between 13:00'*/, style: new TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 16.0)),
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                      ),
                      child: new Column(
                        children: <Widget>[
                          ListTile(
                            title: new Text(LangStrings.gs("field_pickup_address_title"), style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: 18.0),),
                            subtitle: new Text(getAddressName(widget.order.pickup)/*'211, Riviéra Palmeraie, Abidjan, Côte d\'Ivoire'*/, style: new TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 16.0)),
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                      ),
                      child: new Column(
                        children: <Widget>[
                          ListTile(
                            title: new Text(LangStrings.gs("field_delivery_date_title"), style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: 18.0),),
                            subtitle: new Text(widget.order.delivery_slot/*'28 March 2019, 10:00 AM -  13:00 AM'*/, style: new TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 16.0)),
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                      ),
                      child: new Column(
                        children: <Widget>[
                          ListTile(
                            title: new Text(LangStrings.gs("field_delivery_address_title"), style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: 18.0),),
                            subtitle: new Text(getAddressName(widget.order.delivery)/*'211, Riviéra Palmeraie, Abidjan, Côte d\'Ivoire'*/, style: new TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 16.0)),
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                      ),
                      child: new Column(
                        children: <Widget>[
                          ListTile(
                            title: new Text(LangStrings.gs("field_amount_title"), style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: 18.0),),
                            subtitle: new Text(getAmount(widget.order), style: new TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 16.0)),
                            trailing: new Container(
                              padding: EdgeInsets.all(6.0),
                              decoration: new BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  border: Border.all(color: Colors.black12)
                              ),
                              child: new Text(LangStrings.gs("field_invoice_title"),style: new TextStyle(color: Colors.black),),
                            ),
                            onTap: (){
                              if(widget.order != null && widget.order.items != null && widget.order.items.isNotEmpty){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => new ShowOrderItemScreen(widget.profile,widget.order)),
                                );
                              }

                            },
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              new Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 20.0,right: 20.0),

                child: new RaisedButton(onPressed: (){

                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => new MainScreen(profile:widget.profile, position: 1)),
                  );

                },
                    color: AppColors.nopalmaYellow,
                    textColor: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: new Text(LangStrings.gs("button_terminate_title"),style: new TextStyle(fontFamily: 'ArialRoundedMT', fontSize: 18.0,color:Colors.white), ),
                    ),
                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0),
                        side: BorderSide(color: AppColors.nopalmaYellow))


                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  List<Service> getServices(String serviceSlug){
    List<dynamic> services = jsonDecode(serviceSlug);
    List<String> servicesStrings = services.map((c){
            String serviceString   = c.toString();
            if(serviceString != null){
              return serviceString;
            }else{
              return "";
            }

          }).toList();

          return  servicesStrings
          .map((s)=>widget.profile.services.firstWhere((service)=> service.slug == s)).toList();
   
  }

  Widget createServices(String serviceSlug){
   

    List<Widget> views = getServices(serviceSlug)
    .map((service)=>new Container(
       padding: new EdgeInsets.all(8.0),
       margin: EdgeInsets.only(left:10.0 ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        border: Border.all(color: Colors.black12)
      ),
      child: new Text(LangStrings.strLang=='FR'?service.name:service.name_en, style: new TextStyle(fontSize:18.0,color: Colors.black, fontFamily: 'ArialRoundedMT'),),
    )).toList();

     var view  = new Container(
       height: 45,
      child: ListView(
        children: views,
        scrollDirection:Axis.horizontal
      ),
    );


    return view;
  }


  String getStatus(Order o) {
    if(o.current_status == "is-waiting"){
      return LangStrings.gs("order_is_waiting_message");
    }else if(o.current_status == "managed"){
      return LangStrings.gs("order_is_managed_message");
    }else if(o.current_status == "started"){
      return LangStrings.gs("order_is_started_message");
    }else if(o.current_status == "successful"){
      return LangStrings.gs("order_is_successful_message");
    }else if(o.current_status == "failed"){
      return LangStrings.gs("order_is_failed_message");
    }else if(o.current_status == "cancel"){
      return LangStrings.gs("order_is_cancel_message");
    }else{
      return LangStrings.gs("order_is_waiting_message");
    }
  }


  String getAddressName(DeliveryAddress address) {
    if(address != null){
      return address.address_name;
    }else{
      return LangStrings.gs("address_not_available_message");
    }

  }



  String getAmount(Order order) {
    if(order != null){

      if(order.invoice != null){
        return order.invoice.total+" "+order.invoice.currency;
      }else{
        return "0 XOF";
      }
    }else{
      return "0 XOF";
    }


  }



}
