import 'dart:convert';
import 'dart:io';
import 'package:easy_services/models/order.dart';
import 'package:easy_services/rest/response/order/OrderData.dart';
import 'package:easy_services/ui/cinetpay/CinetpayPaymentScreen.dart';
import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/CartItemWidget.dart';
import 'package:easy_services/generic_ui/ArtisanCartItemWidget.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/models/cart.dart';
import 'package:easy_services/models/cart_item.dart';
import 'package:easy_services/models/payment_method.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/OrderRepository.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/cinetpay/PaymentResponse.dart';
import 'package:easy_services/ui/touchpay/TouchPayScreen.dart';
import 'package:easy_services/util/constants.dart';
import 'package:easy_services/ui/cart/order_complete_screen.dart';
import 'package:easy_services/util/CartManager.dart';
import 'package:flutter/material.dart';

class ArtisanCheckOutScreen extends StatefulWidget {
  final Registrationdata profile;
  final Map<String, dynamic> jsonBody;

  ArtisanCheckOutScreen(this.profile, this.jsonBody);

  @override
  _ArtisanCheckOutScreenState createState() => _ArtisanCheckOutScreenState();
}

class _ArtisanCheckOutScreenState extends State<ArtisanCheckOutScreen> {
  Cart _cart;

  List<CartItem> _cartItems;

  double tax;

  double total;
  int feesDelivery = 1500;
  double subTotal = 0.0;

  List<int> itemTotals = [];

  bool _loading = true;

  List<PaymentMethod> paymentMethods;

  String paymentMethodSelected;

  @override
  void initState() {
// print(widget.jsonBody);
// print(widget.jsonBody);

    if (widget.profile.payment_methods != null &&
        widget.profile.payment_methods.isNotEmpty) {
      paymentMethods = widget.profile.payment_methods;
      paymentMethodSelected = "online-payment";
    } else {
      paymentMethods = [];
    }

    feesDelivery = int.parse(widget.jsonBody["delivery_fees"]);

    loadCart();

    super.initState();
  }

  void loadCart() {
    CartManager.obtainCart((cart) {
      setState(() {
        _cart = cart;
        print("CART ${_cart.toString()}");
        calculateInvoice();
      });
    });
  }

  void calculateInvoice() {
    subTotal = 0.0;
    if (_cart.cartItems != null && _cart.cartItems.isNotEmpty) {
      _cart.cartItems.forEach((CartItem cartItem) {
        subTotal += double.parse(cartItem.amount);
      });
    }

    tax = subTotal * 0.18;
    total = tax + subTotal + feesDelivery;
  }

  @override
  Widget build(BuildContext context) {
    if (_cart != null) {
      _cartItems = _cart.cartItems;
    } else {
      _cartItems = [];
    }

    //paymentMethodSelected = paymentMethods[0].slug;

    return new Scaffold(
      appBar: new AppBar(
        primary: true,
        title: new Text(LangStrings.gs("screen_checkout_title"),
            style: new TextStyle(
                color: Colors.white, fontFamily: 'ArialRoundedMT')),
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            height: MediaQuery.of(context).size.height - 80.0,
            child: new Column(
              children: <Widget>[
                new Expanded(
                  child: new Container(
                    color: AppColors.getColorHexFromStr("#f2f9fd"),
                    child: new ArtisanCartItemWidget(
                        widget.profile, _cartItems[0]),
                  ),
                ),
                new Container(
                  padding: EdgeInsets.only(
                      bottom: 15.0, right: 15.0, left: 15.0, top: 15.0),
                  decoration: BoxDecoration(
                      border:
                          Border(bottom: BorderSide(color: Colors.black12))),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Text(
                        LangStrings.gs("field_subtitle_title"),
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold),
                      ),
                      new Text(
                        '$subTotal XOF',
                        style: new TextStyle(
                            color: Colors.black45,
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                new Container(
                  padding: EdgeInsets.only(
                      bottom: 15.0, right: 15.0, left: 15.0, top: 15.0),
                  decoration: BoxDecoration(
                      border:
                          Border(bottom: BorderSide(color: Colors.black12))),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Text(
                        LangStrings.gs("field_delivery_fees_title"),
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold),
                      ),
                      new Text(
                        '$feesDelivery XOF',
                        style: new TextStyle(
                            color: Colors.black45,
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                new Container(
                  padding: EdgeInsets.only(
                      bottom: 15.0, right: 15.0, left: 15.0, top: 15.0),
                  decoration: BoxDecoration(
                      border:
                          Border(bottom: BorderSide(color: Colors.black12))),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Text(
                        LangStrings.gs("field_tax_title"),
                        style: new TextStyle(
                            fontFamily: 'ArialRoundedMT',
                            color: Colors.black,
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold),
                      ),
                      new Text(
                        '$tax XOF',
                        style: new TextStyle(
                            fontFamily: 'ArialRoundedMT',
                            color: Colors.black45,
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                new Container(
                  padding: EdgeInsets.only(
                      bottom: 15.0, right: 15.0, left: 15.0, top: 15.0),
                  decoration: BoxDecoration(
                      border:
                          Border(bottom: BorderSide(color: Colors.black12))),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Text(
                        LangStrings.gs("field_total_title"),
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold),
                      ),
                      new Text(
                        '$total XOF',
                        style: new TextStyle(
                            color: AppColors.nopalmaYellow,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                new Container(
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        margin: EdgeInsets.only(top: 18.0),
                        padding: EdgeInsets.only(left: 15.0),
                        alignment: Alignment.topLeft,
                        child: new Text(
                            LangStrings.gs("field_choose_payment_method_title"),
                            style: new TextStyle(
                                color: Colors.grey,
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold)),
                      ),
                      new Container(
                        height: 60.0 * paymentMethods.length,
                        child: Column(
                          children: paymentMethods
                              .map((p) => new Container(
                                    child: new Row(
                                      children: <Widget>[
                                        new Radio(
                                          value: p.slug,
                                          groupValue: paymentMethodSelected,
                                          onChanged:
                                              (String paymentMethodValue) {
                                            if (mounted) {
                                              setState(() {
                                                paymentMethodSelected =
                                                    paymentMethodValue;
                                              });
                                            }
                                          },
                                        ),
                                        new SizedBox(
                                          width: 10.0,
                                        ),
                                        new Text(
                                            (LangStrings.strLang == 'FR')
                                                ? p.title
                                                : p.title_en,
                                            style: new TextStyle(
                                                color: Colors.black,
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.bold))
                                      ],
                                    ),
                                  ))
                              .toList(),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  width: double.infinity,
                  height: 80.0,
                  padding: EdgeInsets.all(10.0),
                  child: new RaisedButton(
                      textColor: Colors.white,
                      color: AppColors.nopalmaYellow,
                      padding: EdgeInsets.all(10.0),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                      child: new Text(
                        LangStrings.gs("button_next_title"),
                        style: new TextStyle(
                            fontSize: 18.0,
                            color: Colors.white,
                            fontFamily: 'ArialRoundedMT',
                            fontWeight: FontWeight.bold),
                      ),
                      onPressed: () {
                        goNext();
                      }),
                ),
                SizedBox(
                  height: 20.0,
                )
              ],
            ),
          ),
          new Offstage(
            offstage: _loading,
            child: progressView(),
          )
        ],
      ),
    );
  }

  void deleteCartItem(CartItem cartItem) async {
    new Dialogs().confirm(context, LangStrings.gs("dialog_delete_item_title"),
        LangStrings.gs("dialog_delete_item_message"), () {
      Navigator.pop(context);
    }, () async {
      Cart cart = await CartManager.removeToCart(cartItem);
      loadCart();
    });
  }

  void showProgress() {
    setState(() {
      _loading = false;
    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }

  Widget progressView() {
    return new Stack(
      children: [
        new Container(color: Colors.white),
        new Opacity(
          opacity: 0.3,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  void goNext() {
    Map<String, dynamic> jsonBody = widget.jsonBody;
    jsonBody.addAll({
      "items": _cart.cartItems,
      "payment_method_slug": paymentMethodSelected
    });

    createOrder(jsonBody);
  }

  void createOrder(Map<String, dynamic> jsonBody) {
    final String jsonBodyString = jsonEncode(jsonBody);

    print("Body $jsonBodyString");

    OrderRepository.create(jsonBodyString, showProgress, dismissProgress)
        .then((value) {
      if (value.success) {
        if (paymentMethodSelected == "online-payment") {
          // goToPayment(value.data);

          goToTouchPayment(value.data);
        } else {
          CartManager.emptyCart();

          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new OrderCompleteScreen(widget.profile, value.data.order)),
          );
        }
      } else {
        new Dialogs().simpleError(context, LangStrings.gs("dialog_error_title"),
            value.message, () {});
      }
    }).catchError((error) {
      dismissProgress();
      print("LoginResult $error");
      var message;
      if (error is HttpException || error is SocketException) {
        message = LangStrings.gs("internet_error_message");
      } else {
        message = LangStrings.gs("dialog_error_message");
      }

      new Dialogs().simpleError(
          context, LangStrings.gs("dialog_error_title"), message, () {});
    });
  }

  void goToPayment(OrderData data) async {
    PaymentResponse paymentResponse = await Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => new CinetPaymentScreen(
                Constants.CINET_PAY_API_KEY,
                Constants.CINET_PAY_SITE_ID,
                Constants.CINET_PAY_NOTIFICATION_URL,
                double.parse(data.transaction.amount),
                data.transaction.payment_reference,
                'CFA',
                'Paiment en ligne de la commande - Nopalma',
                'Paiment en ligne de la commande - Nopalma')));

    if (paymentResponse != null) {
      if (paymentResponse.success) {
        new Dialogs()
            .showSuccess(context, "Félicitations", paymentResponse.message, () {
          CartManager.emptyCart();

          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new OrderCompleteScreen(widget.profile, data.order)),
          );
        });
      } else {
        new Dialogs().simpleError(
          context,
          "Erreur Rencontrée",
          paymentResponse.message,
          () {},
        );
      }
    } else {
      print('Error');

      new Dialogs().simpleError(
        context,
        "Erreur Rencontrée",
        "Nous rencontrons des erreurs avec le serveurs.",
        () {},
      );
    }
  }

  void goToTouchPayment(OrderData data) async {
    print(">>>> Profile >>> ${widget.profile}");
    print(">>>> Order >>> ${data.order}");
    /*PaymentResponse paymentResponse*/ var result =
        await Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => new TouchPayScreen(
                    profile: widget.profile,
                    order: data.order,
                    agencyCodeData: Constants.IN_TOUCH_PAY_AGENCE_CODE,
                    secretData: Constants.IN_TOUCH_PAY_TOKEN,
                    notificationUrlData:
                        Constants.IN_TOUCH_PAY_NOTIFICATION_URL,
                    domainNameData: Constants.IN_TOUCH_PAY_DOMAIN_NAME,
                    amountData: "${int.parse(data.transaction.amount)}",
                    orderNumberData: "${data.transaction.payment_reference}",
                    emailData: widget.profile.user.email == null
                        ? ""
                        : "${widget.profile.user.email}",
                    clientFirstname: "${widget.profile.user.first_name}",
                    clientLastname: "${widget.profile.user.last_name}",
                    clientPhone: "${widget.profile.user.phone_number}" // 6
                    )));

    /*
    if(paymentResponse != null){

      if(paymentResponse.success){

        new Dialogs ().showSuccess(context, "Félicitations", paymentResponse.message, (){


          CartManager.emptyCart();

          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => new OrderCompleteScreen(widget.profile, data.order)),
          );

        });

      }else{
        new Dialogs ().simpleError(context, "Erreur Rencontrée", paymentResponse.message, (){

        },);


      }
    }else{
      print('Error');

      new Dialogs().simpleError(context, "Erreur Rencontrée", "Nous rencontrons des erreurs avec le serveurs.", (){

      },);
    }*/

    OrderRepository.checkPayment(
            data.transaction.payment_reference, showProgress, dismissProgress)
        .then((value) {
      if (value.success && value.data.status == "SUCCESSFUL") {
        new Dialogs().showSuccess(
            context, "Félicitations", "Votre paiement est validé", () {
          CartManager.emptyCart();

          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new OrderCompleteScreen(widget.profile, data.order)),
          );
        });
      } else {
        new Dialogs().simpleError(
          context,
          "Erreur Rencontrée",
          "Erreur rencontrée",
          () {},
        );
      }
    }).catchError((error) {
      dismissProgress();
      print("PaymentResult $error");
      var message;
      if (error is HttpException || error is SocketException) {
        message = LangStrings.gs("internet_error_message");
      } else {
        message = LangStrings.gs("dialog_error_message");
      }

      new Dialogs().simpleError(
          context, LangStrings.gs("dialog_error_title"), message, () {});
    });
  }
}
