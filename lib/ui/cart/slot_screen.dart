import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/time_slot_picker/slot.dart';
import 'package:easy_services/time_slot_picker/time_slot_picker.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/util/DateUtil.dart';
import 'package:flutter/material.dart';

import 'package:date_utils/date_utils.dart';
import 'package:toast/toast.dart';



class SlotScreen extends StatefulWidget {

  Slot slot;

  DateTime currentDate;

  SlotScreen(this.slot, this.currentDate);

  @override
  _SlotScreenState createState() => _SlotScreenState();
}

class _SlotScreenState extends State<SlotScreen> {

  bool hasSelection = false;

  DateTime currentDate;

  Slot _selectedSlot;

  Locale defaultLocale;

  @override
  void initState() {
    super.initState();
    _selectedSlot = widget.slot;
    hasSelection = _selectedSlot != null;
    currentDate = widget.currentDate;

  }

  @override
  Widget build(BuildContext context) {

    defaultLocale = Locale(LangStrings.strLang.toLowerCase());

    return new Scaffold(
      appBar: new AppBar(
        primary: true,
        title: new Text(LangStrings.gs("screen_define_slot_title"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
      ),
      body: new SingleChildScrollView(
        child: new Container(
          padding: EdgeInsets.all(10.0),
          child: new Column(
            children: <Widget>[
        //       new Calendar(
        //         isExpandable: true,
        //         initialCalendarDateOverride: currentDate,
        // /*       dayBuilder: (BuildContext ctx,DateTime dataTime ){
        //
        //
        //
        //           if(dataTime.isBefore(widget.currentDate)){
        //
        //             return new CalendarTile(
        //               onDateSelected: (){},
        //               date: dataTime,
        //             );
        //           }else{
        //             print('Hello $dataTime');
        //             return new CalendarTile(
        //               onDateSelected: (){
        //                 setState(() {
        //                   currentDate = dataTime;
        //                 });
        //                 handleSelectedDateAndUserCallback(dataTime);
        //               },
        //               date: dataTime,
        //               isSelected: Utils.isSameDay(currentDate, dataTime),
        //             );
        //           }
        //
        //         },*/
        //         onDateSelected:  handleSelectedDateAndUserCallback,
        //       ),
              new SizedBox(height: 20.0,),
              new TimeSlotPicker(
                date:  currentDate/*.subtract(Duration(days: 5))*/, // (Optional)
                // slotBorder: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)), // (Optional)
                textStyle: TextStyle(color: Colors.blue,fontFamily:"ArialRoundedMT"), // (Optional)
                onTap: (DateTime startTime, DateTime endTime){ // (Required)


                  DateTime _startTime = new DateTime(currentDate.year, currentDate.month, currentDate.day, startTime.hour,startTime.minute);
                  DateTime _endTime = new DateTime(currentDate.year, currentDate.month, currentDate.day, endTime.hour,endTime.minute);

                  print(_startTime.toString() +" >> "+_endTime.toString());

                  Slot selectedSlot = new Slot();
                  selectedSlot.startTime = _startTime;
                  selectedSlot.endTime = _endTime;
                  selectedSlot.slotString = DateUtil.get24HourTo12HourString(_startTime) + " - " + DateUtil.get24HourTo12HourString(_endTime);
                  setState(() {
                    _selectedSlot = selectedSlot;
                    hasSelection = true;
                  });

                },
              ),

              new SizedBox(height: 20.0,),

              new Offstage(
                offstage: !hasSelection,
                child: new ListTile(
                  title: _dateSlot(_selectedSlot,defaultLocale),
                  subtitle: _timesSlot(_selectedSlot),
                  trailing: IconButton(icon: new Icon(Icons.do_not_disturb_on),onPressed: (){
                    setState(() {
                      _selectedSlot = null;
                      hasSelection = false;
                    });
                  },),
                ),
              ),
              new SizedBox(height: 20.0,),

              new Container(
                width: double.infinity,
                height: 80.0,
                padding: EdgeInsets.all(10.0),
                child: new RaisedButton(
                    textColor: Colors.white,
                    color: hasSelection?AppColors.nopalmaYellow:Colors.grey,
                    padding: EdgeInsets.all(10.0),
                    shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5.0)
                        ),
                    child: new Text(LangStrings.gs("button_next_title"), style: new TextStyle(fontSize:18.0,color: Colors.white, fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold), ),
                    onPressed: hasSelection?(){

                      goNext();

                    }:null),
              )
            ],
          ),
        ),
      ),
    );
  }

  void goNext() {

    Navigator.pop(context, _selectedSlot);
  }

  Widget _dateSlot(Slot slot, Locale defaultLocale){

    return  new Text(/*"15 Feb 2019"*/slot==null?"":DateUtil.formatDate(slot.startTime.toIso8601String(), defaultLocale), style: new TextStyle(color: Colors.black, fontFamily:"ArialRoundedMT",fontSize: 16.0, fontWeight: FontWeight.bold),);
  }
  Widget _timesSlot(Slot slot){
    String mSlot = "";
    if(slot != null){
         String startTimeString  = DateUtil.formatTime(slot.startTime.toIso8601String(), defaultLocale);
    String endTimeString  = DateUtil.formatTime(slot.endTime.toIso8601String(), defaultLocale);
    mSlot = startTimeString+" - "+endTimeString;
    }
   

    return  new Text(/*"04:46 AM - 08:30 AM"*/mSlot, style: new TextStyle(color: Colors.black, fontFamily:"ArialRoundedMT",fontSize: 16.0, fontWeight: FontWeight.bold),);

  }

  handleSelectedDateAndUserCallback(DateTime dataTimeSelected) {


     if(dataTimeSelected.isBefore(widget.currentDate)){



      setState(() {
        hasSelection = false;
      });

      new Dialogs().simpleError(context, 'Erreur sur la Date', 'Vous devez choisir une date après le ${DateUtil.formatDate(widget.currentDate.toIso8601String(), defaultLocale)}', (){});


     }else{
       setState(() {
         currentDate = dataTimeSelected;

         if(_selectedSlot!= null){
           DateTime _startTime = new DateTime(currentDate.year, currentDate.month, currentDate.day, _selectedSlot.startTime.hour,_selectedSlot.startTime.minute);
           DateTime _endTime = new DateTime(currentDate.year, currentDate.month, currentDate.day, _selectedSlot.endTime.hour,_selectedSlot.endTime.minute);;

           Slot selectedSlot = _selectedSlot;
           selectedSlot.startTime = _startTime;
           selectedSlot.endTime = _endTime;
           selectedSlot.slotString = DateUtil.get24HourTo12HourString(_startTime) + " - " + DateUtil.get24HourTo12HourString(_endTime);

           _selectedSlot = selectedSlot;
           hasSelection = true;

         }
       });
     }


  }
}


