import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/models/account_type.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/UserRepository.dart';
import 'package:easy_services/rest/response/registration_result.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/delivery_address_screen.dart';
import 'package:easy_services/ui/main/main_screen.dart';
import 'package:easy_services/util/LoginManager.dart';
import 'package:flutter/material.dart';

class EditProfileScreen extends StatefulWidget {

  final Registrationdata profile;

  EditProfileScreen(this.profile);

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();

}

class _EditProfileScreenState extends State<EditProfileScreen> {

  TextEditingController firstNameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController companyNameController = new TextEditingController();

  List<AccountType> accountTypes = [];

  AccountType _currentAccountType;

  bool _termsAccepted = false;
  bool hasCompany = false;

  void _termsAcceptedChanged(bool value) => setState(() => _termsAccepted = value);

  bool _loading = true;

  @override
  void initState() {
    super.initState();


    phoneController.text = widget.profile.user.phone;

    firstNameController.text = widget.profile.user.first_name;
    lastNameController.text = widget.profile.user.last_name;
    emailController.text = widget.profile.user.email;

    accountTypes.add(new AccountType("particulier", LangStrings.gs("particulier")));
    accountTypes.add(new AccountType("business", LangStrings.gs("business")));

    if(widget.profile.user.account_type == "particulier"){
      _currentAccountType = accountTypes[0];
    }else{
      _currentAccountType = accountTypes[1];
      companyNameController.text = widget.profile.user.company_name;
    }


    refreshView();

  }

  void refreshView(){

    setState(() {

      hasCompany = _currentAccountType.slug == "business";

    });
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
        appBar: new AppBar(
          primary: true,
          centerTitle: false,
          title: new Text(LangStrings.gs("screen_edit_profile_title"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
        ),
        body: new Stack(
          children: <Widget>[
            new SingleChildScrollView(
              child: new Container(
                padding: EdgeInsets.all(16.0),
                child: new Column(
                  children: <Widget>[
                    const SizedBox(height: 15.0,),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Text(LangStrings.gs("label_type_of_account"), textAlign: TextAlign.start, style: new TextStyle(fontWeight:FontWeight.bold, fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                    new Container(
                      padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0, bottom: 5.0),
                      margin: EdgeInsets.only(top: 5.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.0),
                          border: Border.all(color: Colors.black54)
                      ),
                      child: new DropdownButtonHideUnderline(
                        child: new DropdownButton(
                            isExpanded:true,
                            value: _currentAccountType.slug,
                            items: accountTypes.map((AccountType accountType){
                              return new DropdownMenuItem<String>(
                                  value:accountType.slug,
                                  child: new Text(accountType.name, style: new TextStyle(color: Colors.black, fontSize: 18.0, fontFamily: 'ArialRoundedMT'))
                              );
                            }).toList(),
                            onChanged: ((String itemSelected){
                              setState((){
                                _currentAccountType = accountTypes.where((AccountType aT)=>aT.slug == itemSelected).first;
                                refreshView();
                              });
                            })
                        ),
                      ),
                    ),
                    const SizedBox(height: 15.0,),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Text(LangStrings.gs("edit_phone_number"), textAlign: TextAlign.start, style: new TextStyle(fontWeight:FontWeight.bold, fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                    const SizedBox(height: 10.0,),
                    new TextFormField(
                      enabled: false,
                      controller: phoneController,
                      decoration: new InputDecoration(
                          contentPadding: EdgeInsets.all(20.0),
                          border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                          hintText: '01000000',
                          hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                      style: new TextStyle(color: Colors.black, fontSize: 18.0, fontFamily: 'ArialRoundedMT'),
                      keyboardType: TextInputType.text,

                    ),
                    const SizedBox(height: 15.0,),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Text(LangStrings.gs("edit_first_name"), textAlign: TextAlign.start, style: new TextStyle(fontWeight:FontWeight.bold,fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                    const SizedBox(height: 10.0,),
                    new TextFormField(
                      controller: firstNameController,
                      decoration: new InputDecoration(
                          contentPadding: EdgeInsets.all(20.0),
                          border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                          hintText: LangStrings.gs("edit_first_name"),
                          hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                      style: new TextStyle(color: Colors.black, fontSize: 18.0, fontFamily: 'ArialRoundedMT'),
                      keyboardType: TextInputType.text,

                    ),
                    const SizedBox(height: 15.0,),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Text(LangStrings.gs("edit_last_name"), textAlign: TextAlign.start, style: new TextStyle(fontWeight:FontWeight.bold,fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                    const SizedBox(height: 10.0,),
                    new TextFormField(
                      controller: lastNameController,
                      decoration: new InputDecoration(
                          contentPadding: EdgeInsets.all(20.0),
                          border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                          hintText: LangStrings.gs("edit_last_name"),
                          hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                      style: new TextStyle(color: Colors.black, fontSize: 18.0, fontFamily: 'ArialRoundedMT'),
                      keyboardType: TextInputType.text,

                    ),

                    const SizedBox(height: 15.0,),
                    new Container(
                        alignment: Alignment.topLeft,
                        child: new Text(LangStrings.gs("edit_email"), textAlign: TextAlign.start, style: new TextStyle(fontWeight:FontWeight.bold, fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                    const SizedBox(height: 10.0,),
                    new TextFormField(
                      controller: emailController,
                      decoration: new InputDecoration(
                          contentPadding: EdgeInsets.all(20.0),
                          border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                          hintText: LangStrings.gs("edit_email"),
                          hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                      style: new TextStyle(color: Colors.black, fontSize: 18.0, fontFamily: 'ArialRoundedMT'),
                      keyboardType: TextInputType.text,

                    ),
                    const SizedBox(height: 15.0,),
                    Offstage(
                      offstage: !hasCompany,
                      child: new Column(
                        children: <Widget>[
                          new Container(
                              alignment: Alignment.topLeft,
                              child: new Text(LangStrings.gs("label_edit_company_name"), textAlign: TextAlign.start, style: new TextStyle(fontWeight:FontWeight.bold, fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                          const SizedBox(height: 10.0,),
                          new TextFormField(
                            controller: companyNameController,
                            decoration: new InputDecoration(
                                contentPadding: EdgeInsets.all(20.0),
                                border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                                hintText: LangStrings.gs("edit_company_name"),
                                hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                            style: new TextStyle(color: Colors.black, fontSize: 18.0),
                            keyboardType: TextInputType.text,

                          )
                        ],
                      ),
                    ),




                    new Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(top: 20.0),
                      child: RaisedButton(
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5.0)
                        ),
                        color: AppColors.nopalmaYellow,
                        padding: const EdgeInsets.all(10.0),
                        onPressed:(){

                          String timeZone = DateTime.now().timeZoneName;

                          Map<String, String>  jsonBody = {
                            "dial_code":widget.profile.user.dial_code,
                            "country_code":widget.profile.user.country_code,
                            "timezone": timeZone,
                            "phone_number":widget.profile.user.phone_number,
                            "phone":widget.profile.user.phone,
                            "first_name":firstNameController.text.toString(),
                            "last_name":lastNameController.text.toString(),
                            "account_type":_currentAccountType.slug
                          };

                          if(emailController.text.contains('@')){
                            jsonBody["email"] = emailController.text;
                          }

                          if(_currentAccountType.slug =="business" && companyNameController.text.isNotEmpty){
                            jsonBody["company_name"] = companyNameController.text;
                          }
                          updateCustomer(widget.profile.user.id,jsonBody);

                        },
                        child: new Padding(padding: EdgeInsets.all(10.0),
                          child: new Text(LangStrings.gs("button_save_title"), style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold, fontFamily: 'ArialRoundedMT'),),
                        ),

                      ),
                    ),
                    const SizedBox(height: 50.0,),


                  ],
                ),
              ),
            ),

            new Offstage(
              offstage: _loading,
              child: progressView(),
            )
          ],
        )
    );
  }

  void updateCustomer(int customerId,Map<String, String> jsonBody){


    UserRepository.submitEdition(
        customerId,
        jsonBody,
        showProgress,
        dismissProgress
    ).then((RegistrationResult value){

      if(value.success == true){
        //Successful Operation
        //Save Data in Local Database
        LoginManager.signIn( value.data, (Registrationdata profile){

          if(profile.delivery_addresses.isEmpty){
            Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => new DeliveryAddressScreen(profile:profile)),
            );
          }else{
            Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => new MainScreen(profile: profile)),
            );

          }


        }, (Exception e){
          print("Error found $e");
          new Dialogs().error(
              context,
              LangStrings.gs("dialog_error_title"),
              LangStrings.gs("dialog_error_message"),(){

            updateCustomer(customerId,jsonBody);

          },(){

          });
        });
      }else{
        //Display Error
        new Dialogs().error(
            context,
            LangStrings.gs("dialog_error_title"),
            value.message,(){

          updateCustomer(customerId,jsonBody);

        },(){});
      }


    }).catchError((error){
      //dismissProgress();
      print("Error found $error");
      new Dialogs().error(
          context,
          LangStrings.gs("dialog_error_title"),
          LangStrings.gs("dialog_error_message"),(){

        updateCustomer(customerId,jsonBody);

      },(){});

    });

  }


  void showProgress() {
    setState(() {
      _loading = false;

    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }

  Widget progressView(){
    return new Stack(
      children: [
        new Container(
            color: Colors.white
        ),
        new Opacity(
          opacity: 0.3,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }


}


