import 'dart:convert';
import 'dart:io';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/UserRepository.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/profile/edit_profile_screen.dart';
import 'package:easy_services/util/DateUtil.dart';
import 'package:easy_services/util/LoginManager.dart';
import 'package:easy_services/util/image_utils.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_luban/flutter_luban.dart';
import 'package:path_provider/path_provider.dart';

class ShowProfileScreen extends StatefulWidget {

  final Registrationdata profile;

  ShowProfileScreen(this.profile);

  @override
  _ShowProfileScreenState createState() => _ShowProfileScreenState();
}

class _ShowProfileScreenState extends State<ShowProfileScreen> {

  File _image;

  bool _loading = true;

  Registrationdata _profile;
  @override
  void initState() {
    super.initState();
    _profile = widget.profile;
    
  }

  @override
  Widget build(BuildContext context) {

    var username = _profile.user.name;
    var accountType = "";
    var company = "";
    var isBusiness = false;
    var email = " - ";
    if(_profile.user.account_type == "business"){
      accountType = LangStrings.gs("business_account");
      company = _profile.user.company_name;
      isBusiness =  true;
    }else{
      accountType = LangStrings.gs("basic_account");
    }

    var phone = "+"+_profile.user.phone;
    if(_profile.user.email != null && _profile.user.email.isNotEmpty){
      email = _profile.user.email;
    }

    var created = DateUtil.formatDateTime(_profile.user.created_at, Localizations.localeOf(context));


    return new Scaffold(
        appBar: new AppBar(
        primary: true,
        centerTitle: false,
        title: new Text(LangStrings.gs("screen_profile_title"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.edit, color: Colors.white, size: 18.0,),
          onPressed: (){
            Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => new EditProfileScreen(_profile)),
            );
          },
          )
        ],
        ),

    body: new Stack(
         children: <Widget>[
         new Container(
           padding: EdgeInsets.only(left: 16.0, right: 16.0),
           child: new Column(
             mainAxisAlignment: MainAxisAlignment.start,
             crossAxisAlignment: CrossAxisAlignment.start,
             children: <Widget>[
              new Container(
                width: double.infinity,
                alignment: Alignment.center,
                child: new Center(
                  child: new Column(
                    children: <Widget>[
                      new SizedBox(height: 20.0,),
                      new Container(
                        height: 120.0,
                        width: 120.0,
                        child:  new Stack(
                          children: <Widget>[
                            _profile.user.photo != null && _profile.user.photo.isNotEmpty?new Container(
                              height: 120.0,
                              width: 120.0,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      fit: BoxFit.fill,
                                      image: NetworkImage(_profile.user.photo)
                                  )
                              ),
                            ):new Container(
                              child: new Center(
                                child:Icon(Icons.person, color: Colors.white, size: 80.0,)
                              ),
                            ),
                            Padding(padding:EdgeInsets.only(left: 60.0,),
                                child:IconButton(icon: Icon(Icons.edit, color: Colors.red, size: 18.0,),
                                    onPressed: (){
                                      getImage();
                                    }))


                          ],
                        ),
                        decoration: BoxDecoration(
                            color: AppColors.getColorHexFromStr("#dedede"),
                            shape: BoxShape.circle,
                            border: Border.all(color: AppColors.getColorHexFromStr("#dedede")),

                        ),
                      ),
                      new SizedBox(height: 10.0,),
                      new Container(
                        child: new Text(username,
                          style: new TextStyle(color: Colors.black, fontSize: 25.0,
                              fontWeight: FontWeight.bold, fontFamily: "ArialRoundedMT"),),
                      ),
                      new SizedBox(height: 10.0,),
                      new Container(
                        padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(5.0)
                        ),
                        child: new Text(accountType ,
                          style: new TextStyle(color: Colors.white, fontSize: 15.0, fontFamily: "ArialRoundedMT"),),
                      ),
                      new SizedBox(height: 20.0,),

                    ],
                  ),
                ),
              ),

               Divider(color: Colors.black12,),

               createField(LangStrings.gs("label_type_of_account"), phone),
               createField(LangStrings.gs("edit_email"), email),

               createField(LangStrings.gs("label_type_of_account"), accountType),
               Offstage(
                 offstage: !isBusiness,
                 child: createField(LangStrings.gs("label_edit_company_name"), company),
               ),
               createField(LangStrings.gs("label_account_created"), created),

             ],
           ),
         ),
           new Offstage(
             offstage: _loading,
             child: progressView(),
           )
        ]
      )
    );
  }


  void showProgress() {
    setState(() {
      _loading = false;

    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }

  Widget progressView(){
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Opacity(
          opacity: 0.3,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }



  void getImage() async {
    var fileImage = await ImagePicker.pickImage(source: ImageSource.gallery);

    if(fileImage != null){

      showProgress();

      final tempDir = await getTemporaryDirectory();

         CompressObject compressObject = CompressObject(
         imageFile:fileImage, //image
         path:tempDir.path, //compress to path
         quality: 85,//first compress quality, default 80
         step: 9,//compress quality step, The bigger the fast, Smaller is more accurate, default 6
         //mode: CompressMode.LARGE2SMALL,//default AUTO
       );
    Luban.compressImage(compressObject).then((_path) {
       
          var compressedFile = File(_path);

      String base64Image = ImageUtils.getImageBase64(compressedFile);
      String fileName = fileImage.path.split("/").last;
      dismissProgress();
      UserRepository.editPic(_profile.user.id,
          base64Image,
          fileName,
          showProgress,
          dismissProgress
      )
          .then((value){
        if(value.success){

          LoginManager.signIn(value.data, (Registrationdata mProfile){

            if(mounted){
              setState(() {
                _profile = mProfile;
              });
            }

          }, (error){
            new Dialogs().simpleError(
                context,
                LangStrings.gs("dialog_error_title"),
                LangStrings.gs("dialog_error_message"),(){

              Navigator.pop(context);
            });
          });

        }
      }).catchError((error){
        print("Error $error");
        dismissProgress();

        new Dialogs().simpleError(
            context,
            LangStrings.gs("dialog_error_title"),
            LangStrings.gs("dialog_error_message"),(){

          Navigator.pop(context);
        });



      });
      
    });

     
    }


  }


  Widget createField(String label, String value){
    return new Container(
      alignment: Alignment.topLeft,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Container(
            child: new Text(label,
              style: new TextStyle(color: Colors.black, fontSize: 18.0,
                  fontWeight: FontWeight.bold, fontFamily: "ArialRoundedMT"),),
          ),
          new Container(
            child: new Text(value,
              style: new TextStyle(color: Colors.grey, fontSize: 18.0, fontFamily: "ArialRoundedMT"),),
          ),
          new SizedBox(height: 10.0,),
        ],
      ),
    );
  }
}
