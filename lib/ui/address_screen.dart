import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/Dialogs.dart';
import 'package:easy_services/openstreetmap_places/model/Place.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/DeliveryAddressRepository.dart';
import 'package:easy_services/rest/response/delivery_address_result.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mapbox_autocomplete/flutter_mapbox_autocomplete.dart';


class AddressScreen extends StatefulWidget {

  String type;
  Place placeDetails;
  Registrationdata profile;

  AddressScreen(this.placeDetails, this.type, this.profile);

  @override
  _AddressScreenState createState() => _AddressScreenState();

}

class _AddressScreenState extends State<AddressScreen> {

  TextEditingController addressNameController = new TextEditingController();
  TextEditingController placeController = new TextEditingController();
  TextEditingController townController = new TextEditingController();
  TextEditingController addressTypedController = new TextEditingController();

  bool _loading = true;

  @override
  void initState() {
    super.initState();

    //addressNameController.text = widget.placeDetails.name+","+widget.placeDetails.formattedAddress;
    addressNameController.text = widget.placeDetails.title;

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
        primary: true,
        centerTitle: false,
        title: new Text(LangStrings.gs("screen_location_detail_title"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
    ),
    body: new Stack(
      children: <Widget>[
        new SingleChildScrollView(
            child: new Container(
              padding: EdgeInsets.all(16.0),
              child: new Column(
                children: <Widget>[

                  const SizedBox(height: 15.0,),

                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Text(LangStrings.gs("label_address_title"), textAlign: TextAlign.start, style: new TextStyle(fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                  const SizedBox(height: 10.0,),
                  new TextFormField(
                    controller: addressNameController,
                    decoration: new InputDecoration(
                        contentPadding: EdgeInsets.all(20.0),
                        border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                        hintText: LangStrings.gs("edit_address_title"),
                        hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                    style: new TextStyle(color: Colors.black, fontSize: 18.0),
                    keyboardType: TextInputType.text,

                  ),
                  const SizedBox(height: 15.0,),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Text(LangStrings.gs("edit_appart_num_title"), textAlign: TextAlign.start, style: new TextStyle(fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                  const SizedBox(height: 10.0,),
                  new TextFormField(
                    controller: placeController,
                    decoration: new InputDecoration(
                        contentPadding: EdgeInsets.all(20.0),
                        border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                        hintText: '200',
                        hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                    style: new TextStyle(color: Colors.black, fontSize: 18.0),
                    keyboardType: TextInputType.text,

                  ),

                  const SizedBox(height: 15.0,),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Text( LangStrings.gs("edit_town_title"), textAlign: TextAlign.start, style: new TextStyle(fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                  const SizedBox(height: 10.0,),
                  new TextFormField(
                    controller: townController,
                    decoration: new InputDecoration(
                        contentPadding: EdgeInsets.all(20.0),
                        border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                        hintText: 'Riviera Palmeraie',
                        hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                    style: new TextStyle(color: Colors.black, fontSize: 18.0),
                    keyboardType: TextInputType.text,

                  ),
                  const SizedBox(height: 15.0,),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: new Text( LangStrings.gs("edit_additional_address_title"), textAlign: TextAlign.start, style: new TextStyle(fontSize: 18.0, color: Colors.black,fontFamily: 'ArialRoundedMT'), )),
                  const SizedBox(height: 10.0,),
                  new TextFormField(
                    controller: addressTypedController,
                    decoration: new InputDecoration(
                        contentPadding: EdgeInsets.all(20.0),
                        border: new OutlineInputBorder(borderSide: BorderSide(width: 2.0,color: Colors.black54), borderRadius: BorderRadius.circular(5.0)),
                        hintText: LangStrings.gs("edit_additional_address_hint"),
                        hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                    style: new TextStyle(color: Colors.black, fontSize: 18.0),
                    keyboardType: TextInputType.text,

                  ),
                  new Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 20.0),
                    child: RaisedButton(
                      color: AppColors.nopalmaYellow,
                      padding: const EdgeInsets.all(10.0),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5.0)
                        ),
                      onPressed:(){

                        createCustomerDeliveryAddress();

                      },
                      child: new Padding(padding: EdgeInsets.all(10.0),
                        child: new Text( LangStrings.gs("button_save_title"), style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold, fontFamily: 'ArialRoundedMT'),),
                      ),

                    ),
                  ),
                  const SizedBox(height: 50.0,),


                ],
              ),
            )
        ),
        new Offstage(
          offstage: _loading,
          child: progressView(),
        )
      ],
    ),
    );
  }

  void createCustomerDeliveryAddress() {


    DeliveryAddressRepository.store(
        addressNameController.text,
        double.parse(widget.placeDetails.lat),
        double.parse(widget.placeDetails.lon),
        placeController.text,
        townController.text,
        addressTypedController.text,
        widget.type,
        widget.profile.user.id,
        showProgress, dismissProgress)
        .then((DeliveryAddressResult value){

      if(value.success == true){
        //Successful Operation
        print('DeliveryAddressResult ${value.data}');
        Navigator.pop(context, value.data);

      }else{
        //Display Error
        print('LoginResult ${value.message}');
        new Dialogs().error(
            context,
            LangStrings.gs("dialog_error_title"),
            value.message,(){

          createCustomerDeliveryAddress();

        },(){});
      }

    }).catchError((error){
      //dismissProgress();
      print('Error $error');
      new Dialogs().error(
          context,
          LangStrings.gs("dialog_error_title"),
          error.toString(),(){

        createCustomerDeliveryAddress();

      },(){});

    });


  }

  Widget progressView(){
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  void showProgress() {
    setState(() {
      _loading = false;

    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }


}
