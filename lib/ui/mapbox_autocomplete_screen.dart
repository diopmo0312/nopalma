import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/delivery_address.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/address_screen.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_mapbox_autocomplete/flutter_mapbox_autocomplete.dart';

class MapboxAutocompleteScreen extends StatefulWidget {

  final Registrationdata profile;

  String type;

  MapboxAutocompleteScreen({this.profile, this.type});

  @override
  _MapboxAutocompleteScreenState createState() => _MapboxAutocompleteScreenState();

}

class _MapboxAutocompleteScreenState extends State<MapboxAutocompleteScreen> {

  final _startPointController = TextEditingController();

  final String MAPBOX_ACCESS_TOKEN = "pk.eyJ1IjoiYW5nZWJhZ3VpMjAyMCIsImEiOiJjazk5NGdqNDExMzg4M2hwY2ZoYW8yM2FrIn0.NHLopuPZFlySHRL9umy2iQ";

  MapBoxPlace placeSelected;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        primary: true,
        centerTitle: false,
        title: new Text(LangStrings.gs("screen_geolocation_title"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: CustomTextField(
          hintText: LangStrings.gs("edit_search_title"),
          textController: _startPointController,
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MapBoxAutoCompleteWidget(
                  apiKey: MAPBOX_ACCESS_TOKEN,
                  hint: LangStrings.gs("edit_search_title"),
                  onSelect: (place) {

                  setState(() {

                    placeSelected = place;

                    _startPointController.text = place.placeName;
                  });


                  },
                  limit: 10,
                  country: "SN",
                ),
              ),
            );
          },
          enabled: true,
        ),
      ),
      bottomSheet: new Offstage(
          offstage: !(placeSelected != null),
          child:new Container(
            height:140.0,
            padding: EdgeInsets.all(15.0),
            child: new Column(
              children: <Widget>[
                new Container(

                  child: new Row(
                    children: <Widget>[
                      new Icon(Icons.adjust, color: AppColors.nopalmaBlue,size: 24,),
                      SizedBox(width: 10.0,),
                      new Text(placeSelected!=null?placeSelected.placeName:"", style: TextStyle(color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.bold, fontFamily: 'ArialRoundedMT'),)

                    ],
                  ),
                ),
                new Container(
                  height: 70.0,
                  width: double.infinity,
                  margin: EdgeInsets.only(top: 10.0),
                  child: RaisedButton(
                    color: placeSelected==null?Colors.blueGrey[800]:AppColors.nopalmaYellow,
                    padding: const EdgeInsets.all(10.0),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(5.0)
                    ),
                    onPressed: placeSelected==null?null:(){

                      findAddressDetail();

                    },
                    child: Text(LangStrings.gs("button_terminate_title"), style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold, fontFamily: 'ArialRoundedMT'),),

                  ),
                )
              ],
            ),
          )),
    );
  }

  void findAddressDetail() async{
   /* DeliveryAddress deliveryAddress  = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => new AddressScreen(placeSelected, widget.type, widget.profile)),
    );

    Navigator.pop(context,deliveryAddress);*/

  }

}
