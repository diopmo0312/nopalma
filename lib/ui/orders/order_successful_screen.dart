import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/main/main_screen.dart';
import 'package:flutter/material.dart';

class OrderSuccessfulScreen extends StatefulWidget {

  final Registrationdata profile;

  OrderSuccessfulScreen(this.profile);

  @override
  _OrderSuccessfulScreenState createState() => _OrderSuccessfulScreenState();
}

class _OrderSuccessfulScreenState extends State<OrderSuccessfulScreen> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
        primary: true,
        title: new Text("Résumé Commande", style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
        ),
      body: new Column(
        children: <Widget>[
          new Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width,
            height: 200.0,
            color: Colors.black12,
            child: new Stack(

              children: <Widget>[
                new Container(
                  child: new Image.asset("assets/clothers/wallpaper.png",height:  200.0, fit: BoxFit.fitWidth,),
                ),
                new Container(
                  height: 200.0,
                  width: double.infinity,
                  child: new Column(
                    children: <Widget>[
                      new SizedBox(height: 30.0,),
                      new Image.asset("assets/clothers/checked@2x.png",width: 80.0, height: 80.0,),
                      new SizedBox(height: 10.0,),
                      new Text("Commande Effectuée Avec Succès", style: new TextStyle(fontSize: 18.0, color: Colors.white, fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold),),
                      new SizedBox(height: 10.0,),
                      new Text("28 Mars 2019 10:00 - 13:00", style: new TextStyle(fontSize: 18.0, color: Colors.white, fontFamily: 'ArialRoundedMT'),),

                    ],
                  ),
                )

              ],
            ),
          ),
          new Container(
            height: 80.0,
            width: double.infinity,
            color: AppColors.getColorHexFromStr("#f9f9f9"),
            child: ListTile(
              leading: Image.asset('assets/clothers/moto_pic.png', height: 50.0, width: 50.0,),
              title: new Text('Adresse de ramassage', style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
              subtitle: new Text('211, Riviéra Palmeraie, Abidjan, Côte d\'Ivoire', style: new TextStyle(color: Colors.black)),
            ),
          ),
          new Container(
            height: MediaQuery.of(context).size.height - 520.0,
            width: double.infinity,
            child: ListView(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                  ),
                  child: new Column(
                    children: <Widget>[
                      ListTile(
                        title: new Text('Articles', style: new TextStyle(color: Colors.black54,fontWeight: FontWeight.bold, fontSize: 18.0),),
                        subtitle: new Text('T-shirt X4, Jeans X2', style: new TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18.0)),
                      ),
                    ],
                  ),
                ),
                new Container(
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                  ),
                  child: new Column(
                    children: <Widget>[
                      ListTile(
                        title: new Text('Date et Heure', style: new TextStyle(color: Colors.black54,fontWeight: FontWeight.bold, fontSize: 18.0),),
                        subtitle: new Text('28 Mars 2019, 10:00 à 13:00', style: new TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18.0)),
                      ),
                    ],
                  ),
                ),
                new Container(
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                  ),
                  child: new Column(
                    children: <Widget>[
                      ListTile(
                        title: new Text('Adresse de Ramassage', style: new TextStyle(color: Colors.black54,fontWeight: FontWeight.bold, fontSize: 18.0),),
                        subtitle: new Text('211, Riviéra Palmeraie, Abidjan, Côte d\'Ivoire', style: new TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18.0)),
                      ),
                    ],
                  ),
                ),
                new Container(
                  padding: EdgeInsets.all(10.0),
                  width: double.infinity,
                  //height: 200.0,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                  ),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Container(
                        child: new Column(
                          children: <Widget>[
                           new Text('Tarifs', style: new TextStyle(color: Colors.black54,fontWeight: FontWeight.bold, fontSize: 20.0),),
                           new SizedBox(height: 20.0,),
                           new Text('4200 FCFA', style: new TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25.0))
                          ]

                        ),
                      ),
                      new Container(
                        child: new Column(

                          children: <Widget>[
                                new Text('Mode de paiement', style: new TextStyle(color: Colors.black54,fontWeight: FontWeight.bold, fontSize: 20.0),),
                                new SizedBox(height: 20.0,),
                               new Text('Cash', style: new TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25.0)),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          new Container(
            width: double.infinity,
            height: 90.0,
            padding: EdgeInsets.all(20.0),
            child: new RaisedButton(
                textColor: Colors.white,
                color:AppColors.button_positive_color,
                child: new Text('Terminer', style: new TextStyle(fontSize:18.0,color: Colors.white, fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold), ),
                onPressed: (){
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => new MainScreen(profile:widget.profile, position: 1)));
                }),
          )
        ],
      ),
    );
  }
}
