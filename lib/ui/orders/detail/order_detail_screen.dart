import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/delivery_address.dart';
import 'package:easy_services/models/order.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/orders/detail/pages/order_history_screen.dart';
import 'package:easy_services/ui/orders/detail/pages/order_job_history_screen.dart';
import 'package:easy_services/ui/orders/detail/show_order_item_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating/flutter_rating.dart';

class OrderDetailScreen extends StatefulWidget {

  final Registrationdata profile;
  final Order order;

  OrderDetailScreen(this.profile, this.order);

  @override
  _OrderDetailScreenState createState() => _OrderDetailScreenState();

}

class _OrderDetailScreenState extends State<OrderDetailScreen>  with SingleTickerProviderStateMixin{

  TabController _tabController;

  double rating = 3.5;
  int starCount = 5;

  bool keyboardArrowRight;
  bool orderDelivered;

  double tax;

  double total = 0;
  int feesDelivery = 1500;
  double subTotal = 0;

  List<int> itemTotals = [];

  List<String> servicesSlugs;

  String currency = "XOF";

  TextEditingController noteController = new TextEditingController();

  @override
  void initState() {
    servicesSlugs = getServices(widget.order.service_slug).map((s)=>s.slug).toList();
    super.initState();
    keyboardArrowRight = true;
    orderDelivered = false;
    if(widget.order.delivery_fees != null){
      feesDelivery = int.parse( widget.order.delivery_fees);
    }

    _tabController = new TabController(length: 3,initialIndex: 0, vsync: this);

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        primary: true,
        title: new Text(LangStrings.gs("screen_order_detail_title"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
      ),
      body: new SingleChildScrollView(
        child: new Container(
          color: AppColors.getColorHexFromStr("#f4fafd"),
          child: new Column(
            children: <Widget>[
              new InkWell(
                onTap: (){
                  toggle();
                },
                splashColor: AppColors.getColorHexFromStr("#f4fafd"),
                child: new Container(
                  margin: EdgeInsets.all(10.0),
                  padding: EdgeInsets.all(5.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(color: AppColors.getColorHexFromStr("#dedede")),
                    color: Colors.white,
                    
                  ),
                  child: new ListTile(
                    leading: Image.asset("assets/product11@2x.png",width: 70.0, height: 70.0,fit: BoxFit.contain,),
                    title: new Text('${LangStrings.gs("field_order_numero_title")} : ${widget.order.id}', style: new TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontFamily: "ArialRoundedMT",fontSize: 16.0),),
                    subtitle: new Text(getOrderStatus(widget.order), style: new TextStyle(color: Colors.black45, fontFamily: "ArialRoundedMT",fontSize: 16.0),),
                    trailing: IconButton(icon: Icon(keyboardArrowRight?Icons.keyboard_arrow_right:Icons.keyboard_arrow_down), onPressed: (){

                      toggle();

                    }),
                  ),
                ),
              ),
              new ListTile(
                leading: Image.asset('assets/checked (1)@2x.png'),
                title: new Text(getMessage(widget.order),
                style: new TextStyle(fontFamily: 'ArialRoundedMT'),/*'Bonjour Affoh, votre commande a été livrée avec succès'*/),
              ),

              new Offstage(
                offstage: keyboardArrowRight,
                child: new Container(
                  height: MediaQuery.of(context).size.height - 400.0,
                  width: double.infinity,
                  color: AppColors.getColorHexFromStr("#f4fafd"),
                  child: ListView(
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.all(5.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                        ),
                        child: new Column(
                          children: <Widget>[
                            ListTile(
                              title: new Text(LangStrings.gs("field_services_title"), style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: 18.0),),
                              subtitle: createServices(widget.order.service_slug)
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        padding: EdgeInsets.all(5.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                        ),
                        child: new Column(
                          children: <Widget>[
                            ListTile(
                              title: new Text(LangStrings.gs("field_pickup_date_title"), style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: 18.0),),
                              subtitle: new Text(widget.order.pickup_slot/*'28 March 2019, 10:00 between 13:00'*/, style: new TextStyle(color: Colors.grey, fontFamily: "ArialRoundedMT", fontSize: 16.0)),
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        padding: EdgeInsets.all(5.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                        ),
                        child: new Column(
                          children: <Widget>[
                            ListTile(
                              title: new Text(LangStrings.gs("field_pickup_address_title"), style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: 18.0),),
                              subtitle: new Text(getAddressName(widget.order.pickup)/*'211, Riviéra Palmeraie, Abidjan, Côte d\'Ivoire'*/, style: new TextStyle(color: Colors.grey, fontFamily: "ArialRoundedMT", fontSize: 16.0)),
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        padding: EdgeInsets.all(5.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                        ),
                        child: new Column(
                          children: <Widget>[
                            ListTile(
                              title: new Text(LangStrings.gs("field_delivery_date_title"), style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: 18.0),),
                              subtitle: new Text(widget.order.delivery_slot/*'28 March 2019, 10:00 between 13:00'*/, style: new TextStyle(color: Colors.grey, fontFamily: "ArialRoundedMT",fontSize: 16.0)),
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        padding: EdgeInsets.all(5.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                        ),
                        child: new Column(
                          children: <Widget>[
                            ListTile(
                              title: new Text(LangStrings.gs("field_delivery_address_title"), style: new TextStyle(fontFamily: 'ArialRoundedMT', color: Colors.black,fontWeight: FontWeight.bold, fontSize: 18.0),),
                              subtitle: new Text(getAddressName(widget.order.delivery)/*'211, Riviéra Palmeraie, Abidjan, Côte d\'Ivoire'*/, style: new TextStyle(fontFamily: 'ArialRoundedMT',color: Colors.grey, fontSize: 16.0)),
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        padding: EdgeInsets.all(5.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border(bottom: BorderSide(color: Colors.black12, width: 1.0))
                        ),
                        child: new Column(
                          children: <Widget>[
                            ListTile(
                              title: new Text(LangStrings.gs("field_amount_title"), style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: 18.0),),
                              subtitle: new Text(getAmount(widget.order), style: new TextStyle(color: AppColors.nopalmaYellow, fontSize: 16.0)),
                              trailing: new Container(
                                padding: EdgeInsets.all(8.0),
                                decoration: new BoxDecoration(
                                  color: AppColors.nopalmaBlue,
                                    borderRadius: BorderRadius.circular(5.0),
                                    border: Border.all(color: Colors.black12)
                                ),
                                child: new Text(LangStrings.gs("field_invoice_title"),style: new TextStyle(color: Colors.white),),
                              ),
                              onTap: (){
                                if(widget.order != null && widget.order.items != null && widget.order.items.isNotEmpty){
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => new ShowOrderItemScreen(widget.profile,widget.order)),
                                  );
                                }

                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),

        new Container(

          margin: EdgeInsets.only(top: 10.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Container(
                color:Colors.black12,
                child: new TabBar(
                  controller: _tabController,
                  indicatorColor: AppColors.nopalmaBlue,
                  tabs: <Widget>[
                    new Tab(child: new Text( LangStrings.gs("field_HISTORY_title"), style: new TextStyle(color:Colors.black, fontSize: 14.0,fontFamily: "ArialRoundedMT")),),
                    new Tab(child: new Text( LangStrings.gs("field_PICKUP_title"), style: new TextStyle(color:Colors.black,fontSize: 14.0,fontFamily: "ArialRoundedMT"))),
                    new Tab(child: new Text( LangStrings.gs("field_DELIVERY_title"), style: new TextStyle(color:Colors.black,fontSize: 14.0,fontFamily: "ArialRoundedMT"))),
                  ],
                ),
              ),
              new Container(
                height: 500.0,
                child: new TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    new OrderHistoryScreen(widget.order),
                    new OrderJobHistoryScreen(widget.order, true),
                    new OrderJobHistoryScreen(widget.order, false),

                  ],
                ),
              )

            ],
          ),
        ),

              new Offstage(
                offstage: !orderDelivered,
                child: new Container(
                  child: new Column(
                    children: <Widget>[
                      new SizedBox(height: 10.0,),
                      new Padding(
                        padding: EdgeInsets.only(left: 10.0,right: 10.0),
                        child: new Text(LangStrings.gs("field_evaluate_order_title"), style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: 17.0),),
                      ),
                      new StarRating(
                        size: 25.0,
                        rating: rating,
                        color: Colors.orange,
                        borderColor: Colors.grey,
                        starCount: starCount,
                        onRatingChanged: (rating) => setState(
                              () {
                            this.rating = rating;
                          },
                        ),
                      ),
                      new SizedBox(height: 10.0,),
                      new Container(
                        alignment: Alignment.topLeft,
                        padding: EdgeInsets.only(left: 10.0,right: 10.0),
                        child: new Text(LangStrings.gs("field_share_your_experience_title"), style: new TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: 17.0),),

                      ),
                      new Container(
                        padding: const EdgeInsets.all(15.0),
                        //color: Colors.white,
                        height: 200.0,
                        child: new TextField(
                          controller: noteController,
                          decoration: new InputDecoration(
                              contentPadding: EdgeInsets.all(20.0),
                              border: new OutlineInputBorder(borderSide: BorderSide(width: 1.0,color: AppColors.getColorHexFromStr("#f7fbfd")), borderRadius: BorderRadius.circular(5.0)),
                              hintText: LangStrings.gs("edit_type_something_here_hint"),
                              hintStyle: new TextStyle(color: Colors.grey, fontFamily: 'ArialRoundedMT')),
                          style: new TextStyle(color: Colors.black, fontSize: 18.0),
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                        ),
                      ),
                      new SizedBox(height: 15.0,),
                      new Container(
                        height: 80.0,
                        width: double.infinity,
                        margin: EdgeInsets.only(top: 20.0),
                        padding: EdgeInsets.all(10.0),
                        child: RaisedButton(
                          color: AppColors.button_positive_color,
                          padding: const EdgeInsets.all(10.0),
                          onPressed:(){
                            sendFeedback();
                          },
                          child: Text(LangStrings.gs("button_give_feedback_title"), style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold, fontFamily: 'ArialRoundedMT'),),

                        ),
                      ),

                    ],
                  ),
                ),
              ),


            ],
          ),
        ),
      ),
    );
  }

  List<Service> getServices(String serviceSlug){
    List<dynamic> services = jsonDecode(serviceSlug);
    List<String> servicesStrings = services.map((c){
      String serviceString   = c.toString();
      if(serviceString != null){
        return serviceString;
      }else{
        return "";
      }

    }).toList();

    return  servicesStrings
        .map((s)=>widget.profile.services.firstWhere((service)=> service.slug == s)).toList();

  }

  Widget createServices(String serviceSlug){


    List<Widget> views = getServices(serviceSlug)
        .map((service)=>new Container(
      padding: new EdgeInsets.all(8.0),
      margin: EdgeInsets.only(left:10.0, top:5.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(color: Colors.black12)
      ),
      child: new Text(LangStrings.strLang=='FR'?service.name:service.name_en, style: new TextStyle(fontSize:16.0,color: Colors.black, fontFamily: 'ArialRoundedMT'),),
    )).toList();

    var view  = new Container(
      height: 45,
      child: ListView(
          children: views,
          scrollDirection:Axis.horizontal
      ),
    );


    return view;
  }

  String getOrderStatus(Order o) {
    if(o.current_status == "is-waiting"){
      return LangStrings.gs("order_is_waiting_message");
    }else if(o.current_status == "managed"){
      return LangStrings.gs("order_is_managed_message");
    }else if(o.current_status == "started"){
      return LangStrings.gs("order_is_started_message");
    }else if(o.current_status == "successful"){
      return LangStrings.gs("order_is_successful_message");
    }else if(o.current_status == "failed"){
      return LangStrings.gs("order_is_failed_message");
    }else if(o.current_status == "cancel"){
      return LangStrings.gs("order_is_cancel_message");
    }else if(o.current_status == "edited"){
      return LangStrings.gs("order_edited_message");
    }else if(o.current_status == "customer-pickuped"){
      return LangStrings.gs("order_customer_pickuped_message");
    }else if(o.current_status == "waiting-customer-pickup"){
      return LangStrings.gs("order_waiting_customer_pickup_message");
    }else if(o.current_status == "waiting-customer-delivery"){
      return LangStrings.gs("order_waiting_customer_delivery_message");
    }else if(o.current_status == "customer-delivered"){
      return LangStrings.gs("order_customer_delivered_message");
    }else{
      return LangStrings.gs("order_is_waiting_message");
    }
  }

  Color getOrderStatusColor(Order o) {
    if(o.current_status == "is-waiting"){
      return Colors.blue;
    }else if(o.current_status == "managed"){
      return Colors.orange;
    }else if(o.current_status == "started"){
      return Colors.brown;
    }else if(o.current_status == "successful"){
      return Colors.green;
    }else if(o.current_status == "failed"){
      return Colors.blueGrey;
    }else if(o.current_status == "cancel"){
      return Colors.black;
    }else if(o.current_status == "deleted"){
      return Colors.red;
    }else {
      return Colors.blue;
    };
  }

  void toggle(){
    setState(() {
      if(keyboardArrowRight){
        keyboardArrowRight = false;
      }else{
        keyboardArrowRight = true;
      }

    });
  }

  void sendFeedback() {


  }

  String getMessage(Order o) {

    if(o.current_status == "is-waiting"){
      return "${LangStrings.gs("hello_message")} ${widget.profile.user.name}! ${LangStrings.gs("order_is_waiting_message")}";
    }else if(o.current_status == "managed"){
      return "${LangStrings.gs("hello_message")} ${widget.profile.user.name}! ${LangStrings.gs("order_is_managed_message")}";
    }else if(o.current_status == "started"){
      return "${LangStrings.gs("hello_message")} ${widget.profile.user.name}! ${LangStrings.gs("order_is_started_message")}";
    }else if(o.current_status == "successful"){
      return "${LangStrings.gs("hello_message")} ${widget.profile.user.name}! ${LangStrings.gs("order_is_successful_message")}";
    }else if(o.current_status == "failed"){
      return "${LangStrings.gs("hello_message")} ${widget.profile.user.name}! ${LangStrings.gs("order_is_failed_message")}";
    }else if(o.current_status == "cancel"){
      return "${LangStrings.gs("hello_message")} ${widget.profile.user.name}! ${LangStrings.gs("order_is_cancel_message")}";
    }else if(o.current_status == "edited"){
      return "${LangStrings.gs("hello_message")} ${widget.profile.user.name}! ${LangStrings.gs("order_edited_message")}";
    }else if(o.current_status == "customer-pickuped"){
      return "${LangStrings.gs("hello_message")} ${widget.profile.user.name}! ${LangStrings.gs("order_customer_pickuped_message")}";
    }else if(o.current_status == "waiting-customer-pickup"){
      return "${LangStrings.gs("hello_message")} ${widget.profile.user.name}! ${LangStrings.gs("order_waiting_customer_pickup_message")}";
    }else if(o.current_status == "waiting-customer-delivery"){
      return "${LangStrings.gs("hello_message")} ${widget.profile.user.name}! ${LangStrings.gs("order_waiting_customer_delivery_message")}";
    }else if(o.current_status == "customer-delivered"){
      return "${LangStrings.gs("hello_message")} ${widget.profile.user.name}! ${LangStrings.gs("order_customer_delivered_message")}";
    }else{
      return "${LangStrings.gs("hello_message")} ${widget.profile.user.name}! ${LangStrings.gs("order_is_waiting_message")}";
    }

  }


  String getAddressName(DeliveryAddress address) {
    if(address != null){
      return address.address_name;
    }else{
      return LangStrings.gs("address_not_available_message");
    }

  }



  String getAmount(Order order) {
    if(order != null){

        if(order.items != null){

          if(order.invoice != null){

            return "${order.invoice.total} ${order.invoice.currency}";

          }else{
            return "0 $currency";

          }

        }else{
          return "0 $currency";
        }

      }else{
        return "0 $currency";
      }


  }

}
