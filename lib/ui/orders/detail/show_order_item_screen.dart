import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/show_order_item_widget.dart';
import 'package:easy_services/models/order.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/orders/detail/receipt_screen.dart';
import 'package:flutter/material.dart';

class ShowOrderItemScreen extends StatefulWidget {

  final Order order;

  final Registrationdata profile;

  ShowOrderItemScreen(this.profile,this.order);

  @override
  _ShowOrderItemScreenState createState() => _ShowOrderItemScreenState();
}

class _ShowOrderItemScreenState extends State<ShowOrderItemScreen> {

  double tax = 0.0;

  double total = 1500.0;
  double feesDelivery = 1500.0;
  double subTotal = 1500.0;

  String currency = "XOF";




  @override
  void initState() {
    super.initState();

    if(widget.order!= null){
      if(widget.order.invoice != null){
        tax = double.parse(widget.order.invoice.tax);
        currency = widget.order.invoice.currency;
        feesDelivery = double.parse(widget.order.invoice.fees_delivery);
        subTotal = double.parse(widget.order.invoice.subtotal);
        total = double.parse(widget.order.invoice.total);
      }

    }

  }



  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        primary: true,
        title: new Text("${LangStrings.gs("screen_invoice_title")} #${widget.order.invoice.reference}", style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
      ),
      body: new Container(
        height: MediaQuery.of(context).size.height - 80.0,
        child: new Column(
          children: <Widget>[
            new Container(
              height:MediaQuery.of(context).size.height - 500.0,
              color: AppColors.getColorHexFromStr("#f2f9fd"),
              child: new ListView(
                children: widget.order.items.map((item){
                  return new ShowOrderItemWidget(widget.profile,item);
                }).toList(),
              ),
            ),


            new Container(
              padding: EdgeInsets.only(bottom: 15.0,  right: 15.0, left: 15.0, top: 15.0),
              decoration:  BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.black12))
              ),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text(LangStrings.gs("field_delivery_fees_title"), style: new TextStyle(fontFamily: 'ArialRoundedMT',color: Colors.black, fontSize:14.0,fontWeight: FontWeight.bold),),
                  new Text('$feesDelivery $currency', style: new TextStyle(fontFamily: 'ArialRoundedMT', color: Colors.black45 ,fontSize:14.0,fontWeight: FontWeight.bold),)
                ],
              ),
            ),
            new Container(
              padding: EdgeInsets.only(bottom: 15.0,  right: 15.0, left: 15.0, top: 15.0),
              decoration:  BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.black12))
              ),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text(LangStrings.gs("field_subtitle_title"), style: new TextStyle(fontFamily: 'ArialRoundedMT', color: Colors.black, fontSize:14.0,fontWeight: FontWeight.bold),),
                  new Text('$subTotal $currency', style: new TextStyle(fontFamily: 'ArialRoundedMT', color: Colors.black45 ,fontSize:14.0,fontWeight: FontWeight.bold),)
                ],
              ),
            ),
            new Container(
              padding: EdgeInsets.only(bottom: 15.0,  right: 15.0, left: 15.0, top: 15.0),
              decoration:  BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.black12))
              ),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text(LangStrings.gs("field_tax_title"), style: new TextStyle(fontFamily: 'ArialRoundedMT', color: Colors.black, fontSize:14.0,fontWeight: FontWeight.bold),),
                  new Text('$tax $currency', style: new TextStyle(fontFamily: 'ArialRoundedMT', color: Colors.black45 ,fontSize:14.0,fontWeight: FontWeight.bold),)
                ],
              ),
            ),
            new Container(
              padding: EdgeInsets.only(bottom: 15.0,  right: 15.0, left: 15.0, top: 15.0),
              decoration:  BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.black12))
              ),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text(LangStrings.gs("field_total_title"), style: new TextStyle(fontFamily: 'ArialRoundedMT',color: Colors.black, fontSize:18.0,fontWeight: FontWeight.bold),),
                  new Text('$total $currency', style: new TextStyle(fontFamily: 'ArialRoundedMT',color: AppColors.nopalmaYellow ,fontSize:18.0,fontWeight: FontWeight.bold),)
                ],
              ),
            ),
            new Container(
              width: double.infinity,
              height: 80.0,
              padding: EdgeInsets.all(10.0),
              child: new RaisedButton(
                  textColor: Colors.white,
                  shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(5.0)
                      ),
                  color:AppColors.nopalmaBlue,
                  padding: EdgeInsets.all(10.0),
                  child: new Text('Retour', style: new TextStyle(fontSize:18.0,color: Colors.white, fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold), ),
                  onPressed: (){
                    Navigator.pop(context);
                  }),
            ),
            SizedBox(height: 20.0,)
          ],
        ),
      ),

        bottomSheet: widget.order.invoice.is_paid?new Container(
          padding: EdgeInsets.all(10.0),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              //Take Cash
              new Container(
                width: 240.0,
                height: 60.0,
                padding: EdgeInsets.only(left:10.0, right: 10.0, top:5.0, bottom: 5.0),
                margin: EdgeInsets.only(bottom: 15.0),
                child: new RaisedButton(
                    textColor: Colors.white,
                    color:AppColors.button_positive_color,
                    padding: EdgeInsets.only(left:10.0, right: 10.0, top:5.0, bottom: 5.0),
                    child: new Text(LangStrings.gs('View_Receipt_title'), style: new TextStyle(fontSize:18.0,color: Colors.white, fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold), ),
                    onPressed: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => new ReceiptScreen(widget.profile, widget.order)),
                      );
                    }),
              )
            ],
          ),
        ):null
    );
  }
}
