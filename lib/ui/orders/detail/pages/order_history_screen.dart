import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/order_detail/order_history_widget.dart';
import 'package:easy_services/models/order.dart';
import 'package:easy_services/models/order_history.dart';
import 'package:easy_services/rest/repository/OrderRepository.dart';
import 'package:flutter/material.dart';

class OrderHistoryScreen extends StatefulWidget {

  Order order;

  OrderHistoryScreen(this.order);

  @override
  _OrderHistoryScreenState createState() => _OrderHistoryScreenState();
}

class _OrderHistoryScreenState extends State<OrderHistoryScreen> {

  Order _order;

  List<OrderHistory> orderHistories = [];

  bool _loading = true;

  String emptyMessage = LangStrings.strLang == "FR"?"Aucune Historique":"No History";

  Locale defaultLocale;

  List<String> customerStatus = ["is-waiting", "edited","managed", "started", "successful", "failed", "cancel", "waiting-customer-pickup", "waiting-customer-delivery"];

  void showProgress() {
    setState(() {
      _loading = false;

    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }



  Widget progressView(){
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Opacity(
          opacity: 0.3,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    loadHistories();
  }

  @override
  Widget build(BuildContext context) {

    if(orderHistories == null){
      orderHistories = [];
    }
    defaultLocale = Localizations.localeOf(context);

    if(orderHistories.isNotEmpty){
      orderHistories = orderHistories.where((OrderHistory orderHistory)=>customerStatus.contains(orderHistory.status)).toList();
    }

    orderHistories.sort((a,b)=>b.id.compareTo(a.id));

    return new Stack(
      children: <Widget>[
        orderHistories.isNotEmpty?
        new Container(
            color: Colors.white,
            padding: EdgeInsets.all(10.0),
            alignment: Alignment.center,
            child: ListView(
              scrollDirection: Axis.vertical,
              children: orderHistories.map((OrderHistory orderHistory)=> new OrderHistoryWidget(orderHistory)).toList(),
            )
        ):
        new Container(
          padding: EdgeInsets.all(10.0),
          alignment: Alignment.center,
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text(emptyMessage, style: new TextStyle(color: Colors.black54, fontFamily: "ArialRoundedMT", fontSize: 18.0),),

            ],
          ),
        ),
        new Offstage(
          offstage: _loading,
          child: progressView(),
        )
      ],
    );
  }

  void  loadHistories(){
    OrderRepository.getOrderHistories(widget.order.id, showProgress, dismissProgress)
        .then((value){
          if(value.success){
            if(mounted){

              setState(() {
                this.orderHistories = value.data;
              });

            }

          }
        })
        .catchError((error){
          print("Error $error");
          if(mounted){

            setState(() {
              this.orderHistories = [];
            });

          }
        });
  }
}
