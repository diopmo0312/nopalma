import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/order_detail/job_history_item_widget.dart';
import 'package:easy_services/models/job.dart';
import 'package:easy_services/models/job_history.dart';
import 'package:easy_services/models/order.dart';
import 'package:easy_services/rest/repository/OrderRepository.dart';
import 'package:flutter/material.dart';

class OrderJobHistoryScreen extends StatefulWidget {

  Order order;

  bool is_pickup;

  OrderJobHistoryScreen(this.order, this.is_pickup);

  @override
  _OrderJobHistoryScreenState createState() => _OrderJobHistoryScreenState();

}

class _OrderJobHistoryScreenState extends State<OrderJobHistoryScreen> {

  Job job;

  List<JobHistory> jobHistories = [];

  bool _loading = true;

  String emptyMessage = LangStrings.strLang == "FR"?"Aucune Historique":"No History";

  Locale defaultLocale;

  List<String> customerStatus = ["started", "arrived", "successful", "failed", "cancel"];


  void showProgress() {
    setState(() {
      _loading = false;

    });
  }

  void dismissProgress() {
    setState(() {
      _loading = true;
    });
  }

  Widget progressView(){
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Opacity(
          opacity: 0.3,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    loadHistories();
  }

  @override
  Widget build(BuildContext context) {

    if(jobHistories == null){
      jobHistories = [];
    }
    defaultLocale = Locale(LangStrings.strLang.toLowerCase());

    var statuses =  ["arrived", "started", "successful", "failed", "cancel"];

    jobHistories = jobHistories.where((jH)=>statuses.contains(jH.status)).toList();

    jobHistories.sort((a,b)=>b.id.compareTo(a.id));

    return new Stack(
      children: <Widget>[
        jobHistories.isNotEmpty?
        new Container(
            color: Colors.white,
            padding: EdgeInsets.all(10.0),
            alignment: Alignment.center,
            child: ListView(
              scrollDirection: Axis.vertical,
              children: jobHistories.map((JobHistory jobHistory)=> new JobHistoryItemWidget(jobHistory)).toList(),
            )
        ):
        new Container(
          padding: EdgeInsets.all(10.0),
          alignment: Alignment.center,
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text(emptyMessage, style: new TextStyle(color: Colors.black54, fontFamily: "ArialRoundedMT", fontSize: 18.0),),

            ],
          ),
        ),
        new Offstage(
          offstage: _loading,
          child: progressView(),
        )
      ],
    );
  }

  void  loadHistories(){
    OrderRepository.getJobs(widget.order.id, showProgress, dismissProgress)
        .then((value){
      if(value.success && value.data != null){
        if(mounted){

          setState(() {
            if(widget.is_pickup){
              this.job = value.data.pickup;
            }else{
              this.job = value.data.delivery;
            }
            if(this.job !=null){
              this.jobHistories = this.job.histories;
            }

          });

        }

      }
    })
        .catchError((error){
      print("Error $error");
      if(mounted){

        setState(() {
          this.jobHistories = [];
        });

      }
    });
  }

}
