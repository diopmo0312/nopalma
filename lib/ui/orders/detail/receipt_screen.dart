import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/order.dart';
import 'package:easy_services/models/receipt.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:flutter/material.dart';

class ReceiptScreen extends StatefulWidget {

  final Order order;

  final Registrationdata profile;

  ReceiptScreen( this.profile, this.order);

  @override
  _ReceiptScreenState createState() => _ReceiptScreenState();
}

class _ReceiptScreenState extends State<ReceiptScreen> {

  Receipt _receipt;

  Order _order;

  @override
  void initState() {
    super.initState();
    _order = widget.order;
    if(_order.invoice != null && _order.invoice.receipt != null){
      _receipt = _order.invoice.receipt;
    }

  }

  @override
  Widget build(BuildContext context) {

    String reference = "";
    if(_receipt == null){
      reference = "Unknown";
    }else{
      reference = "#"+_receipt.payment_reference;
    }

    return WillPopScope(
      onWillPop: (){

        Navigator.pop(context);

      },
      child: new Scaffold(
        appBar: new AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          primary: true,
          title: new Text(LangStrings.gs("View_Receipt_title"), style: new TextStyle(color: Colors.white, fontFamily: 'ArialRoundedMT'),),
        ),
        body: new Stack(
          children: <Widget>[
            new SafeArea(
                child: Column(children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 5.0, top: 10.0),
                      color:Colors.black12,
                      child: Card(
                          elevation: 4.0,
                          child: Container(
                              padding: const EdgeInsets.fromLTRB(
                                  10.0, 10.0, 10.0, 10.0),
                              child: GestureDetector(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      // three line description
                                      Container(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "Receipt",
                                          style: TextStyle(
                                            fontSize: 16.0,
                                            fontStyle: FontStyle.normal,
                                            color: Colors.black87,
                                          ),
                                        ),
                                      ),

                                      Container(
                                        margin: EdgeInsets.only(top: 3.0),
                                      ),
                                      Container(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          'Reference : ' +
                                              reference,
                                          style: TextStyle(
                                              fontSize: 13.0, color: Colors.black54),
                                        ),
                                      ),
                                      Divider(
                                        height: 10.0,
                                        color: Colors.amber.shade500,
                                      ),

                                      Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(
                                              padding: EdgeInsets.all(3.0),
                                              child: Column(
                                                mainAxisAlignment:
                                                MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Order Id',
                                                    style: TextStyle(
                                                        fontSize: 13.0,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(top: 3.0),
                                                    child: Text(
                                                      "#${_order.id}",
                                                      style: TextStyle(
                                                          fontSize: 15.0,
                                                          color: Colors.black87),
                                                    ),
                                                  )
                                                ],
                                              )),
                                          Container(
                                              padding: EdgeInsets.all(3.0),
                                              child: Column(
                                                mainAxisAlignment:
                                                MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Order Amount',
                                                    style: TextStyle(
                                                        fontSize: 13.0,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(top: 3.0),
                                                    child: Text(
                                                      "${_order.invoice.currency} ${_order.invoice.total}",
                                                      style: TextStyle(
                                                          fontSize: 15.0,
                                                          color: Colors.black87),
                                                    ),
                                                  ),
                                                ],
                                              )),
                                          Container(
                                              padding: EdgeInsets.all(3.0),
                                              child: Column(
                                                mainAxisAlignment:
                                                MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Payment Type',
                                                    style: TextStyle(
                                                        fontSize: 13.0,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(top: 3.0),
                                                    child: Text(
                                                      "Cash On Delivery",
                                                      style: TextStyle(
                                                          fontSize: 15.0,
                                                          color: Colors.black87),
                                                    ),
                                                  )
                                                ],
                                              )),
                                        ],
                                      ),
                                    ],
                                  ))))),
                ])

            )

          ],
        ),
      ),
    );
  }
}
