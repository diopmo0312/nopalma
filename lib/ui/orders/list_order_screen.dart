import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/order.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/repository/OrderRepository.dart';
import 'package:easy_services/rest/response/order/order_result.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/orders/detail/order_detail_screen.dart';
import 'package:flutter/material.dart';

class ListOrderScreen extends StatefulWidget {

  final Registrationdata profile;

  ListOrderScreen(this.profile);

  @override
  _ListOrderScreenState createState() => _ListOrderScreenState();

}

class _ListOrderScreenState extends State<ListOrderScreen> {

  List<Order> orders;

  List<Widget> orderItemWidgets;

  bool _loading = true;

  @override
  void initState() {
    super.initState();
    this.orders = [];
    this.orderItemWidgets = <Widget>[
      SizedBox(height: 20,),
    ];
    loadData();

  }

  @override
  Widget build(BuildContext context) {

    orders.sort((a,b)=>b.id.compareTo(a.id));
    orders.forEach((o){
      this.orderItemWidgets.add(new InkWell(
        onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => new OrderDetailScreen(widget.profile,o)),
          );
        },
        splashColor: AppColors.getColorHexFromStr("#f4fafd"),
        child: new Container(
          margin: EdgeInsets.only(left: 10.0,right: 10.0,top: 5.0, bottom: 5.0),
          padding: EdgeInsets.all(5.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(color: AppColors.getColorHexFromStr("#dedede")),
            color: Colors.white,
          ),
          child: new ListTile(
            leading: Image.asset("assets/product11@2x.png",width: 70.0, height: 70.0,fit: BoxFit.contain,),
            title: new Text("${LangStrings.gs("field_order_numero_title")} : ${o.id}", style: new TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontFamily: "ArialRoundedMT",fontSize: 16.0),),
            subtitle: new Text(getOrderStatus(o), style: new TextStyle(color: Colors.black45, fontFamily: "ArialRoundedMT",fontSize: 16.0),),
            trailing: IconButton(icon: Icon(Icons.keyboard_arrow_right), onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => new OrderDetailScreen(widget.profile,o)),
              );

            },),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => new OrderDetailScreen(widget.profile,o)),
              );
            },
          ),
        ),
      ));
    });


    return new Stack(
      children: <Widget>[
        orders.isNotEmpty?
        ListView(
          children: orderItemWidgets,
        ):new Container(
          color: Colors.white,
          child: new Center(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Icon(Icons.do_not_disturb,size: 80.0, color: Colors.black12,),
                new Text(LangStrings.gs("no_data_message"), style: new TextStyle(fontSize:20.0, fontFamily: "ArialRoundedMT", color:Colors.grey),),

              ],
            ),
          ),
        ),
        new Offstage(
          offstage: _loading,
          child: progressView(),
        )
      ],
    );

  }


  void loadData(){

    if(widget.profile != null && widget.profile.user != null){

      OrderRepository
          .find(widget.profile.user.id,
          showProgress,
          dismissProgress)
          .then((OrderResult orderResult){
        if(orderResult.success){
          if(orderResult.data != null){
            if(mounted){
              setState(() {
                orders = orderResult.data;
              });
            }

          }else{
            if(mounted){
              setState(() {
                orders = [];
              });
            }

          }

        }else{
          if(mounted){
            setState(() {
              orders = [];
            });
          }

        }
      }).catchError((error){
        print("Error found $error");
      });


    }



  }

  Widget progressView(){
    return new Stack(
      children: [
        new Container(
          color: Colors.white,
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  void showProgress() {
   if(mounted){
     setState(() {
       _loading = false;

     });
   }
  }

  void dismissProgress() {
    if(mounted){
      setState(() {
        _loading = true;
      });
    }

  }

  String getOrderStatus(Order o) {
    if(o.current_status == "is-waiting"){
      return LangStrings.gs("order_is_waiting_message");
    }else if(o.current_status == "managed"){
      return LangStrings.gs("order_is_managed_message");
    }else if(o.current_status == "started"){
      return LangStrings.gs("order_is_started_message");
    }else if(o.current_status == "successful"){
      return LangStrings.gs("order_is_successful_message");
    }else if(o.current_status == "failed"){
      return LangStrings.gs("order_is_failed_message");
    }else if(o.current_status == "cancel"){
      return LangStrings.gs("order_is_cancel_message");
    }else if(o.current_status == "edited"){
      return LangStrings.gs("order_edited_message");
    }else if(o.current_status == "customer-pickuped"){
      return LangStrings.gs("order_customer_pickuped_message");
    }else if(o.current_status == "waiting-customer-pickup"){
      return LangStrings.gs("order_waiting_customer_pickup_message");
    }else if(o.current_status == "waiting-customer-delivery"){
      return LangStrings.gs("order_waiting_customer_delivery_message");
    }else if(o.current_status == "customer-delivered"){
      return LangStrings.gs("order_customer_delivered_message");
    }else{
      return LangStrings.gs("order_is_waiting_message");
    }
  }

  Color getOrderStatusColor(Order o) {
    if(o.current_status == "is-waiting"){
      return Colors.blue;
    }else if(o.current_status == "managed"){
      return Colors.orange;
    }else if(o.current_status == "started"){
      return Colors.brown;
    }else if(o.current_status == "successful"){
      return Colors.green;
    }else if(o.current_status == "failed"){
      return Colors.blueGrey;
    }else if(o.current_status == "cancel"){
      return Colors.black;
    }else if(o.current_status == "deleted"){
      return Colors.red;
    }else {
      return Colors.blue;
    };
  }

}
