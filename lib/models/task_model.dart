import 'package:easy_services/models/job.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'task_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Task{

  int id;
  String current_status;
  String business_username;
  int creator_id;
  String created_by;
  bool is_unassigned;
  bool is_completed;
  bool is_draft;
  bool on_time;
  List<Job> pickups;

  List<Job> deliveries;


  List<Job> getPickups() {
    return pickups == null?[]:pickups;
  }

  List<Job> getDeliveries(){
    return deliveries == null?[]:deliveries;
  }


  String created_at;
  String updated_at;
  String deleted_at;


  Task(this.id, this.current_status, this.business_username, this.creator_id,
      this.created_by, this.is_unassigned, this.is_completed, this.is_draft, this.on_time,
      this.pickups, this.deliveries, this.created_at, this.updated_at,
      this.deleted_at);

  /// A necessary factory constructor for creating a new User this
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory Task.fromJson(Map<String, dynamic> json) => _$TaskFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$TaskToJson(this);

  Map<String, String> toForm() =>  <String, String>{
    'current_status': this.current_status,
    'business_username': this.business_username,
    'creator_id': "${this.creator_id}",
    'created_by': "${this.created_by}",
    'is_unassigned': "${this.is_unassigned?1:0}",
    'is_completed': "${this.is_completed?1:0}",
    'is_draft': "${this.is_draft?1:0}"
  };


}