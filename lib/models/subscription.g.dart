// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'subscription.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Subscription _$SubscriptionFromJson(Map<String, dynamic> json) {
  return Subscription(
    json['id'] as int,
    json['title'] as String,
    json['description'] as String,
    json['meta_data'] as String,
    json['delay'] as String,
    json['delay_unit'] as String,
    json['cost'] as String,
    json['cost_currency'] as String,
    json['slug'] as String,
    json['service_slug'] as String,
    json['service_name'] as String,
  );
}

Map<String, dynamic> _$SubscriptionToJson(Subscription instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'meta_data': instance.meta_data,
      'delay': instance.delay,
      'delay_unit': instance.delay_unit,
      'cost': instance.cost,
      'cost_currency': instance.cost_currency,
      'slug': instance.slug,
      'service_slug': instance.service_slug,
      'service_name': instance.service_name,
    };
