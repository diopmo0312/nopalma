// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_subscription.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerSubscription _$CustomerSubscriptionFromJson(Map<String, dynamic> json) {
  return CustomerSubscription(
    json['id'] as int,
    json['subscription_id'] as int,
    json['customer_id'] as int,
    CustomerSubscription._datetimeFromString(json['delay'] as String),
    json['is_active'] as bool,
    json['subscription'] == null
        ? null
        : Subscription.fromJson(json['subscription'] as Map<String, dynamic>),
    json['slot_week_day'] as String,
    json['slot_time'] as String,
    json['current_balance'] as String,
    json['old_balance'] as String,
    CustomerSubscription._datetimeFromString(json['created_at'] as String),
  );
}

Map<String, dynamic> _$CustomerSubscriptionToJson(
        CustomerSubscription instance) =>
    <String, dynamic>{
      'id': instance.id,
      'subscription_id': instance.subscription_id,
      'customer_id': instance.customer_id,
      'delay': CustomerSubscription.__datetimeToString(instance.delay),
      'is_active': instance.is_active,
      'subscription': instance.subscription,
      'slot_week_day': instance.slot_week_day,
      'slot_time': instance.slot_time,
      'current_balance': instance.current_balance,
      'old_balance': instance.old_balance,
      'created_at':
          CustomerSubscription.__datetimeToString(instance.created_at),
    };
