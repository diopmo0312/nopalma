import 'package:json_annotation/json_annotation.dart';

/// This allows the `Formula` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'formula.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Formula{

  int id;
  String name;
  String time_slot;
  String delivery_delay;
  String delivery_fees;

  Formula({
    this.id,
    this.name,
    this.time_slot,
    this.delivery_delay,
    this.delivery_fees,
  });

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$FormulaFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory  Formula.fromJson(Map<String, dynamic> json) => _$FormulaFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$FormulaToJson(this);



}