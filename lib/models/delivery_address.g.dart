// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'delivery_address.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeliveryAddress _$DeliveryAddressFromJson(Map<String, dynamic> json) {
  return DeliveryAddress(
    json['id'] as int,
    json['address_name'] as String,
    json['place'] as String,
    json['town'] as String,
    json['address_typed'] as String,
    json['address_type'] as String,
    json['customer_id'] as int,
  );
}

Map<String, dynamic> _$DeliveryAddressToJson(DeliveryAddress instance) =>
    <String, dynamic>{
      'id': instance.id,
      'address_name': instance.address_name,
      'place': instance.place,
      'town': instance.town,
      'address_typed': instance.address_typed,
      'address_type': instance.address_type,
      'customer_id': instance.customer_id,
    };
