// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    json['id'] as int,
    json['name'] as String,
    json['first_name'] as String,
    json['last_name'] as String,
    json['email'] as String,
    json['phone'] as String,
    json['country_code'] as String,
    json['dial_code'] as String,
    json['phone_number'] as String,
    json['company_name'] as String,
    json['account_type'] as String,
    json['photo'] as String,
    json['is_phone_verified'] as bool,
    json['is_email_verified'] as bool,
    json['is_first_login'] as bool,
    json['is_blocked'] as bool,
    json['timezone'] as String,
    json['created_at'] as String,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'email': instance.email,
      'phone': instance.phone,
      'country_code': instance.country_code,
      'dial_code': instance.dial_code,
      'phone_number': instance.phone_number,
      'company_name': instance.company_name,
      'account_type': instance.account_type,
      'photo': instance.photo,
      'is_phone_verified': instance.is_phone_verified,
      'is_email_verified': instance.is_email_verified,
      'is_first_login': instance.is_first_login,
      'is_blocked': instance.is_blocked,
      'timezone': instance.timezone,
      'created_at': instance.created_at,
    };
