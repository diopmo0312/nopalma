import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'customer_notification.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class CustomerNotification{

  int id;
  String title;
  String title_en;
  String subtitle;
  String subtitle_en;
  String action;
  String action_by;
  String meta_data;
  String type_notification;
  bool is_read;
  bool is_received;
  String data;
  int user_id;
  int data_id;

  CustomerNotification(this.id,
      this.title,
      this.title_en,
      this.subtitle,
      this.subtitle_en,
      this.action,
      this.action_by,
      this.meta_data,
      this.type_notification,
      this.is_read,
      this.is_received, this.data, this.user_id, this.data_id);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory CustomerNotification.fromJson(Map<String, dynamic> json) => _$CustomerNotificationFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$CustomerNotificationToJson(this);

}