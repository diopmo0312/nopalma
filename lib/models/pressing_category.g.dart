// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pressing_category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PressingCategory _$PressingCategoryFromJson(Map<String, dynamic> json) {
  return PressingCategory(
    json['id'] as int,
    json['name'] as String,
    json['name_en'] as String,
    json['slug'] as String,
  );
}

Map<String, dynamic> _$PressingCategoryToJson(PressingCategory instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'name_en': instance.name_en,
      'slug': instance.slug,
    };
