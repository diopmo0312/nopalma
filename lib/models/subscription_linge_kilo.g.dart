// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'subscription_linge_kilo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubscriptionLingeKilo _$SubscriptionLingeKiloFromJson(
    Map<String, dynamic> json) {
  return SubscriptionLingeKilo(
    json['slug'] as String,
    json['name'] as String,
    json['quantity'] as int,
    json['quantity_unit'] as String,
    json['number_of_passage'] as int,
    json['delay'] as int,
    json['delay_unit'] as String,
    json['amount'] as int,
  );
}

Map<String, dynamic> _$SubscriptionLingeKiloToJson(
        SubscriptionLingeKilo instance) =>
    <String, dynamic>{
      'slug': instance.slug,
      'name': instance.name,
      'quantity': instance.quantity,
      'quantity_unit': instance.quantity_unit,
      'number_of_passage': instance.number_of_passage,
      'amount': instance.amount,
      'delay': instance.delay,
      'delay_unit': instance.delay_unit,
    };
