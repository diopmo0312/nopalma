import 'package:json_annotation/json_annotation.dart';

/// This allows the `RetouchingResult` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'home_slide.g.dart';

@JsonSerializable()
class HomeSlide{

  int id;
  String image_url;
  String title;
  String subtitle;

  String button_title;
  String target_url;

  String action;

  HomeSlide(this.id, this.image_url, this.title, this.subtitle, this.button_title,
      this.target_url, this.action);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory HomeSlide.fromJson(Map<String, dynamic> json) => _$HomeSlideFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$HomeSlideToJson(this);




}