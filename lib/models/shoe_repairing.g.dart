// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shoe_repairing.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShoeRepairing _$ShoeRepairingFromJson(Map<String, dynamic> json) {
  return ShoeRepairing(
    json['id'] as int,
    json['name'] as String,
    json['name_en'] as String,
    json['slug'] as String,
    json['image_url'] as String,
    json['price'] as String,
    json['currency'] as String,
    json['category_slug'] as String,
  );
}

Map<String, dynamic> _$ShoeRepairingToJson(ShoeRepairing instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'name_en': instance.name_en,
      'slug': instance.slug,
      'image_url': instance.image_url,
      'price': instance.price,
      'currency': instance.currency,
      'category_slug': instance.category_slug,
    };
