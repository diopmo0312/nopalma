// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'subscription_pressing.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubscriptionPressing _$SubscriptionPressingFromJson(Map<String, dynamic> json) {
  return SubscriptionPressing(
    json['slug'] as String,
    json['name'] as String,
    json['quantity_clothing'] as int,
    json['quantity_costume_tailors'] as int,
    json['quantity_unit'] as String,
    json['quantity_label'] as String,
    json['number_of_passage'] as int,
    json['delay'] as int,
    json['delay_unit'] as String,
    json['amount'] as int,
  );
}

Map<String, dynamic> _$SubscriptionPressingToJson(
        SubscriptionPressing instance) =>
    <String, dynamic>{
      'slug': instance.slug,
      'name': instance.name,
      'quantity_clothing': instance.quantity_clothing,
      'quantity_costume_tailors': instance.quantity_costume_tailors,
      'quantity_unit': instance.quantity_unit,
      'quantity_label': instance.quantity_label,
      'number_of_passage': instance.number_of_passage,
      'amount': instance.amount,
      'delay': instance.delay,
      'delay_unit': instance.delay_unit,
    };
