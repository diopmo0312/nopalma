// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'retouching.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Retouching _$RetouchingFromJson(Map<String, dynamic> json) {
  return Retouching(
    json['id'] as int,
    json['name'] as String,
    json['name_en'] as String,
    json['slug'] as String,
    json['image_url'] as String,
    json['price'] as String,
    json['currency'] as String,
    json['category_slug'] as String,
  );
}

Map<String, dynamic> _$RetouchingToJson(Retouching instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'name_en': instance.name_en,
      'slug': instance.slug,
      'image_url': instance.image_url,
      'price': instance.price,
      'currency': instance.currency,
      'category_slug': instance.category_slug,
    };
