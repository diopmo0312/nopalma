// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Order _$OrderFromJson(Map<String, dynamic> json) {
  return Order(
    json['id'] as int,
    json['customer_id'] as int,
    json['service_slug'] as String,
    json['metadata'] as String,
    (json['items'] as List)
        ?.map((e) =>
            e == null ? null : OrderItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['invoice'] == null
        ? null
        : Invoice.fromJson(json['invoice'] as Map<String, dynamic>),
    (json['histories'] as List)
        ?.map((e) =>
            e == null ? null : OrderHistory.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['is_waiting'] as bool,
    json['managed_by'] as int,
    json['current_status'] as String,
    json['rating'] as String,
    json['note'] as String,
    Order._datetimeFromString(json['note_date'] as String),
    json['pickup_slot'] as String,
    json['delivery_slot'] as String,
    json['pickup'] == null
        ? null
        : DeliveryAddress.fromJson(json['pickup'] as Map<String, dynamic>),
    json['delivery'] == null
        ? null
        : DeliveryAddress.fromJson(json['delivery'] as Map<String, dynamic>),
    json['delivery_formular_slug'] as String,
    json['delivery_fees'] as String,
    Order._datetimeFromString(json['created_at'] as String),
  );
}

Map<String, dynamic> _$OrderToJson(Order instance) => <String, dynamic>{
      'id': instance.id,
      'customer_id': instance.customer_id,
      'service_slug': instance.service_slug,
      'metadata': instance.metadata,
      'items': instance.items,
      'invoice': instance.invoice,
      'histories': instance.histories,
      'is_waiting': instance.is_waiting,
      'managed_by': instance.managed_by,
      'current_status': instance.current_status,
      'rating': instance.rating,
      'note': instance.note,
      'note_date': Order.__datetimeToString(instance.note_date),
      'pickup_slot': instance.pickup_slot,
      'delivery_slot': instance.delivery_slot,
      'pickup': instance.pickup,
      'delivery': instance.delivery,
      'delivery_formular_slug': instance.delivery_formular_slug,
      'delivery_fees': instance.delivery_fees,
      'created_at': Order.__datetimeToString(instance.created_at),
    };
