import 'package:easy_services/models/job_history.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'job.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()

class Job{

  int id;
  String order_id;
  String order_description;
  String description;
  String pickup_phone;
  String pickup_name;
  String pickup_email;
  String pickup_address;
  double pickup_latitude;
  double pickup_longitude;
  String pickup_datetime;
  bool has_pickup;
  bool has_delivery;
  String tracking_link;
  String timezone;
  String pickup_custom_field_template;
  String pickup_meta_data;
  String delivery_customer_email;
  String delivery_customer_username;
  String delivery_customer_phone;
  String delivery_customer_address;
  double delivery_latitude;
  double delivery_longitude;
  String job_delivery_datetime;
  String delivery_custom_field_template;
  String delivery_meta_data;
  int team_id;
  bool auto_assignment;
  String fleet_id;
  String p_ref_images;
  String notify;
  String geofence;
  String tags;
  String signatures;
  String current_status;
  String pickup_location_address_typed;
  String pickup_location_address_type;
  String pickup_location_address_type_stage;
  String delivery_location_address_typed;
  String delivery_location_address_type;
  String delivery_location_address_type_stage;
  String task_id;

  List<JobHistory> histories;

  String created_at;
  String updated_at;
  String deleted_at;


  Job(this.id, this.order_id, this.order_description, this.description,
      this.pickup_phone, this.pickup_name, this.pickup_email,
      this.pickup_address, this.pickup_latitude, this.pickup_longitude,
      this.pickup_datetime, this.has_pickup, this.has_delivery,
      this.tracking_link, this.timezone, this.pickup_custom_field_template,
      this.pickup_meta_data, this.delivery_customer_email,
      this.delivery_customer_username, this.delivery_customer_phone,
      this.delivery_customer_address, this.delivery_latitude,
      this.delivery_longitude, this.job_delivery_datetime,
      this.delivery_custom_field_template, this.delivery_meta_data,
      this.team_id, this.auto_assignment, this.fleet_id, this.p_ref_images,
      this.notify, this.geofence, this.tags, this.signatures,
      this.current_status, this.pickup_location_address_typed,
      this.pickup_location_address_type,
      this.pickup_location_address_type_stage,
      this.delivery_location_address_typed, this.delivery_location_address_type,
      this.delivery_location_address_type_stage,
      this.task_id,
      this.histories,

      this.created_at,
      this.updated_at, this.deleted_at);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory Job.fromJson(Map<String, dynamic> json) => _$JobFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$JobToJson(this);

  @override
  int get hashCode => id;

  @override
  bool operator ==(o) => o is Job && o.id == id;

}