// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_method.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentMethod _$PaymentMethodFromJson(Map<String, dynamic> json) {
  return PaymentMethod(
    json['id'] as int,
    json['title'] as String,
    json['title_en'] as String,
    json['description'] as String,
    json['slug'] as String,
    json['photo'] as String,
  );
}

Map<String, dynamic> _$PaymentMethodToJson(PaymentMethod instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'title_en': instance.title_en,
      'description': instance.description,
      'slug': instance.slug,
      'photo': instance.photo,
    };
