// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountType _$AccountTypeFromJson(Map<String, dynamic> json) {
  return AccountType(
    json['slug'] as String,
    json['name'] as String,
  );
}

Map<String, dynamic> _$AccountTypeToJson(AccountType instance) =>
    <String, dynamic>{
      'slug': instance.slug,
      'name': instance.name,
    };
