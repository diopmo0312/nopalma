// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'session.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Session _$SessionFromJson(Map<String, dynamic> json) {
  return Session(
    json['id'] as int,
    json['customer_id'] as int,
    json['is_active'] as bool,
    json['location_address'] as String,
    (json['location_latitude'] as num)?.toDouble(),
    (json['location_longitude'] as num)?.toDouble(),
    json['battery'] as String,
    json['version'] as String,
    json['device'] as String,
    json['created_at'] as String,
    json['updated_at'] as String,
    json['deleted_at'] as String,
  );
}

Map<String, dynamic> _$SessionToJson(Session instance) => <String, dynamic>{
      'id': instance.id,
      'customer_id': instance.customer_id,
      'is_active': instance.is_active,
      'location_address': instance.location_address,
      'location_latitude': instance.location_latitude,
      'location_longitude': instance.location_longitude,
      'battery': instance.battery,
      'version': instance.version,
      'device': instance.device,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
      'deleted_at': instance.deleted_at,
    };
