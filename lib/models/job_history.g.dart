// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'job_history.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JobHistory _$JobHistoryFromJson(Map<String, dynamic> json) {
  return JobHistory(
    json['id'] as int,
    json['status'] as String,
    json['creator_id'] as int,
    json['creator_name'] as String,
    json['creator'] as String,
    json['task_id'] as int,
    json['job_id'] as int,
    json['agent_id'] as int,
    json['agent_name'] as String,
    json['job_location_address'] as String,
    json['created_at'] as String,
    json['updated_at'] as String,
    json['deleted_at'] as String,
  );
}

Map<String, dynamic> _$JobHistoryToJson(JobHistory instance) =>
    <String, dynamic>{
      'id': instance.id,
      'status': instance.status,
      'creator_id': instance.creator_id,
      'creator_name': instance.creator_name,
      'creator': instance.creator,
      'task_id': instance.task_id,
      'job_id': instance.job_id,
      'agent_id': instance.agent_id,
      'agent_name': instance.agent_name,
      'job_location_address': instance.job_location_address,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
      'deleted_at': instance.deleted_at,
    };
