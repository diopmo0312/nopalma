import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'receipt.g.dart';

@JsonSerializable()
class Receipt{

  int id;
  int invoice_id;
  String payment_method;
  String payment_reference;
  String amount;
  String currency;
  String coupon;
  String creator_id;
  String creator_name;
  String creator;

  Receipt(this.id, this.invoice_id, this.payment_method, this.payment_reference,
      this.amount, this.currency, this.coupon, this.creator_id,
      this.creator_name, this.creator);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory Receipt.fromJson(Map<String, dynamic> json) => _$ReceiptFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$ReceiptToJson(this);


}