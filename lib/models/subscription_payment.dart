import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'subscription_payment.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class SubscriptionPayment{
  
  int id;
  int subscription_id;
  int customer_subscription_id;
  int customer_id;
  String amount;
  String currency;
  String payment_method;
  String payment_reference;
  String coupon;


  SubscriptionPayment(this.id, this.subscription_id,
      this.customer_subscription_id, this.customer_id, this.amount,
      this.currency, this.payment_method, this.payment_reference, this.coupon);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory SubscriptionPayment.fromJson(Map<String, dynamic> json) => _$SubscriptionPaymentFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$SubscriptionPaymentToJson(this);


}