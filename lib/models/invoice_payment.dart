import 'package:json_annotation/json_annotation.dart';

part 'invoice_payment.g.dart';

@JsonSerializable()
class InvoicePayment{

  int id;

  String title;

  String amount;

  String currency_code;

  String status;

  String payment_reference;

  String transaction_designation;

  String sender;

  String created_at;

  InvoicePayment(this.id, this.title, this.amount, this.currency_code,
      this.status, this.payment_reference, this.transaction_designation,
      this.sender, this.created_at);

  factory InvoicePayment.fromJson(Map<String, dynamic> json) => _$InvoicePaymentFromJson(json);

  Map<String, dynamic> toJson() => _$InvoicePaymentToJson(this);



}