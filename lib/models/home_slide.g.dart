// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_slide.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeSlide _$HomeSlideFromJson(Map<String, dynamic> json) {
  return HomeSlide(
    json['id'] as int,
    json['image_url'] as String,
    json['title'] as String,
    json['subtitle'] as String,
    json['button_title'] as String,
    json['target_url'] as String,
    json['action'] as String,
  );
}

Map<String, dynamic> _$HomeSlideToJson(HomeSlide instance) => <String, dynamic>{
      'id': instance.id,
      'image_url': instance.image_url,
      'title': instance.title,
      'subtitle': instance.subtitle,
      'button_title': instance.button_title,
      'target_url': instance.target_url,
      'action': instance.action,
    };
