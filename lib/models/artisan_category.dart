import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/ServicePricing.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'artisan_category.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class ArtisanCategory implements ServicePricing {
  int id;
  int service_id;
  String name;
  String name_en;
  String description;
  String description_en;
  String slug;
  String price;
  String diagnostic_price;
  String currency;
  String logo;
  String category_slug;


  ArtisanCategory(this.id, this.service_id, this.name, this.name_en,
      this.description, this.description_en, this.slug, this.price,
      this.diagnostic_price, this.currency, this.logo, this.category_slug);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory ArtisanCategory.fromJson(Map<String, dynamic> json) =>
      _$ArtisanCategoryFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$ArtisanCategoryToJson(this);

  @override
  String toString() {
    return jsonEncode(toJson());
  }

  @override
  String categorySlugValue() {
    return category_slug;
  }

  @override
  String currencyValue() {
    return currency;
  }

  @override
  String nameValue() {
    return LangStrings.strLang == 'FR' ? name : name_en;
  }

  @override
  String photoValue() {
    return logo;
  }

  @override
  String priceValue() {
    return price;
  }

  @override
  String quantityUnitValue() {
    return "number";
  }

  @override
  String serviceSlugValue() {
    return "ouvrier_et_artisan";
  }

  @override
  String slugValue() {
    return slug;
  }

  @override
  String toJsonEncoded() {
    return toString();
  }
}
