// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'linge_kilo_option.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LingeKiloOption _$LingeKiloOptionFromJson(Map<String, dynamic> json) {
  return LingeKiloOption(
    json['id'] as int,
    json['slug'] as String,
    json['name'] as String,
    json['name_en'] as String,
    json['price'] as String,
    json['currency'] as String,
    json['description'] as String,
    json['description_en'] as String,
    json['image_url'] as String,
  );
}

Map<String, dynamic> _$LingeKiloOptionToJson(LingeKiloOption instance) =>
    <String, dynamic>{
      'id': instance.id,
      'slug': instance.slug,
      'name': instance.name,
      'name_en': instance.name_en,
      'price': instance.price,
      'currency': instance.currency,
      'description': instance.description,
      'description_en': instance.description_en,
      'image_url': instance.image_url,
    };
