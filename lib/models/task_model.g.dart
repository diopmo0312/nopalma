// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Task _$TaskFromJson(Map<String, dynamic> json) {
  return Task(
    json['id'] as int,
    json['current_status'] as String,
    json['business_username'] as String,
    json['creator_id'] as int,
    json['created_by'] as String,
    json['is_unassigned'] as bool,
    json['is_completed'] as bool,
    json['is_draft'] as bool,
    json['on_time'] as bool,
    (json['pickups'] as List)
        ?.map((e) => e == null ? null : Job.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['deliveries'] as List)
        ?.map((e) => e == null ? null : Job.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['created_at'] as String,
    json['updated_at'] as String,
    json['deleted_at'] as String,
  );
}

Map<String, dynamic> _$TaskToJson(Task instance) => <String, dynamic>{
      'id': instance.id,
      'current_status': instance.current_status,
      'business_username': instance.business_username,
      'creator_id': instance.creator_id,
      'created_by': instance.created_by,
      'is_unassigned': instance.is_unassigned,
      'is_completed': instance.is_completed,
      'is_draft': instance.is_draft,
      'on_time': instance.on_time,
      'pickups': instance.pickups,
      'deliveries': instance.deliveries,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
      'deleted_at': instance.deleted_at,
    };
