import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'user.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()

class User{

  int id;
  String name;
  String first_name;
  String last_name;
  String email;
  String phone;
  String country_code;
  String dial_code;
  String phone_number;
  String company_name;
  String account_type;
  String photo;
  bool is_phone_verified;
  bool is_email_verified;
  bool is_first_login;
  bool is_blocked;
  String timezone;
  String created_at;


  User(this.id, this.name, this.first_name, this.last_name, this.email,
      this.phone, this.country_code, this.dial_code, this.phone_number,
      this.company_name, this.account_type, this.photo, this.is_phone_verified,
      this.is_email_verified, this.is_first_login, this.is_blocked,
      this.timezone, this.created_at);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$UserToJson(this);
}