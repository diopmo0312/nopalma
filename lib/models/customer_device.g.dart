// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_device.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerDevice _$CustomerDeviceFromJson(Map<String, dynamic> json) {
  return CustomerDevice(
    json['id'] as int,
    json['customer_id'] as int,
    json['firebase_id'] as String,
  );
}

Map<String, dynamic> _$CustomerDeviceToJson(CustomerDevice instance) =>
    <String, dynamic>{
      'id': instance.id,
      'customer_id': instance.customer_id,
      'firebase_id': instance.firebase_id,
    };
