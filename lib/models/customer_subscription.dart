import 'package:easy_services/models/subscription.dart';
import 'package:flutter/cupertino.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'customer_subscription.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class CustomerSubscription{

  int id;
  int subscription_id;
  int customer_id;
  @JsonKey(
      name: "delay",
      fromJson: _datetimeFromString,
      toJson: __datetimeToString
  )
  DateTime delay;
  bool is_active;

  Subscription subscription;

  String slot_week_day;
  String slot_time;

  String current_balance;
  String old_balance;


  @JsonKey(
      name: "created_at",
      fromJson: _datetimeFromString,
      toJson: __datetimeToString
  )
  DateTime created_at;


  CustomerSubscription(
      this.id,
      this.subscription_id,
      this.customer_id,
      this.delay,
      this.is_active,
      this.subscription,
      this.slot_week_day,
      this.slot_time,
      this.current_balance,
      this.old_balance,
      this.created_at);

  static DateTime _datetimeFromString(String delay) =>
      delay == null ? null:DateTime.parse(delay);

  static String __datetimeToString(DateTime datetime) =>
      datetime == null ? null:datetime.toIso8601String();

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory CustomerSubscription.fromJson(Map<String, dynamic> json) => _$CustomerSubscriptionFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$CustomerSubscriptionToJson(this);


}