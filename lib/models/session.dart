import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'session.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Session{

  int id;
  int customer_id;
  bool is_active;

  String location_address; //Last Location Address
  double location_latitude;  //Last Location latitude
  double location_longitude; //Last Location longitude
  String battery;
  String version;
  String device;

  String created_at;
  String updated_at;
  String deleted_at;


  Session(this.id, this.customer_id, this.is_active, this.location_address,
      this.location_latitude, this.location_longitude, this.battery,
      this.version, this.device, this.created_at, this.updated_at,
      this.deleted_at);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory Session.fromJson(Map<String, dynamic> json) => _$SessionFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$SessionToJson(this);




}