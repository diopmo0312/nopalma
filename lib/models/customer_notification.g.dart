// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerNotification _$CustomerNotificationFromJson(Map<String, dynamic> json) {
  return CustomerNotification(
    json['id'] as int,
    json['title'] as String,
    json['title_en'] as String,
    json['subtitle'] as String,
    json['subtitle_en'] as String,
    json['action'] as String,
    json['action_by'] as String,
    json['meta_data'] as String,
    json['type_notification'] as String,
    json['is_read'] as bool,
    json['is_received'] as bool,
    json['data'] as String,
    json['user_id'] as int,
    json['data_id'] as int,
  );
}

Map<String, dynamic> _$CustomerNotificationToJson(
        CustomerNotification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'title_en': instance.title_en,
      'subtitle': instance.subtitle,
      'subtitle_en': instance.subtitle_en,
      'action': instance.action,
      'action_by': instance.action_by,
      'meta_data': instance.meta_data,
      'type_notification': instance.type_notification,
      'is_read': instance.is_read,
      'is_received': instance.is_received,
      'data': instance.data,
      'user_id': instance.user_id,
      'data_id': instance.data_id,
    };
