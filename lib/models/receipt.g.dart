// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'receipt.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Receipt _$ReceiptFromJson(Map<String, dynamic> json) {
  return Receipt(
    json['id'] as int,
    json['invoice_id'] as int,
    json['payment_method'] as String,
    json['payment_reference'] as String,
    json['amount'] as String,
    json['currency'] as String,
    json['coupon'] as String,
    json['creator_id'] as String,
    json['creator_name'] as String,
    json['creator'] as String,
  );
}

Map<String, dynamic> _$ReceiptToJson(Receipt instance) => <String, dynamic>{
      'id': instance.id,
      'invoice_id': instance.invoice_id,
      'payment_method': instance.payment_method,
      'payment_reference': instance.payment_reference,
      'amount': instance.amount,
      'currency': instance.currency,
      'coupon': instance.coupon,
      'creator_id': instance.creator_id,
      'creator_name': instance.creator_name,
      'creator': instance.creator,
    };
