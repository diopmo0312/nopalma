import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'order_history.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class OrderHistory{

  int id;
  int order_id;
  int agent_id;

  String status;
  String agent_name;
  int creator_id;
  String creator_name;
  String creator;

  @JsonKey(
      name: "created_at",
      fromJson: _datetimeFromString,
      toJson: __datetimeToString
  )
  DateTime created_at;


  OrderHistory(this.id, this.order_id, this.agent_id, this.status,
      this.agent_name, this.creator_id, this.creator_name, this.creator,
      this.created_at);

  static DateTime _datetimeFromString(String delay) =>
      delay == null ? null: DateTime.parse(delay);

  static String __datetimeToString(DateTime datetime) =>
      datetime == null ? null:datetime.toIso8601String();

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory  OrderHistory.fromJson(Map<String, dynamic> json) => _$OrderHistoryFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$OrderHistoryToJson(this);
}