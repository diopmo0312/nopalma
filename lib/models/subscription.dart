import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'subscription.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Subscription{
  
  int id;
  String title;
  String description;
  String meta_data;
  String delay;
  String delay_unit;
  String cost;
  String cost_currency;
  String slug;
  String service_slug;
  String service_name;

  Subscription(this.id, this.title, this.description, this.meta_data,
      this.delay, this.delay_unit, this.cost, this.cost_currency, this.slug,
      this.service_slug, this.service_name);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory Subscription.fromJson(Map<String, dynamic> json) => _$SubscriptionFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$SubscriptionToJson(this);


}