// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'invoice_payment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InvoicePayment _$InvoicePaymentFromJson(Map<String, dynamic> json) {
  return InvoicePayment(
    json['id'] as int,
    json['title'] as String,
    json['amount'] as String,
    json['currency_code'] as String,
    json['status'] as String,
    json['payment_reference'] as String,
    json['transaction_designation'] as String,
    json['sender'] as String,
    json['created_at'] as String,
  );
}

Map<String, dynamic> _$InvoicePaymentToJson(InvoicePayment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'amount': instance.amount,
      'currency_code': instance.currency_code,
      'status': instance.status,
      'payment_reference': instance.payment_reference,
      'transaction_designation': instance.transaction_designation,
      'sender': instance.sender,
      'created_at': instance.created_at,
    };
