// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'artisan_category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ArtisanCategory _$ArtisanCategoryFromJson(Map<String, dynamic> json) {
  return ArtisanCategory(
    json['id'] as int,
    json['service_id'] as int,
    json['name'] as String,
    json['name_en'] as String,
    json['description'] as String,
    json['description_en'] as String,
    json['slug'] as String,
    json['price'] as String,
    json['diagnostic_price'] as String,
    json['currency'] as String,
    json['logo'] as String,
    json['category_slug'] as String,
  );
}

Map<String, dynamic> _$ArtisanCategoryToJson(ArtisanCategory instance) =>
    <String, dynamic>{
      'id': instance.id,
      'service_id': instance.service_id,
      'name': instance.name,
      'name_en': instance.name_en,
      'description': instance.description,
      'description_en': instance.description_en,
      'slug': instance.slug,
      'price': instance.price,
      'diagnostic_price': instance.diagnostic_price,
      'currency': instance.currency,
      'logo': instance.logo,
      'category_slug': instance.category_slug,
    };
