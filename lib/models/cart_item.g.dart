// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CartItem _$CartItemFromJson(Map<String, dynamic> json) {
  return CartItem(
    json['service_slug'] as String,
    json['service_name'] as String,
    (json['items'] as List)
        ?.map((e) => e == null
            ? null
            : OrderItemRequest.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['amount'] as String,
    json['currency'] as String,
  );
}

Map<String, dynamic> _$CartItemToJson(CartItem instance) => <String, dynamic>{
      'service_slug': instance.service_slug,
      'service_name': instance.service_name,
      'items': instance.items,
      'amount': instance.amount,
      'currency': instance.currency,
    };
