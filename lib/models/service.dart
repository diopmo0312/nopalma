import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/ServicePricing.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'service.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Service implements ServicePricing{

  int id;
  String name;
  String name_en;
  String description;
  String logo;
  String slug;
  bool price_is_variable;
  String price;
  String currency;
  String price_per;
  String price_description;
  String price_description_en;
  String delay;
  String delay_en;
  int delay_time;
  String delay_time_unit;
  int rank;


  Service(this.id,
      this.name,
      this.name_en,
      this.description,
      this.logo,
      this.slug,
      this.price_is_variable,
      this.price,
      this.currency,
      this.price_per,
      this.price_description,
      this.price_description_en,
      this.delay,
      this.delay_en,
      this.delay_time,
      this.delay_time_unit, this.rank);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory Service.fromJson(Map<String, dynamic> json) => _$ServiceFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$ServiceToJson(this);

  @override
  String toString() {
    return jsonEncode(toJson());
  }

  @override
  String categorySlugValue() {
    return null;
  }

  @override
  String currencyValue() {
    return currency;
  }

  @override
  String nameValue() {
    return LangStrings.strLang=='FR'?name:name_en;
  }

  @override
  String photoValue() {
    return logo;
  }

  @override
  String priceValue() {
    return price;
  }

  @override
  String quantityUnitValue() {
    return "KG";
  }

  @override
  String serviceSlugValue() {
    return slug;
  }

  @override
  String slugValue() {
    return slug;
  }

  @override
  String toJsonEncoded() {
    return toString();
  }

}