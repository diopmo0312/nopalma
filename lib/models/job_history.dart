import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'job_history.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()

class JobHistory{

  int id;
  String status;
  int creator_id;
  String creator_name;
  String creator;
  int task_id;
  int job_id;
  int agent_id;
  String agent_name;
  String job_location_address;
  String created_at;
  String updated_at;
  String deleted_at;


  JobHistory(this.id, this.status, this.creator_id, this.creator_name,
      this.creator, this.task_id, this.job_id, this.agent_id, this.agent_name,
      this.job_location_address, this.created_at, this.updated_at,
      this.deleted_at);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory JobHistory.fromJson(Map<String, dynamic> json) => _$JobHistoryFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$JobHistoryToJson(this);


}