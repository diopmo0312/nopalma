import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/ServicePricing.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'pressing_clothes.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class PressingClothes implements ServicePricing{

  int id;
  String name;
  String name_en;
  String slug;
  String image_url;
  String price;
  String currency;
  String category_slug;

  PressingClothes(this.id, this.name, this.name_en, this.slug, this.image_url, this.price,
      this.currency, this.category_slug);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory PressingClothes.fromJson(Map<String, dynamic> json) => _$PressingClothesFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$PressingClothesToJson(this);

  @override
  String toString() {
    return jsonEncode(toJson());
  }
  @override
  bool operator == (other) {
    return id == (other as PressingClothes).id;
  }

  @override
  String categorySlugValue() {
    return category_slug;
  }

  @override
  String currencyValue() {
    return currency;
  }

  @override
  String nameValue() {
    return LangStrings.strLang=='FR'?name:name_en;
  }

  @override
  String photoValue() {
    return image_url;
  }

  @override
  String priceValue() {
    return price;
  }

  @override
  String quantityUnitValue() {
    return "number";
  }

  @override
  String slugValue() {
    return slug;
  }
  @override
  String toJsonEncoded() {
    return toString();
  }

  @override
  String serviceSlugValue() {
    return "pressing";
  }


}