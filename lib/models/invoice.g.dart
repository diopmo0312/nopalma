// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'invoice.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Invoice _$InvoiceFromJson(Map<String, dynamic> json) {
  return Invoice(
    json['id'] as int,
    json['order_id'] as int,
    json['customer_id'] as int,
    json['reference'] as String,
    json['link'] as String,
    json['subtotal'] as String,
    json['tax'] as String,
    json['fees_delivery'] as String,
    json['total'] as String,
    json['status'] as String,
    json['is_paid'] as bool,
    json['currency'] as String,
    json['receipt'] == null
        ? null
        : Receipt.fromJson(json['receipt'] as Map<String, dynamic>),
    json['created_at'] as String,
  );
}

Map<String, dynamic> _$InvoiceToJson(Invoice instance) => <String, dynamic>{
      'id': instance.id,
      'order_id': instance.order_id,
      'customer_id': instance.customer_id,
      'reference': instance.reference,
      'link': instance.link,
      'subtotal': instance.subtotal,
      'tax': instance.tax,
      'fees_delivery': instance.fees_delivery,
      'total': instance.total,
      'status': instance.status,
      'is_paid': instance.is_paid,
      'currency': instance.currency,
      'receipt': instance.receipt,
      'created_at': instance.created_at,
    };
