// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'subscription_payment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubscriptionPayment _$SubscriptionPaymentFromJson(Map<String, dynamic> json) {
  return SubscriptionPayment(
    json['id'] as int,
    json['subscription_id'] as int,
    json['customer_subscription_id'] as int,
    json['customer_id'] as int,
    json['amount'] as String,
    json['currency'] as String,
    json['payment_method'] as String,
    json['payment_reference'] as String,
    json['coupon'] as String,
  );
}

Map<String, dynamic> _$SubscriptionPaymentToJson(
        SubscriptionPayment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'subscription_id': instance.subscription_id,
      'customer_subscription_id': instance.customer_subscription_id,
      'customer_id': instance.customer_id,
      'amount': instance.amount,
      'currency': instance.currency,
      'payment_method': instance.payment_method,
      'payment_reference': instance.payment_reference,
      'coupon': instance.coupon,
    };
