// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_history.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderHistory _$OrderHistoryFromJson(Map<String, dynamic> json) {
  return OrderHistory(
    json['id'] as int,
    json['order_id'] as int,
    json['agent_id'] as int,
    json['status'] as String,
    json['agent_name'] as String,
    json['creator_id'] as int,
    json['creator_name'] as String,
    json['creator'] as String,
    OrderHistory._datetimeFromString(json['created_at'] as String),
  );
}

Map<String, dynamic> _$OrderHistoryToJson(OrderHistory instance) =>
    <String, dynamic>{
      'id': instance.id,
      'order_id': instance.order_id,
      'agent_id': instance.agent_id,
      'status': instance.status,
      'agent_name': instance.agent_name,
      'creator_id': instance.creator_id,
      'creator_name': instance.creator_name,
      'creator': instance.creator,
      'created_at': OrderHistory.__datetimeToString(instance.created_at),
    };
