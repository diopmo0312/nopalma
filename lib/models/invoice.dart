import 'package:easy_services/models/receipt.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'invoice.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Invoice{

  int id;
  int order_id;
  int customer_id;
  String reference;
  String link;
  String subtotal;
  String tax;
  String fees_delivery;
  String total;
  String status;
  bool is_paid;
  String currency;

  Receipt receipt;

  String created_at;


  Invoice(this.id, this.order_id, this.customer_id, this.reference, this.link,
      this.subtotal, this.tax, this.fees_delivery, this.total, this.status,
      this.is_paid, this.currency, this.receipt, this.created_at);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory Invoice.fromJson(Map<String, dynamic> json) => _$InvoiceFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$InvoiceToJson(this);

}