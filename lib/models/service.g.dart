// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Service _$ServiceFromJson(Map<String, dynamic> json) {
  return Service(
    json['id'] as int,
    json['name'] as String,
    json['name_en'] as String,
    json['description'] as String,
    json['logo'] as String,
    json['slug'] as String,
    json['price_is_variable'] as bool,
    json['price'] as String,
    json['currency'] as String,
    json['price_per'] as String,
    json['price_description'] as String,
    json['price_description_en'] as String,
    json['delay'] as String,
    json['delay_en'] as String,
    json['delay_time'] as int,
    json['delay_time_unit'] as String,
    json['rank'] as int,
  );
}

Map<String, dynamic> _$ServiceToJson(Service instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'name_en': instance.name_en,
      'description': instance.description,
      'logo': instance.logo,
      'slug': instance.slug,
      'price_is_variable': instance.price_is_variable,
      'price': instance.price,
      'currency': instance.currency,
      'price_per': instance.price_per,
      'price_description': instance.price_description,
      'price_description_en': instance.price_description_en,
      'delay': instance.delay,
      'delay_en': instance.delay_en,
      'delay_time': instance.delay_time,
      'delay_time_unit': instance.delay_time_unit,
      'rank': instance.rank,
    };
