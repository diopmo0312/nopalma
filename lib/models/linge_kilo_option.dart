import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/ServicePricing.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'linge_kilo_option.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class LingeKiloOption implements ServicePricing{

  final int id;
  final String slug;
  final String name;
  final String name_en;
  final String price;
  final String currency;
  final String description;
  final String description_en;
  final String image_url;

  LingeKiloOption(this.id, this.slug, this.name,this.name_en, this.price, this.currency, this.description, this.description_en, this.image_url);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory  LingeKiloOption.fromJson(Map<String, dynamic> json) => _$LingeKiloOptionFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$LingeKiloOptionToJson(this);

  @override
  String toString() {
    return jsonEncode(toJson());
  }

  @override
  String categorySlugValue() {
    return null;
  }

  @override
  String currencyValue() {
    return currency;
  }

  @override
  String nameValue() {
    return LangStrings.strLang=='FR'?name:name_en;
  }

  @override
  String photoValue() {
    return null;
  }

  @override
  String priceValue() {
    return price;
  }

  @override
  String quantityUnitValue() {
    return "KG";
  }

  @override
  String serviceSlugValue() {
    return "linge_au_kilo";
  }

  @override
  String slugValue() {
    return slug;
  }

  @override
  String toJsonEncoded() {
    return toString();
  }

  @override
  bool operator ==(Object other) => other is LingeKiloOption &&
              slug == other.slug;

  @override
  int get hashCode => slug.hashCode;

}