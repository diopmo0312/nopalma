// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Request _$RequestFromJson(Map<String, dynamic> json) {
  return Request(
    json['url'] as String,
    json['method'] as String,
    json['responseBody'] as String,
    json['made_at'] == null ? null : DateTime.parse(json['made_at'] as String),
  );
}

Map<String, dynamic> _$RequestToJson(Request instance) => <String, dynamic>{
      'url': instance.url,
      'method': instance.method,
      'responseBody': instance.responseBody,
      'made_at': instance.made_at?.toIso8601String(),
    };
