// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderItem _$OrderItemFromJson(Map<String, dynamic> json) {
  return OrderItem(
    json['id'] as int,
    json['order_id'] as int,
    json['service_slug'] as String,
    json['meta_data'] as String,
    json['quantity'] as String,
    json['quantity_unit'] as String,
    json['unit_price'] as String,
    json['total_amount'] as String,
    json['currency'] as String,
  );
}

Map<String, dynamic> _$OrderItemToJson(OrderItem instance) => <String, dynamic>{
      'id': instance.id,
      'order_id': instance.order_id,
      'service_slug': instance.service_slug,
      'meta_data': instance.meta_data,
      'quantity': instance.quantity,
      'quantity_unit': instance.quantity_unit,
      'unit_price': instance.unit_price,
      'total_amount': instance.total_amount,
      'currency': instance.currency,
    };
