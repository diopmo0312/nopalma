import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'order_item.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class OrderItem{

  int id;

  int order_id;

  String service_slug;

  String meta_data;

  String quantity;

  String quantity_unit;

  String unit_price;

  String total_amount;

  String currency;


  OrderItem(this.id, this.order_id, this.service_slug, this.meta_data,
      this.quantity, this.quantity_unit, this.unit_price, this.total_amount,
      this.currency);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory  OrderItem.fromJson(Map<String, dynamic> json) => _$OrderItemFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$OrderItemToJson(this);

}