// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'formula.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Formula _$FormulaFromJson(Map<String, dynamic> json) {
  return Formula(
    id: json['id'] as int,
    name: json['name'] as String,
    time_slot: json['time_slot'] as String,
    delivery_delay: json['delivery_delay'] as String,
    delivery_fees: json['delivery_fees'] as String,
  );
}

Map<String, dynamic> _$FormulaToJson(Formula instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'time_slot': instance.time_slot,
      'delivery_delay': instance.delivery_delay,
      'delivery_fees': instance.delivery_fees,
    };
