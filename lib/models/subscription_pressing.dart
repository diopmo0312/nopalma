import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'subscription_pressing.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class SubscriptionPressing{
  String slug;
  String name;
  int quantity_clothing;
  int quantity_costume_tailors;
  String quantity_unit;
  String quantity_label;
  int number_of_passage;
  int amount;
  int delay;
  String delay_unit;

  SubscriptionPressing(this.slug, this.name, this.quantity_clothing, this.quantity_costume_tailors,this.quantity_unit, this.quantity_label, this.number_of_passage, this.delay,this.delay_unit, this.amount);

      /// A necessary factory constructor for creating a new SubscriptionLingeKilo instance
  /// from a map. Pass the map to the generated `_$SubscriptionLingeKiloFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory SubscriptionPressing.fromJson(Map<String, dynamic> json) => _$SubscriptionPressingFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$SubscriptionLingeKiloToJson`.
  Map<String, dynamic> toJson() => _$SubscriptionPressingToJson(this);

}