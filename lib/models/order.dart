import 'package:easy_services/models/delivery_address.dart';
import 'package:easy_services/models/invoice.dart';
import 'package:easy_services/models/order_history.dart';
import 'package:easy_services/models/order_item.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'order.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Order implements Comparable<Order>{

  int id;
  int customer_id;
  String service_slug; //["pressing", "retouche"]
  String metadata; //[{"service_name":"Pressing", "service_slug": "pressing", "amount":35000}]
  List<OrderItem> items;
  Invoice invoice;
  List<OrderHistory> histories;
  bool is_waiting;
  int managed_by;
  String current_status;
  String rating;
  String note;
  @JsonKey(
      name: "note_date",
      fromJson: _datetimeFromString,
      toJson: __datetimeToString
  )
  DateTime note_date;

  String pickup_slot;
  String delivery_slot;
  DeliveryAddress pickup;
  DeliveryAddress delivery;

  String delivery_formular_slug;
  String delivery_fees;

  @JsonKey(
      name: "created_at",
      fromJson: _datetimeFromString,
      toJson: __datetimeToString
  )
  DateTime created_at;


  Order(this.id, this.customer_id, this.service_slug, this.metadata, this.items,
      this.invoice, this.histories, this.is_waiting, this.managed_by,
      this.current_status, this.rating, this.note, this.note_date,
      this.pickup_slot, this.delivery_slot, this.pickup,
      this.delivery,
      this.delivery_formular_slug,
      this.delivery_fees,
      this.created_at);

  static DateTime _datetimeFromString(String delay) =>
      delay == null ? null: DateTime.parse(delay);

  static String __datetimeToString(DateTime datetime) =>
      datetime == null ? null:datetime.toIso8601String();

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory Order.fromJson(Map<String, dynamic> json) => _$OrderFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$OrderToJson(this);

  @override
  int compareTo(Order other) {

    return other.id.compareTo(this.id);
  }






}