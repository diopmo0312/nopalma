import 'package:easy_services/rest/request/order_item_request.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'cart_item.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class CartItem{

  String service_slug;
  String service_name;
  List<OrderItemRequest> items;
  String amount;
  String currency;

  CartItem(this.service_slug, this.service_name, this.items, this.amount, this.currency);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory  CartItem.fromJson(Map<String, dynamic> json) => _$CartItemFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$CartItemToJson(this);

  @override
  bool operator ==(other) {
    return (other as CartItem).service_slug == this.service_slug;
  }


}