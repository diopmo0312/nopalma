abstract class ServicePricing{

  String slugValue();
  String nameValue();
  String photoValue();
  String priceValue();
  String currencyValue();
  String quantityUnitValue();
  String categorySlugValue();
  String serviceSlugValue();
  String toJsonEncoded();

}