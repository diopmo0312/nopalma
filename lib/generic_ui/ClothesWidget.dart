import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/pressing_clothes.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';

class ClothesWidget extends StatefulWidget {

  PressingClothes pressingClothes;

  Function onQuantityChanged;

  bool isSelected;

  int initialQuantity;

  ClothesWidget({@required this.pressingClothes, @required this.isSelected, @required this.initialQuantity, @required this.onQuantityChanged});


  @override
  _ClothesWidgetState createState() => _ClothesWidgetState();

}

class _ClothesWidgetState extends State<ClothesWidget> {

  int _quantity;

  @override
  void initState() {
    super.initState();
    if(widget.initialQuantity != null){
       _quantity = widget.initialQuantity;
    }else{
     _quantity = 0;
    }


  }
  @override
  Widget build(BuildContext context) {

    var screenWidth = MediaQuery.of(context).size.width;

    //print("Screen Width $screenWidth");

    var clothesImageWidth = 80.0 * 1.5;
    var clothesImageHeight = 60.0 * 1.5;
    var clothesImagePaddingTop = 5.0 * 2;
    var clothesImagePaddingLeft = 10.0 * 2;
    var clothesTitleHeight = 20.0 * 2;
    var quantityControlWidth = ((screenWidth /3) - 10) * 2;
    var quantityControlHeight = 25.0 * 1.2 ;
    var quantityControlTextSize = 20.0 * 1.2;

    if(screenWidth < 400){
      clothesImageWidth = 60.0 * 1.5;
      clothesImageHeight = 40.0 * 1.5;
      clothesImagePaddingTop = 5.0 * 2;
      clothesImagePaddingLeft = 8.0 * 2;
      clothesTitleHeight = 20.0 * 2;
      quantityControlWidth = ((screenWidth /3) - 15) * 2;
      quantityControlHeight = 20.0 * 1.2 ;
      quantityControlTextSize = 15.0 * 1.2 ;
    }


    return new Container(
      margin: EdgeInsets.all(8.0),
      height: 142.0 *2,
      
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black45,
              blurRadius: 3.0, // has the effect of softening the shadow
              spreadRadius: 1.0, // has the effect of extending the shadow
              offset: Offset(
                3.0, // horizontal, move right 10
                3.0, // vertical, move down 10
              ),
            )
          ]
      ),
      child: new Column(
        children: <Widget>[
          new Container(
            height: clothesTitleHeight,
            margin: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
            child: Column(
              children: [
                new Text(LangStrings.strLang=='FR'?widget.pressingClothes.name:widget.pressingClothes.name_en, style: new TextStyle(fontSize:15.0,color: Colors.black38, fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold),),
                new SizedBox(height: 1.0,),
                new Text(widget.pressingClothes.category_slug, style: new TextStyle(fontSize:15.0,color: Colors.black38, fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold),),
              ],
            ),
          ),
          new SizedBox(height: 1.0,),
          new Container(
            width: clothesImageWidth,
            height: clothesImageHeight,
            padding: EdgeInsets.only(left: clothesImagePaddingLeft, right: clothesImagePaddingLeft, top: clothesImagePaddingTop, bottom:clothesImagePaddingTop),
            child: Image(image: NetworkImage(widget.pressingClothes.image_url)),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                border: Border.all( color: widget.isSelected?AppColors.nopalmaBlue: Colors.black12, width: 1.0)
            ),
          ),
          new SizedBox(height: 2.0,),
          new Container(
            width: quantityControlWidth,
            height: quantityControlHeight,
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new InkWell(
                  onTap: (){
                    setState(() {
                      _quantity --;
                      if(_quantity <0){
                        _quantity = 0;
                      }
                      widget.onQuantityChanged(widget.pressingClothes,_quantity);
                    });
                  },
                  child:  new Container(
                    height: quantityControlHeight,
                    width:  quantityControlHeight,
                    padding: EdgeInsets.only(bottom: 5.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: AppColors.getColorHexFromStr("#d8d8d8")),
                        borderRadius: BorderRadius.circular(quantityControlHeight)
                    ),
                    child:  Center(
                      child: new Text("-", style: new TextStyle(fontSize: quantityControlTextSize, color: Colors.black, ),),
                    ),
                  ),
                ),
                new SizedBox(width: 10.0,),
                new Container(
                  child:  Center(
                    child: new Text("$_quantity", style: new TextStyle(fontSize: quantityControlTextSize, color: Colors.black, ),),
                  ),
                ),
                new SizedBox(width: 10.0,),
                new InkWell(
                  onTap: (){
                    setState(() {
                      _quantity ++;
                      widget.onQuantityChanged(widget.pressingClothes,_quantity);
                    });

                  },
                  child:  new Container(
                    height: quantityControlHeight,
                    width:  quantityControlHeight,
                    padding: EdgeInsets.only(bottom: 5.0),
                    decoration: BoxDecoration(
                        color: AppColors.nopalmaBlue,
                        border: Border.all(color: AppColors.nopalmaBlue),
                        borderRadius: BorderRadius.circular(quantityControlHeight)
                    ),
                    child:  Center(
                      child: new Text("+", style: new TextStyle(fontSize: quantityControlTextSize, color: Colors.white, ),),
                    ),
                  ),
                ) ,
              ],
            ),
          )
        ],
      ),
    );
  }

}
