import 'package:easy_services/models/delivery_address.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';

class ChooseDeliveryAddressItemWidget extends StatefulWidget {

  //  bool isSelected;
  DeliveryAddress deliveryAddress;

  final String locationTypeImage;
  final String locationTypeTitle;
  final int position;
  final int radioGroupValue;

  Function handleRadioValueChange;
  final Function addButtonCallback;
  final Function closeButtonCallback;

  ChooseDeliveryAddressItemWidget({
    @required this.locationTypeImage,
    @required  this.locationTypeTitle,
    @required this.position,
    @required this.radioGroupValue,
    @required this.handleRadioValueChange,
    //this.isSelected = false,
    this.deliveryAddress = null,
    @required this.addButtonCallback = null,
   @required this.closeButtonCallback = null});


  @override
  _ChooseDeliveryAddressItemWidgetState createState() => _ChooseDeliveryAddressItemWidgetState();

}

class _ChooseDeliveryAddressItemWidgetState extends State<ChooseDeliveryAddressItemWidget> {

  int radioValue = -1;

  List<Widget> children = [];
  List<Widget> childrenRow = [];

  @override
  void initState() {
    super.initState();
    radioValue = widget.position;
  }


  @override
  Widget build(BuildContext context) {
    children.clear();
    childrenRow.clear();

    bool hasPlace = widget.deliveryAddress != null;

    if(hasPlace && widget.deliveryAddress.id !=null && widget.deliveryAddress.id > radioValue){
      radioValue = widget.deliveryAddress.id;
    }

    childrenRow.add(SizedBox(width: 16.0,));

    if(hasPlace){
      childrenRow.add(new Radio(
        value: radioValue,
        groupValue: widget.radioGroupValue,
        onChanged: widget.handleRadioValueChange,
      ));
    }else{
      childrenRow.add(new SizedBox(width: 50.0,));
    }

    childrenRow.add( SizedBox(width: 10.0,));
    childrenRow.add( Image.asset(
      widget.locationTypeImage,
      height: 40.0,
      fit: BoxFit.fitWidth,
      color: hasPlace?AppColors.colorAccent : Colors.grey,
    ));
    childrenRow.add(SizedBox(width: 16.0,));

    childrenRow.add( new Column(
      children: <Widget>[
        new Text(widget.locationTypeTitle,
            style: TextStyle(color: hasPlace?AppColors.button_positive_color:Colors.grey ,
                fontSize: 20.0,
                fontFamily: "ArialRoundedMT")
        ),

      ],
    ));



    children.add(new Row(
         mainAxisAlignment: MainAxisAlignment.spaceBetween,
         children: <Widget>[
           new Row(
             children: childrenRow,
           ),
           hasPlace? IconButton(icon: Icon(Icons.remove_circle_outline),
               iconSize:30.0,
               onPressed: widget.closeButtonCallback
           ) :IconButton(icon: Icon(Icons.add_circle_outline),
               iconSize:30.0,
               onPressed: widget.addButtonCallback
           )
         ],
       ));


     if(hasPlace){
       children.add(new Container(
         margin: EdgeInsets.only(left: 80.0, right: 30.0),
         child:new Text(widget.deliveryAddress.address_name,
             textAlign: TextAlign.start,
             maxLines: 3,
             style: TextStyle(color: Colors.black, fontSize: 16.0, fontFamily: "ArialRoundedMT")
         ),
       ));
     }




    return new Container(

      child: new Column(
        children: <Widget>[

          new Container(
            padding: EdgeInsets.only(left:10.0,right: 10.0,bottom: 10.0 ),
            child: new Column(
              children: children,
            ),
          ),
          new Container(
            margin: EdgeInsets.only(top: 10.0, bottom: 10.0 ),
            width: double.infinity,
            decoration: BoxDecoration(border: Border.all(color: Colors.black38, width: 0.3)),
          )


        ],
      ),
    );

  }

}
