import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/ui/order_services/services/create_artisan_screen.dart';
import 'package:easy_services/ui/order_services/services/create_linge_kilo_screen.dart';
import 'package:easy_services/ui/order_services/services/pressing_screen.dart';
import 'package:easy_services/ui/order_services/services/create_repassage_screen.dart';
import 'package:easy_services/ui/order_services/services/create_retouching_screen.dart';
import 'package:easy_services/ui/order_services/services/create_shoe_repairing_screen.dart';
import 'package:flutter/material.dart';

class ServiceView extends StatefulWidget {
  final Registrationdata profile;
  final Service service;

  ServiceView(this.profile,this.service);

  @override
  _ServiceViewState createState() => _ServiceViewState();
}

class _ServiceViewState extends State<ServiceView> {

  @override
  Widget build(BuildContext context) {

    return new Container(
      padding: EdgeInsets.all(14.0),
      margin: EdgeInsets.only(bottom: 10.0),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black45,
              blurRadius: 3.0, // has the effect of softening the shadow
              spreadRadius: 1.0, // has the effect of extending the shadow
              offset: Offset(
                3.0, // horizontal, move right 10
                3.0, // vertical, move down 10
              ),
            )
          ]
      ),
      child: new InkWell(
        onTap: (){

          if(widget.service != null){
            onTap();
          }

        },
        child: new Row(
          children: <Widget>[
            new Container(
              width: MediaQuery.of(context).size.width - 150.0,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(widget.service.nameValue(),
                    style: new TextStyle(color: AppColors.nopalmaBlue, fontSize: 20.0, fontWeight: FontWeight.bold, fontFamily: "ArialRoundedMT"),),
                  SizedBox(height: 6.0,),
                  new Container(
                      child:new Text(widget.service.description/*getDelay(widget.service)+" "+pricingDetail(widget.service)*/,
                        style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),)
                  ),

                ],
              ),
            ),
            new Container(
                width: 80.0,
                child: getServiceImage(widget.service)
            )
          ],
        ),
      ),
    );
  }

  Image getServiceImage(Service service) {

    if(service.slug == "pressing"){
      //return Image.asset("assets/sssss@2x.png", color: AppColors.nopalmaYellow,);
      return Image.network(service.logo, color: AppColors.nopalmaYellow,);
    }else if(service.slug == "linge_au_kilo"){
      return Image.network(service.logo, color: AppColors.nopalmaYellow,);
      return Image.asset("assets/cleancult_05_2x@2x.png", color: AppColors.nopalmaYellow,);
    }else if(service.slug == "repassage"){
      return Image.network(service.logo, color: AppColors.nopalmaYellow,);
      return Image.asset("assets/555@2x.png", color: AppColors.nopalmaYellow,);
    }else if(service.slug == "retouche"){
      return Image.network(service.logo, color: AppColors.nopalmaYellow,);
      return Image.asset("assets/RETROUC@2x.png", color: AppColors.nopalmaYellow,);
    }else if(service.slug == "coordonnerie"){
      return Image.network(service.logo, color: AppColors.nopalmaYellow,);
      return Image.asset("assets/CORD@2x.png", color: AppColors.nopalmaYellow,);
    }else if(service.slug == "ouvrier_et_artisan"){
      return Image.network(service.logo, color: AppColors.nopalmaYellow,);
      return Image.asset("assets/cleancult_05_2x@2x.png", color: AppColors.nopalmaYellow,);
    }else{
      return Image.network(service.logo, color: AppColors.nopalmaYellow,);
      //return Image.asset("assets/555@2x.png", color: AppColors.nopalmaYellow,);
    }

  }

  Color getServiceBgColor(Service service) {
    if(service.slug == "pressing"){
      return AppColors.getColorHexFromStr("#00d4db");
    }else if(service.slug == "linge_au_kilo"){
      return AppColors.getColorHexFromStr("#fe8b16");
    }else if(service.slug == "repassage"){
      return AppColors.getColorHexFromStr("#00adf0");
    }else if(service.slug == "retouche"){
      return AppColors.getColorHexFromStr("#8fc650");
    }else if(service.slug == "coordonnerie"){
      return AppColors.getColorHexFromStr("#ffb501");
    }else if(service.slug == "ouvrier_et_artisan"){
      return AppColors.getColorHexFromStr("#fe8b16");
    }else{
      return AppColors.getColorHexFromStr("#00d4db");
    }

  }

  String pricingDetail(Service service){
    if(service.price_is_variable){
      return (LangStrings.strLang=='FR')?"* Prix Variable":"* Variable price";
    }else{
      return LangStrings.strLang=='FR'?"* ${widget.service.price_description}":"* ${widget.service.price_description_en}";
    }
  }

  void onTap() {

     if(widget.service.slug == "pressing"){
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => new PressingScreen(widget.profile,widget.service)));
    }else if(widget.service.slug == "linge_au_kilo"){
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => new CreateLingeKiloScreen(widget.profile,widget.service)));
    }else if(widget.service.slug == "repassage"){
       Navigator.push(context,
           MaterialPageRoute(builder: (context) => new CreateRepassageScreen(widget.profile,widget.service)));
    }else if(widget.service.slug == "retouche"){
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => new CreateRetouchingScreen(widget.profile,widget.service)));
    }else if(widget.service.slug == "coordonnerie"){
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => new CreateShoeRepairingScreen(widget.profile,widget.service)));
    }else if(widget.service.slug == "ouvrier_et_artisan"){
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => new CreateArtisanScreen(widget.profile,widget.service)));
    }else{

    }

    //Navigator.push(context,
      //  MaterialPageRoute(builder: (context) => new ChooseTypeOfDeliveryScreen(widget.profile,widget.service)));
  }

  String getDelay(Service service) {
    return (LangStrings.strLang=='FR')?widget.service.delay:widget.service.delay_en;
  }
}
