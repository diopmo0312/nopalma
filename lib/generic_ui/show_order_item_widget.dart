import 'package:easy_services/models/artisan_category.dart';
import 'package:easy_services/models/linge_kilo_option.dart';
import 'package:easy_services/models/order_item.dart';
import 'package:easy_services/models/pressing_clothes.dart';
import 'package:easy_services/models/retouching.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/models/shoe_repairing.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';


import 'dart:convert';

class ShowOrderItemWidget extends StatefulWidget {

  final OrderItem item;
  final Registrationdata profile;

  ShowOrderItemWidget(this.profile, this.item);

  @override
  _ShowOrderItemWidgetState createState() => _ShowOrderItemWidgetState();
}

class _ShowOrderItemWidgetState extends State<ShowOrderItemWidget> {

  @override
  Widget build(BuildContext context) {

    return new Container(
      margin: EdgeInsets.all(10.0),
      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(color: AppColors.getColorHexFromStr("#d8d8d8"))
      ),
      child: new ListTile(
        leading: Image(image: getIcon(widget.item),width: 30.0,),
        title: new Row(
          children: <Widget>[
            new Text(getName(widget.item), style: new TextStyle(color: Colors.black38, fontSize: 12.0, fontWeight: FontWeight.bold)),
            SizedBox(width: 10.0,),
            new Container(
              width: 80.0,
              child: new Text(getPrice(widget.item), style: new TextStyle(color: AppColors.nopalmaBlue, fontSize: 12.0, fontWeight: FontWeight.bold),),
            ),
            SizedBox(width: 10.0,),
            new Text(getQuantity(widget.item), style: new TextStyle(color: Colors.black38, fontSize: 12.0, fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }

  ImageProvider getIcon(OrderItem item){
    if(item != null){

      if(item.service_slug == "pressing"){
        PressingClothes pressingClothers = PressingClothes.fromJson(jsonDecode(item.meta_data));
        if(pressingClothers != null){
          return NetworkImage(pressingClothers.image_url);
        }else{
          return NetworkImage(getService(item.service_slug).logo);
        }
      }else{

        return NetworkImage(getService(item.service_slug).logo);

      }


    }else{
      return NetworkImage(getService(item.service_slug).logo);
    }

  }

 /* Image getServiceImage(Service service) {
    if(service.slug == "pressing"){
      return Image.asset("assets/sssss@2x.png", color: Colors.white,);
    }else if(service.slug == "linge_au_kilo"){
      return Image.asset("assets/cleancult_05_2x@2x.png", color: Colors.white,);
    }else if(service.slug == "repassage"){
      return Image.asset("assets/555@2x.png", color: Colors.white,);
    }else if(service.slug == "retouche"){
      return Image.asset("assets/RETROUC@2x.png", color: Colors.white,);
    }else if(service.slug == "coordonnerie"){
      return Image.asset("assets/CORD@2x.png", color: Colors.white,);
    }else{
      return Image.asset("assets/555@2x.png", color: Colors.white,);
    }

  }*/

  Service getService(String serviceSlug){
    List<Service> services = [];

    if(widget.profile != null){
      services = widget.profile.services == null?[]:widget.profile.services;
    }
    if(services.isNotEmpty){
      return services.firstWhere((s)=>s.slug == serviceSlug);

    }else{
      return null;
    }

  }

  String getName(OrderItem item){
    if(item != null){

      if(item.service_slug == "pressing"){
        PressingClothes pressingClothers = PressingClothes.fromJson(jsonDecode(item.meta_data));
        if(pressingClothers != null){
          return pressingClothers.nameValue();
        }else{
          return "Unkown Name";
        }
      }else if(item.service_slug == "retouche"){
        Retouching retouching =  Retouching.fromJson(jsonDecode(item.meta_data));
        if(retouching != null){
          return retouching.nameValue();
        }else{
          return "Unkown Name";
        }
      }else if(item.service_slug == "coordonnerie"){
        ShoeRepairing shoeRepairing = ShoeRepairing.fromJson(jsonDecode(item.meta_data));
        if(shoeRepairing != null){
          return shoeRepairing.nameValue();
        }else{
          return "Unkown Name";
        }
      }else if(item.service_slug == "linge_au_kilo"){
        LingeKiloOption shoeRepairing = LingeKiloOption.fromJson(jsonDecode(item.meta_data));
        if(shoeRepairing != null){
          return shoeRepairing.nameValue();
        }else{
          return "Unkown Name";
        }
      }else if(item.service_slug == "repassage"){
        Service service = Service.fromJson(jsonDecode(item.meta_data));
        if(service != null){
          return service.nameValue();
        }else{
          return "Unkown Name";
        }
      }else if(item.service_slug == "ouvrier_et_artisan"){
        Service service = Service.fromJson(jsonDecode(item.meta_data));
        if(service != null){
          return service.nameValue();
        }else{
          return "Unkown Name";
        }
      }else{
        return "Unkown Name";
      }


    }else{
      return "Unkown Name";
    }
  }

  String getPrice(OrderItem item){
    if(item != null){

      if(item.service_slug == "pressing"){
        PressingClothes pressingClothers = PressingClothes.fromJson(jsonDecode(item.meta_data));
        if(pressingClothers != null){
          return pressingClothers.price+" "+pressingClothers.currency;
        }else{
          return "0 ${item.currency}";
        }
      }else if(item.service_slug == "retouche"){
        Retouching retouching =  Retouching.fromJson(jsonDecode(item.meta_data));
        if(retouching != null){
          return retouching.price+" "+retouching.currency;
        }else{
           return "0 ${item.currency}";
        }
      }else if(item.service_slug == "coordonnerie"){
        ShoeRepairing shoeRepairing = ShoeRepairing.fromJson(jsonDecode(item.meta_data));
        if(shoeRepairing != null){
          return shoeRepairing.price+" "+shoeRepairing.currency;
        }else{
           return "0 ${item.currency}";
        }
      }else if(item.service_slug == "ouvrier_et_artisan"){
        ArtisanCategory artisanCategory = ArtisanCategory.fromJson(jsonDecode(item.meta_data));
        if(artisanCategory != null){
          return artisanCategory.price+" "+artisanCategory.currency;
        }else{
           return "0 ${item.currency}";
        }
      }else{
       return "0 ${item.currency}";
      }


    }else{
      return "0 XOF";
    }
  }

  String getQuantity(OrderItem item){
    if(item != null){
      if(item.service_slug == "pressing"){
        return "x${widget.item.quantity}";
      }else if(item.service_slug == "linge_au_kilo"){
        return "x${widget.item.quantity}(/KG)";
      }else if(item.service_slug == "repassage"){
        return "x${widget.item.quantity}(/KG)";
      }else if(item.service_slug == "retouche"){
        return "x${widget.item.quantity}";
      }else if(item.service_slug == "coordonnerie"){
        return "x${widget.item.quantity}";
      }else{
        return "x${widget.item.quantity}";
      }
    }else{
      return "x${widget.item.quantity}";
    }
  }
}
