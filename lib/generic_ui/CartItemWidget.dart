import 'package:easy_services/models/cart_item.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/order_services/services/create_linge_kilo_screen.dart';
import 'package:easy_services/ui/order_services/services/pressing_screen.dart';
import 'package:easy_services/ui/order_services/services/create_repassage_screen.dart';
import 'package:easy_services/ui/order_services/services/create_retouching_screen.dart';
import 'package:easy_services/ui/order_services/services/create_shoe_repairing_screen.dart';
import 'package:easy_services/ui/order_services/services/create_artisan_screen.dart';
import 'package:flutter/material.dart';

class CartItemWidget extends StatefulWidget {

  Registrationdata profile;
  CartItem cartItem;

  ValueChanged<CartItem> delete;

  CartItemWidget(this.profile, this.cartItem, this.delete);

  @override
  _CartItemWidgetState createState() => _CartItemWidgetState();
}

class _CartItemWidgetState extends State<CartItemWidget> {

  Service service;

  @override
  void initState() {
    super.initState();
    List<Service> services = widget.profile.services;
    service = services.firstWhere((s)=>s.slug == widget.cartItem.service_slug);

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      margin: EdgeInsets.fromLTRB(8.0,8.0,8.0,0.0),
      decoration: BoxDecoration(
        color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all( color: Colors.black12, width: 1.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black45,
              blurRadius: 3.0, // has the effect of softening the shadow
              spreadRadius: 1.0, // has the effect of extending the shadow
              offset: Offset(
                3.0, // horizontal, move right 10
                3.0, // vertical, move down 10
              ),
            )
          ]
      ),
      child: ListTile(
        title: InkWell(
            onTap: (){

              if(widget.cartItem.service_slug == "pressing"){
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new PressingScreen(widget.profile,service)));
              }else if(widget.cartItem.service_slug == "linge_au_kilo"){
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new CreateLingeKiloScreen(widget.profile,service)));
              }else if(widget.cartItem.service_slug == "repassage"){
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new CreateRepassageScreen(widget.profile,service)));
              }else if(widget.cartItem.service_slug == "retouche"){
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new CreateRetouchingScreen(widget.profile,service)));
              }else if(widget.cartItem.service_slug == "coordonnerie"){
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new CreateShoeRepairingScreen(widget.profile,service)));
              }else if(widget.cartItem.service_slug == "ouvrier_et_artisan"){
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new CreateArtisanScreen(widget.profile,service)));
              }else{

              }

            },
          child: new Container(
            width: MediaQuery.of(context).size.width - 150.0,
            child: new Text(widget.cartItem.service_name, style: new TextStyle(fontSize:15.0,color: AppColors.nopalmaBlue, fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold),),
          ),
        ),
        subtitle: new Text("${widget.cartItem.items.length}"),
        trailing: new Container(
            width: 150.0,
            child:new Row(
              children: <Widget>[
                new Text("${widget.cartItem.amount} ${widget.cartItem.currency}"),
                new IconButton(icon: Icon(Icons.delete, size:18.0), onPressed: (){
                  widget.delete(widget.cartItem);
                }),
              ],
            )
        ),onTap: (){


      },
      )
    );
  }
}
