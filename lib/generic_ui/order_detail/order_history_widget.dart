import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/order_history.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/util/DateUtil.dart';
import 'package:flutter/material.dart';

class OrderHistoryWidget extends StatefulWidget {

  OrderHistory orderHistory;

  OrderHistoryWidget(this.orderHistory);

  @override
  _OrderHistoryWidgetState createState() => _OrderHistoryWidgetState();
}

class _OrderHistoryWidgetState extends State<OrderHistoryWidget> {

  Locale defaultLocale;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    defaultLocale = Locale(LangStrings.strLang.toLowerCase());
    return new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Row(
          children: <Widget>[
            new Container(
              margin: EdgeInsets.only(right: 10.0,bottom: 25.0),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(getDate(widget.orderHistory, defaultLocale), style: new TextStyle(color: Colors.black54, fontWeight:FontWeight.bold, fontSize: 16.0,fontFamily: "ArialRoundedMT")),
                  new Text(getTime(widget.orderHistory, defaultLocale), style: new TextStyle(color: Colors.black54, fontWeight:FontWeight.bold, fontSize: 16.0,fontFamily: "ArialRoundedMT"))
                ],
              ),
            ),
            new Container(
              child: new Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  new CustomPaint(
                    foregroundPainter: new LinePainter(),
                  ),
                  new Container(
                    width: 10.0,
                    height: 10.0,
                    margin: EdgeInsets.only(bottom: 15.0),
                    decoration: BoxDecoration(
                      color: AppColors.nopalmaBlue,
                      shape: BoxShape.circle,
                    ),
                  )
                ],
              ),
            ),
            new Container(
              width: (MediaQuery.of(context).size.width - 150.0),
              margin: EdgeInsets.only(left: 10.0, bottom: 25.0),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(getHistoryTitle(widget.orderHistory),
                      style: new TextStyle(
                          color: Colors.black,
                          fontWeight:FontWeight.bold,
                          fontSize: 18.0,
                          fontFamily: "ArialRoundedMT")
                  ),
                ],
              ),
            )
          ],
        ),
        /*new Container(
          margin: EdgeInsets.only(left: 87.0),
          child: new CustomPaint(
            foregroundPainter: new LinePainter(),
          ),
        )*/
      ],
    );
  }
  String getTime(OrderHistory orderHistory, Locale locale){

    if(orderHistory == null){
      return "Unknown Time";
    }else{

      return DateUtil.formatTime(orderHistory.created_at.toIso8601String(), locale);

    }

  }

  String getDate(OrderHistory orderHistory, Locale locale){

    if(orderHistory == null){
      return "Unknown Date";
    }else{

      return DateUtil.formatDayMonth(orderHistory.created_at.toIso8601String(), locale);

    }

  }

  String getHistoryTitle(OrderHistory orderHistory){
    if(orderHistory.status == null){
      return " - ";
    }else{

      if(orderHistory.status == "is-waiting"){
        return LangStrings.gs("order_is_waiting_message");
      }else if(orderHistory.status == "managed"){
        return LangStrings.gs("order_is_managed_message");
      }else if(orderHistory.status == "started"){
        return LangStrings.gs("order_is_started_message");
      }else if(orderHistory.status == "successful"){
        return LangStrings.gs("order_is_successful_message");
      }else if(orderHistory.status == "failed"){
        return LangStrings.gs("order_is_failed_message");
      }else if(orderHistory.status == "cancel"){
        return LangStrings.gs("order_is_cancel_message");
      }else if(orderHistory.status == "edited"){
        return LangStrings.gs("order_edited_message");
      }else if(orderHistory.status == "customer-pickuped"){
        return LangStrings.gs("order_customer_pickuped_message");
      }else if(orderHistory.status == "waiting-customer-pickup"){
        return LangStrings.gs("order_waiting_customer_pickup_message");
      }else if(orderHistory.status == "waiting-customer-delivery"){
        return LangStrings.gs("order_waiting_customer_delivery_message");
      }else if(orderHistory.status == "customer-delivered"){
        return LangStrings.gs("order_customer_delivered_message");
      }else{
        return LangStrings.gs("order_is_waiting_message");
      }

    }


  }

}

class LinePainter extends CustomPainter {


  @override
  void paint(Canvas canvas, Size size) {
    Paint linePaint = new Paint()
      ..color = AppColors.colorPrimary
      ..strokeWidth = 0.5
      ..style = PaintingStyle.stroke;

    canvas.drawLine(Offset(0.0, -50.0), Offset(0.0, 40.0), linePaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {

    return true;
  }
}

class BallPainter extends CustomPainter {


  @override
  void paint(Canvas canvas, Size size) {
    Paint ballPaint = new Paint()
      ..color = AppColors.colorPrimary
      ..strokeWidth = 2.0
      ..style = PaintingStyle.stroke;

    canvas.drawCircle( Offset(0.0, 0.0),5.0,ballPaint);

  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {

    return true;
  }
}

