import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:easy_services/models/job_history.dart';
import 'package:easy_services/util/DateUtil.dart';


class JobHistoryItemWidget extends StatefulWidget {

  JobHistory jobHistory;

  JobHistoryItemWidget(this.jobHistory);

  @override
  _JobHistoryItemWidgetState createState() => _JobHistoryItemWidgetState();
}

class _JobHistoryItemWidgetState extends State<JobHistoryItemWidget> {

  Locale defaultLocale;
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    defaultLocale = Locale(LangStrings.strLang.toLowerCase());

    return new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Row(
          children: <Widget>[
            new Container(
              margin: EdgeInsets.only(right: 10.0,bottom: 25.0),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(getDate(widget.jobHistory, defaultLocale), style: new TextStyle(color: Colors.black54, fontWeight:FontWeight.bold, fontSize: 16.0,fontFamily: "ArialRoundedMT")),
                  new Text(getTime(widget.jobHistory, defaultLocale), style: new TextStyle(color: Colors.black54, fontWeight:FontWeight.bold, fontSize: 16.0,fontFamily: "ArialRoundedMT"))
                ],
              ),
            ),
            new Container(
              child: new Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  new CustomPaint(
                    foregroundPainter: new LinePainter(),
                  ),
                  new Container(
                    width: 10.0,
                    height: 10.0,
                    margin: EdgeInsets.only(bottom: 15.0),
                    decoration: BoxDecoration(
                      color: AppColors.colorPrimary,
                      shape: BoxShape.circle,
                    ),
                  )
                ],
              ),
            ),
            new Container(
              width: (MediaQuery.of(context).size.width - 150.0),
              margin: EdgeInsets.only(left: 10.0, bottom: 25.0),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(getHistoryTitle(widget.jobHistory),
                      style: new TextStyle(
                          color: Colors.black,
                          fontWeight:FontWeight.bold,
                          fontSize: 18.0,
                          fontFamily: "ArialRoundedMT")
                  ),
                  Offstage(
                    offstage: false,
                    child: new Text(getHistoryAddress(widget.jobHistory), style: new TextStyle(color: Colors.grey, fontSize: 14.0,fontFamily: "ArialRoundedMT")),
                  )
                ],
              ),
            )
          ],
        ),
        /*new Container(
          margin: EdgeInsets.only(left: 87.0),
          child: new CustomPaint(
            foregroundPainter: new LinePainter(),
          ),
        )*/
      ],
    );
  }

  String getTime(JobHistory jobHistory, Locale locale){

    if(jobHistory == null){
      return "Unknown Time";
    }else{

      return DateUtil.formatTime(jobHistory.created_at, locale);

    }

  }

  String getDate(JobHistory jobHistory, Locale locale){

    if(jobHistory == null){
      return "Unknown Date";
    }else{

      return DateUtil.formatDayMonth(jobHistory.created_at, locale);

    }

  }

  String getHistoryTitle(JobHistory jobHistory){
    if(jobHistory.status == null){
      return " - ";
    }else{
      if(jobHistory.status == "arrived"){
        return "Agent ${jobHistory.agent_name} ${LangStrings.gs('job_arrived_message')}";
      }else if(jobHistory.status == "started"){
        return "Agent ${jobHistory.agent_name} ${LangStrings.gs('job_started_message')}";
      } else if(jobHistory.status == "successful"){
        return "${LangStrings.gs('job_completed_message')} ${jobHistory.agent_name}";
      }else if(jobHistory.status == "failed"){
        return "${LangStrings.gs('job_failed_message')} ${jobHistory.agent_name}";
      }else if(jobHistory.status == "cancel"){
        return "${LangStrings.gs('job_canceled_message')} ${jobHistory.agent_name}";
      }else{
        return " - ";
      }

    }


  }

  String getHistoryAddress(JobHistory jobHistory) {
    if(jobHistory.job_location_address == null){
      return " - ";
    }else{
      return jobHistory.job_location_address;
    }
  }
}

class LinePainter extends CustomPainter {


  @override
  void paint(Canvas canvas, Size size) {
    Paint linePaint = new Paint()
      ..color = AppColors.colorPrimary
      ..strokeWidth = 0.5
      ..style = PaintingStyle.stroke;

    canvas.drawLine(Offset(0.0, -50.0), Offset(0.0, 40.0), linePaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {

    return true;
  }
}

class BallPainter extends CustomPainter {


  @override
  void paint(Canvas canvas, Size size) {
    Paint ballPaint = new Paint()
      ..color = AppColors.colorPrimary
      ..strokeWidth = 2.0
      ..style = PaintingStyle.stroke;

    canvas.drawCircle( Offset(0.0, 0.0),5.0,ballPaint);

  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {

    return true;
  }
}
