import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/artisan_category.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/cart/choose_artisan_time_slot_screen.dart';
import 'package:easy_services/rest/request/order_item_request.dart';
import 'package:easy_services/models/cart_item.dart';
import 'package:easy_services/models/cart.dart';
import 'package:easy_services/util/CartManager.dart';

class ArtisanCategoryWidget extends StatefulWidget {
  Registrationdata profile;

  ArtisanCategory artisan_category;

  Function artisanCategorySelected;

  bool isSelected;

  List<OrderItemRequest> item;

  CartItem cartItem;

  ArtisanCategoryWidget(
      {@required this.artisan_category,
      @required this.isSelected,
      @required this.profile,
      @required this.item,
      @required this.cartItem});

  @override
  _ArtisanCategoryWidgetState createState() => _ArtisanCategoryWidgetState();
}

class _ArtisanCategoryWidgetState extends State<ArtisanCategoryWidget> {
  int _quantity;
  Registrationdata _profile;
  ArtisanCategory _artisanCategory;
  List<OrderItemRequest> _items = [];

  @override
  void initState() {
    super.initState();

    _profile = widget.profile;
    _artisanCategory = widget.artisan_category;
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;

    var quantityControlWidth = ((screenWidth / 3) - 15) * 2;
    var quantityControlHeight = 20.0 * 1.2;
    var quantityControlTextSize = 15.0 * 1.2;

    return Container(
      alignment: Alignment.topLeft,
      margin: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(
              color: widget.isSelected ? AppColors.nopalmaBlue : Colors.black12,
              width: 1.0),
          boxShadow: [
            BoxShadow(
              color: widget.isSelected ? AppColors.nopalmaBlue : Colors.black45,
              blurRadius: 3.0, // has the effect of softening the shadow
              spreadRadius: 1.0, // has the effect of extending the shadow
              offset: Offset(
                3.0, // horizontal, move right 10
                3.0, // vertical, move down 10
              ),
            )
          ]),
      child: ListTile(
        title: new Text(
          LangStrings.strLang == 'FR'
              ? _artisanCategory.name
              : _artisanCategory.name_en,
          style: new TextStyle(
              fontSize: 15.0,
              color: Colors.black38,
              fontFamily: 'ArialRoundedMT',
              fontWeight: FontWeight.bold),
        ),
        subtitle: new Text((LangStrings.strLang == 'FR'
                ? " Frais de déplacement + diagnostic: "
                : "Travel costs + diagnosis: ") +
            _artisanCategory.price +
            " " +
            _artisanCategory.currency), //
        onTap: () {
          saveToLocalCart();
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => new ChooseArtisanTimeSlot(
                      widget.profile, _artisanCategory)));
        },
      ),
    );
  }

  void saveToLocalCart() async {
    Cart _cart;
    CartItem _cartItem;
    double subTotal = 0;
    print("User ====> $_profile");
    print("Category ====> ${_artisanCategory}");
    List<OrderItemRequest> itemRequest = [
      new OrderItemRequest(
          _artisanCategory.category_slug,
          _artisanCategory.slug,
          _artisanCategory.toJsonEncoded(),
          1,
          "number",
          _artisanCategory.price,
          _artisanCategory.currency)
    ];
    _cartItem = new CartItem(
        "ouvrier_et_artisan",
        "Ouvrier et Artisan",
        itemRequest,
        _artisanCategory.price,
        _artisanCategory.currency);

    //Save in the Cart
    _cart = await CartManager.addToCart(_cartItem);
  }
}
