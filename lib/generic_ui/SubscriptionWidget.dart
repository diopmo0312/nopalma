import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/subscription.dart';
import 'package:easy_services/models/subscription_linge_kilo.dart';
import 'package:easy_services/models/subscription_pressing.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:easy_services/ui/subscriptions/subscribe_screen.dart';

class SubscriptionWidget extends StatefulWidget {

  Registrationdata profile;
  Subscription subscription;
  
  SubscriptionWidget(this.profile,this.subscription);
  
  @override
  _SubscriptionWidgetState createState() => _SubscriptionWidgetState();
}

class _SubscriptionWidgetState extends State<SubscriptionWidget> {

  bool isSelected = true;

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    return new Container(
      margin: EdgeInsets.fromLTRB(8.0,8.0,8.0,0.0),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        border: Border.all( color: isSelected?getServiceBgColor(widget.subscription.service_slug): Colors.black12, width: 1.0),
        boxShadow: [
          BoxShadow(
            color: Colors.black45,
            blurRadius: 3.0, // has the effect of softening the shadow
            spreadRadius: 1.0, // has the effect of extending the shadow
            offset: Offset(
              3.0, // horizontal, move right 10
              3.0, // vertical, move down 10
            ),
          )
        ]
      ),
      child: new Column(
        children: <Widget>[
          new Container(
            width: 80.0,
            child: getServiceImage(widget.subscription.service_slug)
            ),
          SizedBox(height: 10.0,),
          new Container(
            alignment: Alignment.center,
            child: new Text(widget.subscription.title, 
            style:new TextStyle(
              color: Colors.black,
              fontFamily: "ArialRoundedMT", 
              fontSize:25.0, 
              fontWeight: FontWeight.bold)),
          ),
          SizedBox(height: 10.0,),
          new Container(
            alignment: Alignment.center,
            child: new Text(widget.subscription.service_name,
                style: new TextStyle(
                  color: Colors.black,
                  fontFamily: "ArialRoundedMT",
                  fontSize: 16.0,
                )
                ),
          ),
          SizedBox(height: 10.0,),  
          new Container(
            alignment: Alignment.center,
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text(widget.subscription.cost_currency,
                style: new TextStyle(
                  color: Colors.black,
                  fontFamily: "ArialRoundedMT",
                  fontSize: 16.0,
                )
                ),
                new Text(widget.subscription.cost,
                style: new TextStyle(
                  color: Colors.black,
                  fontFamily: "ArialRoundedMT",
                  fontSize: 25.0,
                  fontWeight: FontWeight.bold
                )
                ),
                new Text("/"+getPer(widget.subscription), style: new TextStyle(
                  color: Colors.grey,
                  fontFamily: "ArialRoundedMT",
                  fontSize: 14.0
                ),)
                              ],
                            ),
                          ),
                SizedBox(height: 10.0,),
                new Container(
                  height: 30.0 * getDetails(widget.subscription).length,
                  alignment: Alignment.center,
                  child: ListView(
                    children: getDetails(widget.subscription),
                  )
                ),
                SizedBox(height: 10.0,),
                new Container(
                  height: 70.0,
                  padding: EdgeInsets.all(10.0),
                  child: new RaisedButton(
                      textColor: getServiceBgColor(widget.subscription.service_slug),
                      color: Colors.white,
                      padding: EdgeInsets.all(10.0),
                      shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0), side: BorderSide(color: getServiceBgColor(widget.subscription.service_slug))),
                      child: new Text(LangStrings.gs("button_subscribe_title"), style: new TextStyle(fontSize:18.0,color: getServiceBgColor(widget.subscription.service_slug), fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold), ),
                      onPressed: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => new SubscribeScreen(widget.profile, widget.subscription)),
                            );
                      }),
                )

                        ],
                      ),
                    );
                  }


                  List<Widget> getDetails(Subscription sub){
                    if(sub.service_slug == "pressing"){
                        if(sub.meta_data != null){
                          List<Widget> views = [];

                           Map map = jsonDecode(sub.meta_data);
                           SubscriptionPressing result = SubscriptionPressing.fromJson(map);

                              views.add(new Container(
                                 margin: EdgeInsets.only(bottom: 10.0),
                                 alignment: Alignment.center,
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0, color: AppColors.nopalmaBlue),
                                    SizedBox(width: 10.0,),
                                    new Text("Maximum ${result.quantity_clothing} ${LangStrings.gs("Clothes_title")}",
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));

                               views.add(new Container(
                                  margin: EdgeInsets.only(bottom: 10.0),
                                  alignment: Alignment.center,
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0, color: AppColors.nopalmaBlue,),
                                    SizedBox(width: 10.0,),
                                    new Text("Maximum ${result.quantity_costume_tailors} ${LangStrings.gs("Suits_Tailors_title")}",
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));
                             

                              views.add(new Container(
                                alignment: Alignment.center,
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0, color: AppColors.nopalmaBlue),
                                    SizedBox(width: 10.0,),
                                    new Text(getTitlePassage(result.number_of_passage),
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));

                              views.add(new Container(
                                alignment: Alignment.center,
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0, color: AppColors.nopalmaBlue,),
                                    SizedBox(width: 10.0,),
                                    new Text(LangStrings.gs("no_delivery_fees_title"),
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));

                          return views;
                        }else{
                          return [];
                        }
                    }else if(sub.service_slug == "linge_au_kilo"){
                        if(sub.meta_data != null){
                          List<Widget> views = [];

                            Map map = jsonDecode(sub.meta_data);
                            SubscriptionLingeKilo result = SubscriptionLingeKilo.fromJson(map);

                            views.add(new Container(
                              alignment: Alignment.center,
                                margin: EdgeInsets.only(bottom: 10.0),
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0, color: AppColors.nopalmaBlue,),
                                    SizedBox(width: 10.0,),
                                    new Text("Maximum ${result.quantity} ${result.quantity_unit} ${LangStrings.gs("of_clothes_title")}",
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));

                             views.add(new Container(
                               alignment: Alignment.center,
                                margin: EdgeInsets.only(bottom: 10.0),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0, color: AppColors.nopalmaBlue,),
                                    SizedBox(width: 10.0,),
                                    new Text(getTitlePassage(result.number_of_passage),
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));

                              views.add(new Container(
                                alignment: Alignment.center,
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0, color: AppColors.nopalmaBlue,),
                                    SizedBox(width: 10.0,),
                                    new Text(LangStrings.gs("no_delivery_fees_title"),
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));


                          return views;
                        }else{
                          return [];
                        }
                    }else{
                      return [];
                    }

                   
                  }
                
                    Image getServiceImage(String service_slug) {
                    if(service_slug == "pressing"){
                      return Image.asset("assets/sssss@2x.png", color: getServiceBgColor(service_slug),);
                    }else if(service_slug == "linge_au_kilo"){
                      return Image.asset("assets/cleancult_05_2x@2x.png", color: getServiceBgColor(service_slug),);
                    }else if(service_slug == "repassage"){
                      return Image.asset("assets/555@2x.png", color: getServiceBgColor(service_slug),);
                    }else if(service_slug == "retouche"){
                      return Image.asset("assets/RETROUC@2x.png", color: getServiceBgColor(service_slug),);
                    }else if(service_slug == "coordonnerie"){
                      return Image.asset("assets/CORD@2x.png", color: getServiceBgColor(service_slug),);
                    }else{
                      return Image.asset("assets/555@2x.png", color: getServiceBgColor(service_slug),);
                    }
                
                  }
                
                    Color getServiceBgColor(String service_slug) {
                    if(service_slug == "pressing"){
                      return AppColors.getColorHexFromStr("#00d4db");
                    }else if(service_slug == "linge_au_kilo"){
                      return AppColors.getColorHexFromStr("#fe8b16");
                    }else if(service_slug == "repassage"){
                      return AppColors.getColorHexFromStr("#00adf0");
                    }else if(service_slug == "retouche"){
                      return AppColors.getColorHexFromStr("#8fc650");
                    }else if(service_slug == "coordonnerie"){
                      return AppColors.getColorHexFromStr("#ffb501");
                    }else{
                      return AppColors.getColorHexFromStr("#00d4db");
                    }
                
                  }
                
                  String getPer(Subscription subscription) {
                    if(subscription.delay_unit == "month"){
                      return LangStrings.gs("month");
                    }else{
                      return LangStrings.gs("month");
                    }
                  }

                  String getTitlePassage(int passage){
                    if(passage == 2){
                        return LangStrings.gs("Passage_every2weeks_title");
                    }else if(passage == 4){
                      return LangStrings.gs("Passage_every_week_title");
                    }else{
                      return LangStrings.gs("Nothing_title");
                    }
                  }
}