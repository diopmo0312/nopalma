import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';

class DeliveryAddressItemWidget extends StatefulWidget {

  final String locationTypeImage;
  final String locationTypeTitle;
  final Function addButtonCallback;
  final Function closeButtonCallback;
  final String addressName;

  DeliveryAddressItemWidget({this.locationTypeImage, this.locationTypeTitle, this.addressName = "", this.addButtonCallback = null, this.closeButtonCallback = null});


  @override
  _DeliveryAddressItemWidgetState createState() => _DeliveryAddressItemWidgetState();

}

class _DeliveryAddressItemWidgetState extends State<DeliveryAddressItemWidget> {

  @override
  Widget build(BuildContext context) {
    return new Container(

      child: new Column(
        children: <Widget>[

          new Container(
            padding: EdgeInsets.only(left:10.0,right:10.0,bottom:10.0 ),
            child: Column(
              children: <Widget>[
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        SizedBox(width: 16.0,),
                        Image.asset(
                          widget.locationTypeImage,
                          height: 40.0,
                          fit: BoxFit.fitWidth,
                          color: AppColors.colorAccent,
                        ),
                        SizedBox(width: 16.0,),

                        new Column(
                          children: <Widget>[
                            new Text(widget.locationTypeTitle, style: TextStyle(color: Colors.black54, fontSize: 20.0, fontFamily: "ArialRoundedMT"),),

                          ],
                        ),


                      ],
                    ),


                    widget.addressName.isNotEmpty? IconButton(icon: Icon(Icons.remove_circle_outline),
                        iconSize:30.0,
                        onPressed: widget.closeButtonCallback
                    ) :IconButton(icon: Icon(Icons.add_circle_outline),
                        iconSize:30.0,
                        onPressed: widget.addButtonCallback
                    )
                  ],
                )
                ,
                new Container(

                  margin: EdgeInsets.only(left: 80.0, right: 30.0),

                  child: new Offstage(
                    offstage: !widget.addressName.isNotEmpty,
                    child:  new Text(widget.addressName,
                      textAlign: TextAlign.start,
                      maxLines: 3,
                      style: TextStyle(color: Colors.black, fontSize: 16.0, fontFamily: "ArialRoundedMT")
                      ,

                    ),
                  ),
                )
              ],
            ),
          ),
          new Container(
            margin: EdgeInsets.only(top: 10.0, bottom: 10.0 ),
            width: double.infinity,
            decoration: BoxDecoration(border: Border.all(color: Colors.black38, width: 0.3)),
          )


        ],
      ),
    );
  }

}
