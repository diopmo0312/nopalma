import 'package:easy_services/models/linge_kilo_option.dart';
import 'package:easy_services/models/service.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';

class ChooseLingeKiloOptionItem extends StatefulWidget {
  Service service;
  LingeKiloOption radioValue;
  final LingeKiloOption radioGroupValue;

  Function handleRadioValueChange;

  ChooseLingeKiloOptionItem(this.service, this.radioValue, this.radioGroupValue,
      this.handleRadioValueChange);

  @override
  _ChooseLingeKiloOptionItemState createState() =>
      _ChooseLingeKiloOptionItemState();
}

class _ChooseLingeKiloOptionItemState extends State<ChooseLingeKiloOptionItem> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          new InkWell(
            //Pressing
            onTap: () {
              print(widget.radioValue);
              print("--------------------");
              print(widget.radioGroupValue);
              print("++++++++++++++++++++");
              print(widget.handleRadioValueChange = (){});
            },
            child: new Container(
              height: 90.0,
              width: double.infinity,
              padding: EdgeInsets.all(14.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 3.0, // has the effect of softening the shadow
                      spreadRadius:
                          1.0, // has the effect of extending the shadow
                      offset: Offset(
                        3.0, // horizontal, move right 10
                        3.0, // vertical, move down 10
                      ),
                    )
                  ]),
              child: new Center(
                child: new Row(
                  children: <Widget>[
                    new Radio(
                      activeColor: AppColors.nopalmaBlue,
                      value: widget.radioValue,
                      groupValue: widget.radioGroupValue,
                      // onChanged: widget.handleRadioValueChange,
                      onChanged: widget.handleRadioValueChange,
                    ),
                    new SizedBox(
                      width: 10.0,
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width - 200,
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            widget.radioValue.nameValue(),
                            style: new TextStyle(
                                color: Colors.black,
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: "ArialRoundedMT"),
                          ),
                          SizedBox(
                            height: 6.0,
                          ),
                          new Expanded(
                              child: new Text(
                            widget.service.delay +
                                " * " +
                                widget.radioValue.priceValue() +
                                " " +
                                widget.radioValue.currencyValue(),
                            style: new TextStyle(
                                color: Colors.black45,
                                fontSize: 14.0,
                                fontFamily: "ArialRoundedMT"),
                          )),
                        ],
                      ),
                    ),
                    new Container(
                        width: 70.0,
                        child: Image.asset(
                          "assets/cleancult_05_2x@2x.png",
                          color: AppColors.nopalmaYellow,
                        ))
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            height: 10.0,
          )
        ],
      ),
    );
  }
}
