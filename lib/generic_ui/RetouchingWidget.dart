import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/retouching.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';

class RetouchingWidget extends StatefulWidget {

  Retouching retouching;

  Function onQuantityChanged;

  bool isSelected;

  int initialQuantity;

  RetouchingWidget({
  @required this.retouching,
  @required this.initialQuantity,
  @required this.onQuantityChanged,
  @required this.isSelected});

  @override
  _RetouchingWidgetState createState() => _RetouchingWidgetState();
}

class _RetouchingWidgetState extends State<RetouchingWidget> {

  int _quantity;

  @override
  void initState() {
    super.initState();
    if(widget.initialQuantity != null){
      _quantity = widget.initialQuantity;
    }else{
      _quantity = 0;
    }


  }

  @override
  Widget build(BuildContext context) {

    var screenWidth = MediaQuery.of(context).size.width;

    var quantityControlWidth = ((screenWidth /3) - 15) * 2;
    var quantityControlHeight = 20.0 * 1.2 ;
    var quantityControlTextSize = 15.0 * 1.2 ;
    

    return Container(
      alignment: Alignment.topLeft,
      margin: EdgeInsets.fromLTRB(8.0,8.0,8.0,0.0),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all( color: widget.isSelected?AppColors.nopalmaBlue: Colors.black12, width: 1.0),
          boxShadow: [
            BoxShadow(
              color: widget.isSelected?AppColors.nopalmaBlue: Colors.black45,
              blurRadius: 3.0, // has the effect of softening the shadow
              spreadRadius: 1.0, // has the effect of extending the shadow
              offset: Offset(
                3.0, // horizontal, move right 10
                3.0, // vertical, move down 10
              ),
            )
          ]
      ),
      child: ListTile(
        title: new Text(LangStrings.strLang=='FR'?widget.retouching.name:widget.retouching.name_en, style: new TextStyle(fontSize:15.0,color: Colors.black38, fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold),),
        subtitle: new Text(widget.retouching.price+" "+widget.retouching.currency),
        trailing: new Container(
          width: 100.0,
          height: quantityControlHeight,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new InkWell(
                onTap: (){
                  setState(() {
                    _quantity --;
                    if(_quantity <0){
                      _quantity = 0;
                    }
                    widget.onQuantityChanged(widget.retouching,_quantity);
                  });
                },
                child:  new Container(
                  height: quantityControlHeight,
                  width:  quantityControlHeight,
                  padding: EdgeInsets.only(bottom: 5.0),
                  decoration: BoxDecoration(
                      border: Border.all(color: AppColors.getColorHexFromStr("#d8d8d8")),
                      borderRadius: BorderRadius.circular(quantityControlHeight)
                  ),
                  child:  Center(
                    child: new Text("-", style: new TextStyle(fontSize: quantityControlTextSize, color: Colors.black, ),),
                  ),
                ),
              ),
              new SizedBox(width: 10.0,),
              new Container(
                child:  Center(
                  child: new Text("$_quantity", style: new TextStyle(fontSize: quantityControlTextSize, color: Colors.black, ),),
                ),
              ),
              new SizedBox(width: 10.0,),
              new InkWell(
                onTap: (){
                  setState(() {
                    _quantity ++;
                    widget.onQuantityChanged(widget.retouching,_quantity);
                  });

                },
                child:  new Container(
                  height: quantityControlHeight,
                  width:  quantityControlHeight,
                  padding: EdgeInsets.only(bottom: 5.0),
                  decoration: BoxDecoration(
                      color: AppColors.nopalmaBlue,
                      border: Border.all(color: AppColors.nopalmaBlue),
                      borderRadius: BorderRadius.circular(quantityControlHeight)
                  ),
                  child:  Center(
                    child: new Text("+", style: new TextStyle(fontSize: quantityControlTextSize, color: Colors.white, ),),
                  ),
                ),
              ) ,
            ],
          ),
        ),
      ),
    );
  }

}
