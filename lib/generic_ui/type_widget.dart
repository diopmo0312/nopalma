import 'package:flutter/material.dart';

import 'package:easy_services/ui/app_colors.dart';

class TypeWidget extends StatefulWidget {


  Tween value;

  List<Tween> items;

  ValueChanged<Tween> onChanged;


  TypeWidget({this.value, @required this.items, @required this.onChanged});

  @override
  _TypeWidgetState createState() => _TypeWidgetState();

}

class _TypeWidgetState extends State<TypeWidget>{

   List<TypeWidgetItem> listWidgets;

   Tween selected;

   @override
  void initState() {
    super.initState();
    print("TypeWidgetItem ===> ${widget.value}");
    selected = widget.value;
    listWidgets = widget.items.map((Tween type){
      return new TypeWidgetItem( type, selected.begin == type.begin, (Tween tt){
        _handleTap(tt);
      });
    }).toList();


  }


  @override
  Widget build(BuildContext context) {


    return new Container(
        width: double.infinity,
        height: 50.0,
        child:ListView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.only(right: 10.0),
          children: listWidgets,
        )

    );
  }

  void _handleTap(Tween type){

    setState(() {
      selected = type;

      listWidgets = widget.items.map((Tween type){
        return new TypeWidgetItem( type, selected.begin == type.begin, (Tween tt){
          _handleTap(tt);
        });
      }).toList();


    });

    widget.onChanged (type);


  }


}
class TypeWidgetItem extends StatelessWidget {

  Tween value;

  bool isSelected = false;

  Function onTap;


  TypeWidgetItem(this.value, this.isSelected, this.onTap);

  @override
  Widget build(BuildContext context) {

    return new Container(
      child: new Row(
        children: <Widget>[
          new RaisedButton(
            padding: new EdgeInsets.all(8.0),
            shape:new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
            onPressed: (){
              this.onTap(this.value);
            },
            color: isSelected?AppColors.nopalmaBlue:AppColors.getColorHexFromStr("#d8d8d8"),
            textColor:  isSelected?Colors.white:Colors.black45,
            child: new Text(value.end as String, style: new TextStyle(fontSize:18.0,color: isSelected?Colors.white:Colors.black, fontFamily: 'ArialRoundedMT'),),
          ),
          SizedBox(width: 10.0,)


        ],
      ),
    ); /*InkWell(
      onTap: (){

      },
      child: new Container(
        margin: EdgeInsets.only(left:5.0),
        padding: EdgeInsets.all(5.0),
        width: 55.0,
        height: 40.0,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(color: isSelected?AppColors.colorPrimary:Colors.black12, width: 2.0)
        )
        ,
        child: Image.asset(value.image,width: 40.0, height: 40.0,),
      ),
    );*/
  }
}




