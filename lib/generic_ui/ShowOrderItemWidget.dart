import 'package:easy_services/models/service.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/rest/request/order_item_request.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';

class ShowOrderItemWidget extends StatefulWidget {

  final Registrationdata profile;

  final Service service;

  final OrderItemRequest item;

  Function onTapEdit;

  Function onTapDelete;

  ShowOrderItemWidget(this.profile, this.service, this.item, this.onTapEdit, this.onTapDelete);

  @override
  _ShowOrderItemWidgetState createState() => _ShowOrderItemWidgetState();
}

class _ShowOrderItemWidgetState extends State<ShowOrderItemWidget> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.only(top:5.0, right: 10.0, left: 10.0),
      padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(color: AppColors.getColorHexFromStr("#d8d8d8"))
      ),
      child: new ListTile(
        leading: Image(image: getIcon(),width: 30.0,),
        title: new Row(
          children: <Widget>[
            new Expanded(
              child: new Text(widget.item.servicePricing().nameValue(), style: new TextStyle(color: Colors.black38, fontSize: 12.0, fontWeight: FontWeight.bold)),
            ),
            SizedBox(width: 8.0,),
            new Container(
              child: new Text(widget.item.servicePricing().priceValue()+" "+widget.item.servicePricing().currencyValue(), style: new TextStyle(color: AppColors.nopalmaBlue, fontSize: 12.0, fontWeight: FontWeight.bold),),
            ),
            SizedBox(width: 8.0,),
            new Text("x${widget.item.quantity} ${widget.item.servicePricing().quantityUnitValue() =="KG"?"KG":""}", style: new TextStyle(color: Colors.black38, fontSize: 12.0, fontWeight: FontWeight.bold)),
          ],
        ),
        trailing: new Icon(Icons.edit, size: 18,),
        onTap: (){
          widget.onTapEdit();
        },
      ),
    );
  }

  ImageProvider getIcon(){

    if(widget.item.servicePricing().serviceSlugValue()=="pressing"){
      return NetworkImage(widget.item.servicePricing().photoValue());
    }else{
      return NetworkImage(getService(widget.item.servicePricing().serviceSlugValue()).logo);
    }




  }

  Service getService(String serviceSlug){
    List<Service> services = [];

    if(widget.profile != null){
      services = widget.profile.services == null?[]:widget.profile.services;
    }
    if(services.isNotEmpty){
      return services.firstWhere((s)=>s.slug == serviceSlug,orElse: ()=> null);

    }else{
      return null;
    }

  }


}
