import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/customer_subscription.dart';
import 'package:easy_services/models/subscription.dart';
import 'package:easy_services/models/subscription_linge_kilo.dart';
import 'package:easy_services/models/subscription_pressing.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:easy_services/util/DateUtil.dart';
import 'package:flutter/material.dart';


class MySubscriptionWidget extends StatefulWidget {

  Registrationdata profile;
  CustomerSubscription customerSubscription;
  
  MySubscriptionWidget(this.profile,this.customerSubscription);
  
  @override
  _MySubscriptionWidgetState createState() => _MySubscriptionWidgetState();
}

class _MySubscriptionWidgetState extends State<MySubscriptionWidget> {

  bool isSelected = true;

  Subscription subscription;

  Locale defaultLocale;

  @override
  void initState() {
    super.initState();
    subscription = widget.customerSubscription.subscription;

  }

  @override
  Widget build(BuildContext context) {

    defaultLocale = Locale(LangStrings.strLang.toLowerCase());

    return new Container(
       alignment: Alignment.topLeft,
      margin: EdgeInsets.fromLTRB(8.0,8.0,8.0,0.0),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all( color: getServiceBgColor(subscription.service_slug), width: 1.0)
      ),
      child: new Column(
        children: <Widget>[
          new Container(
            width: 80.0,
            child: getServiceImage(subscription.service_slug)
            ),
          SizedBox(height: 10.0,),
          new Container(
            alignment: Alignment.center,
            child: new Text(subscription.title, 
            style:new TextStyle(
              color: Colors.black,
              fontFamily: "ArialRoundedMT", 
              fontSize:25.0, 
              fontWeight: FontWeight.bold)),
          ),
          SizedBox(height: 10.0,),
          new Container(
            alignment: Alignment.center,
            child: new Text(subscription.service_name,
                style: new TextStyle(
                  color: Colors.black,
                  fontFamily: "ArialRoundedMT",
                  fontSize: 16.0,
                )
                ),
          ),
          SizedBox(height: 10.0,),  
          new Container(
            alignment: Alignment.center,
            child:  new Text("Reference: #${widget.customerSubscription.id}",
                style: new TextStyle(
                  color: Colors.black,
                  fontFamily: "ArialRoundedMT",
                  fontSize: 16.0,
                )
                ),
            ),
                SizedBox(height: 10.0,),
                new Container(
                  height: 30.0 * getDetails(subscription).length,
                  alignment: Alignment.center,
                  child: ListView(
                    children: getDetails(subscription),
                  )
                ),
                SizedBox(height: 10.0,),
                new Container(
                  height: 50.0,
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.circular(5.0),
                    border: Border.all(color: getServiceBgColor(subscription.service_slug))
                  ),
                  child: new Text('${LangStrings.gs("Expiration_Date_title")}: ${DateUtil.formatDateTime(widget.customerSubscription.delay.toIso8601String(), defaultLocale)}', style: new TextStyle(fontSize:15.0,color: Colors.black, fontFamily: 'ArialRoundedMT', fontWeight: FontWeight.bold), ),
                )

                        ],
                      ),
                    );
                  }


                  List<Widget> getDetails(Subscription sub){
                    if(sub.service_slug == "pressing"){
                        if(sub.meta_data != null){
                          List<Widget> views = [];

                           Map map = jsonDecode(sub.meta_data);
                           SubscriptionPressing result = SubscriptionPressing.fromJson(map);

                              views.add(new Container(
                                 margin: EdgeInsets.only(bottom: 10.0),
                                 alignment: Alignment.center,
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0),
                                    SizedBox(width: 10.0,),
                                    new Text("Maximum ${result.quantity_clothing} ${LangStrings.gs("Clothes_title")}",
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));

                               views.add(new Container(
                                  margin: EdgeInsets.only(bottom: 10.0),
                                  alignment: Alignment.center,
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0),
                                    SizedBox(width: 10.0,),
                                    new Text("Maximum ${result.quantity_costume_tailors} ${LangStrings.gs("Suits_Tailors_title")}",
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));
                             

                              views.add(new Container(
                                margin: EdgeInsets.only(bottom: 10.0),
                                alignment: Alignment.center,
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0),
                                    SizedBox(width: 10.0,),
                                    new Text(getTitlePassage(result.number_of_passage),
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));

                              views.add(new Container(
                                margin: EdgeInsets.only(bottom: 10.0),
                                alignment: Alignment.center,
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0),
                                    SizedBox(width: 10.0,),
                                    new Text(LangStrings.gs("no_delivery_fees_title"),
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));



                              if(widget.customerSubscription.is_active){
                                  views.add(new Container(
                                alignment: Alignment.center,
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.album, size:18.0, color: Colors.green,),
                                    SizedBox(width: 10.0,),
                                    new Text(LangStrings.gs("Active_title"),
                                    style: new TextStyle(color: Colors.green, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));
                              }else{
                                  views.add(new Container(
                                alignment: Alignment.center,
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.warning, size:18.0, color: Colors.red,),
                                    SizedBox(width: 10.0,),
                                    new Text(LangStrings.gs("Inactive_title"),
                                    style: new TextStyle(color: Colors.red, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));
                              }

                          return views;
                        }else{
                          return [];
                        }
                    }else if(sub.service_slug == "linge_au_kilo"){
                        if(sub.meta_data != null){
                          List<Widget> views = [];

                            Map map = jsonDecode(sub.meta_data);
                            SubscriptionLingeKilo result = SubscriptionLingeKilo.fromJson(map);

                            views.add(new Container(
                              alignment: Alignment.center,
                                margin: EdgeInsets.only(bottom: 10.0),
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0),
                                    SizedBox(width: 10.0,),
                                    new Text("Maximum ${result.quantity} ${result.quantity_unit} ${LangStrings.gs("of_clothes_title")}",
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));

                             views.add(new Container(
                               alignment: Alignment.center,
                                margin: EdgeInsets.only(bottom: 10.0),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0),
                                    SizedBox(width: 10.0,),
                                    new Text(getTitlePassage(result.number_of_passage),
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));

                              views.add(new Container(
                                margin: EdgeInsets.only(bottom: 10.0),
                                alignment: Alignment.center,
                                child: new Row(
                          
                                   mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0),
                                    SizedBox(width: 10.0,),
                                    new Text(LangStrings.gs("no_delivery_fees_title"),
                                    style: new TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));

                              views.add(new Container(
                                alignment: Alignment.center,
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(Icons.check_circle, size:18.0, color: AppColors.nopalmaBlue,),
                                    SizedBox(width: 10.0,),
                                    new Text(widget.customerSubscription.current_balance == null?"Restants: 0 KG": "Restants: ${widget.customerSubscription.current_balance} KG",
                                      style: new TextStyle(color: Colors.black, fontSize: 14.0, fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                              ));


                              if(widget.customerSubscription.is_active){
                                  views.add(new Container(
                                    margin: EdgeInsets.only(bottom: 10.0),
                                alignment: Alignment.center,
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.album, size:18.0, color: Colors.green,),
                                    SizedBox(width: 10.0,),
                                    new Text(LangStrings.gs("Active_title"),
                                    style: new TextStyle(color: Colors.green, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));
                              }else{
                                  views.add(new Container(
                                    margin: EdgeInsets.only(bottom: 10.0),
                                alignment: Alignment.center,
                                child: new Row(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.warning, size:18.0, color: Colors.red,),
                                    SizedBox(width: 10.0,),
                                    new Text(LangStrings.gs("Inactive_title"),
                                    style: new TextStyle(color: Colors.red, fontSize: 14.0, fontFamily: "ArialRoundedMT"),
                                    )
                                  ],
                                ),
                              ));
                              }

                          return views;
                        }else{
                          return [];
                        }
                    }else{
                      return [];
                    }

                   
                  }
                
                    Image getServiceImage(String service_slug) {
                    if(service_slug == "pressing"){
                      return Image.asset("assets/sssss@2x.png", color: getServiceBgColor(service_slug),);
                    }else if(service_slug == "linge_au_kilo"){
                      return Image.asset("assets/cleancult_05_2x@2x.png", color: getServiceBgColor(service_slug),);
                    }else if(service_slug == "repassage"){
                      return Image.asset("assets/555@2x.png", color: getServiceBgColor(service_slug),);
                    }else if(service_slug == "retouche"){
                      return Image.asset("assets/RETROUC@2x.png", color: getServiceBgColor(service_slug),);
                    }else if(service_slug == "coordonnerie"){
                      return Image.asset("assets/CORD@2x.png", color: getServiceBgColor(service_slug),);
                    }else{
                      return Image.asset("assets/555@2x.png", color: getServiceBgColor(service_slug),);
                    }
                
                  }
                
                    Color getServiceBgColor(String service_slug) {
                    if(service_slug == "pressing"){
                      return AppColors.getColorHexFromStr("#00d4db");
                    }else if(service_slug == "linge_au_kilo"){
                      return AppColors.getColorHexFromStr("#fe8b16");
                    }else if(service_slug == "repassage"){
                      return AppColors.getColorHexFromStr("#00adf0");
                    }else if(service_slug == "retouche"){
                      return AppColors.getColorHexFromStr("#8fc650");
                    }else if(service_slug == "coordonnerie"){
                      return AppColors.getColorHexFromStr("#ffb501");
                    }else{
                      return AppColors.getColorHexFromStr("#00d4db");
                    }
                
                  }
                
                  String getPer(Subscription subscription) {
                    if(subscription.delay_unit == "month"){
                      return LangStrings.gs("month");
                    }else{
                      return LangStrings.gs("month");
                    }
                  }

                  String getTitlePassage(int passage){
                    if(passage == 2){
                        return LangStrings.gs("Passage_every2weeks_title");
                    }else if(passage == 4){
                      return LangStrings.gs("Passage_every_week_title");
                    }else{
                      return LangStrings.gs("Nothing_title");
                    }
                  }
}