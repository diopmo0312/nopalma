import 'dart:convert';

import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/models/customer_notification.dart';
import 'package:easy_services/models/order.dart';
import 'package:easy_services/rest/registration_data.dart';
import 'package:easy_services/ui/orders/detail/order_detail_screen.dart';
import 'package:flutter/material.dart';

class NotificationWidget extends StatefulWidget {

  final Registrationdata profile;
  CustomerNotification notification;

  NotificationWidget(this.profile, this.notification);

  @override
  _NotificationWidgetState createState() => _NotificationWidgetState();
}

class _NotificationWidgetState extends State<NotificationWidget> {

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(Icons.notifications_active),
      title: new Text(getNotificationTitle(widget.notification)/*'Customer Marc Affoh has been refreshed geolocation of task  - 212222'*/, style: new TextStyle(fontSize: 18.0, color: Colors.black, fontFamily: 'ArialRoundedMT',fontWeight: FontWeight.bold),),
      subtitle: new Text(getNotificationSubTitle(widget.notification)/*"Pharmacie Las palmas, Boulevard des Martyrs, Abidjan, Côte d'Ivoire, lelele enene enenen bba a,a,,aaaaa"*/, style: new TextStyle(color: Colors.black38, fontFamily: 'ArialRoundedMT',fontSize: 14.0,),),
      onTap: (){
        // Go to Task's detail
        if(widget.notification.type_notification == "order_managed"){
          var o = Order.fromJson(jsonDecode(widget.notification.data));
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => new OrderDetailScreen(widget.profile,o)),
          );

        }else if(widget.notification.type_notification == "order_edited"){
          var o = Order.fromJson(jsonDecode(widget.notification.data));
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => new OrderDetailScreen(widget.profile,o)),
          );

        }else if(widget.notification.type_notification == "order_customer_pickup"){
          var o = Order.fromJson(jsonDecode(widget.notification.data));
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => new OrderDetailScreen(widget.profile,o)),
          );
        }else if(widget.notification.type_notification == "order_customer_delivery"){
          var o = Order.fromJson(jsonDecode(widget.notification.data));
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => new OrderDetailScreen(widget.profile,o)),
          );
        }else if(widget.notification.type_notification == "order_payment"){
          var o = Order.fromJson(jsonDecode(widget.notification.data));
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => new OrderDetailScreen(widget.profile,o)),
          );
        }else{

        }

      },
    );
  }

  String getNotificationTitle(CustomerNotification notification) {
   return LangStrings.strLang == "FR"?notification.title:notification.title_en;
  }

  String getNotificationSubTitle(CustomerNotification notification) {
    return LangStrings.strLang == "FR"?notification.subtitle:notification.subtitle_en;
  }

}
