import 'package:easy_services/LangStrings.dart';
import 'package:easy_services/generic_ui/success.dart';
import 'package:easy_services/ui/app_colors.dart';
import 'package:flutter/material.dart';

class Dialogs{

  info(BuildContext context, String title, String description){
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return AlertDialog(
          title: new Text(title, style: new TextStyle(fontFamily: "ArialRoundedMT", fontWeight: FontWeight.bold),),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(description, style: new TextStyle(fontFamily: "ArialRoundedMT"),)
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: ()=> Navigator.pop(context),
              child: new Container(
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5.0)),
                child: new Text("Ok", style: new TextStyle(color: Colors.white, fontFamily: "ArialRoundedMT", fontWeight: FontWeight.bold),),
              ),
            )
          ],
        );
      }
    );
  }

  error(BuildContext context, String title, String description,Function retry, Function okeyAction  ){
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(
            title: new Text(title, style: new TextStyle(fontFamily: "ArialRoundedMT", fontWeight: FontWeight.bold),),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(description, style: new TextStyle(fontFamily: "ArialRoundedMT"),)
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: (){
                      Navigator.pop(context);
                      okeyAction();
                },
                child: new Container(
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(color: Colors.black87, borderRadius: BorderRadius.circular(5.0)),
                  child: new Text("Ok", style: new TextStyle(color: Colors.white, fontFamily: "ArialRoundedMT", fontWeight: FontWeight.bold),),
                ),
              ),

              FlatButton(
                onPressed: (){
                  Navigator.pop(context);
                  retry();
                },
                child: new Container(
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(color: AppColors.nopalmaYellow, borderRadius: BorderRadius.circular(5.0)),
                  child: new Text("Recommencer", style: new TextStyle(color: Colors.white, fontFamily: "ArialRoundedMT", fontWeight: FontWeight.bold),),
                ),
              )
            ],
          );
        }
    );
  }

  confirm(BuildContext context, String title, String description,Function cancel, Function okeyAction  ){
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(
            title: new Text(title, style: new TextStyle(fontFamily: "ArialRoundedMT", fontWeight: FontWeight.bold),),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(description, style: new TextStyle(fontFamily: "ArialRoundedMT"),)
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: (){
                  Navigator.pop(context);
                  okeyAction();
                },
                child: new Container(
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(color: AppColors.nopalmaYellow, borderRadius: BorderRadius.circular(5.0)),
                  child: new Text(LangStrings.gs("dialog_button_confirm"), style: new TextStyle(color: Colors.white, fontFamily: "ArialRoundedMT", fontWeight: FontWeight.bold),),
                ),
              ),

              FlatButton(
                onPressed: (){
                  Navigator.pop(context);
                  cancel();
                },
                child: new Container(
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(color: Colors.black87, borderRadius: BorderRadius.circular(5.0)),
                  child: new Text(LangStrings.gs("dialog_button_cancel"), style: new TextStyle(color: Colors.white, fontFamily: "ArialRoundedMT", fontWeight: FontWeight.bold),),
                ),
              )
            ],
          );
        }
    );
  }

  simpleError(BuildContext context, String title, String description, Function okeyAction  ){
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(
            title: new Text(title, style: new TextStyle(fontFamily: "ArialRoundedMT", fontWeight: FontWeight.bold),),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(description, style: new TextStyle(fontFamily: "ArialRoundedMT"),)
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: (){
                  Navigator.pop(context);
                  okeyAction();
                },
                child: new Container(
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(color: Colors.black87, borderRadius: BorderRadius.circular(5.0)),
                  child: new Text("Ok", style: new TextStyle(color: Colors.white, fontFamily: "ArialRoundedMT", fontWeight: FontWeight.bold),),
                ),
              ),


            ],
          );
        }
    );
  }

  showSuccess(BuildContext context, String title, String description, Function okeyAction  ){
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return new Container(
            color: Colors.transparent,
            child: new Padding(
              padding: new EdgeInsets.all(40.0),
              child: new Scaffold(
                backgroundColor: Colors.transparent,
                body: new Center(
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.all(new Radius.circular(5.0)),
                      child: new Container(
                          color: Colors.white,
                          width: double.infinity,
                          child: new Padding(
                            padding: new EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
                            child: new Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                new SizedBox(
                                  width: 64.0,
                                  height: 64.0,
                                  child: new SuccessView(),
                                ),

                                new SizedBox(height: 10.0,),

                                new Text(
                                  title,
                                  style: new TextStyle(fontSize: 25.0, color: Colors.black,),
                                ),
                                new Padding(
                                  padding: new EdgeInsets.only(top: 10.0,),
                                  child: new Text(
                                    description==null?'':description,
                                    style: new TextStyle(fontSize: 16.0, color: Colors.black),textAlign: TextAlign.center,
                                  ),
                                ),
                                new Padding(
                                  padding: new EdgeInsets.only(top: 10.0),
                                  child: new RaisedButton(
                                    onPressed: (){
                                      Navigator.pop(context);
                                      okeyAction();
                                    },
                                    color: Colors.green,
                                    child: new Text(
                                      "Ok",
                                      style: new TextStyle(color: Colors.white, fontSize: 16.0),
                                    ),
                                  ),
                                )

                              ],
                            ),
                          )
                      ),
                    )),
              ),
            ),
          );
        }
    );
  }

}